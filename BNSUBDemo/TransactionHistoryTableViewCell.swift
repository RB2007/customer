//
//  TransactionHistoryTableViewCell.swift
//  Customer
//
//  Created by Annie Lo on 2016-02-22.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import UIKit

class TransactionHistoryTableViewCell: UITableViewCell {
    
    
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var transactionLabel: UILabel!
    @IBOutlet weak var debitLabel: UILabel!
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }
    
    func setField(productCode: String, transaction: AccountTransactionHistory, indexPath: NSIndexPath) {
        
        guard let bankinglist = ScotiaProductTypes.getProductsCodes(ScotiaProductTypes.Banking) else {
            transactionLabel.text = ""
            creditLabel.text = ""
            debitLabel.text = ""
            dateLabel.text = ""
            balanceLabel.text = ""
            return
        }
        
        // Balance Label
        balanceLabel.hidden = !bankinglist.contains(productCode)
        
        var transactionText = ""
        if var uLine1Description = transaction.line1Desc {
            uLine1Description = uLine1Description.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            transactionText = uLine1Description
        }
        
        if var uLine2Description = transaction.line2Desc {
            uLine2Description = uLine2Description.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            transactionText =  transactionText + " | " + uLine2Description
        }
        
        if var uLine3Description = transaction.line3Desc {
            uLine3Description = uLine3Description.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet())
            transactionText =  transactionText + " | " + uLine3Description
        }
        
        transactionLabel.text = transactionText
        
        if let creditDebitIndicator = transaction.crDrInd, amount = transaction.txnAmount {
            if (creditDebitIndicator == "0") {
                creditLabel.text = Util.formatCurrency(amount)
                debitLabel.text = ""
            } else {
                debitLabel.text = Util.formatCurrency(amount)
                creditLabel.text = ""
            }
        }
        
        
        if let uTxnDate = transaction.txnDate {
            dateLabel.text = uTxnDate
        }
        
        if let uRunningBalance = transaction.runningBalance {
            balanceLabel.text = Util.formatCurrency(uRunningBalance)
        } else {
            balanceLabel.text = ""
        }
        
        // Change background color
        self.backgroundColor = indexPath.row % 2 == 0 ? Constants.UBColours.scotiaGrey5 : UIColor.whiteColor()
        
    }
    
    
    
}
