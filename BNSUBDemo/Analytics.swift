//
//  Analytics.swift
//  BNSUBDemo
//
//  Created by B2E Mobile on 2015-10-06.
//  Copyright © 2015 Scotiabank. All rights reserved.
//

import UIKit
import SplunkMint

// MARK: - Custom Analytics
class Analytics
{
    // MARK: - Properties
    static let sharedInstance = Analytics()
    let theMint:Mint = Mint.sharedInstance()
    let thePriority = DISPATCH_QUEUE_PRIORITY_BACKGROUND
    var isActive:Bool = false
    
    // MARK: - Methods
    func initAnalytics(theToken:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.initAndStartSession(theToken)
                }
        }
    }
    
    func startSession()
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.startSessionAsyncWithCompletionBlock(nil)
                }
        }
    }
    
    func stopSession()
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.closeSessionAsyncWithCompletionBlock(nil)
                }
        }
    }
    
    func flush()
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.enableFlushOnlyOverWiFi(false)
                    self.theMint.flushAsyncWithBlock { (responseResult) -> Void in
                        if (responseResult.resultState.rawValue == OKResultState.rawValue)
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("Flush finished unsuccessfully.");
                            #endif
                        }
                    }
                }
        }
    }
    
    func startTransaction(theTransactionName:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.transactionStart(theTransactionName, andResultBlock:
                        {(transactionStartResult: TransactionStartResult!) -> Void in
                            if transactionStartResult.transactionStatus.rawValue == SuccessfullyStartedTransaction.rawValue
                            {
                                //  do nothing on success
                            }
                            else
                            {
                                #if DEBUG
                                    print("Failed to Start Transaction \(theTransactionName)")
                                #endif
                            }
                    })
                }
        }
    }
    
    func cancelTransaction(theTransactionName:String, theReason:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.transactionCancel(theTransactionName, reason: theReason, andResultBlock: {(theResult:TransactionStopResult!) -> Void in
                        if theResult.transactionStatus.rawValue == UserCancelledTransaction.rawValue
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("Failed to Cancel Transaction \(theTransactionName)")
                            #endif
                        }
                    })
                }
        }
    }
    
    func stopTransaction(theTransactionName:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.transactionStop(theTransactionName, andResultBlock:
                        {(theResult:TransactionStopResult!) -> Void in
                            if theResult.transactionStatus.rawValue == UserSuccessfullyStoppedTransaction.rawValue
                            {
                                //  do nothing on success
                            }
                            else
                            {
                                #if DEBUG
                                    print("Failed to Stop Transaction \(theTransactionName)")
                                #endif
                            }
                    })
                }
        }
    }
    
    func startTransaction(theTransactionName:String, theKey:String, theValue:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.transactionStart(theTransactionName, extraDataKey: theKey, extraDataValue: theValue, andResultBlock:
                        { (transactionResult:TransactionStartResult!) -> Void in
                            if transactionResult.transactionStatus.rawValue == OKResultState.rawValue
                            {
                                //  do nothing on success
                            }
                            else
                            {
                                #if DEBUG
                                    print("Failed to Start Transaction \(theTransactionName) | \(theKey)")
                                #endif
                            }
                    })
                }
        }
    }
    
    func stopTransaction(theTransactionName:String, theKey:String, theValue:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.transactionStop(theTransactionName, extraDataKey: theKey, extraDataValue: theValue, andResultBlock:
                        { (transactionResult:TransactionStopResult!) -> Void in
                            if transactionResult.transactionStatus.rawValue == UserSuccessfullyStoppedTransaction.rawValue
                            {
                                //  do nothing on success
                            }
                            else
                            {
                                #if DEBUG
                                    print("Failed to Stop Transaction \(theTransactionName) | \(theKey)")
                                #endif
                            }
                    })
                }
        }
    }
    
    func cancelTransaction(theTransactionName:String, theKey:String, theValue:String, theReason:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.transactionCancel(theTransactionName, extraDataKey: theKey, extraDataValue: theValue, reason: theReason, andResultBlock:
                        { (transactionResult:TransactionStopResult!) -> Void in
                            if transactionResult.transactionStatus.rawValue == UserCancelledTransaction.rawValue
                            {
                                //  do nothing on success
                            }
                            else
                            {
                                #if DEBUG
                                    print("Failed to Cancel Transaction \(theTransactionName) | \(theKey)")
                                #endif
                            }
                    })
                }
        }
    }
    
    func startTransaction(theTransactionName:String, theList:NSDictionary)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    
                    let dataList:LimitedExtraDataList = LimitedExtraDataList()

                    let mutableArray = NSMutableArray()
                    for key in theList.allKeys as! [String] {
                        let value = theList[key] as! String
                        let extraData = ExtraData(key: key, andValue: value)
                        mutableArray.addObject(extraData)
                    }
                    dataList.extraDataArray = mutableArray
                    
                    self.theMint.transactionStart(theTransactionName, limitedExtraDataList: dataList, andResultBlock: { (transactionResult:TransactionStartResult!) -> Void in
                        if transactionResult.transactionStatus.rawValue == OKResultState.rawValue
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("Failed to Start Transaction \(theTransactionName) | \(theList)")
                            #endif
                        }
                    })
                }
        }
    }
    
    func cancelTransaction(theTransactionName:String, theList:NSDictionary, theReason: String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    let dataList:LimitedExtraDataList = LimitedExtraDataList()
                    
                    let mutableArray = NSMutableArray()
                    for key in theList.allKeys as! [String] {
                        let value = theList[key] as! String
                        let extraData = ExtraData(key: key, andValue: value)
                        mutableArray.addObject(extraData)
                    }
                    dataList.extraDataArray = mutableArray
                    self.theMint.transactionCancel(theTransactionName, limitedExtraDataList: dataList, reason: theReason, andResultBlock: { (transactionResult:TransactionStopResult!) -> Void in
                        if transactionResult.transactionStatus.rawValue == OKResultState.rawValue
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("Failed to Cancel Transaction \(theTransactionName) | \(theList)")
                            #endif
                        }
                    })
                    
                }
        }
    }
    
    func stopTransaction(theTransactionName:String, theList:NSDictionary)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    let dataList:LimitedExtraDataList = LimitedExtraDataList()
                    
                    let mutableArray = NSMutableArray()
                    for key in theList.allKeys as! [String] {
                        let value = theList[key] as! String
                        let extraData = ExtraData(key: key, andValue: value)
                        mutableArray.addObject(extraData)
                    }
                    dataList.extraDataArray = mutableArray
                    
                    self.theMint.transactionStop(theTransactionName, limitedExtraDataList: dataList, andResultBlock: { (transactionResult:TransactionStopResult!) -> Void in
                        if transactionResult.transactionStatus.rawValue == OKResultState.rawValue
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("Failed to Stop Transaction \(theTransactionName) | \(theList)")
                            #endif
                        }
                    })
                }
        }
    }
    
    func addEventWithName(theEventName:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.logEventAsyncWithName(theEventName, logLevel: InfoLogLevel, andCompletionBlock: {(logResult:MintLogResult!)->Void in
                        if (logResult.resultState.rawValue == OKResultState.rawValue)
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("\(theEventName) event not added")
                            #endif
                        }
                    })
                }
        }
    }
    
    func addEventWithName(theEventName:String, theKey:String, theValue:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.logEventAsyncWithName(theEventName, logLevel: InfoLogLevel, extraDataKey: theKey, extraDataValue: theValue, andCompletionBlock: {(logResult:MintLogResult!)->Void in
                        if (logResult.resultState.rawValue == OKResultState.rawValue)
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("\(theEventName)| \(theKey) event not added")
                            #endif
                        }
                    })
                }
        }
    }
    
    func addEventWithName(theEventName:String, theList:NSDictionary)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    let dataList:LimitedExtraDataList = LimitedExtraDataList()

                    let mutableArray = NSMutableArray()
                    for key in theList.allKeys as! [String] {
                        let value = theList[key] as! String
                        let extraData = ExtraData(key: key, andValue: value)
                        mutableArray.addObject(extraData)
                    }
                    dataList.extraDataArray = mutableArray
                    
                    self.theMint.logEventAsyncWithName(theEventName, logLevel: InfoLogLevel, limitedExtraDataList: dataList, andCompletionBlock: {(logResult:MintLogResult!)->Void in
                        if (logResult.resultState.rawValue == OKResultState.rawValue)
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("\(theEventName) event not added with List")
                            #endif
                        }
                    })
                }
        }
    }
    
    func addEventWithTag(theEventName:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.logEventAsyncWithTag(theEventName, completionBlock: {(logResult:MintLogResult!)->Void in
                        if (logResult.resultState.rawValue == OKResultState.rawValue)
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("\(theEventName) event not tagged")
                            #endif
                        }
                    })
                }
        }
    }
    
    func addEventWithTag(theEventName:String, theKey:String, theValue:String)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    self.theMint.logEventAsyncWithName(theEventName, logLevel: InfoLogLevel, extraDataKey: theKey, extraDataValue: theValue, andCompletionBlock:  {(logResult:MintLogResult!)->Void in
                        if (logResult.resultState.rawValue == OKResultState.rawValue)
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("\(theEventName)| \(theKey) event not tagged")
                            #endif
                        }
                    })
                }
        }
    }
    
    func addEventWithTag(theEventName:String, theList:NSDictionary)
    {
        dispatch_async(dispatch_get_global_queue(thePriority, 0))
            {
                if (self.isActive)
                {
                    let dataList:LimitedExtraDataList = LimitedExtraDataList()
                    
                    let mutableArray = NSMutableArray()
                    for key in theList.allKeys as! [String] {
                        let value = theList[key] as! String
                        let extraData = ExtraData(key: key, andValue: value)
                        mutableArray.addObject(extraData)
                    }
                    
                    dataList.extraDataArray = mutableArray
                    self.theMint.logEventAsyncWithTag(theEventName, limitedExtraDataList: dataList, completionBlock: {(logResult:MintLogResult!)->Void in
                        if (logResult.resultState.rawValue == OKResultState.rawValue)
                        {
                            //  do nothing on success
                        }
                        else
                        {
                            #if DEBUG
                                print("\(theEventName) event not tagged with List")
                            #endif
                        }
                    })
                }
        }
    }
    
}
