//
//  CustomerAccountContainerViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-14.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit
import SVProgressHUD
import LocalAuthentication

protocol CustomerAccountDelegate: class {
    var customerProfile: CustomerProfile? {get set}
    var selectedSplitMasterViewKey: CustomerAccountSplitViewMasterKeys? {get set}
    var isUserAuthenticated: Bool {get set}
    var isEditingCustomer: Bool {get set}
    var isToolbarHidden: Bool {get set}
}

class CustomerAccountContainerViewController: UIViewController, CustomerAccountDelegate {
    
    // MARK: - Properties
    @IBOutlet weak var labelCustomerName: UILabel!
    @IBOutlet weak var labelCustomerBranchNameAndTransit: UILabel!
    @IBOutlet weak var labelCustomerBranchAddress: UILabel!
    
    @IBOutlet weak var buttonAuthenticate: UIButton!
    @IBOutlet weak var buttonEdit: UIButton!
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var buttonEndSession: UIButton!
    @IBOutlet weak var imageViewScotiabankLogo: UIImageView!
    
    private var customerAccountSplitViewMasterViewController: CustomerAccountSplitViewMasterViewController?
    
    // Customer Profile
    var customerProfileJSON: [String: AnyObject]? {
        didSet {
            // When a new customer profile is set, we update the customerProfile object
            if let uCustomerProfileJSON = customerProfileJSON {
                // Assuming we are only dealing with Person profiles for this phase
                customerProfile = PersonProfile(json: uCustomerProfileJSON)
            } else {
                customerProfile = nil
            }
        }
    }
    
    var customerProfile: CustomerProfile? {
        didSet {
            notifiyViewControllerOfDataChange()
        }
    }
    
    // Customer Editing
    var selectedSplitMasterViewKey: CustomerAccountSplitViewMasterKeys? {
        didSet {
            updateToolBarButtons()
        }
    }
    
    var isUserAuthenticated: Bool = false {
        didSet {
            updateToolBarButtons()
            notifiyViewControllerOfDataChange()
        }
    }
    
    var isEditingCustomer: Bool = false {
        didSet {
            updateToolBarButtons()
            notifiyViewControllerOfDataChange()
        }
    }
    
    var isToolbarHidden: Bool = false {
        didSet {
            updateToolBarButtons()
        }
    }
    
    // MARK: - View Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        Analytics.sharedInstance.startTransaction(SplunkConstants.kCustomerProfileDisplayedKey, theList: SplunkConstants.params())
        customizeUI()
        setup()
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    deinit {
        Analytics.sharedInstance.stopTransaction(SplunkConstants.kCustomerProfileDisplayedKey)
        print("Deinit is called on CustomerAccountContainerViewController")
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "embedSplitViewController" {
            // Setting the delegate for the customer account master split view
            if let splitVC = segue.destinationViewController as? UISplitViewController {
                if let navController = splitVC.viewControllers.first as? UINavigationController {
                    if let vc = navController.viewControllers.first as? CustomerAccountSplitViewMasterViewController {
                        customerAccountSplitViewMasterViewController = vc
                        vc.customerAccountDelegate = self
                    }
                }
            }
        }
    }
    
    // MARK: - Instance Methods
    func customizeUI() {
        // End Session Button
        buttonEndSession.setTitle(NSLocalizedString("end_session", comment: "Button"), forState: UIControlState.Normal)
        buttonEndSession.layer.cornerRadius = 5
        
        // Touch ID Button
        buttonAuthenticate.setTitle(NSLocalizedString("authenticate", comment: "Button"), forState: UIControlState.Normal)
        buttonAuthenticate.layer.cornerRadius = 5
        buttonAuthenticate.layer.borderColor = Constants.UBColours.scotiaRed.CGColor
        buttonAuthenticate.layer.borderWidth = 1
        
        // Edit Button
        buttonEdit.setTitle(NSLocalizedString("edit", comment: "Button"), forState: UIControlState.Normal)
        buttonEdit.layer.cornerRadius = 5
        buttonEdit.layer.borderColor = Constants.UBColours.scotiaRed.CGColor
        buttonEdit.layer.borderWidth = 1
        
        // Save Button
        buttonSave.setTitle(NSLocalizedString("save", comment: "Button"), forState: UIControlState.Normal)
        buttonSave.layer.cornerRadius = 5
        buttonSave.layer.borderColor = Constants.UBColours.scotiaRed.CGColor
        buttonSave.layer.borderWidth = 1
        
        // Cancel Button
        buttonCancel.setTitle(NSLocalizedString("cancel", comment: ""), forState: UIControlState.Normal)
        buttonCancel.layer.cornerRadius = 5
        buttonCancel.layer.borderColor = Constants.UBColours.scotiaRed.CGColor
        buttonCancel.layer.borderWidth = 1
        
        // Localizing the scotiabank logo
        let scotiaBank = NSLocalizedString("scotia_bank", comment: "")
        imageViewScotiabankLogo.image = UIImage(named: scotiaBank + " Logo")
    }
    
    func setup() {
        // Customer Name
        labelCustomerName.text = customerProfile?.nameLine1?.capitalizedString ?? ""
        
        // Branch Name and Transit
        let branchName = customerProfile?.domicileBranch?.name?.additionalSpacesRemoved.capitalizedString ?? ""
        let branchTransit = customerProfile?.domicileBranch?.id ?? ""
        labelCustomerBranchNameAndTransit.text = branchName.characters.count > 0 && branchTransit.characters.count > 0 ? branchName + " - " + branchTransit : branchName + branchTransit
        
        // Branch Address
        labelCustomerBranchAddress.text = customerProfile?.domicileBranch?.address?.additionalSpacesRemoved.capitalizedString ?? ""
    }
    
    // Change the End Session / Edit / Save / Cancel Touch ID buttons based on state
    @IBAction func endSessionButtonPressed() {
        customerAccountSplitViewMasterViewController?.removeCustomerProfile()
    }
    
    @IBAction func touchIDButtonPressed() {
        let authenticationContet = LAContext()
        var error: NSError?
        if authenticationContet.canEvaluatePolicy(LAPolicy.DeviceOwnerAuthentication, error: &error) {
            authenticationContet.evaluatePolicy(LAPolicy.DeviceOwnerAuthentication, localizedReason: NSLocalizedString("customer_profile_authentication", comment: "User Authentication."), reply: { (success:Bool, error:NSError?) -> Void in
                print("Authentication success: \(success), error: \(error)")
                // The callback happens on a background thread, need to update the view on the main thread
                dispatch_async(dispatch_get_main_queue(), { () -> Void in
                    self.isUserAuthenticated = success
                })
            })
        } else {
            let alertController = AlertControllerUtil.createOKAlertController(NSLocalizedString("unable_to_authenticate", comment: "Error title."), message: NSLocalizedString("unable_to_authenticate_description", comment: "Error message body."))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
        // Check if there is an errror from canEvaluatePolicy and present an alert controller
        if let uError = error {
            let alertController = AlertControllerUtil.createOKAlertController(uError)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func editButtonPressed() {
        isEditingCustomer = true
    }
    
    @IBAction func saveButtonPressed() {
        // Remove keyboard
        self.view.endEditing(true)
        
        guard let uPersonProfile = customerProfile as? PersonProfile, let uParams = (customerProfile as? PersonProfile)?.paramsForSave else {
            return
        }
        
        if uPersonProfile.validEditableFields {
            saveUpdatedData(uParams)
        } else {
            let alertController = AlertControllerUtil.createOKAlertController(NSLocalizedString("invalid_fields", comment: "Error title."), message: NSLocalizedString("check_fields", comment: "Error message body."))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    @IBAction func cancelButtonPressed() {
        // Remove keyboard
        self.view.endEditing(true)
        
        // Check if the user changed the data
        if (customerProfile as? PersonProfile)?.paramsForSave == nil {
            // End editing
            isEditingCustomer = false
        } else {
            // Present alert controller to either reset data or continue editing
            let alertController = UIAlertController(title: NSLocalizedString("confirm_cancel", comment: "Error title."), message: NSLocalizedString("cancel_reset_fields", comment: "Error message body"), preferredStyle: UIAlertControllerStyle.Alert)
            let resetAlertAction = UIAlertAction(title: NSLocalizedString("discard", comment: "Button"), style: UIAlertActionStyle.Default) { (alertAction:UIAlertAction) -> Void in
                self.isEditingCustomer = false
                self.resetCustomerProfile()
            }
            let cancelAlertAction = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: UIAlertActionStyle.Cancel, handler: nil)
            alertController.addAction(resetAlertAction)
            alertController.addAction(cancelAlertAction)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    private func updateToolBarButtons() {
        selectedSplitMasterViewKey?.configureToolBarButtons(buttonEndSession, authenticatedButton: buttonAuthenticate, editButton: buttonEdit, saveButton: buttonSave, cancelButton: buttonCancel, isUserAuthenticated: isUserAuthenticated, isEditing: isEditingCustomer, hideToolBarButtons: isToolbarHidden)
    }
    
    func notifiyViewControllerOfDataChange() {
        
        if let uCustomerAccountSplitViewMasterViewController = customerAccountSplitViewMasterViewController?.splitViewController?.viewControllers.last as? AbstractCustomerAccountTableViewController {
            uCustomerAccountSplitViewMasterViewController.customerAccountDataChanged()
        }
        
        // Check if the view controller is embedded in a navigation controller
        if let nav = customerAccountSplitViewMasterViewController?.splitViewController?.viewControllers.last as? UINavigationController {
            if let uCustomerAccountSplitViewMasterViewController = nav.viewControllers.first as? AbstractCustomerAccountTableViewController {
                uCustomerAccountSplitViewMasterViewController.customerAccountDataChanged()
            }
        }
    }
    
    // MARK: - Data Management
    private func resetCustomerProfile() {
        
        guard let uCustomerProfileJSON = customerProfileJSON else {
            return
        }
        customerProfile = PersonProfile(json: uCustomerProfileJSON)
    }
    
    // MARK: - Save
    func saveUpdatedData(params: [String: AnyObject]) {
        SVProgressHUD.showWithStatus(NSLocalizedString("saving", comment: "Progress indicator"))
        APIManagement.sharedInstance.updateCustomerData(params, completion: { (data, error) -> (
            ) in
            SVProgressHUD.dismiss()
            
            if let uError = error {
                let alertController = AlertControllerUtil.createOKAlertController(uError)
                self.presentViewController(alertController, animated: true, completion: nil)
                return
            }
            self.loadCustomer()
        })
    }
    
    func loadCustomer() {
        
        guard let uCid = customerProfile?.cid else {
            return
        }
        
        SVProgressHUD.showWithStatus(NSLocalizedString("updating_profile", comment: "Progress indicator"))
        APIManagement.sharedInstance.loadCustomerProfile(uCid) { (data: AnyObject?, error: NSError?) -> () in
            self.isEditingCustomer = false
            SVProgressHUD.dismiss()
            
            if let uData = data as? [String: AnyObject] {
                print(uData)
                SVProgressHUD.showSuccessWithStatus(NSLocalizedString("profile_updated", comment: "Success indicator"))
                self.customerProfileJSON = uData
                return
            }
            
            // If there is an error, give the option to either reload or cancel
            if let uError = error {
                let alertMessage = Util.createErrorMessage(uError)
                let alertController = UIAlertController(title: alertMessage.title, message: alertMessage.message, preferredStyle: UIAlertControllerStyle.Alert)
                let actionRetry = UIAlertAction(title: NSLocalizedString("retry", comment: "Retry failed attempt to load updated customer profile"), style: UIAlertActionStyle.Default, handler: { (action: UIAlertAction) -> Void in
                    self.loadCustomer()
                })
                let actionCancel = UIAlertAction(title: NSLocalizedString("cancel", comment: ""), style: UIAlertActionStyle.Cancel, handler: { (action: UIAlertAction) -> Void in
                    self.customerAccountSplitViewMasterViewController?.removeCustomerProfile()
                })
                alertController.addAction(actionRetry)
                alertController.addAction(actionCancel)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
}
