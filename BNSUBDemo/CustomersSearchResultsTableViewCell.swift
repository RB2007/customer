//
//  CustomersSearchResultsTableViewCell.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-09.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class CustomersSearchResultsTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var labelCustomerName: UILabel!
    @IBOutlet weak var labelAddress: UILabel!
    @IBOutlet weak var imageViewLock: UIImageView!

    // MARK: - Cell Delegate Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        // Remove the selection style
    }
    
    // MARK: - Class Methods
    
    // Set cell content
    func setCellContentFor(row: Int, customerSearch: CustomerSearch) {
        contentView.backgroundColor = row % 2 == 0 ? Constants.UBColours.scotiaGrey4 : UIColor.whiteColor()
        labelCustomerName.text = customerSearch.nameLine1?.capitalizedString ?? NSLocalizedString("unknown_name", comment: "Customer Search Cell")
        labelAddress.text = customerSearch.addressLine?.capitalizedString ?? NSLocalizedString("unknown_address", comment: "Customer Search Cell")
        
        // Display lock if the sensitivity code is domicile branch
        imageViewLock.hidden = true
        if let uSensitivityCode = customerSearch.sensitivityCode {
            imageViewLock.hidden = !(uSensitivityCode == .DomicileBranch)
        }
    }
}
