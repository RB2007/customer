//
//  UBConstants.swift
//  BNSUBDemo
//
//  Created by Laura Reategui on 2015-07-03.
//  Copyright (c) 2015 ScotiaBank. All rights reserved.
//

import Foundation

struct Constants {
    struct UBColours {
        
        // Colours
        static let scotiaRed = UIColor(red: 216/255, green: 30/255, blue: 5/255, alpha: 1) // Original Red

        static let scotiaBlue = UIColor(red: 9/255, green: 104/255, blue: 238/255, alpha: 1) // Links
        static let scotiaBlue2 = UIColor(red: 161/255, green: 210/255, blue: 247/255, alpha: 1) // Visualizations
        //static let scotiaGreen = UIColor(red: 67/255, green: 172/255, blue: 106/255, alpha: 1) // Success messages, verified info, check marks
        static let scotiaYellowWarning = UIColor(red: 252/255, green: 246/255, blue: 202/255, alpha: 1) // Warning messages
        static let scotiaRedError = UIColor(red: 246/255, green: 191/255, blue: 185/255, alpha: 1) // Error messages
        static let scotiaRedHover = UIColor(red: 178/255, green: 36/255, blue: 17/255, alpha: 1) // Hover messages
        static let scotiaRedActive = UIColor(red: 118/255, green: 26/255, blue: 14/255, alpha: 1) // Active messages
        
        // Greys
        static let scotiaGrey1 = UIColor(red: 102/255, green: 102/255, blue: 102/255, alpha: 1)
        static let scotiaGrey2 = UIColor(red: 136/255, green: 136/255, blue: 136/255, alpha: 1)
        static let scotiaGrey3 = UIColor(red: 197/255, green: 197/255, blue: 197/255, alpha: 1)
        static let scotiaGrey4 = UIColor(red: 244/255, green: 244/255, blue: 244/255, alpha: 1)
        static let scotiaGrey5 = UIColor(red: 250/255, green: 250/255, blue: 250/255, alpha: 1)
        
        // Other
        static let grey50 = UIColor(red: 50/255, green: 50/255, blue: 50/255, alpha: 1)
        static let scotiaRedHighlighted = UIColor(red: 0.933, green: 0.384, blue: 0.322, alpha: 1)
    }
}

