//
//  ManualSearchType.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-30.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

enum ManualSearchType: Int {
    case CustomerName,
//    BusinessName,
    ScotiaCardNumber
    
    // General
    static let numberOfKeys = 2
    
    func getKeyName() -> String {
        switch self {
        case .CustomerName:
            return NSLocalizedString("customer_name", comment: "Manual Search")
//        case .BusinessName:
//            return NSLocalizedString("business_name", comment: "Manual Search")
        case .ScotiaCardNumber:
            return NSLocalizedString("scotia_card_number", comment: "Manual Search")
        }
    }
    
    // Cusotmize View
    func customizeIcon(imageView: UIImageView) {
        var imageName: String
        switch self {
        case .CustomerName:
            imageName = "PersonIcon"
//        case .BusinessName:
//            imageName = "BusinessIcon"
        case .ScotiaCardNumber:
            imageName = "CardIcon"
        }
        imageView.image = UIImage(named: imageName) ?? UIImage()
    }
    
    func customizePrefixText(label: UILabel) {
        switch self {
        case .CustomerName:
            label.text = ""
            label.hidden = true
//        case .BusinessName:
//            label.text = "*"
//            label.hidden = false
        case .ScotiaCardNumber:
            label.text = "453"
            label.hidden = false
        }
    }
    
    func customizeTextField(textField: UITextField) {
        switch self {
        case .CustomerName:
            textField.text = ""
            textField.placeholder = NSLocalizedString("enter_customer_first_last_name", comment: "Manual search text box placeholder.")
            textField.keyboardType = UIKeyboardType.Default
//        case .BusinessName:
//            textField.text = ""
//            textField.placeholder = NSLocalizedString("enter_business_name", comment: "Manual search text box placeholder.")
//            textField.keyboardType = UIKeyboardType.Default
        case .ScotiaCardNumber:
            textField.text = ""
            textField.placeholder = NSLocalizedString("enter_scotia_card_number", comment: "Manual search text box placeholder.")
            textField.keyboardType = UIKeyboardType.NumberPad
        }
    }
    
    // Textfield Delegate Methods
    func valueChanged(var newString: String) -> String {
        switch self {
        case .CustomerName:
            break
//        case .BusinessName:
//            break
        case .ScotiaCardNumber:
            newString = newString.spacesRemoved
            switch newString.characters.count {
            case 5,6,7:
                let startIndex = newString.startIndex
                newString.insert(" ", atIndex: startIndex.advancedBy(4))
            case 8,9,10:
                let startIndex = newString.startIndex
                newString.insert(" ", atIndex: startIndex.advancedBy(7))
                newString.insert(" ", atIndex: startIndex.advancedBy(4))
            case 11,12,13:
                let startIndex = newString.startIndex
                newString.insert(" ", atIndex: startIndex.advancedBy(10))
                newString.insert(" ", atIndex: startIndex.advancedBy(7))
                newString.insert(" ", atIndex: startIndex.advancedBy(4))
            default:
                break
            }
        }
        return newString
    }
    
    func shouldChangeStringInRangeFor(currentString: String, newString: String, range: NSRange) -> Bool {
        switch self {
        case .CustomerName:
            return Util.isValidFullNameStringInRange(newString, range: range)
//        case .BusinessName:
//            return Util.isValidBusinessNameStringInRange(newString, range: range)
        case .ScotiaCardNumber:
            return Util.isValidScotiaCardStringInRange(newString, range: range)
        }
    }
    
    // Validation
    func isValidValueIn(var aString: String) -> Bool {
        switch self {
        case .CustomerName:
            return Util.isValidFullNameString(aString)
//        case .BusinessName:
//            return Util.isValidBusinessNameString(aString)
        case .ScotiaCardNumber:
            aString = aString.spacesRemoved
            return Util.isValidScotiaCardString(aString)
        }
    }
    
    // Prepare value for submission
    private func getValueFrom(aString: String) -> AnyObject? {
        switch self {
        case .CustomerName:
            return Util.getCustomerFirstLastNameFromString(aString)
//        case .BusinessName:
//            return Util.getBusinessName(aString)
        case .ScotiaCardNumber:
            let trimmedScotiaCardNumber = aString.spacesRemoved
            return Util.getScotiaCardString(trimmedScotiaCardNumber)
        }
    }
    
    func locateCustomerFrom(aString: String, callback:(AnyObject?, NSError?)->()) {
        switch self {
        case .CustomerName:
            if let customerName = getValueFrom(aString) as? [String] {
                if let customerFirstName = customerName.first, customerLastName = customerName.last {
                    APIManagement.sharedInstance.locateCustomerByFirstNameLastName(customerFirstName, customerLastName: customerLastName, dateOfBirth: nil, postalCode: nil, lookUpSubType: LookUpSubType.Manual, completion: { (data: AnyObject?, error: NSError?) -> () in
                        callback(data, error)
                    })
                }
            }
//        case .BusinessName:
//            if let businessName = getValueFrom(aString) as? String {
//                APIManagement.sharedInstance.locateCustomerByBusiness(businessName, completion: { (data: AnyObject?, error: NSError?) -> () in
//                    callback(data, error)
//                })
//            }
        case .ScotiaCardNumber:
            if let scotiaCardNumber = getValueFrom(aString) as? String {
                APIManagement.sharedInstance.locateCustomerByProduct(scotiaCardNumber, lookupSubType: LookUpSubType.Manual, completion: { (data: AnyObject?, error: NSError?) -> () in
                    callback(data, error)
                })
            }
        }
    }
    
    func getErrorMessage() -> (title: String, message: String) {
        
        switch self {
        case .CustomerName:
            return (NSLocalizedString("invalid_full_name_title", comment: "Error title."), NSLocalizedString("invalid_full_name_message", comment: "Error message body."))
//        case .BusinessName:
//            return (NSLocalizedString("invalid_business_name_title", comment: "Error title."), NSLocalizedString("invalid_business_name_message", comment: "Error message body."))
        case .ScotiaCardNumber:
            return (NSLocalizedString("invalid_scotiacard_number_title", comment: "Error title."), NSLocalizedString("invalid_scotiacard_number_message", comment: "Error message body."))
        }
    }
}