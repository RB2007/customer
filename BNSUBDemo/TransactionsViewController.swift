//
//  TransactionsViewController.swift
//  Customer
//
//  Created by Annie Lo on 2016-02-22.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import UIKit
import SVProgressHUD

class TransactionsViewController: UIViewController, UIPageViewControllerDataSource, TransactionHistoryPoppverListSelectorProtocol, UIPageViewControllerDelegate {
    
    // MARK: - Properties
    weak var customerAccountDelegate: CustomerAccountDelegate?
    
    @IBOutlet weak var filterButton: UIButton!
    @IBOutlet weak var containerView: UIView!
    @IBOutlet weak var backButton: UIButton!
    @IBOutlet weak var availableBalanceLabel: UILabel!
    @IBOutlet weak var accountNumberLabel: UILabel!
    @IBOutlet weak var balanceLabel: UILabel!
    @IBOutlet weak var labelAccountNumber: UILabel!
    
    var product: Product? = nil
    var pageViewController: UIPageViewController? = nil
    var filteredProducts: [Product]? = nil
    var selectedFilter: Int = 0
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.pageViewController?.delegate = self
        var selectedProductIndex: Int?
        
        backButton.setTitle(NSLocalizedString("back", comment: "back button"), forState: UIControlState.Normal)
        balanceLabel.text = NSLocalizedString("balance_dashboard", comment: "balance label in transaction history")
        labelAccountNumber.text = NSLocalizedString("account_number_dashboard", comment: "Account number label in transaction history")
        
        if let products = customerAccountDelegate?.customerProfile?.products {
            guard let bankinglist = ScotiaProductTypes.getProductsCodes(ScotiaProductTypes.Banking), creditcardlist = ScotiaProductTypes.getProductsCodes(ScotiaProductTypes.Creditcard) else {
                return
            }
            
            if filteredProducts == nil {
                var tempProductArray = [Product]()
                
                let customerScotiaProductTypes = ScotiaProductTypes.getProductTypes(products)
                
                for customerScotiaProductType in customerScotiaProductTypes {
                    let customerProductsForType = customerScotiaProductType.getProductsFrom(products)
                    
                    for customerProductForType in customerProductsForType {
                        if (bankinglist.contains(customerProductForType.productCode!) || creditcardlist.contains(customerProductForType.productCode!)) {
                            tempProductArray.append(customerProductForType)
                            if (customerProductForType.accountNumber == product?.accountNumber) {
                                selectedProductIndex = tempProductArray.count - 1
                            }
                        }
                    }
                }
                filteredProducts = tempProductArray
            }
        }
        
        customerAccountDelegate?.isToolbarHidden = true
        backButton.layer.cornerRadius = 5;
        self.pageViewController?.dataSource = self
        
        if let uSelectedProductIndex = selectedProductIndex {
            let startingViewController = self.viewControllerAtIndex(uSelectedProductIndex)! as UIViewController
            let viewControllers = [startingViewController]
            accountNumberLabel.text = product?.accountNumber
            if let uQuantity = product?.balanceQuantity {
                let quantity = Util.formatCurrency(uQuantity)
                var currency = product?.balanceCurrency ?? ""
                currency = currency.isEmpty ? "" : currency + " "
                let balance = currency + quantity
                self.availableBalanceLabel.text = balance
            }
            
            displayResultWithDate(uSelectedProductIndex)
            
            self.pageViewController?.setViewControllers(viewControllers, direction: UIPageViewControllerNavigationDirection.Reverse, animated: false, completion: nil)
        }
    }
    
    @IBAction func backButtonPressed(sender: UIButton) {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated)
        customerAccountDelegate?.isToolbarHidden = false
    }
    
    @IBAction func filterButtonPressed(sender: UIButton) {
        let storyboard = UIStoryboard(name: "AccountTransactionHistory", bundle: nil)
        if let vc = storyboard.instantiateViewControllerWithIdentifier("showTransactionHistoryPopup") as? TransactionHistoryPopoverViewController {
            vc.modalPresentationStyle = UIModalPresentationStyle.Popover
            vc.preferredContentSize = CGSizeMake(200, 300)
            vc.popoverPresentationController?.sourceView = filterButton
            vc.popoverPresentationController?.sourceRect = filterButton.bounds
            vc.popoverPresentationController?.permittedArrowDirections = [UIPopoverArrowDirection.Down, UIPopoverArrowDirection.Up]
            
            vc.delegate = self
            vc.selectedValue = selectedFilter
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        if segue.identifier == "embedTransactionHistory" {
            if let vc = segue.destinationViewController as? UIPageViewController {
                self.pageViewController = vc
            }
        }
    }
    
    func retrieveTransactionHistory(index: Int?, dateIndex: Int?, startDate: String?, endDate: String?) {
        if let uProduct = product, uStartDate = startDate, uEndDate = endDate, uIndex = index {
            
            SVProgressHUD.showWithStatus(NSLocalizedString("loading", comment: "Progress indicator"))
            let accountNumberString = uProduct.accountNumber!.stringByReplacingOccurrencesOfString(" ", withString: "")
            
            if let productCode = uProduct.productCode {
                APIManagement.sharedInstance.getAccountTransactionHistory(accountNumberString, accountType: productCode, dateFrom: uStartDate
                    , dateTo: uEndDate) { (data: AnyObject?, error: NSError?) -> () in
                        SVProgressHUD.dismiss()
                        var tempDateTransactionArray = [AccountTransactionHistory]()
                        if let uHistoryTransactions = data as? [[String: AnyObject]] {
                            for historyTransaction in uHistoryTransactions {
                                let dateTransaction = AccountTransactionHistory(json: historyTransaction)
                                tempDateTransactionArray.append(dateTransaction)
                            }
                            self.filteredProducts?[uIndex].transaction = tempDateTransactionArray
                            if let transactionViewController = self.pageViewController?.viewControllers?.last as? TransactionHistoryViewController {
                                transactionViewController.transactions = tempDateTransactionArray
                                self.filteredProducts?[uIndex].dateIndex = dateIndex
                                
                                transactionViewController.transactionHistoryChanged()
                            }
                            
                        }
                }
            }
            
        }
        
    }
    
    func viewControllerAtIndex(index: Int) -> TransactionHistoryViewController? {
        var transactions: [AccountTransactionHistory]?
        if let uTransactions = filteredProducts?[index].transaction {
            
                        if filteredProducts?[index].dateIndex == selectedFilter {
                            transactions = uTransactions
                        }
                    }
        
        if let storyboard = self.storyboard {
            if let contentViewController = storyboard.instantiateViewControllerWithIdentifier("transactionHistory") as? TransactionHistoryViewController {
                contentViewController.pageIndex = index
                contentViewController.productCode = filteredProducts?[index].productCode
                if (transactions != nil) {
                    contentViewController.transactions = transactions
                }
                
                return contentViewController
            }
            
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerBeforeViewController viewController: UIViewController) -> UIViewController? {
        
        if let controller = viewController as? TransactionHistoryViewController {
            var index = controller.pageIndex
            
            if (index != 0 && index != NSNotFound) {
                index--
                return self.viewControllerAtIndex(index)
            }
        }
        
        return nil
    }
    
    func pageViewController(pageViewController: UIPageViewController, viewControllerAfterViewController viewController: UIViewController) -> UIViewController? {
        
        if let controller = viewController as? TransactionHistoryViewController {
            var index = controller.pageIndex
            if (index != NSNotFound && index != filteredProducts!.count - 1) {
                index++
                return self.viewControllerAtIndex(index)
            }
        }
        return nil
    }
    
    func presentationCountForPageViewController(pageViewController: UIPageViewController) -> Int {
        return (self.filteredProducts?.count)!
    }
    
    func presentationIndexForPageViewController(pageViewController: UIPageViewController) -> Int {
        return 0
    }
    
    func didSelectTransactionPeriod(rowIndex: Int?, key: NSDateComponents?) {
        if let uIndex = rowIndex {
            selectedFilter = uIndex
        }
        
        if let transactionViewController = self.pageViewController?.viewControllers?.last as? TransactionHistoryViewController {

            if let uKey = key {
                let startEndDate = generateStartEndDate(uKey.month, year: uKey.year)
                if let startDate = startEndDate.startDate, endDate = startEndDate.endDate {
                    retrieveTransactionHistory(transactionViewController.pageIndex,
                        dateIndex: selectedFilter, startDate: Util.formatDate(startDate), endDate: Util.formatDate(endDate))
                }
            }
        }
    }
    
    func displayResultWithDate(index: Int?) {
        
        let date = NSDate()
        if let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian) {
            let components = cal.components([.Day, .Month, .Year], fromDate: date)
            let startEndDate = generateStartEndDate(components.month, year: components.year)
            if let startDate = startEndDate.startDate, endDate = startEndDate.endDate {
                retrieveTransactionHistory(index, dateIndex: selectedFilter, startDate: Util.formatDate(startDate), endDate: Util.formatDate(endDate))
            }
            
        }
    }
    
    func generateStartEndDate(month: Int?, year: Int?) -> (startDate: NSDate?, endDate: NSDate?) {
        if let uMonth = month, uYear = year {
            if isCurrentPeriod(uMonth, year: uYear) {
                let date = NSDate()
                
                var totalEndDays = 0

                if let nextToNextMonth = NSCalendar.currentCalendar().dateByAddingUnit(.Month, value: 2, toDate: date, options: []) {
                    let nextRange = NSCalendar.currentCalendar().rangeOfUnit(.Day, inUnit: .Month, forDate: nextToNextMonth)
                    totalEndDays = nextRange.length
                }

                if let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian) {
                    let components = cal.components([.Day, .Month, .Year], fromDate: date)
                    components.day = 1
                    
                    let endComponents = cal.components([.Day, .Month, .Year], fromDate: date)
                    endComponents.month = components.month + 2
                    endComponents.day = totalEndDays
                    
                    
                    if let startDateTime = cal.dateFromComponents(components), endDateTime = cal.dateFromComponents(endComponents) {
                        return (startDateTime, endDateTime)
                    }
                }
                
            } else {
                let components = NSDateComponents()
                components.year = uYear
                components.month = uMonth
                components.day = 1
                
                let calendar = NSCalendar.currentCalendar()
                if let startOfMonth = calendar.dateFromComponents(components) {
                    
                    
                    let comps2 = NSDateComponents()
                    comps2.month = 1
                    comps2.day = -1
                    
                    if let endOfMonth = calendar.dateByAddingComponents(comps2, toDate: startOfMonth, options: NSCalendarOptions.MatchStrictly) {
                        return (startOfMonth, endOfMonth)
                    }
                }
            }
        }
        return (nil, nil)
    }
    
    func isCurrentPeriod(month: Int, year: Int) -> Bool {
        let currentDate = NSDate()
        if let cal = NSCalendar(calendarIdentifier: NSCalendarIdentifierGregorian) {
            let currentDateComponents = cal.components([.Month, .Year], fromDate: currentDate)
            if month == currentDateComponents.month && year == currentDateComponents.year {
                return true
            } else {
                return false
            }
        }
        
        return true
        
    }
    
    func pageViewController(pageViewController: UIPageViewController, didFinishAnimating finished: Bool, previousViewControllers: [UIViewController], transitionCompleted completed: Bool) {
        if let lastController = self.pageViewController?.viewControllers?.last as? TransactionHistoryViewController {
            let index = lastController.pageIndex
            accountNumberLabel.text = filteredProducts?[index].accountNumber
            
            if let uQuantity = filteredProducts?[index].balanceQuantity {
                let quantity = Util.formatCurrency(uQuantity)
                var currency = filteredProducts?[index].balanceCurrency ?? ""
                currency = currency.isEmpty ? "" : currency + " "
                let balance = currency + quantity
                availableBalanceLabel.text = balance
            } else {
                availableBalanceLabel.text = ""
            }
            
            var transactions: [AccountTransactionHistory]?
            
            if let uTransactions = filteredProducts?[index].transaction {
                if filteredProducts?[index].dateIndex == selectedFilter {
                    transactions = uTransactions
                }
            }
            
            if transactions == nil {
                displayResultWithDate(index)
            }

        }
    }
}
