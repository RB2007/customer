//
//  EmploymentContactKey.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-02.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

// MARK: - Employment Contact Keys

enum EmploymentContactKey: Int, CustomerKeys {
    case EmployerPhone
    
    static var numberOfKeys: Int {
        get {
            return 1
        }
    }
    
    static var parentSectionNumber: Int {
        get {
            return CustomerEmploymentInfoKeys.EmploymentContact.rawValue
        }
    }
    
    func getKeyName(object: AnyObject) -> String {
        switch self {
        case .EmployerPhone:
            return NSLocalizedString("employer_phone", comment: "Customer Employment Contact Info")
        }
    }
    
    func shouldDisplay(object: AnyObject) -> Bool {
        switch self {
        case .EmployerPhone:
            return true
        }
    }
    
    func isEditable(object: AnyObject) -> Bool {
        switch self {
        case .EmployerPhone:
            return true
        }
    }

    func editingType() -> CustomerInfoEditType {
        switch self {
        case .EmployerPhone:
            return .Keyboard
        }
    }
    
    func getMasterDataForListSelector(object: AnyObject) -> [String: String]? {
        switch self {
        case .EmployerPhone:
            return nil
        }
    }
    
    func getValueFrom(object: AnyObject) -> String {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return ""
        }
        
        switch self {
        case .EmployerPhone:
            guard let phoneNumber = uPersonProfile.employment?.getEmploymentPhoneWithType(PhoneUsageType.EmployerPhone)?.formattedPhoneNumber else {
                return ""
            }
            return phoneNumber
        }
    }

    func valueChanged(object: AnyObject, newValue: String, newValue2: String?) -> String {
        setValueTo(object, newValue: newValue, newValue2: newValue2)
        return getValueFrom(object)
    }

    func setValueTo(object: AnyObject, newValue: String, newValue2: String?) {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return
        }
        
        switch self {
        case .EmployerPhone:
            guard let phone = uPersonProfile.employment?.getEmploymentPhoneWithType(PhoneUsageType.EmployerPhone) else {
                let unformattedValue = Phone.removePhoneNumberFormatting(newValue)
                let newPhone = Phone(number: unformattedValue, phoneExtension: nil, usageTp: PhoneUsageType.EmployerPhone)
                uPersonProfile.employment?.addPhone(newPhone)
                return
            }
            phone.number = Phone.removePhoneNumberFormatting(newValue)
        }
    }

    func isValidValueIn(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
        
        case .EmployerPhone:
            return uPersonProfile.employment?.getEmploymentPhoneWithType(PhoneUsageType.EmployerPhone)?.isValidNumber ?? true
        }
    }

    func shouldChangeStringInRange(object: AnyObject, newString: String, range: NSRange) -> Bool {
        switch self {
        case .EmployerPhone:
            return Phone.isValidPhoneNumberStringInRange(newString, range: range)
        }
    }
    
    func customizeTextField(object: AnyObject, textField: UITextField, editing: Bool) {
        textField.keyboardType = UIKeyboardType.Default
        textField.returnKeyType = UIReturnKeyType.Done
        textField.enabled = isEditable(object) ? editing : false
        textField.borderStyle = isEditable(object) && editing ? UITextBorderStyle.RoundedRect : UITextBorderStyle.None
        textField.backgroundColor = isEditable(object) && editing ? UIColor.whiteColor() : UIColor.clearColor()
        textField.autocapitalizationType = UITextAutocapitalizationType.Words
        
        switch self {
            
        case .EmployerPhone:
            textField.keyboardType = UIKeyboardType.NumberPad
            textField.placeholder = editing ? NSLocalizedString("enter_employer_phone_number", comment: "Customer Employment Contact Info") : ""
        }
    }
    
    func reloadRowsAtIndexPathsWithAnimation(object: AnyObject) -> [[String: AnyObject]]? {
        return nil
    }
}