//
//  Product.swift
//  Customer
//
//  Created by Emad Toukan on 2015-12-11.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

enum ScotiaProductTypes: Int {
    case ScotiaCard, Banking, Borrowing, Investment, Other, Creditcard
    
    static let numberOfScotiaProductTypes: Int = 5
    
    func getName() -> String {
        switch self {
        case .ScotiaCard:
            return NSLocalizedString("scotiacard", comment: "Scotia product name.")
        case .Banking:
            return NSLocalizedString("scotia_banking", comment: "Scotia product name.")
        case .Borrowing:
            return NSLocalizedString("scotia_borrowing", comment: "Scotia product name.")
        case .Investment:
            return NSLocalizedString("scotia_investment", comment: "Scotia product name.")
        case .Other:
            return NSLocalizedString("scotia_other", comment: "Scotia product name.")
        default:
            return ""
        }
    }
    
    static func getProductTypes(products: [Product]) -> [ScotiaProductTypes] {
        var customerScotiaProductTypes: [ScotiaProductTypes] = [ScotiaProductTypes]()
        for rawValue in 0..<ScotiaProductTypes.numberOfScotiaProductTypes {
            if let scotiaProduct = ScotiaProductTypes(rawValue: rawValue) {
                if scotiaProduct.getProductsFrom(products).count > 0 {
                    customerScotiaProductTypes.append(scotiaProduct)
                }
            }
        }
        
        return customerScotiaProductTypes
    }
    
    
    func getProductsFrom(products: [Product]) -> [Product] {
        switch self {
        case .ScotiaCard:
            break
        case .Banking:
            break
        case .Borrowing:
            break
        case .Investment:
            break
        case .Other:
            return products.filter({ (product: Product) -> Bool in
                if let scotiaCardProductCodes = ScotiaProductTypes.getProductsCodes(.ScotiaCard), depositProductCodes = ScotiaProductTypes.getProductsCodes(.Banking), creditProductCodes = ScotiaProductTypes.getProductsCodes(.Borrowing), investmentProductCodes = ScotiaProductTypes.getProductsCodes(.Investment), uProductCode = product.productCode {
                    return !scotiaCardProductCodes.contains(uProductCode) && !depositProductCodes.contains(uProductCode) && !creditProductCodes.contains(uProductCode) && !investmentProductCodes.contains(uProductCode)
                }
                return false
            })
        default:
            break
        }
        
        return products.filter({ (product: Product) -> Bool in
            if let productCodes = ScotiaProductTypes.getProductsCodes(self), uProductCode = product.productCode {
                return productCodes.contains(uProductCode)
            }
            return false
        })
    }
    
    static func getProductsCodes(scotiaProductType: ScotiaProductTypes) -> [String]? {
        switch scotiaProductType {
        case .ScotiaCard:
            return ["CSS"]
        case .Banking:
            return ["DDA", "SAV"]
        case .Borrowing:
            return ["APP", "LOC", "MOR", "OLL", "SCL", "SPL", "SSL", "VAX", "VCL", "VFA", "VFF", "VGD", "VIC", "VLR", "VUS", "VFB", "VFC", "VFG", "VFP", "AFA", "AFB", "AFC", "AFF", "AFG", "AFP"]
        case .Investment:
            return ["IPP"]
        case .Other:
            return nil
        case .Creditcard:
            return ["VCL", "VFA", "VFF", "VGD", "VIC", "VLR", "VUS", "VFB", "VFC", "VFG", "VFP", "AFA", "AFB", "AFC", "AFF", "AFG", "AFP"]
        }
    }
}


class Product {
    // MARK: - JSON Keys
    static let kAccountNumberKey = "AccountNumber"
    static let kAccountStatusKey = "AccountStatus"
    static let kBalanceKey = "Balance"
    static let kBalanceCurrencyKey = "Currency"
    static let kBalanceQuantityKey = "Quantity"
    static let kBranchLocatorTransitKey = "BranchLocatorTransit"
    static let kCabTransitNumberKey = "CabTransitNumber"
    static let kDisclosureIndicatorKey = "DisclosureIndicator"
    static let kLocationKey = "Location"
    static let kDateClosedKey = "DateClosed"
    static let kDateOpenedKey = "DateOpened"
    static let kProductCodeKey = "ProductCode"
    static let kProductNameEnglishKey = "ProductNameEnglish"
    static let kProductNameFrenchKey = "ProductNameFrench"
    static let kProductRelationshipTpKey = "ProductRelationshipTp"
    static let kSubProductCodeKey = "SubProductCode"
    static let kRiskLevelKey = "RiskLevel"
    static let lSubProductCodeKey = "SubProductCode"
    
    // MARK: - Properties
    let json: [String: AnyObject]
    let accountNumber: String?
    let accountStatus: String?
    let balanceCurrency: String?
    let balanceQuantity: Double?
    let branchLocatorTransit: String?
    let cabTransitNumber: String?
    let dateOpened: NSDate?
    let dateClosed: NSDate?
    let disclosureIndicator: String?
    let location: String?
    let productCode: String?
    let productNameEnglish: String?
    let productNameFrench: String?
    let productRelationshipTp: String?
    let riskLevel: String?
    let subProductCode: String?
    var transaction: [AccountTransactionHistory]?
    var dateIndex: Int?
    
    // MARK: - Init
    init(json: [String: AnyObject]) {
        self.json = json
        
        self.accountNumber = json[Product.kAccountNumberKey] as? String
        self.accountStatus = json[Product.kAccountStatusKey] as? String
        
        if let uBalance = json[Product.kBalanceKey] as? [String: AnyObject] {
            self.balanceCurrency = uBalance[Product.kBalanceCurrencyKey] as? String
            self.balanceQuantity = uBalance[Product.kBalanceQuantityKey] as? Double
        } else {
            self.balanceCurrency = nil
            self.balanceQuantity = nil
        }
        
        self.branchLocatorTransit = json[Product.kBranchLocatorTransitKey] as? String
        self.cabTransitNumber = json[Product.kCabTransitNumberKey] as? String
        
        if let dateOpenedString = json[Product.kDateOpenedKey] as? String {
            self.dateOpened = Util.convertAPIStringDate(dateOpenedString)
        } else {
            self.dateOpened = nil
        }
        
        if let dateClosedString = json[Product.kDateClosedKey] as? String {
            self.dateClosed = Util.convertAPIStringDate(dateClosedString)
        } else {
            self.dateClosed = nil
        }
        
        self.disclosureIndicator = json[Product.kDisclosureIndicatorKey] as? String
        self.location = json[Product.kLocationKey] as? String
        self.productCode = json[Product.kProductCodeKey] as? String
        self.productNameEnglish = json[Product.kProductNameEnglishKey] as? String
        self.productNameFrench = json[Product.kProductNameFrenchKey] as? String
        self.productRelationshipTp = json[Product.kProductRelationshipTpKey] as? String
        self.riskLevel = json[Product.kRiskLevelKey] as? String
        self.subProductCode = json[Product.kSubProductCodeKey] as? String
    }
}