//
//  TransactionHistoryPopoverViewController.swift
//  Customer
//
//  Created by Annie Lo on 2016-03-07.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import UIKit

protocol TransactionHistoryPoppverListSelectorProtocol {
    func didSelectTransactionPeriod(index: Int?, key: NSDateComponents?)
}


class TransactionHistoryPopoverViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {
    
    @IBOutlet weak var tableView: UITableView!
    
    var selectedValue: Int?
    var delegate: TransactionHistoryPoppverListSelectorProtocol?
    var months = Util.getLastTwelveMonthsDateComponents()
    
    // MARK: - View Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize view
        self.popoverPresentationController?.backgroundColor = Constants.UBColours.scotiaGrey1
        
        if let uSelectedValue = selectedValue {
            let indexPath = NSIndexPath(forItem: uSelectedValue, inSection: 0)
            self.tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.Top)
        }
    }
    
    // MARK: - Tableview Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return months.count
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("MasterDataListSelectorCell", forIndexPath: indexPath) as? MasterDataListSelectorTableViewCell else {
            return UITableViewCell()
        }
    

        if indexPath.row == 0 {
            cell.setCellContent(NSLocalizedString("current_period", comment: "current period"))
        } else {
            cell.setCellContent(Util.getStringFromDateComponent(months[indexPath.row]))
        }
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        selectedValue = indexPath.row
        self.delegate?.didSelectTransactionPeriod(indexPath.row, key: months[indexPath.row])
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}