//
//  VerifyCustomerIDViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-10.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

protocol CustomerIDVerifiedDelegate {
    func customerVerified(customerSearch: CustomerSearch)
}

class VerifyCustomerIDViewController: UIViewController {

    // MARK: - Properties
    var customerSearch: CustomerSearch?
    @IBOutlet weak var labelVerifyID: UILabel!
    @IBOutlet weak var labelCustomerName: UILabel!
    @IBOutlet weak var labelCustomerAddress: UILabel!
    @IBOutlet weak var buttonVerified: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    @IBOutlet weak var labelCheckCustomerID: UILabel!
    
    var delegate: CustomerIDVerifiedDelegate?
    
    // MARK: - View Delegate Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Customize UI
        labelVerifyID.text = NSLocalizedString("verify_id", comment: "Verify Customer ID")
        buttonVerified.setTitle(NSLocalizedString("verified", comment: "Verify Customer ID"), forState: UIControlState.Normal)
        buttonCancel.setTitle(NSLocalizedString("cancel", comment: ""), forState: UIControlState.Normal)
        buttonVerified.layer.cornerRadius = 5
        labelCheckCustomerID.text = NSLocalizedString("check_customer_id", comment: "Verify Customer ID")
        
        // Setup Customer Information
        if let uCustomerSearch = customerSearch {
            labelCustomerName.text = uCustomerSearch.nameLine1?.capitalizedString ?? NSLocalizedString("unknown_name", comment: "Verify Customer ID")
            labelCustomerAddress.text = uCustomerSearch.addressLine?.capitalizedString ?? NSLocalizedString("unknown_address", comment: "Verify Customer ID")
        }
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Change the corner radius to match the UIDropShadowView corner radius
        self.view.layer.cornerRadius = 16
        self.view.layer.masksToBounds = true
    }

    // MARK: - Class Methods
    @IBAction func verifiedButtonPressed() {
        if let uCustomerSearch = customerSearch {
            delegate?.customerVerified(uCustomerSearch)
        }
    }
    
    @IBAction func cancelButtonPressed() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
