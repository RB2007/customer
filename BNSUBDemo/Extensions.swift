//
//  StringExtensions.swift
//  Customer
//
//  Created by Emad Toukan on 2016-01-11.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import Foundation

extension String {
    
    /// Make the first letter of the string small letter
    var decapitalizedString: String {
        if self.characters.count == 0 {
            return self
        }
        
        let startIndexRange = self.startIndex ... self.startIndex
        let remainingIndexRange = self.startIndex.advancedBy(1) ..< self.endIndex
        return self[startIndexRange].lowercaseString + self[remainingIndexRange]
    }
    
    /// Clean a string by removing more than 1 space between characters
    var additionalSpacesRemoved: String {
        var newString = String()
        for index in self.characters.indices {
            
            // Remove space from the start of the string
            if newString.characters.count == 0 && self[index] == " " {
                continue
            }
            
            // Remove reoccuring spaces if they exist
            if index.advancedBy(1) != self.endIndex {
                if self[index] == " " && self[index.advancedBy(1)] == " " {
                    continue
                }
                newString.append(self[index])
            } else {
                if self[index] != " " {
                    newString.append(self[index])
                }
            }
        }
        return newString
    }
    
    /// Removed spaces from string
    var spacesRemoved: String {
        var newString = String()
        for char in self.characters where char != " "{
            newString.append(char)
        }
        return newString
    }
    
    // Trim string to **** **** **** *771
    var masked: String {
        
        var maskedString = String()
        
        for char in self.characters {
            maskedString += char != " " ? "*" : " "
        }
        
        if self.characters.count > 3 {
            let range = Range(start: self.endIndex.advancedBy(-3), end: self.endIndex)
            let lastThreeDigits = self.substringWithRange(range)
            maskedString.replaceRange(range, with: lastThreeDigits)
        }
        return maskedString
    }
}

extension Bool {
    var localizedDescription: String {
        return self == true ? NSLocalizedString("Yes", comment: "") : NSLocalizedString("No", comment: "")
    }
}

extension Array where Element: Equatable {
    // Remove objects from an array
    mutating func removeObjects(element: Element) {
        for object in self {
            if object == element {
                if let index = self.indexOf(object){
                    self.removeAtIndex(index)
                }
            }
        }
    }
}