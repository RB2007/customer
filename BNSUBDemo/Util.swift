//
//  Util.swift
//  Customer
//
//  Created by Emad Toukan on 2015-10-16.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

let kAction = "action"
enum SaveActions: String {
    case Added = "A",
    Changed = "C",
    Deleted = "D"
}

enum AppSupportedLanguages: String {
    case English = "en",
    French = "fr"
}

enum EnvironmentConfigurationKeys: String {
    case APIBaseURL = "UBAPIBaseUrl",
    P12Code = "P12code",
    SplunkAPIKey = "UBCustomerSplunkAPIKey"
}

enum CountryCode: String {
    case Canada = "CA",
    UnitedStates = "US",
    NotApplicable = "000",
    Unknown = "099"
}

enum CanadianProvinceCodes: String {
    case Alberta = "AB",
    BritishColumbia = "BC",
    Manitoba = "MB",
    NewBrunswick = "NB",
    NewfoundlandLabrador = "NL",
    NorthwestTerritories = "NT",
    NovaScotia = "NS",
    Nunavut = "NU",
    Ontario = "ON",
    PrinceEdwardIsland = "PE",
    Saskatchewan = "SK",
    Quebec = "QC",
    Yukon = "YT"
    
    static func allCodes() -> [String] {
        return ["AB", "BC", "MB", "NB", "NL", "NT", "NS", "NU", "ON", "PE", "SK", "QC", "YT"]
    }
    
    func ageOfMajority() -> Int {
        switch self {
        case .Alberta, .Manitoba, .Ontario, .PrinceEdwardIsland, .Quebec, .Saskatchewan:
            return 18
        case .BritishColumbia, .NewBrunswick, .NewfoundlandLabrador, .NorthwestTerritories, .NovaScotia, .Nunavut, .Yukon:
            return 19
        }
    }
}

struct Util {
    
    static let kFullNameMaxCharacterCount = 50
    static let kBusinessNameMaxCharacterCount = 50
    static let kScotiaCardMaxCharacterCount = 13 // Excluding the first 3 numbers – 453
    static let kUserDefaultsConfigurationKey = "com.apple.configuration.managed"
    static let kAllowedLettersForTextInput = ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
    static let kAllowedCharacterForTextInput = ["-", "+", "'", "/", ".", " ", "à", "â", "è", "é", "ê", "ë", "ï", "î", "ô", "ù", "ü", "û", "ÿ", "ç", "*"]
    static let kAllowedNumbersForTextInput = ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
    
    // Required actions to notify the backend
    static let kAction = "action"
    static let kActionValueAdded = "A"
    static let kActionValueChanged = "C"
    
    
    // Airwarch Setup
    static func getConfigurationVariableFor(key: EnvironmentConfigurationKeys) -> String? {
        let dictionary = NSUserDefaults.standardUserDefaults().dictionaryForKey(Util.kUserDefaultsConfigurationKey) as? [String: String]
        return dictionary?[key.rawValue]
    }
    
    // Format date strings to short style
    static func formatDateString(dateString:String) -> String {
        guard dateString.characters.count > 0 else {
            return ""
        }
        
        // Available date formats
        let dateFormats = ["yyyy/mm/dd","yyyy-mm-dd", "yyyymmdd"]
        
        // Create a date formatter
        let dateFormatter = NSDateFormatter()
        
        for dateFormat in dateFormats {
            dateFormatter.dateFormat = dateFormat
            let date = dateFormatter.dateFromString(dateString)
            if date != nil {
                return formatDateStringFromDate(date!)
            }
        }
        return ""
    }
    
    private static func formatDateStringFromDate(date: NSDate) -> String {
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateStyle = NSDateFormatterStyle.ShortStyle
        return dateFormatter.stringFromDate(date)
    }
    
    // Format Number
    static func formatCurrency(number: Double) -> String {
        let numberFormatter = NSNumberFormatter()
        numberFormatter.numberStyle = NSNumberFormatterStyle.CurrencyStyle
        numberFormatter.minimumFractionDigits = 0
        return numberFormatter.stringFromNumber(NSNumber(double: number)) ?? ""
    }
    
    static func unformatCurrency(string: String) -> Double {
        var unformattedString = ""
        // Remove all characters that are not numbers
        // Keep the decimal if found
        for char in string.characters where kAllowedNumbersForTextInput.contains(String(char)) || char == "." {
            unformattedString.append(char)
        }
        return Double(unformattedString) ?? 0
    }
    
    static func formatDate(date: NSDate) -> String {
        return dateFormatter().stringFromDate(date)
    }
    
    static func formatDate(dateString: String) -> NSDate? {
        return dateFormatter().dateFromString(dateString)
    }
    
    static func formatDate(timeInterval: NSTimeInterval) -> String {
        let date = NSDate(timeIntervalSince1970: timeInterval)
        return Util.formatDate(date)
    }
    
    static func convertAPIStringDate(dateString: String) -> NSDate? {
        return apiDateFormatter().dateFromString(dateString)
    }
    
    static func convertAPIStringDate(date: NSDate) -> String? {
        return apiDateFormatter().stringFromDate(date)
    }
    
    private static func dateFormatter() -> NSDateFormatter {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-dd"
        return formatter
    }
    
    private static func apiDateFormatter() -> NSDateFormatter {
        let formatter = NSDateFormatter()
        formatter.dateFormat = "yyyy-MM-ddXXXXX"
        return formatter
    }
    
    // MARK: - Full Name
    static func isValidFullNameStringInRange(aString: String, range: NSRange) -> Bool {
        // Check that the Full Name is not more than 50 characters
        return range.location < kFullNameMaxCharacterCount || aString == "" ? true : false
    }
    
    static func isValidFullNameString(aString: String) -> Bool {
        
        // Check that its less that or equal to 50 characters
        if aString.characters.count > kFullNameMaxCharacterCount {
            return false
        }
        
        return getCustomerFirstLastNameFromString(aString) != nil ? true : false
    }
    
    // Split a string into a first name and last name assuming a space separates them
    static func getCustomerFirstLastNameFromString(aString: String) -> [String]? {
        // Should have two parts for first and last name, separated by " "
        var names = aString.componentsSeparatedByString(" ")
        
        // Remove empty spaces in the name
        names.removeObjects("")
        
        if names.count < 2 {
            return nil
        }
        
        guard let firstName = names.first, lastName = names.last else {
            return nil
        }
        return [firstName, lastName]
    }
    
    // MARK: - Business Name
    static func getBusinessName(businessName: String) -> String {
        return "*" + businessName
    }
    
    static func isValidBusinessNameStringInRange(aString: String, range: NSRange) -> Bool {
        // Check that the Business Name is not more than 50 characters
        return range.location < kBusinessNameMaxCharacterCount || aString == "" ? true : false
    }
    
    static func isValidBusinessNameString(aString: String) -> Bool {
        // Check that its less that or equal to 50 characters
        return aString.characters.count <= kBusinessNameMaxCharacterCount ? true : false
    }
    
    // MARK: - ScotiaCard
    static func getScotiaCardString(scotiaCardNumber: String) -> String {
        return "453" + scotiaCardNumber
    }
    
    static func isValidScotiaCardStringInRange(aString: String, range: NSRange) -> Bool {
        
        // Check for Integers
        if Int(aString) == nil && aString != "" {
            return false
        }
        
        // Check that the ScotiaCard number is not more than 13 characters
        // Adding 3 more characters due to formatting card number with 3 spaces
        if range.location >= kScotiaCardMaxCharacterCount + 3 && aString != "" {
            return false
        }
        
        return true
    }
    
    static func isValidScotiaCardString(aString: String) -> Bool {
        
        // Check that all characters are numbers
        for char in aString.characters where Int(String(char)) == nil {
            return false
        }
        
        // Check that its equal to 13 characters
        if aString.characters.count != kScotiaCardMaxCharacterCount {
            return false
        }
        
        return true
    }
    
    static var currentLanguage: AppSupportedLanguages {
        get {
            let preferredLanguage = NSLocale.preferredLanguages()[0]
            if preferredLanguage.characters.count >= 2 {
                let preferredLanguageShort = preferredLanguage[preferredLanguage.startIndex ..< preferredLanguage.startIndex.advancedBy(2)]
                return preferredLanguageShort == "fr" ? AppSupportedLanguages.French : AppSupportedLanguages.English
            }
            return AppSupportedLanguages.English
        }
    }
    
    // MARK: - General
    
    // Create error messag from NSError
    static func createErrorMessage(error: NSError) -> (title: String, message: String) {
        let title = NSLocalizedString("error", comment: "") + " - " + error.domain + String(error.code)
        let message = error.localizedDescription
        return (title, message)
    }
    
    /**
     Get JSON Object from Bundle
     
     - parameter fileName: The file name in the bundle containing JSON string data
     
     - returns: Returns a dictionary with the json data
     */
    func getJSONFromBundle(fileName: String) -> [String: AnyObject]? {
        if let filePath = NSBundle.mainBundle().pathForResource(fileName, ofType: nil) {
            do {
                if let data = try String(contentsOfFile: filePath, encoding: NSUTF8StringEncoding).dataUsingEncoding(NSUTF8StringEncoding) {
                    return try NSJSONSerialization.JSONObjectWithData(data, options: NSJSONReadingOptions.AllowFragments) as? [String: AnyObject]
                }
            }
            catch let error as NSError {
                print(error)
            }
        }
        print("File not found")
        return nil
    }
    
    /**
     Iterate through StringTypesForInput array and return if the value is true
     
     - parameter string:             String to be inserted
     - parameter allowedStringTypes: Array of allowed StringTypesForInput
     - parameter maxRangeLocation:   Max range location for the string
     - parameter range:              Range for which the string will be inserted
     
     - returns: Bool value determining whether the string can be inserted or not
     */
    static func isValidStringInRange(string: String, allowedStringTypes: [StringTypesForInput], maxRangeLocation: Int, range: NSRange) -> Bool {
        
        // Check that the type exists in the allowed string types
        for stringType in allowedStringTypes {
            if stringType.isAllowed(string, range: range, maxRangeLocation: maxRangeLocation) {
                return true
            }
        }
        return false
    }
    
    static func getLastTwelveMonthsDateComponents() -> [NSDateComponents] {
        var dateComponents = [NSDateComponents]()
        let cal = NSCalendar.currentCalendar()
        // start with today
        var date = cal.startOfDayForDate(NSDate())
        let testDate = NSCalendar.currentCalendar().dateByAddingUnit(.Month, value: 1, toDate: NSDate(), options: [])
        print (testDate)
        for _ in 1 ... 12 {
            
            // get day component:
            let month = cal.components([.Month, .Year], fromDate: date)
            
            dateComponents.append(month)
            print (cal.components([.Day, .Month, .Year], fromDate: date))
            
            // move back in time by one day:
            date = cal.dateByAddingUnit(NSCalendarUnit.Month, value: -1, toDate: date, options: NSCalendarOptions.MatchStrictly)!
        }
        
        return dateComponents
    }
    
    static func getStringFromDateComponent(dateComponent: NSDateComponents) -> String? {
        
        let dateFormatter1 = NSDateFormatter()
        dateFormatter1.dateFormat = "MM"
        
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.dateFormat = "MMMM"
        
        let tempMonth = dateComponent.month
        let tempYear = dateComponent.year
        
        if let monthObj = dateFormatter1.dateFromString(String(tempMonth)) {
            let monthString = dateFormatter2.stringFromDate(monthObj)
            return monthString + " " + String(tempYear)
        }
        
        return nil
    }
}