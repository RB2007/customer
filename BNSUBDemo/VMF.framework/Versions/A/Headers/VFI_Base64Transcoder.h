//Duplicating for Duplicate Symbols Error when using -all_load flag!
// 8/12/2015
extern size_t EstimateBas64EncodedDataSize(size_t inDataSize);
extern size_t EstimateBas64DecodedDataSize(size_t inDataSize);

extern size_t EstimateBas64EncodedDataSize2(size_t inDataSize);
extern size_t EstimateBas64DecodedDataSize2(size_t inDataSize);

extern size_t EstimateBas64EncodedDataSize3(size_t inDataSize);
extern size_t EstimateBas64DecodedDataSize3(size_t inDataSize);

extern bool Base64EncodeData(const void *inInputData, size_t inInputDataSize, char *outOutputData, size_t *ioOutputDataSize, BOOL wrapped);
extern bool Base64DecodeData(const void *inInputData, size_t inInputDataSize, void *ioOutputData, size_t *ioOutputDataSize);

extern bool Base64EncodeData2(const void *inInputData, size_t inInputDataSize, char *outOutputData, size_t *ioOutputDataSize, BOOL wrapped);
extern bool Base64DecodeData2(const void *inInputData, size_t inInputDataSize, void *ioOutputData, size_t *ioOutputDataSize);

extern bool Base64EncodeData3(const void *inInputData, size_t inInputDataSize, char *outOutputData, size_t *ioOutputDataSize, BOOL wrapped);
extern bool Base64DecodeData3(const void *inInputData, size_t inInputDataSize, void *ioOutputData, size_t *ioOutputDataSize);