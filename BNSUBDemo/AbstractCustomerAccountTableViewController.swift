
//  AbstractCustomerAccountTableViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-15.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class AbstractCustomerAccountTableViewController: UITableViewController, CustomerInfoEditProtocol {

    // MARK: - Properties
    weak var customerAccountDelegate: CustomerAccountDelegate?
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Remove top navigation
        self.navigationController?.setNavigationBarHidden(true, animated: false)

        // Customize UI
        tableView.backgroundColor = Constants.UBColours.scotiaGrey4
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        
        // Register cells
        let cellNib = UINib(nibName: "CustomerInfoTableViewCell", bundle: nil)
        tableView.registerNib(cellNib, forCellReuseIdentifier: "CustomerInfoCell")
        let actionCellNib = UINib(nibName: "SaveCancelActionButtonsTableViewCell", bundle: nil)
        tableView.registerNib(actionCellNib, forCellReuseIdentifier: "SaveCancelActionButtonsCell")
        let headerNib = UINib(nibName: "CustomerInfoSectionHeader", bundle: nil)
        tableView.registerNib(headerNib, forHeaderFooterViewReuseIdentifier: "CustomerInfoSectionHeader")
    }
    
    deinit {
        print("Deinit is called on AbstractCustomerAccountTableViewController")
    }
    
    // MARK: - Tableview Delegate Methods
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
    
    // MARK: - Instance Methods
    func customerAccountDataChanged() {
        self.tableView.reloadData()
    }
    
    // MARK: - Customer Info Edit Protocol
    func showMasterDataPopoverListSelectorFor(view: UIView, masterData: [String: String]?, currentValue: String?, delegate: MasterDataPoppverListSelectorProtocol) {
        self.view.endEditing(true)
        let storyboard = UIStoryboard(name: "CustomerAccount", bundle: nil)
        if let vc = storyboard.instantiateViewControllerWithIdentifier("MasterDataListSelectorPopover") as? MasterDataPopoverListSelectorViewController, uMasterData = masterData {
            vc.modalPresentationStyle = UIModalPresentationStyle.Popover
            let preferredHeight = uMasterData.count < 3 ? 150 : 300
            vc.preferredContentSize = CGSizeMake(view.bounds.width, CGFloat(preferredHeight))
            vc.popoverPresentationController?.sourceView = view
            vc.popoverPresentationController?.sourceRect = view.bounds
            vc.popoverPresentationController?.permittedArrowDirections = [UIPopoverArrowDirection.Down, UIPopoverArrowDirection.Up]
            vc.data = uMasterData
            vc.delegate = delegate
            vc.selectedValue = currentValue
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }
    
    func showYearMonthPopoverPickerFor(view: UIView, currentValue: String?, delegate: YearMonthPoppverPickerProtocol) {
        self.view.endEditing(true)
        let storyboard = UIStoryboard(name: "CustomerAccount", bundle: nil)
        if let vc = storyboard.instantiateViewControllerWithIdentifier("YearMonthPIckerPopover") as? YearMonthPopoverPickerViewController {
            vc.modalPresentationStyle = UIModalPresentationStyle.Popover
            vc.preferredContentSize = CGSizeMake(view.bounds.width, 200)
            vc.popoverPresentationController?.sourceView = view
            vc.popoverPresentationController?.sourceRect = view.bounds
            vc.popoverPresentationController?.permittedArrowDirections = [UIPopoverArrowDirection.Up, UIPopoverArrowDirection.Down]
            vc.delegate = delegate
            vc.selectedValue = currentValue
            self.presentViewController(vc, animated: true, completion: nil)
        }
    }

    func reloadRowsAtIndexPaths(indexPathObjects: [[String: AnyObject]]) {
        for object in indexPathObjects {
            if let uIndexPath = object["indexPath"] as? NSIndexPath, shouldDisplay = object["shouldDisplay"] as? Bool {
                var rowAnimation = UITableViewRowAnimation.None
                if let uCell = tableView.cellForRowAtIndexPath(uIndexPath) as? CustomerInfoTableViewCell {
                    if uCell.isCellHidden && shouldDisplay {
                        rowAnimation = UITableViewRowAnimation.Bottom
                    }
                    
                    if !uCell.isCellHidden && !shouldDisplay {
                        rowAnimation = UITableViewRowAnimation.Top
                    }
                }
                tableView.reloadRowsAtIndexPaths([uIndexPath], withRowAnimation: rowAnimation)
            }
        }
    }
}
