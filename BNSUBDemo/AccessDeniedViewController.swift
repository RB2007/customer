//
//  AccessDeniedViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-03.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class AccessDeniedViewController: UIViewController {

    // MARK: - Properties
    @IBOutlet weak var labelOops: UILabel!
    @IBOutlet weak var labelAccessDeniedString: UILabel!
    @IBOutlet weak var buttonRetry: UIButton!
    
    // MARK: - View Delegate Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Localizing strings
        labelOops.text = NSLocalizedString("oops", comment: "Access Denied View Controller")
        labelAccessDeniedString.text = NSLocalizedString("access_denied_string", comment: "Access Denied View Controller")
        
        // Disable swipe back gestsure
        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
        
        // Customize UI
        buttonRetry.setTitle(NSLocalizedString("retry", comment: "Access Denied View Controller"), forState: UIControlState.Normal)
        buttonRetry.layer.borderWidth = 1
        buttonRetry.layer.borderColor = Constants.UBColours.scotiaRed.CGColor
        buttonRetry.layer.cornerRadius = 5
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }
    
    // MARK: - Instance Methods
    @IBAction func retryButtonPressed() {
        self.navigationController?.popViewControllerAnimated(true)
    }
}
