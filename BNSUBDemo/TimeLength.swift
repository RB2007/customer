//
//  TimeLength.swift
//  Customer
//
//  Created by Emad Toukan on 2015-12-11.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

class TimeLength {
    
    // MARK: - JSON Keys
    static let kYearsKey = "Years"
    static let kMonthsKey = "Months"
    static let kDaysKey = "Days"
    static let kHoursKey = "Hours"
    static let kMinutesKey = "Minutes"
    
    // MARK: - Properties
    let json: [String: AnyObject]?
    var years: Int?
    var months: Int?
    let days: Int?
    let hours: Int?
    let minutes: Int?
    
    // MARK: - Init
    init(json: [String: AnyObject]) {
        self.json = json
        self.years = json[TimeLength.kYearsKey] as? Int
        self.months = json[TimeLength.kMonthsKey] as? Int
        self.days = json[TimeLength.kDaysKey] as? Int
        self.hours = json[TimeLength.kHoursKey] as? Int
        self.minutes = json[TimeLength.kMinutesKey] as? Int
    }
    
    init(years: Int, months: Int, days: Int, hours: Int, minutes: Int) {
        self.json = nil
        self.years = years
        self.months = months
        self.days = days
        self.hours = hours
        self.minutes = minutes
    }
    
    // MARK: - Validation Properties
    var isValidYears: Bool? {
        return isValidTimeLengthProperty(self.years)
    }
    
    var isValidMonths: Bool? {
        return isValidTimeLengthProperty(self.months)
    }
    
    var isValidDays: Bool? {
        return isValidTimeLengthProperty(self.days)
    }
    
    var isValidHours: Bool? {
        return isValidTimeLengthProperty(self.hours)
    }
    
    var isValidMinutes: Bool? {
        return isValidTimeLengthProperty(self.minutes)
    }
    
    // MARK: - Utility
    private func isValidTimeLengthProperty(property: Int?) -> Bool? {
        guard let uProperty = property else {
            return nil
        }
        
        return uProperty >= 0 && uProperty < 100 ? true : false
    }
    
     // MARK: - Save
    var paramsForSave: [String: AnyObject]? {
        
        // If the json is nil, then the object was added
        guard let uJSON = json else {
            return timeLengthParams
        }
        
        // Check if anything changed from the original values
        let originalTimeLength = TimeLength(json: uJSON)
        if self.years != originalTimeLength.years || self.months != originalTimeLength.months {
            return timeLengthParams
        }
        
        // Return nil if nothing has changed
        return nil
    }
    
    private var timeLengthParams: [String: AnyObject] {
        var timeLengthParams = [String: AnyObject]()
        timeLengthParams[TimeLength.kYearsKey.decapitalizedString] = years ?? 0
        timeLengthParams[TimeLength.kMonthsKey.decapitalizedString] = months ?? 0
        timeLengthParams[TimeLength.kDaysKey.decapitalizedString] = days ?? 0
        timeLengthParams[TimeLength.kHoursKey.decapitalizedString] = hours ?? 0
        timeLengthParams[TimeLength.kMinutesKey.decapitalizedString] = minutes ?? 0
        return timeLengthParams
    }
}