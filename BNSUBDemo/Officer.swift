//
//  Officer.swift
//  Customer
//
//  Created by Emad Toukan on 2016-01-15.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import Foundation

class Officer {
    
    // MARK: - JSON Keys
    static let kOfficerIdKey = "officerId"
    static let kOfficerNameKey = "officerName"
    static let kOfficerEmailKey = "officerEmail"
    static let kOfficerRolesKey = "officerRoles"
    
    // MARK: - Properties
    static var id: String?
    static var name: String?
    static var email: String?
    static var officerRoles: [OfficerRole]?
    
    static var currentSelectedRole: OfficerRole? {
        didSet {
            if let _ = Officer.id, _ = Officer.currentSelectedRole?.transit?.id {
                Analytics.sharedInstance.addEventWithName(SplunkConstants.kTransitSelectedKey, theList: SplunkConstants.params())
            }
        }
    }
    
    static var officerTransitParams: [String: String] {
        var params = [String: String]()
        params[Officer.kOfficerIdKey] = Officer.id
        params[OfficerRole.kTransitIdKey] = Officer.currentSelectedRole?.transit?.id
        return params
    }
    
    // MARK: - Init
    static func saveOfficer(json: [String: AnyObject]) {
        Officer.id = json[Officer.kOfficerIdKey] as? String
        Officer.name = json[Officer.kOfficerNameKey] as? String
        Officer.email = json[Officer.kOfficerEmailKey] as? String
        var tempOfficerRoles: [OfficerRole]?
        if let uOfficerRolesJSON = json[Officer.kOfficerRolesKey] as? [[String: AnyObject]] {
            for officerRoleJSON in uOfficerRolesJSON {
                let officerRole = OfficerRole(json: officerRoleJSON)
                if tempOfficerRoles == nil {
                    tempOfficerRoles = [OfficerRole]()
                }
                tempOfficerRoles?.append(officerRole)
            }
        }
        Officer.officerRoles = tempOfficerRoles
    }
}