//
//  CustomerInfoTableViewCell.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-17.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

protocol CustomerInfoEditProtocol: class {
    func showMasterDataPopoverListSelectorFor(view: UIView, masterData: [String: String]?, currentValue: String?, delegate: MasterDataPoppverListSelectorProtocol)
    func showYearMonthPopoverPickerFor(view: UIView, currentValue: String?, delegate: YearMonthPoppverPickerProtocol)
    func reloadRowsAtIndexPaths(indexPathObjects: [[String: AnyObject]])
}

class CustomerInfoTableViewCell: UITableViewCell, UITextFieldDelegate, MasterDataPoppverListSelectorProtocol, YearMonthPoppverPickerProtocol {

    // MARK: - Properties
    @IBOutlet weak var labelKey: UILabel!
    @IBOutlet weak var textFieldValue: UITextField!
    @IBOutlet weak var imageViewErrorIcon: UIImageView!
    @IBOutlet weak var labelIncompleteEntry: UILabel!
    @IBOutlet weak var viewMain: UIView!
    
    var displayedObject: AnyObject?
    var customerKey: CustomerKeys?
    weak var delegate: CustomerInfoEditProtocol?
    var isCellHidden: Bool {
        return viewMain.hidden
    }
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()        
        // Set TextField delegate
        self.textFieldValue.delegate = self
        self.textFieldValue.addTarget(self, action: Selector("textFieldValueChanged:"), forControlEvents: .EditingChanged)
    }
    
    // MARK: - Instance Methods
    func setCellContent(displayedObject: AnyObject, customerKey: CustomerKeys, isEditing: Bool, indexPath: NSIndexPath, delegate: CustomerInfoEditProtocol) {
        
        // Set instance variables
        self.displayedObject = displayedObject
        self.customerKey = customerKey
        self.delegate = delegate
        
        // Configure view
        self.backgroundColor = indexPath.section % 2 == 0 ? UIColor.whiteColor() : Constants.UBColours.scotiaGrey4
        customerKey.customizeTextField(displayedObject, textField: textFieldValue, editing: isEditing)
        self.viewMain.hidden = !customerKey.shouldDisplay(displayedObject)

        // Set cell content
        self.labelKey.text = customerKey.getKeyName(displayedObject)
        self.textFieldValue.text = customerKey.getValueFrom(displayedObject)
        
        // Update view if entry is invalid
        self.updateInvalidEntryView()
    }
    
    func updateInvalidEntryView() {
        
        guard let uDisplayedObject = displayedObject, let uCustomerKey = customerKey else {
            return
        }
        let isValidEntry = uCustomerKey.isValidValueIn(uDisplayedObject)
        self.imageViewErrorIcon.image = isValidEntry ? UIImage() : UIImage(named: "Red Cross Cricle")
        self.labelIncompleteEntry.text = isValidEntry ? "" : NSLocalizedString("incomplete_entry", comment: "Customer info entry fields.")
    }
    
    // TextField Delegate Methods
    func textFieldShouldBeginEditing(textField: UITextField) -> Bool {
        
        guard let uEditingType = customerKey?.editingType(), uDisplayedObject = displayedObject else {
            return false
        }
        
        switch uEditingType {
        case .Keyboard:
            return true
        case .MasterDataListSelector:
            self.delegate?.showMasterDataPopoverListSelectorFor(textField, masterData: customerKey?.getMasterDataForListSelector(uDisplayedObject), currentValue: customerKey?.getValueFrom(uDisplayedObject), delegate: self)
            return false
        case .YearMonthPicker:
            self.delegate?.showYearMonthPopoverPickerFor(textField, currentValue: customerKey?.getValueFrom(uDisplayedObject), delegate: self)
            return false
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        return true
    }
    
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard let uCustomerKey = customerKey, uDisplayedObject = displayedObject else {
            return false
        }
        return uCustomerKey.shouldChangeStringInRange(uDisplayedObject, newString: string, range: range)
    }
    
    func textFieldValueChanged(textField: UITextField) {
        guard let uDisplayedObject = displayedObject, let uCustomerKey = customerKey, newValue = textField.text else {
            return
        }
        textField.text = uCustomerKey.valueChanged(uDisplayedObject, newValue: newValue, newValue2: nil)
        self.updateInvalidEntryView()
    }
    
    // MARK: - Master Data Popover List Selector Protocol
    func didSelectMasterDataKey(key: String) {
        guard let uDisplayedObject = displayedObject, let uCustomerKey = customerKey else {
            return
        }
        self.textFieldValue.text = uCustomerKey.valueChanged(uDisplayedObject, newValue: key, newValue2: nil)
        self.updateInvalidEntryView()
        if let uReloadIndexPathObjects = uCustomerKey.reloadRowsAtIndexPathsWithAnimation(uDisplayedObject) {
            delegate?.reloadRowsAtIndexPaths(uReloadIndexPathObjects)
        }
        
    }
    
    // MARK: - Year Month Popover Picker Protocol
    func didSelectYearMonth(year: Int, month: Int) {
        guard let uDisplayedObject = displayedObject, let uCustomerKey = customerKey else {
            return
        }
        self.textFieldValue.text = uCustomerKey.valueChanged(uDisplayedObject, newValue: String(year), newValue2: String(month))
        self.updateInvalidEntryView()
    }
}
