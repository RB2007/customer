//
//  Email.swift
//  Customer
//
//  Created by Emad Toukan on 2015-12-10.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

enum EmailUsageType: String {
    case GeneralEmail = "GenEmail",
    Unknown = "Unknown"
}

class Email {

    // MARK: - JSON Keys
    static let kUsageTpKey = "UsageTp"
    static let kEmailAddressKey = "EmailAddress"
    
    // MARK: - Constants
    static let kEmailMaxCharacterCount = 129
    
    // MARK: - Properties
    let json: [String: AnyObject]?
    let usageTp: EmailUsageType
    var address: String?
    
    // MARK: - Init
    init?(json: [String: AnyObject]) {
        self.json = json
        self.address = json[Email.kEmailAddressKey] as? String
        
        // Usage type
        guard let tempUsageTp = json[Email.kUsageTpKey] as? String else {
            self.usageTp = EmailUsageType.Unknown
            return nil
        }
        
        guard let uUsageType = EmailUsageType(rawValue: tempUsageTp) else {
            self.usageTp = EmailUsageType.Unknown
            return nil
        }
        
        self.usageTp = uUsageType
    }
    
    init(address: String, usageTp: EmailUsageType) {
        self.json = nil
        self.usageTp = usageTp
        self.address = address
    }
    
    // MARK: - Validation
    var isValidEmailAddress: Bool? {
        
        guard let uAddress = address where uAddress.isEmpty == false else {
            return nil
        }
        
        // Should have two parts, separated by @
        var pieces : [String] = uAddress.componentsSeparatedByString("@")
        if pieces.count != 2 {
            return false
        }
        
        // None of the strings should be empty
        if pieces[0].characters.count < 1 || pieces[1].characters.count < 1 {
            return false
        }
        
        // Tail (what comes after @ sign) should be string separated by a minimum 1 dot
        pieces = pieces[1].componentsSeparatedByString(".")
        if pieces.count < 2 {
            return false
        }
        
        // None of the strings should be less than 2 characters
        for piece in pieces {
            if piece.characters.count < 2 {
                return false
            }
        }
        return true
    }
    
    // MARK: - Validation in Range
    static func isValidEmailStringInRange(aString: String, range: NSRange) -> Bool {
        
        // Check if the entered string is a number or a backspace
        if aString == " " && aString != "" {
            return false
        }
        
        // Check if the string exceeds the allowed string limit for an email
        if range.location >= Email.kEmailMaxCharacterCount && aString != "" {
            return false
        }
        
        return true
    }
    
    // MARK: - Save
    var paramsForSave: [String: AnyObject]? {
        // If the json is nil, then the object was added
        guard let uJSON = json else {
            return createEmailParams(SaveActions.Added)
        }
        
        // Check if anything changed from the original values
        if let uOriginalEmail = Email(json: uJSON) {
            if self.address != uOriginalEmail.address {
                return createEmailParams(SaveActions.Changed)
            }
        }
        
        // Return nil if nothing has changed
        return nil
    }
    
    private func createEmailParams(action: SaveActions) -> [String: AnyObject] {
        var params = [String: AnyObject]()
        params[kAction] = action.rawValue
        params[Email.kUsageTpKey.decapitalizedString] = self.usageTp.rawValue
        params[Email.kEmailAddressKey.decapitalizedString] = self.address ?? ""
        return params
    }
}