//
//  ScannerDriverLicenseViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-12.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit
import AVFoundation

class ScannerDriverLicenseViewController: UIViewController, AVCaptureMetadataOutputObjectsDelegate {
    
    // MARK: - Properties
    var captureSession: AVCaptureSession?
    var videoPreviewLayer: AVCaptureVideoPreviewLayer?
    var pdf417CodeFrameView: UIView?
    
    @IBOutlet weak var labelUnavailableVideo: UILabel!
    @IBOutlet weak var viewUnavailable: UIView!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var viewBottomDescription: UIView!
    @IBOutlet weak var viewVideoHelpControls: UIView!
    @IBOutlet weak var labelScanBarcodeHelpText: UILabel!
    
    var delegate: ScannerDriverLicenseDelegate?
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize UI
        labelUnavailableVideo.text = NSLocalizedString("video_unavailable", comment: "Scanner Driver License View Controller")
        labelScanBarcodeHelpText.text = NSLocalizedString("scan_barcode_help_text", comment: "Scanner Driver License View Controller")
        buttonClose.setTitle(NSLocalizedString("close", comment: "Button"), forState: UIControlState.Normal)
        buttonClose.layer.borderColor = UIColor.whiteColor().CGColor
        buttonClose.layer.borderWidth = 1.5
        buttonClose.layer.cornerRadius = 5
        viewBottomDescription.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        
        // Check for the current authorization
        let AVMediaAuthorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        if AVMediaAuthorizationStatus == AVAuthorizationStatus.Authorized || AVMediaAuthorizationStatus == AVAuthorizationStatus.NotDetermined {
            startCaptureSession()
        } else {
            viewUnavailable.hidden = false
        }
    }
    
    deinit {
        print("Deinit is called on ScannerDriverLicenseViewController")
    }
    
    // MARK : - Setup Methods
    func startCaptureSession() {
        // Initializing the video session
        let captureDevice = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo)
        do {
            let input = try AVCaptureDeviceInput(device: captureDevice)
            captureSession = AVCaptureSession()
            captureSession?.addInput(input)
        } catch  {
            print("We are unable to load the camera on the device")
            return
        }

        let captureMetadataOutput = AVCaptureMetadataOutput()
        captureSession?.addOutput(captureMetadataOutput)
        captureMetadataOutput.setMetadataObjectsDelegate(self, queue: dispatch_get_main_queue())
        // Assigning PDF417 as the output to look for
        captureMetadataOutput.metadataObjectTypes = [AVMetadataObjectTypePDF417Code]
        captureSession?.sessionPreset = AVCaptureSessionPresetHigh
        self.configureZoom()
        // Initializing the video
        videoPreviewLayer = AVCaptureVideoPreviewLayer(session: captureSession)
        videoPreviewLayer?.videoGravity = AVLayerVideoGravityResizeAspectFill
        let currentInterfaceOrientation = UIApplication.sharedApplication().statusBarOrientation
        videoPreviewLayer?.connection.videoOrientation = interfaceOrientationToVideoOrientation(currentInterfaceOrientation)
        videoPreviewLayer?.frame = self.view.layer.bounds
        if let uVideoPreviewLayer = videoPreviewLayer {
            self.view.layer.addSublayer(uVideoPreviewLayer)
        }
        
        setupCodeFrameView()
        
        // Start the capture session
        captureSession?.startRunning()
    }
    
    func setupCodeFrameView() {
        // Initializing the red box
        pdf417CodeFrameView = UIView()
        pdf417CodeFrameView?.layer.borderColor = Constants.UBColours.scotiaRed.CGColor
        pdf417CodeFrameView?.layer.borderWidth = 2
        if let uPDF417CodeFrameView = pdf417CodeFrameView {
            self.view.addSubview(uPDF417CodeFrameView)
            self.view.bringSubviewToFront(uPDF417CodeFrameView)
        }
        // Bring the help controls to the front
        self.view.bringSubviewToFront(viewVideoHelpControls)
    }
    
    // MARK: - AVCapture Metadata Output Objects Delegate
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputMetadataObjects metadataObjects: [AnyObject]!, fromConnection connection: AVCaptureConnection!) {
        
        if metadataObjects.count == 0 {
            pdf417CodeFrameView?.frame = CGRectZero
            return
        }
        
        if let metaDataObject = metadataObjects[0] as? AVMetadataMachineReadableCodeObject {
            
            // Create the box around the code
            if let codeObject = videoPreviewLayer?.transformedMetadataObjectForMetadataObject(metaDataObject) as? AVMetadataMachineReadableCodeObject {
                pdf417CodeFrameView?.frame = codeObject.bounds
            }
            
            // Get the string out of the object
            if let metaDataObjectStringValue = metaDataObject.stringValue {
                captureSession?.stopRunning()
                self.dismissViewControllerAnimated(true, completion: { () -> Void in
                    print(metaDataObjectStringValue)
                    let driverLicense = DriverLicense(driverLicenseCode: metaDataObjectStringValue)
                    self.delegate?.didReceiveDriverLicense(driverLicense)
                })
            }
        }
    }
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval) {
        videoPreviewLayer?.connection.videoOrientation = interfaceOrientationToVideoOrientation(toInterfaceOrientation)
    }
    
    func interfaceOrientationToVideoOrientation(interfaceOrientation: UIInterfaceOrientation) -> AVCaptureVideoOrientation {
        switch interfaceOrientation {
        case .Portrait:
            return .Portrait
        case .PortraitUpsideDown:
            return .PortraitUpsideDown
        case .LandscapeLeft:
            return .LandscapeLeft
        case .LandscapeRight:
            return .LandscapeRight
        default:
            return .LandscapeRight
        }
    }
    
    func configureZoom() {
        if let currentDeviceInput = captureSession?.inputs.first as? AVCaptureDeviceInput {
            do {
                try currentDeviceInput.device.lockForConfiguration()
                currentDeviceInput.device.videoZoomFactor = 1.5
                currentDeviceInput.device.unlockForConfiguration()
            } catch {
                print("Couldn't lock the device for configuration for zoom")
                return
            }
        }
    }
    
    @IBAction func closeButtonPressed() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
