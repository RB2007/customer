//
//  CustomerInfoSectionHeader.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-17.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

protocol CustomerInfoSectionHeaderProtocol {
    func addButtonPressedInSection(section: Int)
}


class CustomerInfoSectionHeader: UITableViewHeaderFooterView {

    // MARK: - Properties
    @IBOutlet weak var labelSectionTitle: UILabel!
    @IBOutlet weak var viewBackgroundView: UIView!
    @IBOutlet weak var buttonAdd: UIButton!
    var section: Int?
    var delegate: CustomerInfoSectionHeaderProtocol?
    
    // MARK: - Cell Override Methods
    override func awakeFromNib() {
        viewBackgroundView.backgroundColor = Constants.UBColours.scotiaGrey4
    }
    
    // MARK: - Instance Methods
    func setHeaderContent(headerText: String, section: Int, alternateBackgroundColor: Bool, addbuttonHidden: Bool, delegate: CustomerInfoSectionHeaderProtocol?) {
        self.labelSectionTitle.text = headerText
        if alternateBackgroundColor {
            viewBackgroundView.backgroundColor = section % 2 == 0 ? UIColor.whiteColor() : Constants.UBColours.scotiaGrey4
        }
        self.section = section
        self.delegate = delegate
        self.buttonAdd.hidden = addbuttonHidden
    }
    
    @IBAction func addButtonPressed() {
        if let uSection = section {
            delegate?.addButtonPressedInSection(uSection)
        }
    }
}
