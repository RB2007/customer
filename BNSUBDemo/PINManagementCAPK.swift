//
//  PINManagementCAPK.swift
//  BNSUBDemo
//
//  Created by Annie Lo on 2015-09-21.
//  Copyright (c) 2015 ScotiaBank. All rights reserved.
//

import Foundation

/**
 *  Model for PINManagement CAPK files
 */
struct PINManagementCAPK {
    
    /// rid
    var rid: String?
    
    /// PKI index
    var pkiIndex: String?
    
    /// modulus
    var modulus: NSData?
    
    /// checksum
    var checksum: NSData?
    
    /// expiry date
    var expiryDate: NSDate?
    
    /// exponent
    var exponent: NSData?
    
    
    /**
     Model initialization
     
     - parameter rid:        rid
     - parameter pkiIndex:   pki index
     - parameter modulus:    modulus
     - parameter exponent:   exponent
     - parameter checksum:   checksum
     - parameter expiryDate: expiry date
     
     - returns: model of a CAPK file
     */
    init(rid: String, pkiIndex: String, modulus: NSData, exponent: NSData, checksum: NSData, expiryDate: NSDate?) {
        self.rid = rid
        self.pkiIndex = pkiIndex
        self.modulus = modulus
        self.exponent = exponent
        self.checksum = checksum
        self.expiryDate = expiryDate
    }
}