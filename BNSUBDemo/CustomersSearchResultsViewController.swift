//
//  CustomersSearchResultsViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-10.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class CustomersSearchResultsViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate, CustomerIDVerifiedDelegate {
    
    // MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var buttonCancel: UIButton!
    
    var customerSearchResults: [CustomerSearch]? {
        didSet {
            filteredCustomerSearchResults = customerSearchResults
        }
    }
    var filteredCustomerSearchResults: [CustomerSearch]?
    var delegate: CustomersLookupDelegate?
    
    // MARK: - View Delegate Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize UI
        labelTitle.text = NSLocalizedString("select_customer", comment: "Customer Search Results")
        buttonCancel.setTitle(NSLocalizedString("cancel", comment: ""), forState: UIControlState.Normal)
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        searchBar.placeholder = NSLocalizedString("search_customer_address", comment: "CUstomer Search Results")
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Removing the background for the UIDropShadowView, which is added with the Modal Transition Form Sheet
        self.view.superview?.backgroundColor = UIColor.clearColor()
        self.view.superview?.layer.shadowColor = UIColor.clearColor().CGColor
        
        // Change the corner radius to match the UIDropShadowView corner radius
        self.view.layer.cornerRadius = 16
        self.view.layer.masksToBounds = true
        
        // Deselect the cell when the view reappears
        if let indexPath = tableView.indexPathForSelectedRow {
            tableView.deselectRowAtIndexPath(indexPath, animated: true)
        }
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showVerifyID" {
            if let vc = segue.destinationViewController as? VerifyCustomerIDViewController, customerSearch = sender as? CustomerSearch {
                vc.delegate = self
                vc.customerSearch = customerSearch
            }
        }
    }
    
    @IBAction func cancelButtonPressed() {
        self.delegate?.cancelSearch()
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - TableView Delegate
    
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        if let uFilteredCustomerSearchResults = filteredCustomerSearchResults {
            return uFilteredCustomerSearchResults.count
        }
        return 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("CustomersSearchResultsCell", forIndexPath: indexPath) as? CustomersSearchResultsTableViewCell, let uFilteredCustomerSearchResults = filteredCustomerSearchResults else {
            return UITableViewCell()
        }
        
        let customerSearch = uFilteredCustomerSearchResults[indexPath.row]
        cell.setCellContentFor(indexPath.row, customerSearch: customerSearch)
        
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let uFilteredCustomerSearchResults = filteredCustomerSearchResults {
            let customerSearch = uFilteredCustomerSearchResults[indexPath.row]

            if customerSearch.pinned {
                customerVerified(customerSearch)
            } else {
                self.performSegueWithIdentifier("showVerifyID", sender: customerSearch)
            }
        }
    }
    
    // MARK: - Search Bar Delegate
    func searchBarSearchButtonClicked(searchBar: UISearchBar) {
        self.view.endEditing(true)
    }
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        // Filter the customers based on address
        if let uCustomerSearchResults = customerSearchResults where searchText.characters.count != 0 {
            filteredCustomerSearchResults = uCustomerSearchResults.filter({(item: CustomerSearch) -> Bool in
                let stringMatch = item.addressLine?.rangeOfString(searchText.uppercaseString)
                return stringMatch != nil ? true : false
            })
        } else {
            filteredCustomerSearchResults = customerSearchResults
        }
        tableView.reloadData()
    }
    
    // MARK: - Customer ID Verified Delegate
    func customerVerified(customerSearch: CustomerSearch) {
        self.presentingViewController?.dismissViewControllerAnimated(true, completion: { () -> Void in
            self.delegate?.loadSelectedCustomer(customerSearch)
        })
    }
}
