//
//  PINManagementEMVConfig.swift
//  BNSUBDemo
//
//  Created by Annie Lo on 2015-09-21.
//  Copyright (c) 2015 ScotiaBank. All rights reserved.
//

import Foundation

/**
 *  Model for PINManagementEMVConfig used on the pinpad
 */
struct PINManagementEMVConfig {
    
    /// Create a model for EMV configuration
    let emv = VFIEMVConfiguration()
    
    /**
     Initialize the object with the following methods
     
     - parameter schemeReference:          scheme reference
     - parameter issuerReference:          issuer reference
     - parameter trmDataPresent:           trm data present
     - parameter targetRSPercent:          target rs percent
     - parameter floorLimit:               floor limit
     - parameter rsThreshold:              rs threshold
     - parameter maxTargetRSPercent:       max target rs percent
     - parameter merchantForceOnlineFlag:  merchant force online flag
     - parameter blackListCardSupportFlag: blacklist card support flag
     - parameter fallbackAllowedFlag:      fallback allowed flag
     - parameter tacDefault:               tac Default
     - parameter tacDenial:                tac denial
     - parameter tacOnline:                tac online
     - parameter defaultTDOL:              default TDOL
     - parameter defaultDDOL:              default DDOL
     - parameter autoSelectApplication:    auto select application
     - parameter countryCode:              country code
     - parameter currencyCode:             currency code
     - parameter terminalCapacity:         terminal capacity
     - parameter additionalCapacity:       additional capacity
     - parameter terminalType:             terminal type
     - parameter merchantCategoryCode:     merchant category code
     - parameter terminalCategoryCode:     terminal category code
     - parameter terminalID:               terminalID
     - parameter merchantID:               merchantID
     - parameter acquireID:                acquireID
     - parameter capkIndex:                capk index
     - parameter pinBypassFlag:            pin bypass flag
     - parameter pinTimeout:               pin timeout
     - parameter pinFormat:                pin format
     - parameter pinScriptNumber:          pin script number
     - parameter pinMacroNumber:           pin macro number
     - parameter pinDevriKey:              pin devri key
     - parameter pinDevriMacro:            pin devri macro
     - parameter cardStatusDisplay:        card status display
     - parameter termCurExp:               term cur exp
     - parameter issAcqFlag:               iss acq flag
     - parameter noDisplaySupportFlag:     no display support flag
     - parameter modifyCandidateListFlag:  modify candidate list flag
     - parameter rfu1String:               rfu1 string
     - parameter rfu2String:               rfu2 string
     - parameter rfu3String:               rfu3 string
     - parameter vfi_rfu1:                 vfi_rfu1
     - parameter vfi_rfu2:                 vfi_rfu2
     - parameter vfi_rfu3:                 vfi_rfu3
     
     - returns: VFIEMVConfiguration object with all the 
     */
    init(schemeReference: Int32,
        issuerReference: Int32,
        trmDataPresent: Int32,
        targetRSPercent: Int32,
        floorLimit: Double,
        rsThreshold: Double,
        maxTargetRSPercent: Int32,
        merchantForceOnlineFlag: Int32,
        blackListCardSupportFlag: Int32,
        fallbackAllowedFlag: Int32,
        tacDefault: String,
        tacDenial: String,
        tacOnline: String,
        defaultTDOL: String,
        defaultDDOL: String,
        autoSelectApplication: Int32,
        countryCode: String,
        currencyCode: String,
        terminalCapacity: String,
        additionalCapacity: String,
        terminalType: String,
        merchantCategoryCode: String,
        terminalCategoryCode: String,
        terminalID: String,
        merchantID: String,
        acquireID: String,
        capkIndex: String,
        pinBypassFlag: String,
        pinTimeout: String,
        pinFormat: String,
        pinScriptNumber: String,
        pinMacroNumber: String,
        pinDevriKey: String,
        pinDevriMacro: String,
        cardStatusDisplay: String,
        termCurExp: Int32,
        issAcqFlag: Int32,
        noDisplaySupportFlag: Int32,
        modifyCandidateListFlag: Int32,
        rfu1String: String,
        rfu2String: String,
        rfu3String: String,
        vfi_rfu1: Int32,
        vfi_rfu2: Int32,
        vfi_rfu3: Int32) {
            
        emv.clear()
        emv.schemeReference = schemeReference
        emv.issuerReference = issuerReference
        emv.trmDataPresent = trmDataPresent
        emv.targetRSPercent = targetRSPercent
        emv.floorLimit = floorLimit
        emv.rsThreshold = rsThreshold
        emv.maxTargetRSPercent = maxTargetRSPercent
        emv.merchantForceOnlineFlag = merchantForceOnlineFlag
        emv.blackListCardSupportFlag = blackListCardSupportFlag
        emv.fallbackAllowedFlag = fallbackAllowedFlag
        emv.tacDefault = tacDefault
        emv.tacDenial = tacDenial
        emv.tacOnline = tacOnline
        emv.defaultTDOL = defaultTDOL //a list of tags that we want to be provided
        emv.defaultDDOL = defaultDDOL
        emv.autoSelectApplication = autoSelectApplication
        emv.countryCode = countryCode
        emv.currencyCode = currencyCode
        emv.terminalCapacity = terminalCapacity
        emv.additionalCapacity = additionalCapacity
        emv.terminalType = terminalType
        emv.merchantCategoryCode = merchantCategoryCode
        emv.terminalCategoryCode = terminalCategoryCode
        emv.terminalID = terminalID
        emv.merchantID = merchantID
        emv.acquireID = acquireID
        emv.capkIndex = capkIndex
        emv.pinBypassFlag = pinBypassFlag
        emv.pinTimeout = pinTimeout
        emv.pinFormat = pinFormat
        emv.pinScriptNumber = pinScriptNumber
        emv.pinMacroNumber = pinMacroNumber
        emv.pinDevriKey = pinDevriKey
        emv.pinDevriMacro = pinDevriMacro
        emv.cardStatusDisplay = cardStatusDisplay
        emv.termCurExp = termCurExp
        emv.issAcqFlag = issAcqFlag
        emv.noDisplaySupportFlag = noDisplaySupportFlag
        emv.modifyCandidateListFlag = modifyCandidateListFlag
        emv.rfu1String = rfu1String
        emv.rfu2String = rfu2String
        emv.rfu3String = rfu3String
        emv.vfi_rfu1 = vfi_rfu1
        emv.vfi_rfu2 = vfi_rfu2
        emv.vfi_rfu3 = vfi_rfu3
        
    }
}