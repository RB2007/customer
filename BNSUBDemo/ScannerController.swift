//
//  ViewController.swift
//  MWBarcodeCameraDemo
//
//  Created by vladimir zivkovic on 7/24/14.
//  Copyright (c) 2014 Manateeworks. All rights reserved.
//  v2.1

/*


Changes in v2.1:
- Added Parser lib
- Added ANalytics lib

Changes in v2.0:
- Added Multithreading support
- New structured result type - MWResult

Changes in v1.4:
- Added ITF-14 support
- Added Code 11 support
- Added MSI Plessey support
- GS1 support


*/

import UIKit
import Foundation
import AVFoundation
import CoreVideo
import CoreMedia


class ScannerController: UIViewController, AVCaptureVideoDataOutputSampleBufferDelegate, UIAlertViewDelegate {
    
    @IBOutlet weak var viewVideoHelpControls: UIView!
    @IBOutlet weak var viewVideoUnavailable: UIView!
    @IBOutlet weak var viewBottomDescription: UIView!
    @IBOutlet weak var labelUnavailableVideo: UILabel!
    @IBOutlet weak var buttonClose: UIButton!
    @IBOutlet weak var labelScanBarcodeHelpText: UILabel!
    
    
    var delegate: ScannerDriverLicenseDelegate?
    
    let MAX_THREADS = 4
    let USE_MWOVERLAY = true
    let PDF_OPTIMIZED = true
    let USE_TOUCH_TO_ZOOM = false
    
    let USE_MWPARSER = true
    let USE_MWANALYTICS = false
    
    let USE_60_FPS = false
    
    /* Parser */
    /*
    *   Set the desired parser type
    *   Available options:
    *       MWP_PARSER_MASK_NONE
    *       MWP_PARSER_MASK_IUID
    *       MWP_PARSER_MASK_ISBT
    *       MWP_PARSER_MASK_AAMVA
    *       MWP_PARSER_MASK_HIBC
    */
    let MWPARSER_MASK = MWP_PARSER_MASK_AAMVA
    
    
    // !!! Rects are in format: x, y, width, height!!!
    let RECT_LANDSCAPE_1D: Array<Float> =      [4, 20, 92, 60]
    let RECT_LANDSCAPE_2D: Array<Float> =      [20, 5, 60, 90]
    let RECT_PORTRAIT_1D: Array<Float> =       [20, 4, 60, 92]
    let RECT_PORTRAIT_2D: Array<Float> =       [20, 5, 60, 90]
    let RECT_FULL_1D: Array<Float> =           [4, 4, 92, 92]
    let RECT_FULL_2D: Array<Float> =           [20, 5, 60, 90]
    let RECT_DOTCODE: Array<Float> =           [30,20,40,60]
    
    
    enum camera_state {
        case NORMAL,
        LAUNCHING_CAMERA,
        CAMERA,
        CAMERA_DECODING,
        DECODE_DISPLAY,
        CANCELLING
    }
    
    let DecoderResultNotification = "DecoderResultNotification"
    
    var device: AVCaptureDevice!
    var captureSession: AVCaptureSession!
    var prevLayer: AVCaptureVideoPreviewLayer!
    var state:camera_state = camera_state.NORMAL
    var lastFormat:String!
    var lastResultString:String!
    var focusTimer: NSTimer!
    var activeThreads:Int!
    var availableThreads:Int!
    var videoZoomSupported = false
    
    var param_ZoomLevel1:Int!
    var param_ZoomLevel2:Int!
    var zoomLevel:Int!
    var firstZoom:Double!
    var secondZoom:Double!
    
    //    var totalFrames = 0
    
    override func prefersStatusBarHidden() -> Bool {
        return true;
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        param_ZoomLevel1 = 0; //set automatic
        param_ZoomLevel2 = 0; //set automatic
        zoomLevel = 0;
        
        initDecoder();
        /* Analytics */
        /*
        *   Register analytics with given apiUser and apiKey
        */
        /*
        if USE_MWANALYTICS {
        MWBAnalytics.getInstance().initializeAnalyticsWithUsername("apiUser", apiKey: "apiKey")
        }*/
        
        /* Parser */
        /*
        * Register parser with given username and key
        */
        
        if USE_MWPARSER {
            MWP_registerParser(MWP_PARSER_MASK_ISBT,      "username", "key");
            MWP_registerParser(MWP_PARSER_MASK_IUID,      "username", "key");
            MWP_registerParser(MWP_PARSER_MASK_AAMVA,     "harish.mullapudi@scotiabank.com", "72D2188943C524C5E764982B63E744D8831DE39859C3FAA60AEF5DAA229854BE");
            MWP_registerParser(MWP_PARSER_MASK_HIBC,      "username", "key");
        }
        
        
        //        var timer = NSTimer.scheduledTimerWithTimeInterval(3.0, target: self, selector: Selector("measureFPS"), userInfo: nil, repeats: true)
        
        // Do any additional setup after loading the view, typically from a nib.
        
    }
    /*
    func measureFPS() {
    var fps = Float(totalFrames)/3
    totalFrames = 0
    
    println("FPS: \(fps)")
    
    }*/
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated);
        self.customizeView()
        // Check for the current authorization
        let AVMediaAuthorizationStatus = AVCaptureDevice.authorizationStatusForMediaType(AVMediaTypeVideo)
        if AVMediaAuthorizationStatus == AVAuthorizationStatus.Authorized || AVMediaAuthorizationStatus == AVAuthorizationStatus.NotDetermined {
            self.initCapture();
            self.startScanning();
        } else {
            viewVideoUnavailable.hidden = false
        }
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillDisappear(animated);
        if self.captureSession != nil {
            self.stopScanning();
            self.deinitCapture();
        }
    }
    
    override func viewDidLayoutSubviews() {
        super.viewDidLayoutSubviews()
        if self.prevLayer != nil {
            self.prevLayer.frame = self.view.bounds
            if USE_MWOVERLAY == true {
                MWOverlay.removeFromPreviewLayer();
                MWOverlay.addToPreviewLayer(self.prevLayer);
            }
        }
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    
    func customizeView() {
        // Removing the background for the UIDropShadowView, which is added with the Modal Transition Form Sheet
        self.view.backgroundColor = UIColor.clearColor()
        self.view.superview?.backgroundColor = UIColor.clearColor()
        self.view.superview?.layer.shadowColor = UIColor.clearColor().CGColor
        
        labelUnavailableVideo.text = NSLocalizedString("video_unavailable", comment: "Scanner Controller")
        labelScanBarcodeHelpText.text = NSLocalizedString("scan_barcode_help_text", comment: "Scanner Controller")
        viewBottomDescription.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.5)
        buttonClose.setTitle(NSLocalizedString("close", comment: "Button"), forState: UIControlState.Normal)
        buttonClose.layer.borderColor = UIColor.whiteColor().CGColor
        buttonClose.layer.borderWidth = 1.5
        buttonClose.layer.cornerRadius = 5
    }
    
    
    override func willRotateToInterfaceOrientation(toInterfaceOrientation: UIInterfaceOrientation, duration: NSTimeInterval ){
        
        // UIView.setAnimationsEnabled(false);
        
        if toInterfaceOrientation == UIInterfaceOrientation.LandscapeLeft{
            self.prevLayer.connection.videoOrientation = AVCaptureVideoOrientation.LandscapeLeft;
            self.prevLayer.frame = CGRectMake(0, 0, max(self.view.frame.size.width,self.view.frame.size.height), min(self.view.frame.size.width,self.view.frame.size.height));
        }
        
        if toInterfaceOrientation == UIInterfaceOrientation.LandscapeRight{
            self.prevLayer.connection.videoOrientation = AVCaptureVideoOrientation.LandscapeRight;
            self.prevLayer.frame = CGRectMake(0, 0, max(self.view.frame.size.width,self.view.frame.size.height), min(self.view.frame.size.width,self.view.frame.size.height));
        }
        
        if toInterfaceOrientation == UIInterfaceOrientation.Portrait{
            self.prevLayer.connection.videoOrientation = AVCaptureVideoOrientation.Portrait;
            self.prevLayer.frame = CGRectMake(0, 0, min(self.view.frame.size.width,self.view.frame.size.height), max(self.view.frame.size.width,self.view.frame.size.height));
        }
        
        if toInterfaceOrientation == UIInterfaceOrientation.PortraitUpsideDown{
            self.prevLayer.connection.videoOrientation = AVCaptureVideoOrientation.PortraitUpsideDown;
            self.prevLayer.frame = CGRectMake(0, 0, min(self.view.frame.size.width,self.view.frame.size.height), max(self.view.frame.size.width,self.view.frame.size.height));
        }
        
        
        
        
        if USE_MWOVERLAY == true {
            MWOverlay.removeFromPreviewLayer();
            MWOverlay.addToPreviewLayer(self.prevLayer);
        }
        
        
    }
    
    func startScanning() {
        self.captureSession.startRunning()
        self.prevLayer.hidden = false
        self.state = camera_state.CAMERA
    }
    
    func stopScanning() {
        self.captureSession.stopRunning();
        self.state = camera_state.NORMAL
        self.prevLayer.hidden = true
    }
    
    func reFocus() {
        do{
            
            try self.device?.lockForConfiguration()
            if (self.device.focusPointOfInterestSupported){
                self.device.focusPointOfInterest = CGPointMake(0.49,0.49);
                self.device.focusMode = AVCaptureFocusMode.AutoFocus;
            }
            self.device.unlockForConfiguration();
            
        }catch _{
            
        }
    }
    
    func toggleTorch()
    {
        if (self.device.isTorchModeSupported(AVCaptureTorchMode.On)) {
            do{
                try self.device?.lockForConfiguration()
                if (self.device.torchMode == AVCaptureTorchMode.On){
                    self.device.torchMode = AVCaptureTorchMode.Off;
                }else {
                    self.device.torchMode = AVCaptureTorchMode.On;
                }
                
                self.device.unlockForConfiguration();
                
            }catch _{
            }
        }
    }
    
    override func touchesBegan(touches: Set<UITouch>, withEvent event: UIEvent?) {
        
        if USE_TOUCH_TO_ZOOM {
            self.doZoomToggle()
        }else
        {
            self.toggleTorch()
        }
        
    }
    
    
    func initDecoder() {
        
        
        //register your copy of library with givern user/password
        MWB_registerCode(MWB_CODE_MASK_39,      "username", "key");
        MWB_registerCode(MWB_CODE_MASK_93,      "username", "key");
        MWB_registerCode(MWB_CODE_MASK_25,      "username", "key");
        MWB_registerCode(MWB_CODE_MASK_128,     "username", "key");
        MWB_registerCode(MWB_CODE_MASK_AZTEC,   "username", "key");
        MWB_registerCode(MWB_CODE_MASK_DM,      "username", "key");
        MWB_registerCode(MWB_CODE_MASK_EANUPC,  "username", "key");
        MWB_registerCode(MWB_CODE_MASK_QR,      "username", "key");
        MWB_registerCode(MWB_CODE_MASK_PDF,     "harish.mullapudi@scotiabank.com", "047DEF0F3DB4989C1BA509DC618EED557C71D5D267DE9A197E4C5C051501118C");
        MWB_registerCode(MWB_CODE_MASK_RSS,     "username", "key");
        MWB_registerCode(MWB_CODE_MASK_CODABAR, "username", "key");
        MWB_registerCode(MWB_CODE_MASK_DOTCODE, "username", "key");
        MWB_registerCode(MWB_CODE_MASK_11,      "username", "key");
        MWB_registerCode(MWB_CODE_MASK_MSI,     "username", "key");
        
        
        // choose code type or types you want to search for
        
        if (PDF_OPTIMIZED){
            MWB_setActiveCodes(MWB_CODE_MASK_PDF);
            MWB_setDirection(MWB_SCANDIRECTION_HORIZONTAL);
            MWB_setScanningRect(MWB_CODE_MASK_PDF,    RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3] );
        } else {
            // Our sample app is configured by default to search all supported barcodes...
            MWB_setActiveCodes(MWB_CODE_MASK_PDF);
            //            MWB_setActiveCodes(MWB_CODE_MASK_25    |
            //                MWB_CODE_MASK_39     |
            //                MWB_CODE_MASK_93     |
            //                MWB_CODE_MASK_128    |
            //                MWB_CODE_MASK_AZTEC  |
            //                MWB_CODE_MASK_DM     |
            //                MWB_CODE_MASK_EANUPC |
            //                MWB_CODE_MASK_PDF    |
            //                MWB_CODE_MASK_QR     |
            //                MWB_CODE_MASK_CODABAR |
            //                MWB_CODE_MASK_11     |
            //                MWB_CODE_MASK_MSI    |
            //                MWB_CODE_MASK_RSS);
            
            // Our sample app is configured by default to search both directions...
            MWB_setDirection(MWB_SCANDIRECTION_HORIZONTAL | MWB_SCANDIRECTION_VERTICAL);
            // set the scanning rectangle based on scan direction(format in pct: x, y, width, height)
            MWB_setScanningRect(MWB_CODE_MASK_25,     RECT_FULL_1D[0], RECT_FULL_1D[1], RECT_FULL_1D[2], RECT_FULL_1D[3] );
            MWB_setScanningRect(MWB_CODE_MASK_39,     RECT_FULL_1D[0], RECT_FULL_1D[1], RECT_FULL_1D[2], RECT_FULL_1D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_93,     RECT_FULL_1D[0], RECT_FULL_1D[1], RECT_FULL_1D[2], RECT_FULL_1D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_128,    RECT_FULL_1D[0], RECT_FULL_1D[1], RECT_FULL_1D[2], RECT_FULL_1D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_AZTEC,  RECT_FULL_2D[0], RECT_FULL_2D[1], RECT_FULL_2D[2], RECT_FULL_2D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_DM,     RECT_FULL_2D[0], RECT_FULL_2D[1], RECT_FULL_2D[2], RECT_FULL_2D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_EANUPC, RECT_FULL_1D[0], RECT_FULL_1D[1], RECT_FULL_1D[2], RECT_FULL_1D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_PDF,    RECT_FULL_1D[0], RECT_FULL_1D[1], RECT_FULL_1D[2], RECT_FULL_1D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_QR,     RECT_FULL_2D[0], RECT_FULL_2D[1], RECT_FULL_2D[2], RECT_FULL_2D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_RSS,    RECT_FULL_1D[0], RECT_FULL_1D[1], RECT_FULL_1D[2], RECT_FULL_1D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_CODABAR,RECT_FULL_1D[0], RECT_FULL_1D[1], RECT_FULL_1D[2], RECT_FULL_1D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_DOTCODE,RECT_DOTCODE[0], RECT_DOTCODE[1], RECT_DOTCODE[2], RECT_DOTCODE[3]);
            MWB_setScanningRect(MWB_CODE_MASK_11,     RECT_FULL_1D[0], RECT_FULL_1D[1], RECT_FULL_1D[2], RECT_FULL_1D[3]);
            MWB_setScanningRect(MWB_CODE_MASK_MSI,    RECT_FULL_1D[0], RECT_FULL_1D[1], RECT_FULL_1D[2], RECT_FULL_1D[3]);
        }
        
        
        // But for better performance, only activate the symbologies your application requires...
        // MWB_setActiveCodes( MWB_CODE_MASK_25 );
        // MWB_setActiveCodes( MWB_CODE_MASK_39 );
        // MWB_setActiveCodes( MWB_CODE_MASK_93 );
        // MWB_setActiveCodes( MWB_CODE_MASK_128 );
        // MWB_setActiveCodes( MWB_CODE_MASK_AZTEC );
        // MWB_setActiveCodes( MWB_CODE_MASK_DM );
        // MWB_setActiveCodes( MWB_CODE_MASK_EANUPC );
        MWB_setActiveCodes( MWB_CODE_MASK_PDF );
        // MWB_setActiveCodes( MWB_CODE_MASK_QR );
        // MWB_setActiveCodes( MWB_CODE_MASK_RSS );
        // MWB_setActiveCodes( MWB_CODE_MASK_CODABAR );
        // MWB_setActiveCodes( MWB_CODE_MASK_DOTCODE );
        // MWB_setActiveCodes( MWB_CODE_MASK_11 );
        // MWB_setActiveCodes( MWB_CODE_MASK_MSI );
        
        
        // But for better performance, set like this for PORTRAIT scanning...
        // MWB_setDirection(MWB_SCANDIRECTION_VERTICAL);
        // set the scanning rectangle based on scan direction(format in pct: x, y, width, height)
        // MWB_setScanningRect(MWB_CODE_MASK_25,     RECT_PORTRAIT_1D[0], RECT_PORTRAIT_1D[1], RECT_PORTRAIT_1D[2], RECT_PORTRAIT_1D[3] );
        // MWB_setScanningRect(MWB_CODE_MASK_39,     RECT_PORTRAIT_1D[0], RECT_PORTRAIT_1D[1], RECT_PORTRAIT_1D[2], RECT_PORTRAIT_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_93,     RECT_PORTRAIT_1D[0], RECT_PORTRAIT_1D[1], RECT_PORTRAIT_1D[2], RECT_PORTRAIT_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_128,    RECT_PORTRAIT_1D[0], RECT_PORTRAIT_1D[1], RECT_PORTRAIT_1D[2], RECT_PORTRAIT_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_AZTEC,  RECT_PORTRAIT_2D[0], RECT_PORTRAIT_2D[1], RECT_PORTRAIT_2D[2], RECT_PORTRAIT_2D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_DM,     RECT_PORTRAIT_2D[0], RECT_PORTRAIT_2D[1], RECT_PORTRAIT_2D[2], RECT_PORTRAIT_2D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_EANUPC, RECT_PORTRAIT_1D[0], RECT_PORTRAIT_1D[1], RECT_PORTRAIT_1D[2], RECT_PORTRAIT_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_PDF,    RECT_PORTRAIT_1D[0], RECT_PORTRAIT_1D[1], RECT_PORTRAIT_1D[2], RECT_PORTRAIT_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_QR,     RECT_PORTRAIT_2D[0], RECT_PORTRAIT_2D[1], RECT_PORTRAIT_2D[2], RECT_PORTRAIT_2D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_RSS,    RECT_PORTRAIT_1D[0], RECT_PORTRAIT_1D[1], RECT_PORTRAIT_1D[2], RECT_PORTRAIT_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_CODABAR,RECT_PORTRAIT_1D[0], RECT_PORTRAIT_1D[1], RECT_PORTRAIT_1D[2], RECT_PORTRAIT_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_DOTCODE,RECT_DOTCODE[0], RECT_DOTCODE[1], RECT_DOTCODE[2], RECT_DOTCODE[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_11,     RECT_PORTRAIT_1D[0], RECT_PORTRAIT_1D[1], RECT_PORTRAIT_1D[2], RECT_PORTRAIT_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_MSI,    RECT_PORTRAIT_1D[0], RECT_PORTRAIT_1D[1], RECT_PORTRAIT_1D[2], RECT_PORTRAIT_1D[3]);
        
        // or like this for LANDSCAPE scanning - Preferred for dense or wide codes...
        // MWB_setDirection(MWB_SCANDIRECTION_HORIZONTAL);
        // set the scanning rectangle based on scan direction(format in pct: x, y, width, height)
        // MWB_setScanningRect(MWB_CODE_MASK_25,     RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_39,     RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_93,     RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_128,    RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_AZTEC,  RECT_LANDSCAPE_2D[0], RECT_LANDSCAPE_2D[1], RECT_LANDSCAPE_2D[2], RECT_LANDSCAPE_2D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_DM,     RECT_LANDSCAPE_2D[0], RECT_LANDSCAPE_2D[1], RECT_LANDSCAPE_2D[2], RECT_LANDSCAPE_2D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_EANUPC, RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3]);
        MWB_setScanningRect(MWB_CODE_MASK_PDF,    RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_QR,     RECT_LANDSCAPE_2D[0], RECT_LANDSCAPE_2D[1], RECT_LANDSCAPE_2D[2], RECT_LANDSCAPE_2D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_RSS,    RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_CODABAR,RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_DOTCODE,RECT_DOTCODE[0], RECT_DOTCODE[1], RECT_DOTCODE[2], RECT_DOTCODE[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_11,     RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3]);
        // MWB_setScanningRect(MWB_CODE_MASK_MSI,    RECT_LANDSCAPE_1D[0], RECT_LANDSCAPE_1D[1], RECT_LANDSCAPE_1D[2], RECT_LANDSCAPE_1D[3]);
        
        
        // set decoder effort level (1 - 5)
        // for live scanning scenarios, a setting between 1 to 3 will suffice
        // levels 4 and 5 are typically reserved for batch scanning
        MWB_setLevel(3)
        
        
        //Set minimum result length for low-protected barcode types
        MWB_setMinLength(MWB_CODE_MASK_25, 5);
        MWB_setMinLength(MWB_CODE_MASK_MSI, 5);
        MWB_setMinLength(MWB_CODE_MASK_39, 5);
        MWB_setMinLength(MWB_CODE_MASK_CODABAR, 5);
        MWB_setMinLength(MWB_CODE_MASK_11, 5);
        
        MWB_setResultType(MWB_RESULT_TYPE_MW)
        //get and print Library version
        let ver = MWB_getLibVersion()
        let v1 = (ver >> 16)
        let v2 = (ver >> 8) & 0xff
        let v3 = (ver & 0xff)
        print(NSString(format:"Lib version: %d.%d.%d", v1, v2, v3))
        
        
    }
    
    func deinitCapture() {
        if (self.focusTimer != nil){
            self.focusTimer?.invalidate();
            self.focusTimer = nil;
        }
        
        if (self.captureSession != nil){
            if USE_MWOVERLAY{
                MWOverlay.removeFromPreviewLayer();
            }
            
            
            self.captureSession=nil;
            
            self.prevLayer.removeFromSuperlayer();
            self.prevLayer = nil;
        }
    }
    
    func initCapture(){
        
        self.device = AVCaptureDevice.defaultDeviceWithMediaType(AVMediaTypeVideo);
        
        //if you want to use front cammera
        /*  let devices = AVCaptureDevice.devices()
        
        
        for device in devices {
        if (device.hasMediaType(AVMediaTypeVideo)) {
        if(device.position == AVCaptureDevicePosition.Front) {
        self.device = device as? AVCaptureDevice
        }
        }
        }*/
        
        
        do {
            
            let captureImput:AVCaptureDeviceInput = try AVCaptureDeviceInput(device: self.device)
            let captureOutput:AVCaptureVideoDataOutput = AVCaptureVideoDataOutput();
            captureOutput.alwaysDiscardsLateVideoFrames = true;
            captureOutput.setSampleBufferDelegate(self, queue: dispatch_get_main_queue());
            
            captureOutput.videoSettings = [kCVPixelBufferPixelFormatTypeKey: Int(kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange)]
            
            self.captureSession = AVCaptureSession();
            
            self.captureSession.addInput(captureImput);
            self.captureSession.addOutput(captureOutput);
            
            
        }catch _{}
        var resX:Int32 = 640
        var resY:Int32 = 480
        
        if self.captureSession.canSetSessionPreset(AVCaptureSessionPreset1280x720){
            print("Set preview port to 1280X720");
            self.captureSession.sessionPreset = AVCaptureSessionPreset1280x720;
            
            resX = 1280
            resY = 720
            
        } else {
            if self.captureSession.canSetSessionPreset(AVCaptureSessionPreset640x480){
                print("Set preview port to 640x480");
                self.captureSession.sessionPreset = AVCaptureSessionPreset640x480;
            }
        }
        
        //        // Limit camera FPS to 15 for single core devices (iPhone 4 and older) so more CPU power is available for decoder
        
        print("Number of processors \(NSProcessInfo.processInfo().processorCount)")
        
        
        
        if (NSProcessInfo.processInfo().processorCount < 2){
            do{
                try self.device.lockForConfiguration();
                self.device.activeVideoMinFrameDuration = CMTimeMake(1,15)
                self.device .unlockForConfiguration()
                print("activeVideoMinFrameDuration: \(self.device.activeVideoMinFrameDuration)")
            }catch _{}
        }else if USE_60_FPS {
            /*
            for(AVCaptureDeviceFormat *vFormat in [self.device formats] )
            {
            CMFormatDescriptionRef description= vFormat.formatDescription;
            float maxrate=((AVFrameRateRange*)[vFormat.videoSupportedFrameRateRanges objectAtIndex:0]).maxFrameRate;
            float minrate=((AVFrameRateRange*)[vFormat.videoSupportedFrameRateRanges objectAtIndex:0]).minFrameRate;
            CMVideoDimensions dimension = CMVideoFormatDescriptionGetDimensions(description);
            
            if(maxrate>59 && CMFormatDescriptionGetMediaSubType(description)==kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange &&
            dimension.width == resX && dimension.height == resY)
            {
            if ( YES == [self.device lockForConfiguration:NULL] )
            {
            self.device.activeFormat = vFormat;
            [self.device setActiveVideoMinFrameDuration:CMTimeMake(10,minrate * 10)];
            [self.device setActiveVideoMaxFrameDuration:CMTimeMake(10,600)];
            [self.device unlockForConfiguration];
            
            NSLog(@"formats  %@ %@ %@",vFormat.mediaType,vFormat.formatDescription,vFormat.videoSupportedFrameRateRanges);
            //break;
            }
            }
            }*/
            
            
            for vFormat in self.device.formats{
                let description:CMFormatDescription = vFormat.formatDescription
                let rates = (vFormat as! AVCaptureDeviceFormat).videoSupportedFrameRateRanges[0] as! AVFrameRateRange
                
                let maxrate = rates.maxFrameRate
                let minrate = rates.minFrameRate
                
                
                
                
                let dimensions: CMVideoDimensions = CMVideoFormatDescriptionGetDimensions(description);
                if maxrate > 59 && Int32(CMFormatDescriptionGetMediaSubType(description)) == Int32(kCVPixelFormatType_420YpCbCr8BiPlanarVideoRange) && dimensions.width == resX && dimensions.height == resY {
                    do{
                        try self.device.lockForConfiguration()
                        self.device.activeFormat = vFormat as! AVCaptureDeviceFormat
                        self.device.activeVideoMinFrameDuration = CMTimeMake(Int64(10), Int32(minrate * 10))
                        self.device.activeVideoMaxFrameDuration = CMTimeMake(Int64(10), Int32(600))
                        self.device.unlockForConfiguration()
                    }catch _{}
                    
                }
                
            }
        }
        
        
        availableThreads = min(MAX_THREADS, NSProcessInfo.processInfo().processorCount)
        activeThreads = 0
        
        self.prevLayer = AVCaptureVideoPreviewLayer(session: self.captureSession)
        
        if UIApplication.sharedApplication().statusBarOrientation == UIInterfaceOrientation.LandscapeLeft{
            self.prevLayer.connection.videoOrientation = AVCaptureVideoOrientation.LandscapeLeft;
            self.prevLayer.frame = self.view.bounds //CGRectMake(0, 0, max(self.view.frame.size.width,self.view.frame.size.height), min(self.view.frame.size.width,self.view.frame.size.height));
        }
        
        if UIApplication.sharedApplication().statusBarOrientation == UIInterfaceOrientation.LandscapeRight{
            self.prevLayer.connection.videoOrientation = AVCaptureVideoOrientation.LandscapeRight;
            self.prevLayer.frame = CGRectMake(0, 0, max(self.view.frame.size.width,self.view.frame.size.height), min(self.view.frame.size.width,self.view.frame.size.height));
        }
        
        if UIApplication.sharedApplication().statusBarOrientation == UIInterfaceOrientation.Portrait{
            self.prevLayer.connection.videoOrientation = AVCaptureVideoOrientation.Portrait;
            self.prevLayer.frame = CGRectMake(0, 0, min(self.view.frame.size.width,self.view.frame.size.height), max(self.view.frame.size.width,self.view.frame.size.height));
        }
        
        if UIApplication.sharedApplication().statusBarOrientation == UIInterfaceOrientation.PortraitUpsideDown{
            self.prevLayer.connection.videoOrientation = AVCaptureVideoOrientation.PortraitUpsideDown;
            self.prevLayer.frame = CGRectMake(0, 0, min(self.view.frame.size.width,self.view.frame.size.height), max(self.view.frame.size.width,self.view.frame.size.height));
        }
        
        
        self.prevLayer.videoGravity = AVLayerVideoGravityResizeAspectFill;
        
        self.view.layer.addSublayer(self.prevLayer);
        self.view.bringSubviewToFront(viewVideoHelpControls)
        
        if USE_MWOVERLAY {
            MWOverlay.addToPreviewLayer(self.prevLayer);
        }
        
        
        videoZoomSupported = false
        
        
        let maxZoom = self.device.activeFormat.videoZoomFactorUpscaleThreshold
        let maxZoomTotal = self.device.activeFormat.videoMaxZoomFactor
        
        
        if maxZoomTotal > 1.1 {
            videoZoomSupported = true
            if param_ZoomLevel1 != 0 && param_ZoomLevel2 != 0{
                
                if CGFloat(param_ZoomLevel1) > maxZoomTotal * 100 {
                    param_ZoomLevel1 = Int(maxZoomTotal * 100)
                }
                if CGFloat(param_ZoomLevel2) > maxZoomTotal * 100 {
                    param_ZoomLevel2 = Int(maxZoomTotal * 100);
                }
                
                firstZoom = 0.01 * Double(param_ZoomLevel1);
                secondZoom = 0.01 * Double(param_ZoomLevel2);
                
                
            } else {
                
                if maxZoomTotal > 2{
                    
                    if (maxZoom > 1.0 && maxZoom <= 2.0){
                        firstZoom = Double(maxZoom);
                        secondZoom = Double(maxZoom) * 2;
                    } else
                        if (maxZoom > 2.0){
                            firstZoom = 2.0;
                            secondZoom = 4.0;
                    }
                    
                }
            }
            
            
        }
        
        self.focusTimer = NSTimer.scheduledTimerWithTimeInterval(2.0, target: self, selector: "reFocus", userInfo: nil, repeats: true);
        
        
    }
    
    
    func captureOutput(captureOutput: AVCaptureOutput!, didOutputSampleBuffer sampleBuffer: CMSampleBuffer!, fromConnection connection: AVCaptureConnection!)
        
    {
        //        totalFrames++
        if state != camera_state.CAMERA && state != camera_state.CAMERA_DECODING {
            return;
        }
        
        if self.activeThreads >= self.availableThreads {
            return;
        }
        
        if self.state != camera_state.CAMERA_DECODING
        {
            self.state = camera_state.CAMERA_DECODING;
        }
        
        self.activeThreads = self.activeThreads+1
        // print("active threads: \(self.activeThreads) / \(self.availableThreads)")
        
        //  var imageBuffer : COpaquePointer = CMSampleBufferGetImageBuffer(sampleBuffer).toOpaque()
        //  var pixelBuffer : CVPixelBuffer = (Unmanaged<CVPixelBuffer>.fromOpaque(imageBuffer)).takeUnretainedValue()
        
        
        
        
        let pixelBuffer = CMSampleBufferGetImageBuffer(sampleBuffer);
        
        CVPixelBufferLockBaseAddress(pixelBuffer!,0);
        
        //Get information about the image
        let baseAddress = CVPixelBufferGetBaseAddressOfPlane(pixelBuffer!,0);
        //        var pixelFormat:OSType = CVPixelBufferGetPixelFormatType(pixelBuffer!);
        
        //        var pixelFormatInt:UInt32 = pixelFormat.bigEndian;
        
        let bytesPerRow = CVPixelBufferGetBytesPerRowOfPlane(pixelBuffer!,0);
        let width:Int32 = Int32(bytesPerRow);//CVPixelBufferGetWidthOfPlane(imageBuffer,0);
        let height:Int32 = Int32(CVPixelBufferGetHeightOfPlane(pixelBuffer!,0));
        
        let frameBuffer  = UnsafeMutablePointer<UInt8>.alloc(Int(width * height));
        frameBuffer.initializeFrom(UnsafeMutablePointer<UInt8>(baseAddress), count: Int(width * height));
        
        
        CVPixelBufferUnlockBaseAddress(pixelBuffer!, 0);
        
        dispatch_async(dispatch_get_global_queue(DISPATCH_QUEUE_PRIORITY_DEFAULT, 0)) {
            
            var pResult = UnsafeMutablePointer<UInt8>.alloc(8);
            var resLength:Int32 = 0
            
            resLength = MWB_scanGrayscaleImage(frameBuffer,width,height, &pResult);
            
            
            
            
            frameBuffer.dealloc(Int(width * height));
            
            var mwResults:MWResults! = nil
            var mwResult:MWResult! = nil;
            if resLength > 0 {
                if self.state == camera_state.NORMAL {
                    resLength = 0
                    free(pResult)
                } else {
                    mwResults =  MWResults(buffer: pResult, length: Int(resLength))
                    if mwResults != nil && mwResults.count > 0 {
                        mwResult = mwResults.results.objectAtIndex(0) as! MWResult
                    }
                    free(pResult)
                }
                
            }
            
            
            if (mwResult != nil)
            {
                self.state = camera_state.NORMAL
                /* Analytics */
                /*
                * Send custom tag by replacing "TestTag" with custom String
                */
                /*
                if self.USE_MWANALYTICS {
                MWBAnalytics.getInstance().MWA_sendReport(UnsafeMutablePointer<UInt8> (mwResult.encryptedResult), resultType: mwResult.typeName, tag: "TestTag")
                }*/
                
                self.captureSession.stopRunning()
                
                if self.USE_MWOVERLAY && mwResult.locationPoints != nil  {
                    
                    let locPoints: [CGPoint]! = [mwResult.locationPoints[0] ,mwResult.locationPoints[1], mwResult.locationPoints[2],mwResult.locationPoints[3]]
                    
                    MWOverlay.showLocation(locPoints! , width: mwResult.imageWidth, height: mwResult.imageHeight)
                }
                
                var typeName = mwResult.typeName;
                
                
                if (mwResult.isGS1){
                    
                    typeName = NSString(format:"%@ (GS1)", typeName) as String;
                    
                }
                
                dispatch_async(dispatch_get_main_queue()) {
                    if self.captureSession == nil {
                        return
                    }
                    self.captureSession.stopRunning();
                    
                    var resultString:String = ""
                    /* Parser */
                    /*
                    *   Parser result handler. Edit for custm parser result handling
                    */
                    
                    if self.USE_MWPARSER && self.MWPARSER_MASK != MWP_PARSER_MASK_NONE {
                        var p_output:UnsafeMutablePointer<UInt8> = nil
                        let parserRes =  MWP_getFormattedText(Int32(self.MWPARSER_MASK), UnsafePointer<UInt8>(mwResult.encryptedResult), mwResult.bytesLength, &p_output)
                        if (parserRes >= 0){
                            resultString = NSString(CString: UnsafeMutablePointer<Int8>(p_output), encoding: NSUTF8StringEncoding) as! String
                        } else {
                            var parserMask:String = ""
                            switch (self.MWPARSER_MASK) {
                            case MWP_PARSER_MASK_IUID:
                                parserMask = "IUID";
                                break;
                            case MWP_PARSER_MASK_ISBT:
                                parserMask = "ISBT";
                                break;
                            case MWP_PARSER_MASK_AAMVA:
                                parserMask = "AAMVA";
                                break;
                            case MWP_PARSER_MASK_GS1:
                                parserMask = "GS1";
                                break;
                            default:
                                break;
                            }
                            
                            resultString = "\(mwResult.text)\nNot a valid \(parserMask) formatted barcode"
                        }
                        
                    } else {
                        
                        resultString = mwResult.text
                        /* Parser */
                        
                    }
                    let driverLicense = DriverLicense(driverLicenseString: resultString)
                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
                        self.delegate?.didReceiveDriverLicense(driverLicense)

                    })
                }
            } else {
                self.state = camera_state.CAMERA;
            }
            
            self.activeThreads = self.activeThreads-1
            
        }
        
    }
    
    
    
    func doZoomToggle(){
        zoomLevel = zoomLevel+1;
        if zoomLevel > 2 {
            zoomLevel = 0
        }
        self.updateDigitalZoom()
        
    }
    
    func updateDigitalZoom(){
        if videoZoomSupported {
            
            do {
                try self.device.lockForConfiguration()
                
                switch zoomLevel {
                case 0:
                    self.device.videoZoomFactor = 1
                    break;
                case 1:
                    self.device.videoZoomFactor = CGFloat(firstZoom)
                    break;
                case 2:
                    self.device.videoZoomFactor = CGFloat(secondZoom)
                    break;
                    
                default:
                    break;
                }
                self.device.unlockForConfiguration()
                
            }catch _{
                
            }
            
        }
    }
    
    @IBAction func closeButtonPressed() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
}
