//
//  StringTypesForInput.swift
//  Customer
//
//  Created by Emad Toukan on 2016-03-04.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import Foundation

enum StringTypesForInput {
    case Letters, SpecialCharacters, Numbers, Space, Backspace
    
    /**
     Check if the string is allowed in a range given the max range location
     
     - parameter string:           String that is entered
     - parameter range:            Range of the string
     - parameter maxRangeLocation: Max location allowed for that string
     
     - returns: Bool value if the string is allowed to be entered in that location
     */
    func isAllowed(string: String, range: NSRange, maxRangeLocation: Int) -> Bool {
        return allowedStrings().contains(string.lowercaseString) && allowedStringInRange(string.lowercaseString, range: range) && allowedRangeInMaxRangeLocation(range, maxRangeLocation: maxRangeLocation)
    }
    
    /**
     Allowed strings for each type
     
     - returns: An array of all the valid string values for a type
     */
    private func allowedStrings() -> [String] {
        switch self {
        case .Letters:
            return ["a", "b", "c", "d", "e", "f", "g", "h", "i", "j", "k", "l", "m", "n", "o", "p", "q", "r", "s", "t", "u", "v", "w", "x", "y", "z"]
        case .SpecialCharacters:
            return ["-", "+", "'", "/", ".", "à", "â", "è", "é", "ê", "ë", "ï", "î", "ô", "ù", "ü", "û", "ÿ", "ç", "*", "!", "#", "$", "%", "&", "=", "?", "^", "_", "`", "{", "|", "}", "~"]
        case .Numbers:
            return ["0", "1", "2", "3", "4", "5", "6", "7", "8", "9"]
        case .Space:
            return [" "]
        case .Backspace:
            return [""]
        }
    }
    
    /**
     Check if the string is allowd in range
     
     - parameter string: String that is entered
     - parameter range:  Range of the string
     
     - returns: Bool value if the string is allowed in that location
     */
    private func allowedStringInRange(string: String, range: NSRange) -> Bool {
        
        switch self {
        case .Numbers:
            return range.location == 0 && string == "0" ? false : true
        case .Space:
            return range.location == 0 && string == " " ? false : true
        default:
            return true
        }
    }
    
    /**
     Check if the string is allowed to be in the range with respect to the max location allowed
     
     - parameter range:            Range in which the string is entered in
     - parameter maxRangeLocation: Max range location that is allowed
     
     - returns: Bool value if the range is allowed with respect to the max range location
     */
    private func allowedRangeInMaxRangeLocation(range: NSRange, maxRangeLocation: Int) -> Bool {
        switch self {
        case .Backspace:
            return true
        default:
            return range.location >= maxRangeLocation ? false : true
        }
    }
}
