//
//  IdentificationStatusKey.swift
//  Customer
//
//  Created by Emad Toukan on 2016-02-22.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import Foundation

enum IdentificationStatusKey: Int, CustomerKeys {
    case IdentificationStatus, MinorProfileEstablishedBy, CertifiedCopyOfGuardianshipOnFile, MinorBeASigningAuthority
    
    static var numberOfKeys: Int {
        get {
            return 4
        }
    }
    
    static var parentSectionNumber: Int {
        get {
            return CustomerIdentificationKeys.IdentificationStatus.rawValue
        }
    }
    
    func getKeyName(object: AnyObject) -> String {
        switch self {
        case .IdentificationStatus:
            return NSLocalizedString("customer_identification_status", comment: "Identification Status")
        case .MinorProfileEstablishedBy:
            return NSLocalizedString("minor_profile_establish_by", comment: "Identification Status")
        case .CertifiedCopyOfGuardianshipOnFile:
            return NSLocalizedString("certified_copy_of_guardianship_papers_on_file", comment: "Identification Status")
        case .MinorBeASigningAuthority:
            return NSLocalizedString("will_minor_be_a_signing_authority", comment: "Identification Status")
        }
    }
    
    func getValueFrom(object: AnyObject) -> String {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return ""
        }
        
        switch self {
        case .IdentificationStatus:
            return uPersonProfile.partyIdStatus?.getName() ?? ""
        case .MinorProfileEstablishedBy:
            return uPersonProfile.establishedBy?.getName() ?? ""
        case .CertifiedCopyOfGuardianshipOnFile:
            return uPersonProfile.guardianshipPaperFiled?.localizedDescription ?? ""
        case .MinorBeASigningAuthority:
            return uPersonProfile.minorSigningAuthority?.localizedDescription ?? ""
        }
    }
    
    func shouldDisplay(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
            
        case .IdentificationStatus:
            return true
            
        case .MinorProfileEstablishedBy:
            if let uIdentificationStatus = uPersonProfile.partyIdStatus where uIdentificationStatus == PartyIdStatus.MinorLess12 || uIdentificationStatus == PartyIdStatus.MinorGreaterOrEqual12 {
                return true
            }
            return false
            
        case .CertifiedCopyOfGuardianshipOnFile:
            if let uEstablishedBy = uPersonProfile.establishedBy where uEstablishedBy == IdentificationEstablishedBy.Guardian {
                return true
            }
            return false
            
        case .MinorBeASigningAuthority:
            if let uEstablishedBy = uPersonProfile.establishedBy where uEstablishedBy == IdentificationEstablishedBy.Parent || uEstablishedBy == IdentificationEstablishedBy.Guardian {
                return true
            }
            return false
        }
    }
    
    func isEditable(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
            
        case .IdentificationStatus:
            return true
            
        case .MinorProfileEstablishedBy:
            if let uIdentificationStatus = uPersonProfile.partyIdStatus where uIdentificationStatus == PartyIdStatus.MinorLess12 || uIdentificationStatus == PartyIdStatus.MinorGreaterOrEqual12 {
                return true
            }
            return false
            
        case .CertifiedCopyOfGuardianshipOnFile:
            if let uMinorProfileEstablishedBy = uPersonProfile.establishedBy where uMinorProfileEstablishedBy == IdentificationEstablishedBy.Guardian {
                return true
            }
            return false
            
        case .MinorBeASigningAuthority:
            if let _ = uPersonProfile.establishedBy {
                return true
            }
            return false
        }
    }
    
    func editingType() -> CustomerInfoEditType {
        switch self {
        case .IdentificationStatus:
            return .MasterDataListSelector
        case .MinorProfileEstablishedBy:
            return .MasterDataListSelector
        case .CertifiedCopyOfGuardianshipOnFile:
            return .MasterDataListSelector
        case .MinorBeASigningAuthority:
            return .MasterDataListSelector
        }
    }
    
    func getMasterDataForListSelector(object: AnyObject) -> [String: String]? {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return nil
        }
        
        switch self {
        case .IdentificationStatus:

            // Identification Status based on age
            if let uAge = uPersonProfile.age {
                
                // Minor age < 12 for all countries and provinces
                if uAge < 12 {
                    var data = [String: String]()
                    data[PartyIdStatus.MinorLess12.rawValue] = PartyIdStatus.MinorLess12.getName()
                    return data
                }
                
                // Minor age >= 12 & adult age for Canadian Provinces
                if let uProvinceCode = uPersonProfile.getCustomerAddressWithType(AddressUsageType.CustomerResidence)?.region {
                    if let uCanadianAgeMajority = CanadianProvinceCodes(rawValue: uProvinceCode)?.ageOfMajority() {
                        // Check if the age is beween 12 and Canada's Province Age of Majority
                        if uAge >= 12 && uAge < uCanadianAgeMajority {
                            var data = [String: String]()
                            data[PartyIdStatus.MinorGreaterOrEqual12.rawValue] = PartyIdStatus.MinorGreaterOrEqual12.getName()
                            return data
                        } else if uAge > uCanadianAgeMajority {
                            // If the age is greater than the Canada's Age of Majority
                            var data = [String: String]()
                            data[PartyIdStatus.AdultStandard.rawValue] = PartyIdStatus.AdultStandard.getName()
                            data[PartyIdStatus.AdultNonStandard.rawValue] = PartyIdStatus.AdultNonStandard.getName()
                            return data
                        }
                    }
                }
                
                // For all other countries check if age is between 12 and 18
                if uAge >= 12 && uAge < 18 {
                    var data = [String: String]()
                    data[PartyIdStatus.MinorGreaterOrEqual12.rawValue] = PartyIdStatus.MinorGreaterOrEqual12.getName()
                    return data
                }
                
                // For all other countries check if age is greater than 18
                if uAge > 18 {
                    var data = [String: String]()
                    data[PartyIdStatus.AdultStandard.rawValue] = PartyIdStatus.AdultStandard.getName()
                    data[PartyIdStatus.AdultNonStandard.rawValue] = PartyIdStatus.AdultNonStandard.getName()
                    return data
                }
            }
            
            // If no age was provided, then return the full list
            var data = [String: String]()
            data[PartyIdStatus.MinorLess12.rawValue] = PartyIdStatus.MinorLess12.getName()
            data[PartyIdStatus.MinorGreaterOrEqual12.rawValue] = PartyIdStatus.MinorGreaterOrEqual12.getName()
            data[PartyIdStatus.AdultStandard.rawValue] = PartyIdStatus.AdultStandard.getName()
            data[PartyIdStatus.AdultNonStandard.rawValue] = PartyIdStatus.AdultNonStandard.getName()
            return data
            
        case .MinorProfileEstablishedBy:
            var data = [String: String]()
            data[IdentificationEstablishedBy.Parent.rawValue] = IdentificationEstablishedBy.Parent.getName()
            data[IdentificationEstablishedBy.Guardian.rawValue] = IdentificationEstablishedBy.Guardian.getName()
            // EstablishedBy Minor can only be set to MinorGreaterOrEqual12
            if uPersonProfile.partyIdStatus == PartyIdStatus.MinorGreaterOrEqual12 {
                data[IdentificationEstablishedBy.Minor.rawValue] = IdentificationEstablishedBy.Minor.getName()
            }
            return data
            
        case .CertifiedCopyOfGuardianshipOnFile:
            var data = [String: String]()
            data["1"] = NSLocalizedString("yes", comment: "")
            data["0"] = NSLocalizedString("no", comment: "")
            return data
            
        case .MinorBeASigningAuthority:
            
            // If the profile is established by a minor, then the minor should be signing authroity
            if let uEstablishedBy = uPersonProfile.establishedBy where uEstablishedBy == IdentificationEstablishedBy.Minor {
                var data = [String: String]()
                data["1"] = NSLocalizedString("yes", comment: "")
                return data
            }
            var data = [String: String]()
            data["1"] = NSLocalizedString("yes", comment: "")
            data["0"] = NSLocalizedString("no", comment: "")
            return data
        }
    }
    
    func valueChanged(object: AnyObject, newValue: String, newValue2: String?) -> String {
        setValueTo(object, newValue: newValue, newValue2: newValue2)
        return getValueFrom(object)
    }
    
    func setValueTo(object: AnyObject, newValue: String, newValue2: String?) {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return
        }
        
        switch self {
            
        case .IdentificationStatus:
            uPersonProfile.partyIdStatus = PartyIdStatus(rawValue: newValue)
            
        case .MinorProfileEstablishedBy:
            uPersonProfile.establishedBy = IdentificationEstablishedBy(rawValue: newValue)
            
        case .CertifiedCopyOfGuardianshipOnFile:
            if let intValue = Int(newValue) {
                uPersonProfile.guardianshipPaperFiled = Bool(intValue)
            }
            
        case .MinorBeASigningAuthority:
            if let intValue = Int(newValue) {
                uPersonProfile.minorSigningAuthority = Bool(intValue)
            }
        }
    }
    
    func isValidValueIn(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
        case .IdentificationStatus:
            return uPersonProfile.isValidPartyIdStatus
        case .MinorProfileEstablishedBy:
            return uPersonProfile.isValidEstablishedBy
        case .CertifiedCopyOfGuardianshipOnFile:
            return uPersonProfile.isValidGuardianshipPaperFiled
        case .MinorBeASigningAuthority:
            return uPersonProfile.isValidMinorSigningAuthority
        }
    }
    
    func shouldChangeStringInRange(object: AnyObject, newString: String, range: NSRange) -> Bool {
        switch self {
        case .IdentificationStatus:
            return true
        case .MinorProfileEstablishedBy:
            return true
        case .CertifiedCopyOfGuardianshipOnFile:
            return true
        case .MinorBeASigningAuthority:
            return true
        }
    }
    
    func customizeTextField(object: AnyObject, textField: UITextField, editing: Bool) {
        textField.keyboardType = UIKeyboardType.Default
        textField.returnKeyType = UIReturnKeyType.Done
        textField.enabled = isEditable(object) ? editing : false
        textField.borderStyle = isEditable(object) && editing ? UITextBorderStyle.RoundedRect : UITextBorderStyle.None
        textField.backgroundColor = isEditable(object) && editing ? UIColor.whiteColor() : UIColor.clearColor()
        textField.autocapitalizationType = UITextAutocapitalizationType.Words
        
        switch self {
            
        case .IdentificationStatus:
            textField.placeholder = editing ? NSLocalizedString("select_customer_identification_status", comment: "Customer Identification Status") : ""
        case .MinorProfileEstablishedBy:
            textField.placeholder = editing ? NSLocalizedString("select_minor_profile_established_by", comment: "Customer Identification Status") : ""
        case .CertifiedCopyOfGuardianshipOnFile:
            textField.placeholder = editing ? NSLocalizedString("select_guardianship_paper_filed", comment: "Customer Identification Status") : ""
        case .MinorBeASigningAuthority:
            textField.placeholder = editing ? NSLocalizedString("select_minor_signing_authority", comment: "Customer Identification Status") : ""
        }
    }
    
    func reloadRowsAtIndexPathsWithAnimation(object: AnyObject) -> [[String: AnyObject]]? {
        
        var indexPathObjects = [[String: AnyObject]]()
        
        switch self {
        case .IdentificationStatus:
            let indexPath1 = NSIndexPath(forRow: IdentificationStatusKey.MinorProfileEstablishedBy.rawValue, inSection: IdentificationStatusKey.parentSectionNumber)
            let shouldDisplay1 = IdentificationStatusKey.MinorProfileEstablishedBy.shouldDisplay(object)
            let indexPathObject1 = ["indexPath": indexPath1, "shouldDisplay": shouldDisplay1]
            indexPathObjects.append(indexPathObject1)
            
            let indexPath2 = NSIndexPath(forRow: IdentificationStatusKey.CertifiedCopyOfGuardianshipOnFile.rawValue, inSection: IdentificationStatusKey.parentSectionNumber)
            let shouldDisplay2 = IdentificationStatusKey.CertifiedCopyOfGuardianshipOnFile.shouldDisplay(object)
            let indexPathObject2 = ["indexPath": indexPath2, "shouldDisplay": shouldDisplay2]
            indexPathObjects.append(indexPathObject2)
            
            let indexPath3 = NSIndexPath(forRow: IdentificationStatusKey.MinorBeASigningAuthority.rawValue, inSection: IdentificationStatusKey.parentSectionNumber)
            let shouldDisplay3 = IdentificationStatusKey.MinorBeASigningAuthority.shouldDisplay(object)
            let indexPathObject3 = ["indexPath": indexPath3, "shouldDisplay": shouldDisplay3]
            indexPathObjects.append(indexPathObject3)
            
        case .MinorProfileEstablishedBy:
            let indexPath1 = NSIndexPath(forRow: IdentificationStatusKey.CertifiedCopyOfGuardianshipOnFile.rawValue, inSection: IdentificationStatusKey.parentSectionNumber)
            let shouldDisplay1 = IdentificationStatusKey.CertifiedCopyOfGuardianshipOnFile.shouldDisplay(object)
            let indexPathObject1 = ["indexPath": indexPath1, "shouldDisplay": shouldDisplay1]
            indexPathObjects.append(indexPathObject1)
            
            let indexPath2 = NSIndexPath(forRow: IdentificationStatusKey.MinorBeASigningAuthority.rawValue, inSection: IdentificationStatusKey.parentSectionNumber)
            let shouldDisplay2 = IdentificationStatusKey.MinorBeASigningAuthority.shouldDisplay(object)
            let indexPathObject2 = ["indexPath": indexPath2, "shouldDisplay": shouldDisplay2]
            indexPathObjects.append(indexPathObject2)
            
        case .CertifiedCopyOfGuardianshipOnFile:
            break
        case .MinorBeASigningAuthority:
            break
        }
        
        return indexPathObjects.isEmpty ? nil : indexPathObjects
    }
}