//
//  AccountTransactionHistory.swift
//  Customer
//
//  Created by Annie Lo on 2016-02-22.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import Foundation

struct AccountTransactionHistory {

    // MARK: - JSON Keys
    static let kTransactionIdKey = "transactionId"
    static let kCrDrIndKey = "crDrInd"
    static let kTxnAmountKey = "txnAmount"
    static let kLine1DescKey = "line1Desc"
    static let kLine2DescKey = "line2Desc"
    static let kLine3DescKey = "line3Desc"
    static let kTxnDateKey = "txnDate"
    static let kRunningBalanceKey = "runningBalance"
    
    // MARK: - Properties
    let transactionId: Int?
    let crDrInd: String?
    let txnAmount: Double?
    let line1Desc: String?
    let line2Desc: String?
    let line3Desc: String?
    let txnDate: String?
    let runningBalance: Double?
    
    
    init(json: [String: AnyObject]) {
        self.transactionId = json[AccountTransactionHistory.kTransactionIdKey] as? Int
        self.crDrInd = json[AccountTransactionHistory.kCrDrIndKey] as? String
        self.txnAmount = json[AccountTransactionHistory.kTxnAmountKey] as? Double
        self.line1Desc = json[AccountTransactionHistory.kLine1DescKey] as? String
        self.line2Desc = json[AccountTransactionHistory.kLine2DescKey] as? String
        self.line3Desc = json[AccountTransactionHistory.kLine3DescKey] as? String
        self.txnDate = json[AccountTransactionHistory.kTxnDateKey] as? String
        self.runningBalance = json[AccountTransactionHistory.kRunningBalanceKey] as? Double
    }
}
