
//
//  PINManagementUtilities.swift
//  BNSUBDemo
//
//  Created by Annie Lo on 2015-08-18.
//  Copyright (c) 2015 ScotiaBank. All rights reserved.
//

import Foundation

/// Utilities class for PIN management
class PINManagementUtilities {
    
    /**
     Convert a hex string into binary
     
     - parameter string: hex string
     
     - returns: NSData binary object
     */
    static func fromHexString (string: String) -> NSData {
        let data = NSMutableData()
        
        var temp = ""
        
        for char in string.characters {
            temp+=String(char)
            if(temp.characters.count == 2) {
                let scanner = NSScanner(string: temp)
                var value: CUnsignedInt = 0
                scanner.scanHexInt(&value)
                data.appendBytes(&value, length: 1)
                temp = ""
            }
        }
        return data as NSData
    }
    
    /**
     Convert from binary to hex string
     
     - parameter data: NSData binary object
     
     - returns: hex string
     */
    static func toHexString(data: NSData) -> String {
        let hexString = NSMutableString()
        
        let bytes = UnsafeBufferPointer<UInt8>(start: UnsafePointer(data.bytes), count: data.length)
        for byte in bytes {
            hexString.appendFormat("%02hhx", byte)
        }
        
        return hexString.copy() as! String
    }
    
    /**
     Convert base64 to hex string
     
     - parameter base64: string in base64
     
     - returns: string in hex
     */
    static func base64ToHexString(base64: String) -> String {
        let data = PINManagementUtilities.base64ToData(base64)
        if (data != nil) {
            return PINManagementUtilities.toHexString(data!)
        } else {
            return ""
        }
    }
    
    /**
     Convert NSData binary to base64 string
     
     - parameter dataValue: data value in binary
     
     - returns: string in base64
     */
    static func dataToBase64(dataValue: NSData) -> String? {
        return dataValue.base64EncodedStringWithOptions(NSDataBase64EncodingOptions(rawValue: 0))
    }
    
    /**
     Convert base64 string to NSData data value
     
     - parameter base64Encoded: string in base64 encoding
     
     - returns: data value in binary
     */
    static func base64ToData(base64Encoded: String) -> NSData? {
        return NSData(base64EncodedString: base64Encoded, options: NSDataBase64DecodingOptions(rawValue: 0))
    }
    
    
    /**
     Convert base64 to string
     
     - parameter base64Encoded: string in base64 encoding
     
     - returns: decoded string
     */
    static func base64ToString(base64Encoded: String) -> String? {
        if let decodedData = NSData(base64EncodedString: base64Encoded, options:NSDataBase64DecodingOptions(rawValue: 0)) {
            let decodedString = NSString(data: decodedData, encoding: NSUTF8StringEncoding)
            return decodedString as? String
        }
        
        return nil
        
    }
    
    /**
     Convert hex to decimal
     
     - parameter hex: string in hex format
     
     - returns: decimal
     */
    static func hexToDecimal(hex: String) -> UInt8 {
        let result =  UInt8(strtoul(hex, nil, 16))
        return result
    }
    
    /**
     A helper method to truncate a string based on length and trailing string required
     
     - parameter originalString: the original string
     - parameter length:         the length required
     - parameter trailing:       a string to be ammended after the deletion of the substring
     
     - returns: <#return value description#>
     */
    static func truncate(originalString: String, length: Int, trailing: String?) -> String {
        if (length > originalString.characters.count) {
            return originalString
        }
        
        return originalString.substringToIndex(originalString.startIndex.advancedBy(length)) + (trailing ?? "")
    }
    
}