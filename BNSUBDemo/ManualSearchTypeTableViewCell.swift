//
//  ManualSearchTypeTableViewCell.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-06.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class ManualSearchTypeTableViewCell: UITableViewCell {

    @IBOutlet weak var labelManualSearchTypeName: UILabel!
    @IBOutlet weak var imageViewCheckMark: UIImageView!
    
    // MARK: - Cell Delegate Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Remove the selection style
        self.selectionStyle = UITableViewCellSelectionStyle.None
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.imageViewCheckMark.image = selected ? UIImage(named: "CheckmarkBlue") : UIImage()
    }
    
    // MARK: - Class Methods
    func setCellContent(manualSearchTypeName: String) {
        labelManualSearchTypeName.text = manualSearchTypeName
    }

}
