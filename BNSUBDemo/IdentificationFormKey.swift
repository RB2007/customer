//
//  IdentificationKey.swift
//  Customer
//
//  Created by Emad Toukan on 2016-02-19.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import Foundation

enum IdentificationFormKey: Int, CustomerKeys {
    case Type, Description, Number, Province, Country, PlaceOfBirth, NotExpired
    
    static var numberOfKeys: Int {
        get {
            return 7
        }
    }
    
    static var parentSectionNumber: Int {
        get {
            return 0
        }
    }
    
    func getKeyName(object: AnyObject) -> String {
        
        guard let uIdentification = object as? Identification else {
            return ""
        }
        
        switch self {
        case .Type:
            return NSLocalizedString("identification_type", comment: "Identification Form")
        case .Description:
            return NSLocalizedString("identification_description", comment: "Identification Form")
        case .Number:
            return NSLocalizedString("identification_number", comment: "Identification Form")
        case .Province:
            var keyName = NSLocalizedString("issuing_province", comment: "Identification Form")
            if let uCountryOfIssue = uIdentification.countryOfIssue where uCountryOfIssue == CountryCode.UnitedStates.rawValue {
                keyName = NSLocalizedString("issuing_state", comment: "Identification Form")
            }
            return keyName
        case .Country:
            return NSLocalizedString("issuing_country", comment: "Identification Form")
        case .PlaceOfBirth:
            return NSLocalizedString("place_of_birth", comment: "Identification Form")
        case .NotExpired:
            return NSLocalizedString("identification_expired", comment: "Identification Form")
        }
    }
    
    func getValueFrom(object: AnyObject) -> String {
        
        guard let uIdentification = object as? Identification else {
            return ""
        }
        
        switch self {
            
        case .Type:
            return uIdentification.idTp.getName().capitalizedString
        case .Description:
            return uIdentification.idDescription?.capitalizedString ?? ""
        case .Number:
            return uIdentification.idNumber?.uppercaseString ?? ""
        case .Province:
            if let uProvinceOfIssue = uIdentification.provinceOfIssue {
                return CustomerMasterData.getValueForKey(uProvinceOfIssue, customerMasterDataType: .ProvinceOfIssueForIdentification)
            }
            return ""
        case .Country:
            if let uCountryOfIssue = uIdentification.countryOfIssue {
                return CustomerMasterData.getValueForKey(uCountryOfIssue, customerMasterDataType: .CountryOfIssueForIdentification)
            }
            return ""
        case .PlaceOfBirth:
            if let uPlaceOfBirth = uIdentification.placeOfBirth {
                return CustomerMasterData.getValueForKey(uPlaceOfBirth, customerMasterDataType: .CountryPlaceOfBirthForIdentification)
            }
            return ""
        case.NotExpired:
            if let uNotExpired = uIdentification.notExpired {
                return (!uNotExpired).localizedDescription
            }
            return ""
        }
    }
    
    func shouldDisplay(object: AnyObject) -> Bool {
        guard let uIdentification = object as? Identification else {
            return false
        }
        
        switch self {
            
        case .Type:
            return true
        case .Description:
            return uIdentification.idTp.isDescriptionApplicable() 
        case .Number:
            return uIdentification.idTp.isIdNumberApplicable()
        case .Province:
            if let uCountryCode = uIdentification.countryOfIssue where uCountryCode == CountryCode.Canada.rawValue || uCountryCode == CountryCode.UnitedStates.rawValue {
                return true
            }
            return false
        case .Country:
            return uIdentification.idTp.isCountryOfIssueApplicable()
        case .PlaceOfBirth:
            return uIdentification.idTp.isPlaceOfBirthApplicable()
        case .NotExpired:
            return uIdentification.idTp.isIdNotExpiredApplicable()
        }
    }
    
    func isEditable(object: AnyObject) -> Bool {
        guard let uIdentification = object as? Identification else {
            return false
        }
        
        switch self {
            
        case .Type:
            return uIdentification.json == nil
        case .Description:
            return uIdentification.idTp.isDescriptionApplicable()
        case .Number:
            return uIdentification.idTp.isIdNumberApplicable()
        case .Province:
            if let _ = uIdentification.idTp.provinceOfIssueDefaultValue() {
                return false
            }
            return true
        case .Country:
            if let _ = uIdentification.idTp.countryOfIssueDefaultValue() {
                return false
            }
            return true
        case .PlaceOfBirth:
            return uIdentification.idTp.isPlaceOfBirthApplicable()
        case .NotExpired:
            return uIdentification.idTp.isIdNotExpiredApplicable()
        }
    }
    
    func editingType() -> CustomerInfoEditType {
        
        switch self {
            
        case .Type:
            return .MasterDataListSelector
        case .Description:
            return .Keyboard
        case .Number:
            return .Keyboard
        case .Province:
            return .MasterDataListSelector
        case .Country:
            return .MasterDataListSelector
        case .PlaceOfBirth:
            return .MasterDataListSelector
        case .NotExpired:
            return .MasterDataListSelector
        }
    }
    
    func getMasterDataForListSelector(object: AnyObject) -> [String: String]? {
        guard let uIdentification = object as? Identification else {
            return nil
        }
        
        switch self {
            
        case .Type:
            return uIdentification.getApplicableIdentificationTypesMasterData()
        case .Description:
            return nil
        case .Number:
            return nil
        case .Province:
            if let uCountryOfIssue = uIdentification.countryOfIssue {
                if let uCountryCode = CountryCode(rawValue: uCountryOfIssue) {
                    return uIdentification.idTp.getProvinceOfIssueMasterData(uCountryCode)
                }
            }
            return nil
        case .Country:
            return uIdentification.idTp.getCountryOfIssueMasterData()
        case .PlaceOfBirth:
            return CustomerMasterData.getDataForType(CustomerMasterDataType.CountryPlaceOfBirthForIdentification)
        case .NotExpired:
            var data = [String: String]()
            data["1"] = NSLocalizedString("yes", comment: "")
            data["0"] = NSLocalizedString("no", comment: "")
            return data
        }
    }
    
    func valueChanged(object: AnyObject, newValue: String, newValue2: String?) -> String {
        setValueTo(object, newValue: newValue, newValue2: newValue2)
        return getValueFrom(object)
    }
    
    func setValueTo(object: AnyObject, newValue: String, newValue2: String?) {
        guard let uIdentification = object as? Identification else {
            return
        }
        
        switch self {
        case .Type:
            if let uIdentificationType = IdentificationType(rawValue: newValue) {
                uIdentification.idTp = uIdentificationType
            }
        case .Description:
            uIdentification.idDescription = newValue
        case .Number:
            uIdentification.idNumber = newValue.uppercaseString
        case .Province:
            uIdentification.provinceOfIssue = newValue
        case .Country:
            uIdentification.countryOfIssue = newValue
        case .PlaceOfBirth:
            uIdentification.placeOfBirth = newValue
        case .NotExpired:
            if let intValue = Int(newValue) {
                uIdentification.notExpired = !Bool(intValue)
            }
        }
    }
    
    func isValidValueIn(object: AnyObject) -> Bool {
        guard let uIdentification = object as? Identification else {
            return false
        }
        
        switch self {
        case .Type:
            return uIdentification.isValidIdentificationType
        case .Description:
            return uIdentification.isValidIdentificationDescription
        case .Number:
            return uIdentification.isValidIdentificationNumber
        case .Province:
            return uIdentification.isValidProvinceOfIssue
        case .Country:
            return uIdentification.isValidCountryOfIssue
        case .PlaceOfBirth:
            return uIdentification.isValidPlaceOfBirth
        case .NotExpired:
            return uIdentification.isValidNotExpired
        }
    }
    
    func shouldChangeStringInRange(object: AnyObject, newString: String, range: NSRange) -> Bool {
        guard let uIdentification = object as? Identification else {
            return false
        }
        
        switch self {
        case .Type:
            return true
        case .Description:
            return Identification.isValidIdDescriptionInRange(newString, range: range)
        case .Number:
            return Identification.isValidIdNumberInRange(uIdentification.idTp, aString: newString, range: range)
        case .Province:
            return true
        case .Country:
            return true
        case .PlaceOfBirth:
            return true
        case .NotExpired:
            return true
        }
    }
    
    func customizeTextField(object: AnyObject, textField: UITextField, editing: Bool) {
        
        guard let uIdentification = object as? Identification else {
            return
        }
        
        textField.keyboardType = UIKeyboardType.Default
        textField.returnKeyType = UIReturnKeyType.Done
        textField.enabled = isEditable(object) ? editing : false
        textField.borderStyle = isEditable(object) && editing ? UITextBorderStyle.RoundedRect : UITextBorderStyle.None
        textField.backgroundColor = isEditable(object) && editing ? UIColor.whiteColor() : UIColor.clearColor()
        textField.autocapitalizationType = UITextAutocapitalizationType.Words
        
        switch self {
        case .Type:
            textField.placeholder = NSLocalizedString("select_identification_type", comment: "Identification Form Key")
        case .Description:
            textField.placeholder = NSLocalizedString("enter_identification_description", comment: "Identification Form Key")
        case .Number:
            textField.placeholder = NSLocalizedString("enter_identification_number", comment: "Identification Form Key")
        case .Province:
            var placeHolder =  NSLocalizedString("select_issuing_province", comment: "Identification Form Key")
            if let uCountryOfIssue = uIdentification.countryOfIssue where uCountryOfIssue == CountryCode.UnitedStates.rawValue {
                placeHolder = NSLocalizedString("select_issuing_state", comment: "Identification Form Key")
            }
            textField.placeholder = placeHolder
        case .Country:
            textField.placeholder = NSLocalizedString("select_issuing_country", comment: "Identification Form Key")
        case .PlaceOfBirth:
            textField.placeholder = NSLocalizedString("select_place_of_birth", comment: "Identification Form Key")
        case .NotExpired:
            textField.placeholder = NSLocalizedString("select_id_not_expired", comment: "Identification Form Key")
        }
    }
    
    func reloadRowsAtIndexPathsWithAnimation(object: AnyObject) -> [[String: AnyObject]]? {
        var indexPathObjects = [[String: AnyObject]]()
        
        switch self {
        case .Type:
            // Reload all the other cells
            for index in 1..<IdentificationFormKey.numberOfKeys {
                let indexPath = NSIndexPath(forRow: index, inSection: IdentificationStatusKey.parentSectionNumber)
                let shouldDisplay = IdentificationFormKey(rawValue: index)?.shouldDisplay(object) ?? true
                let indexPathObject1 = ["indexPath": indexPath, "shouldDisplay": shouldDisplay]
                indexPathObjects.append(indexPathObject1)
            }
        case .Description:
            break
        case .Number:
            break
        case .Province:
            break
        case .Country:
            let indexPath1 = NSIndexPath(forRow: IdentificationFormKey.Province.rawValue, inSection: IdentificationStatusKey.parentSectionNumber)
            let shouldDisplay1 = IdentificationFormKey.Province.shouldDisplay(object)
            let indexPathObject1 = ["indexPath": indexPath1, "shouldDisplay": shouldDisplay1]
            indexPathObjects.append(indexPathObject1)
        case .PlaceOfBirth:
            break
        case .NotExpired:
            break
        }
        
        return indexPathObjects.isEmpty ? nil : indexPathObjects
    }
}