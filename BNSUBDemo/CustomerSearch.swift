//
//  CustomerSearch.swift
//  Customer
//
//  Created by Emad Toukan on 2015-12-07.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

enum CustomerAccountSensitivityCode: String {
    case DomicileBranch, AllBranches
}

class CustomerSearch {

    // MARK: - JSON Keys
    static let kCidKey = "Cid"
    static let kNameLine1Key = "NameLine1"
    static let kAddressLineKey = "AddressLine"
    static let kSensitivityCodeKey = "SensitivityCode"
    
    // MARK: - Properties
    let cid: String?
    let nameLine1: String?
    let addressLine: String?
    let sensitivityCode: CustomerAccountSensitivityCode?
    let pinned: Bool
    
    // MARK: - Init
    init?(json: [String: AnyObject], pinned: Bool) {
        
        self.cid = json[CustomerSearch.kCidKey] as? String
        self.nameLine1 = json[CustomerSearch.kNameLine1Key] as? String
        self.addressLine = json[CustomerSearch.kAddressLineKey] as? String

        if let tempSensitivityCode = json[CustomerSearch.kSensitivityCodeKey] as? String {
            self.sensitivityCode = CustomerAccountSensitivityCode(rawValue: tempSensitivityCode)
        } else {
            self.sensitivityCode = nil
        }
        
        self.pinned = pinned
        
        // Return nil if the cid is not provided
        if self.cid == nil {
            return nil
        }
    }
}