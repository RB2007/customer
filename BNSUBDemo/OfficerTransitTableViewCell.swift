//
//  OfficerTransitTableViewCell.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-03.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class OfficerTransitTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var viewLeftSelectionIndicator: UIView!
    @IBOutlet weak var labelBranchName: UILabel!
    @IBOutlet weak var labelBranchStreetAddress: UILabel!
    @IBOutlet weak var labelBranchCityProvincePostalCode: UILabel!
    @IBOutlet weak var labelTransitNumber: UILabel!
    @IBOutlet weak var labelTransitKey: UILabel!
    
    // MARK: - UITableView Methods
    
    override func awakeFromNib() {
        super.awakeFromNib()
        // Remove the selection style
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
        // Localization
        labelTransitKey.text = NSLocalizedString("transit_number_key", comment: "Officer Transit Table View Cell")
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        self.viewLeftSelectionIndicator.backgroundColor = selected ? Constants.UBColours.scotiaRed : UIColor.clearColor()
    }
    
    // MARK: - Custom Methods
    
    func setCellContent(row: Int, officerRole: OfficerRole) {
        contentView.backgroundColor = row % 2 == 0 ? Constants.UBColours.scotiaGrey4 : UIColor.whiteColor()
        
        if let branchName = officerRole.transit?.name {
            labelBranchName.text = branchName.additionalSpacesRemoved.capitalizedString
        }
        
        if let branchAddress = officerRole.transit?.address {
            labelBranchStreetAddress.text = branchAddress.additionalSpacesRemoved.capitalizedString
        }
        
        if let branchCity = officerRole.transit?.city {
            labelBranchCityProvincePostalCode.text = branchCity.additionalSpacesRemoved.capitalizedString
        }
        
        if let branchPostalCode = officerRole.transit?.postalCode {
            if let currentText = labelBranchCityProvincePostalCode.text {
                labelBranchCityProvincePostalCode.text = currentText + ", " + branchPostalCode.additionalSpacesRemoved.capitalizedString
            } else {
                labelBranchCityProvincePostalCode.text = branchPostalCode.additionalSpacesRemoved.capitalizedString
            }
        }
        
        if let branchTransit = officerRole.transit?.id {
            labelTransitNumber.text = branchTransit.additionalSpacesRemoved.capitalizedString
        }
        
    }
}
