//
//  SplunkConstants.swift
//  Customer
//
//  Created by Emad Toukan on 2016-02-02.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import Foundation

struct SplunkConstants {
    
    // Param Keys
    static let kErrorTitle = "errorTitle"
    static let kErrorMessage = "errorMessage"
    
    // Selecting Transit
    static let kTransitSelectedKey = "Transit Selected"
    
    // Customer Profile
    static let kCustomerPersonalProfileRetreivedKey = "Customer Personal Profile Retreived"
    static let kCustomerBusinessProfileRetreivedKey = "Customer Business Profile Retreived"
    static let kCustomerProfileDisplayedKey = "Customer Profile Displayed"
    
    // API
    static let kApiInitiateNetworkingErrorKey = "API Initiate Networking Error"
    static let kApiNetworkingErrorKey = "API Networking Error"
    
    // Create Params
    static func params() -> [String: String] {
        return Officer.officerTransitParams
    }
    
    static func params(error: NSError?) -> [String: String] {
        var params = SplunkConstants.params()
        if let uError = error {
            let errorMessage = Util.createErrorMessage(uError)
            params[SplunkConstants.kErrorTitle] = errorMessage.title
            params[SplunkConstants.kErrorMessage] = errorMessage.message
        }
        return params
    }
}