//
//  LaunchViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-02.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit
import SVProgressHUD

class LaunchViewController: UIViewController {
    
    // MARK: - Properties
    @IBOutlet weak var labelWaiting: UILabel!
    @IBOutlet weak var activityIndicator: UIActivityIndicatorView!
    @IBOutlet weak var constraintScotiabankLogoVertical: NSLayoutConstraint!
    @IBOutlet weak var imageViewScotiabankLogo: UIImageView!
    
    // MARK: - View Delegate Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize view
        labelWaiting.text = NSLocalizedString("launch_waiting_message", comment: "Launch View Controller")
        labelWaiting.alpha = 0
        let scotiaBank = NSLocalizedString("scotia_bank", comment: "")
        imageViewScotiabankLogo.image = UIImage(named: scotiaBank + " Logo")
        self.navigationController?.navigationBar.hidden = true
    }
    
    override func viewDidAppear(animated: Bool) {
        super.viewDidAppear(animated)
        
        // Animate the scotiabank logo up
        self.constraintScotiabankLogoVertical.constant = -100
        UIView.animateWithDuration(1, delay: 0.5, usingSpringWithDamping: 2, initialSpringVelocity: 1, options: UIViewAnimationOptions.TransitionNone, animations: { () -> Void in
            // Force layout redraw
            self.view.layoutIfNeeded()
            
            }) { (success:Bool) -> Void in
                
                UIView.animateWithDuration(1, animations: { () -> Void in
                    // Fade in the waiting label
                    self.labelWaiting.alpha = 1
                    
                    }, completion: { (success:Bool) -> Void in
                        let appDelegate = UIApplication.sharedApplication().delegate as! AppDelegate
                        if appDelegate.hasConnection {
                            self.initiateNetworking()
                        } else {
                            let error = APIManagement.sharedInstance.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.NOInternetError.rawValue, errorDescription: CXErrorCode.NOInternetError.localizedDescription())
                            let alertController = AlertControllerUtil.createOKAlertController(error, handler: { (action: UIAlertAction) -> () in
                                    self.performSegueWithIdentifier("showAccessDeniedVC", sender: nil)
                            })
                            self.presentViewController(alertController, animated: true, completion: nil)
                        }
                })
        }
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }
    
    // Mark: - Instance Methods
    func initiateNetworking() {
        // Start the activity indicator
        self.activityIndicator.startAnimating()

        APIManagement.sharedInstance.initiateNetworking({ (data: AnyObject?, error: NSError?) -> () in
            self.activityIndicator.stopAnimating()
            if let uError = error {
                let alertController = AlertControllerUtil.createOKAlertController(uError, handler: { (action: UIAlertAction) -> () in
                    self.performSegueWithIdentifier("showAccessDeniedVC", sender: nil)
                })
                self.presentViewController(alertController, animated: true, completion: nil)
            } else {
                if let officerRoles = Officer.officerRoles {
                    if officerRoles.count == 1 {
                        
                        // Set the selected branch on the user object
                        Officer.currentSelectedRole = officerRoles.first
                        
                        // Go to the customer lookup view controller
                        let storyBoard = UIStoryboard(name: "CustomerLookup", bundle: NSBundle.mainBundle())
                        if let viewController = storyBoard.instantiateInitialViewController() {
                            self.navigationController?.pushViewController(viewController, animated: true)
                        }
                    } else {
                        // Go to the select transit view controller
                        self.performSegueWithIdentifier("showSelectTransitVC", sender: nil)
                    }
                    return
                }
                self.performSegueWithIdentifier("showAccessDeniedVC", sender: nil)
            }
        })
    }
}
