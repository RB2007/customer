//
//  Address.swift
//  BNSUBDemo
//
//  Created by Laura Reategui on 2015-10-01.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

enum AddressUsageType: String {
    case CustomerResidence = "CR", // Personal Party
    BusinessAddress = "BA", //  Personal Party && Non-Personal Party
    BusinessEmployerAddress = "EmplAddr", // Personal Party
    BusinessMailingAddress = "BusMail", // Non-Personal Party
    Unknown = "Unknown"
}

class Address {
    
    // MARK: - JSON Keys
    static let kUsageTpKey = "UsageTp"
    static let kLine1Key = "Line1"
    static let kLine2Key = "Line2"
    static let kCityOrLine3Key = "CityOrLine3"
    static let kRegionKey = "Region"
    static let kPostalCodeKey = "PostalCode"
    static let kCountryKey = "Country"
    
    // MARK: - Constants
    static let kLine1MaxCharacterLimit = 30
    static let kLine2MaxCharacterLimit = 30
    static let kCityOrLine3MaxCharacterLimit = 18
    static let kPostalCodeCanadaMaxCharacterLimit = 6
    static let kPostalCodeUS1MaxCharacterLimit = 5
    static let kPostalCodeUS2MaxCharacterLimit = 9
    static let kPostalCodeOthersMaxCharacterLimit = 9
    
    // MARK: - Properties
    let json: [String: AnyObject]?
    let usageTp: AddressUsageType
    var line1: String?
    var line2: String?
    var cityOrLine3: String?
    var region: String?
    var postalCode: String?
    var country: String? {
        willSet {
            if country != newValue {
                region = nil
                postalCode = nil
            }
        }
    }
    
    // MARK: - Init
    init?(json: [String: AnyObject]) {
        self.json = json
        self.line1 = json[Address.kLine1Key] as? String
        self.line2 = json[Address.kLine2Key] as? String
        self.cityOrLine3 = json[Address.kCityOrLine3Key] as? String
        self.region = json[Address.kRegionKey] as? String
        self.postalCode = json[Address.kPostalCodeKey] as? String
        self.country = json[Address.kCountryKey] as? String
        
        // Usage type
        // If there is no usageTp then return nil
        guard let tempUsageTp = json[Address.kUsageTpKey] as? String else {
            self.usageTp = AddressUsageType.Unknown
            return nil
        }
        
        guard let uUsageType =  AddressUsageType(rawValue: tempUsageTp) else {
            self.usageTp = AddressUsageType.Unknown
            return nil
        }
        self.usageTp = uUsageType
    }
    
    init(usageTp: AddressUsageType) {
        self.json = nil
        self.usageTp = usageTp
    }
    
    // MARK: - Validation Properties
    var isValidLine1: Bool? {
        return validateAddressLine(line1, maxCharacterCount: Address.kLine1MaxCharacterLimit)
    }
    
    var isValidLine2: Bool? {
        return validateAddressLine(line2, maxCharacterCount: Address.kLine2MaxCharacterLimit)
    }
    
    var isValidCityOrLine3: Bool? {
        return validateAddressLine(cityOrLine3, maxCharacterCount: Address.kCityOrLine3MaxCharacterLimit)
    }
    
    var isValidRegion: Bool? {
        guard let uRegion = region else {
            // Region is mandatory only for Canada and US
            if let uCountry = country where uCountry == CountryCode.Canada.rawValue || uCountry == CountryCode.UnitedStates.rawValue {
                return false
            }
            return nil
        }
        return CustomerMasterData.getKeysFor(CustomerMasterDataType.ProvinceForAddress).contains(uRegion)
    }
    
    var isValidPostalCode: Bool? {
        guard let uPostalCode = postalCode where uPostalCode.isEmpty == false else {
            return nil
        }
        
        // Postal code should not be more than 9 characters in length
        if uPostalCode.characters.count > Address.kPostalCodeOthersMaxCharacterLimit {
            return false
        }
        
        // Check postal code character validity
        for index in 0..<uPostalCode.characters.count {
            let charIndex = uPostalCode.startIndex.advancedBy(index)
            let char = uPostalCode.lowercaseString[charIndex]
            
            // All character should be either letters or numbers
            if !Util.kAllowedLettersForTextInput.contains(String(char)) && !Util.kAllowedNumbersForTextInput.contains(String(char)) {
                return false
            }
            
            if let uCountry = country  {
                
                // Canada postal code should follow A1A1A1 format
                if uCountry == CountryCode.Canada.rawValue {
                    if (index == 0 || index == 2 || index == 4) && !Util.kAllowedLettersForTextInput.contains(String(char)) {
                        return false
                    }
                    
                    if (index == 1 || index == 3 || index == 5) && !Util.kAllowedNumbersForTextInput.contains(String(char)) {
                        return false
                    }
                }
                
                // All characters should be numbers
                if (uCountry == CountryCode.UnitedStates.rawValue && !Util.kAllowedNumbersForTextInput.contains(String(char))) {
                    return false
                }
            }
        }
        
        if let uCountry = country {
            // Canada postal code should be 6 characters long
            if uCountry == CountryCode.Canada.rawValue && uPostalCode.characters.count != Address.kPostalCodeCanadaMaxCharacterLimit {
                return false
            }
            
            // US ZIP code should be either 5 or 9 characters long
            if uCountry == CountryCode.UnitedStates.rawValue && uPostalCode.characters.count != Address.kPostalCodeUS1MaxCharacterLimit && uPostalCode.characters.count != Address.kPostalCodeUS2MaxCharacterLimit {
                return false
            }
        }
        return true
    }
    
    var isValidCountry: Bool? {
        guard let uCountry = country else {
            return nil
        }
        return CustomerMasterData.getKeysFor(CustomerMasterDataType.CountryForAddress).contains(uCountry)
    }
    
    private func validateAddressLine(line: String?, maxCharacterCount: Int) -> Bool? {
        guard let uLine = line where uLine.additionalSpacesRemoved.isEmpty == false else {
            return nil
        }
        
        for char in uLine.lowercaseString.characters where !Util.kAllowedLettersForTextInput.contains(String(char)) && !Util.kAllowedCharacterForTextInput.contains(String(char)) && !Util.kAllowedNumbersForTextInput.contains(String(char)) {
            return false
        }
        
        if uLine.characters.count > maxCharacterCount {
            return false
        }
        
        return true
    }

    // MARK: - Validation in Range
    static func isValidLine1InRange(aString: String, range: NSRange) -> Bool {
        return Address.isValidLineInRange(aString, range: range, maxCharacterCount: Address.kLine1MaxCharacterLimit)
    }
    
    static func isValidLine2InRange(aString: String, range: NSRange) -> Bool {
        return Address.isValidLineInRange(aString, range: range, maxCharacterCount: Address.kLine2MaxCharacterLimit)
    }
    
    static func isValidCityOrLine3InRange(aString: String, range: NSRange) -> Bool {
        return Address.isValidLineInRange(aString, range: range, maxCharacterCount: Address.kCityOrLine3MaxCharacterLimit)
    }
    
    static func isValidPostalCodeInRange(aString: String, range: NSRange, countryCode: String?) -> Bool {
        
        // Check if the string is an allowed character
        if !Util.kAllowedLettersForTextInput.contains(aString.lowercaseString) && !Util.kAllowedNumbersForTextInput.contains(aString) && aString != "" {
            return false
        }
        
        if let uCountryCode = countryCode where uCountryCode == CountryCode.Canada.rawValue || uCountryCode == CountryCode.UnitedStates.rawValue {
            
            // Canada postal code should be 6 characters long
            // Adding 1 for formatting
            // The format should be A1A 1A1
            if uCountryCode == CountryCode.Canada.rawValue && aString != "" {
                if range.location >= Address.kPostalCodeCanadaMaxCharacterLimit + 1  {
                    return false
                }
                
                if (range.location == 0 && !Util.kAllowedLettersForTextInput.contains(aString.lowercaseString)) || (range.location == 1 && !Util.kAllowedNumbersForTextInput.contains(aString)) || (range.location == 2 && !Util.kAllowedLettersForTextInput.contains(aString.lowercaseString)) || (range.location == 3 && !Util.kAllowedNumbersForTextInput.contains(aString)) || (range.location == 5 && !Util.kAllowedLettersForTextInput.contains(aString.lowercaseString)) || (range.location == 6 && !Util.kAllowedNumbersForTextInput.contains(aString)) {
                    return false
                }
            }
            
            // US postal code should be either 5 or 9 characters long
            // Adding 1 for formatting
            // Numbers only
            if uCountryCode == CountryCode.UnitedStates.rawValue && aString != "" {
                if (range.location >= Address.kPostalCodeUS1MaxCharacterLimit && range.location >= Address.kPostalCodeUS2MaxCharacterLimit + 1) || (!Util.kAllowedNumbersForTextInput.contains(aString)) {
                    return false
                }
            }
            
        } else {
            // Postal code should not be more than 9 characters in length
            // Adding 1 for formatting
            if range.location >= Address.kPostalCodeOthersMaxCharacterLimit && aString != "" {
                return false
            }
        }
        return true
    }
    
    private static func isValidLineInRange(aString: String, range: NSRange, maxCharacterCount: Int) -> Bool {
        // Check that the first entry is not a space
        if range.location == 0 && aString == " " {
            return false
        }
        
        if !Util.kAllowedLettersForTextInput.contains(aString.lowercaseString) && !Util.kAllowedCharacterForTextInput.contains(aString.lowercaseString) && !Util.kAllowedNumbersForTextInput.contains(aString) && aString != "" {
            return false
        }
        
        // Check if the string exceeds the allowed string limit
        if range.location >= maxCharacterCount && aString != "" {
            return false
        }
        
        return true
    }
    
    // MARK: - Save
    var paramsForSave: [String: AnyObject]? {
        // If the json is nil, then the object was added
        guard let uJSON = json else {
            return createAddressParams(SaveActions.Added)
        }
        
        // Check if anything changed from the original values
        if let uOriginalAddress = Address(json: uJSON) {
            if self.line1 != uOriginalAddress.line1 || self.line2 != uOriginalAddress.line2 || self.cityOrLine3 != uOriginalAddress.cityOrLine3 || self.region != uOriginalAddress.region || self.postalCode != uOriginalAddress.postalCode || self.country != uOriginalAddress.country {
                return createAddressParams(SaveActions.Changed)
            }
        }
        
        // Return nil if nothing has changed
        return nil
    }
    
    private func createAddressParams(action: SaveActions) -> [String: AnyObject] {
        var params = [String: AnyObject]()
        params[kAction] = action.rawValue
        params[Address.kUsageTpKey.decapitalizedString] = self.usageTp.rawValue // This field should always be availble
        params[Address.kLine1Key.decapitalizedString] = self.line1 ?? ""
        params[Address.kLine2Key.decapitalizedString] = self.line2 ?? ""
        params[Address.kCityOrLine3Key.decapitalizedString] = self.cityOrLine3 ?? ""
        params[Address.kRegionKey.decapitalizedString] = self.region ?? ""
        params[Address.kPostalCodeKey.decapitalizedString] = self.postalCode ?? ""
        params[Address.kCountryKey.decapitalizedString] = self.country ?? "099" // Unkown country code
        return params
    }
    
    
    // MARK: - Utility

    /**
     Format Canadian postal codes
     
     - parameter string: Canadian postal code string
     
     - returns: Formatted postal code string in the format of A1A 1A1
     */
    static func formatCanadianPostalCode(var string: String) -> String {
        string = Address.removeCanadianPostalCodeFormatting(string)
        switch string.characters.count {
        case 0...3:
            return string
        default:
            string.insert(" ", atIndex: string.startIndex.advancedBy(3))
            return string
        }
    }
    
    /**
     Remove Canada postal code formatting
     
     - parameter string: Formatted Canadian postal code string
     
     - returns: Unformatted Canadian postal code
     */
    static func removeCanadianPostalCodeFormatting(string: String) -> String {
        return string.spacesRemoved.uppercaseString
    }
    
    /**
     Format US ZIP Code
     
     - parameter string: US ZIP code string
     
     - returns: Formatted ZIP code string in the format 00000 or 00000-0000
     */
    static func formatUSZipCode(var string: String) -> String {
        string = Address.removeUSZipCodeFormatteing(string)
        switch string.characters.count {
        case 6,7,8,9:
            string.insert("-", atIndex: string.startIndex.advancedBy(5))
            return string
        default:
            return string
        }
    }
    
    /**
     Remove US ZIP code formatting
     
     - parameter string: Formatted US ZIP code string
     
     - returns: Unformatted US ZIP code string
     */
    static func removeUSZipCodeFormatteing(string: String) -> String {
        return string.stringByReplacingOccurrencesOfString("-", withString: "")
    }
}
