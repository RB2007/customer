//
//  PINManagementConstants.swift
//  BNSUBDemo
//
//  Created by Annie Lo on 2015-09-21.
//  Copyright (c) 2015 ScotiaBank. All rights reserved.
//

import Foundation
/**
 * This contains constants used for PIN management flows (eg. test and production CAPK keys used on the pin pads, EMV configurations used on the VeriFone pin pads, sleeping time in between commands to keep pin pads in sync, warning code used for customer session, but they are errors. )
 */
struct PINManagementConstants {
    
    /**
     *  A list of CAPK keys used to load into the VeriFone pin pads.
     */
    struct CAPK {
        
        /// test Visa CAPK key
        static let visaTestCAPK = [
            PINManagementCAPK(rid: "A000000003", pkiIndex: "99", modulus: PINManagementUtilities.fromHexString("AB79FCC9520896967E776E64444E5DCDD6E13611874F3985722520425295EEA4BD0C2781DE7F31CD3D041F565F747306EED62954B17EDABA3A6C5B85A1DE1BEB9A34141AF38FCF8279C9DEA0D5A6710D08DB4124F041945587E20359BAB47B7575AD94262D4B25F264AF33DEDCF28E09615E937DE32EDC03C54445FE7E382777"), exponent: PINManagementUtilities.fromHexString("03"), checksum: PINManagementUtilities.fromHexString("4ABFFD6B1C51212D05552E431C5B17007D2F5E6D"), expiryDate: nil),
            
            PINManagementCAPK(rid: "A000000003", pkiIndex: "95", modulus: PINManagementUtilities.fromHexString("BE9E1FA5E9A803852999C4AB432DB28600DCD9DAB76DFAAA47355A0FE37B1508AC6BF38860D3C6C2E5B12A3CAAF2A7005A7241EBAA7771112C74CF9A0634652FBCA0E5980C54A64761EA101A114E0F0B5572ADD57D010B7C9C887E104CA4EE1272DA66D997B9A90B5A6D624AB6C57E73C8F919000EB5F684898EF8C3DBEFB330C62660BED88EA78E909AFF05F6DA627B"), exponent: PINManagementUtilities.fromHexString("03"), checksum: PINManagementUtilities.fromHexString("EE1511CEC71020A9B90443B37B1D5F6E703030F6"), expiryDate: nil),
            
            PINManagementCAPK(rid: "A000000003", pkiIndex: "92", modulus: PINManagementUtilities.fromHexString("996AF56F569187D09293C14810450ED8EE3357397B18A2458EFAA92DA3B6DF6514EC060195318FD43BE9B8F0CC669E3F844057CBDDF8BDA191BB64473BC8DC9A730DB8F6B4EDE3924186FFD9B8C7735789C23A36BA0B8AF65372EB57EA5D89E7D14E9C7B6B557460F10885DA16AC923F15AF3758F0F03EBD3C5C2C949CBA306DB44E6A2C076C5F67E281D7EF56785DC4D75945E491F01918800A9E2DC66F60080566CE0DAF8D17EAD46AD8E30A247C9F"), exponent: PINManagementUtilities.fromHexString("03"), checksum: PINManagementUtilities.fromHexString("429C954A3859CEF91295F663C963E582ED6EB253"), expiryDate: nil),
            
            PINManagementCAPK(rid: "A000000003", pkiIndex: "94", modulus: PINManagementUtilities.fromHexString("ACD2B12302EE644F3F835ABD1FC7A6F62CCE48FFEC622AA8EF062BEF6FB8BA8BC68BBF6AB5870EED579BC3973E121303D34841A796D6DCBC41DBF9E52C4609795C0CCF7EE86FA1D5CB041071ED2C51D2202F63F1156C58A92D38BC60BDF424E1776E2BC9648078A03B36FB554375FC53D57C73F5160EA59F3AFC5398EC7B67758D65C9BFF7828B6B82D4BE124A416AB7301914311EA462C19F771F31B3B57336000DFF732D3B83DE07052D730354D297BEC72871DCCF0E193F171ABA27EE464C6A97690943D59BDABB2A27EB71CEEBDAFA1176046478FD62FEC452D5CA393296530AA3F41927ADFE434A2DF2AE3054F8840657A26E0FC617"), exponent: PINManagementUtilities.fromHexString("03"), checksum: PINManagementUtilities.fromHexString("C4A3C43CCF87327D136B804160E47D43B60E6E0F"), expiryDate: nil)
        
        ]
        
        /// test Interac CAPK keys
        static let interacTestCAPK = [
            PINManagementCAPK(rid: "A000000277", pkiIndex: "02", modulus: PINManagementUtilities.fromHexString("E0FFBEE77CEE02ADEE8B4B004D26FB46FAF3CE033E3B874D73D099A966A0657497CEE214E95DF2BF5C9D359A3B10C05CFC9929BEF3070036FD19AE661B173E486CBC9B04E4B2E2D0D8209CAF5C200929FA252A21BD69A9A47488844A1DE5BF8729CE5E5E92117047BDDE9FBE72397FD9FC3BE459D8ED06FD6CADF0AFF39C93CC07B312EE2ACB3D4A15E919F57481CFEF"), exponent: PINManagementUtilities.fromHexString("010001"), checksum: PINManagementUtilities.fromHexString("BAF3652A9F5D0FF45EECECA55B4597B1F53BE283"), expiryDate: nil),
            
            PINManagementCAPK(rid: "A000000277", pkiIndex: "03", modulus: PINManagementUtilities.fromHexString("E51505CCECDD0799BAFE097200DF40FFC2154836B8D67F1E99D4D415F73C04A9FAD73825D32AE5BA77FCD02597393CE4D8104B6010D5A8F28F2B47BB8298DAFD63C9C0BEF62AF937265614E31AE2AB45B60B968DB9DEE55602C169F6C16D4579BAA24765560300A3056F894BA8FFA566D9CCD79453D804B97DA14F21F9C3528ED8B8A368A9FACF05C46C13A9BA020618425EF1ACF6CB4DD5DCD050273502114B59EE90EBD833F70C2F324741E79A19A1"), exponent: PINManagementUtilities.fromHexString("010001"), checksum: PINManagementUtilities.fromHexString("0FB60A1BCA38095F3CC578D2DEC95F779840A343"), expiryDate: nil),
            
            PINManagementCAPK(rid: "A000000277", pkiIndex: "07", modulus: PINManagementUtilities.fromHexString("B2CE89726B6B2D1690A0F8CA75CD0B6B1ACDFBE66D53BA00C500BE612FB421423684FF1868554A7722258012E588ED1CD206C67BB2F7598F3CE67F23A888B8ED7421C49A2EBA224DFBD866F62E59338E9F626D0DFD4C9ACC626E42EC0375EE424C93E5DB5773FC98E4AC9FCDEBD0521B15295C797DCABB4679C055AC374A81B245ABF1092A3D46E1CCE10A84F9FC2FC6ED91025121589C9AA68B56B7FBF0D86B4C8E3C9C08406DBB4609BBFC5308F325A8273CB6D36F3E0AF772507BF8A2A9F5A749B5AF98CCEAB2826C2D9C2AB9103B1BAB0DE92099C8072477EC564062AC38184C60DAD298EF352D92DFA041DDE3DB260CE5302B47A7E9"), exponent: PINManagementUtilities.fromHexString("010001"), checksum: PINManagementUtilities.fromHexString("44F2C13373A5068B63C9334E914DDE6AB70CE0F1"), expiryDate: nil),
        ]
        
        
        /// production Visa CAPK keys
        static let visaProductionCAPK = [
            PINManagementCAPK(rid: "A000000003", pkiIndex: "07", modulus: PINManagementUtilities.fromHexString("A89F25A56FA6DA258C8CA8B40427D927B4A1EB4D7EA326BBB12F97DED70AE5E4480FC9C5E8A972177110A1CC318D06D2F8F5C4844AC5FA79A4DC470BB11ED635699C17081B90F1B984F12E92C1C529276D8AF8EC7F28492097D8CD5BECEA16FE4088F6CFAB4A1B42328A1B996F9278B0B7E3311CA5EF856C2F888474B83612A82E4E00D0CD4069A6783140433D50725F"), exponent: PINManagementUtilities.fromHexString("03"), checksum: PINManagementUtilities.fromHexString("B4BC56CC4E88324932CBC643D6898F6FE593B172"), expiryDate: nil),
            
            PINManagementCAPK(rid: "A000000003", pkiIndex: "08", modulus: PINManagementUtilities.fromHexString("D9FD6ED75D51D0E30664BD157023EAA1FFA871E4DA65672B863D255E81E137A51DE4F72BCC9E44ACE12127F87E263D3AF9DD9CF35CA4A7B01E907000BA85D24954C2FCA3074825DDD4C0C8F186CB020F683E02F2DEAD3969133F06F7845166ACEB57CA0FC2603445469811D293BFEFBAFAB57631B3DD91E796BF850A25012F1AE38F05AA5C4D6D03B1DC2E568612785938BBC9B3CD3A910C1DA55A5A9218ACE0F7A21287752682F15832A678D6E1ED0B"), exponent: PINManagementUtilities.fromHexString("03"), checksum: PINManagementUtilities.fromHexString("20D213126955DE205ADC2FD2822BD22DE21CF9A8"), expiryDate: nil),
            
            PINManagementCAPK(rid: "A000000003", pkiIndex: "09", modulus: PINManagementUtilities.fromHexString("9D912248DE0A4E3992C1A4095AFBD18226D2EFD904B4B5499654F8F9B0D2AB5F3912C6576945FAB806B8FE6E3DBA18AFBE03427C9D64F695EA34AD1D76BFCAD121ECEF5D278A96E234B937F1AD999DC5100E857F431A4A5AB59FDF237D6BB1DD6481477C75D95CDD7F330AC5D67BCD7526DBDE971A37CD3E0501EFC6509D7A41C1A7DDE3F6D258894D1BA74847F2BC4954CD189A54C5D1170357EB642FEDA95D97E7062CAA44A4AA6AE3738E30429EE9FA8CAB4BFE3768535908C077FFE6DC556F57359FFAEDA194C41EB11935B44C186BB65114F174C2D70916E644D709DED568254615F7740EC0BF23D28A140826C0F9B8DF644AC38501"), exponent: PINManagementUtilities.fromHexString("03"), checksum: PINManagementUtilities.fromHexString("1FF80A40173F52D7D27E0F26A146A1C8CCB29046"), expiryDate: nil)
            
        ]
        
        /// production Interac CAPK keys
        static let interacProductionCAPK = [
            PINManagementCAPK(rid: "A000000277", pkiIndex: "08", modulus: PINManagementUtilities.fromHexString("AB1B0667B2A68883477B2ED48F3068CB0F57ABBEC93E0AF40180BACD895120E36E2710784599CDE9035550D96BD6C5CEA55C4E8C88A5D0A81CE1309559BCE91930C7AA3E3D0A2D79A6036BE03C4000658A78ECA742034BE5FB0E08D530C7FF9458211E78E33E3803F8DFF24A4117EE0EDFE7A98CB3AE2ECCB2A3C3A75C32512EDD1183CF218BE1642FA78430A18A495E6FEFA7B98860C6FCEBFD27537D34F4E55B9CBDEB19DF029BDF00993E1A2E0B9E89E1B49777FCB7C1610CDA94A488C9177908B75C48DFE3F8BBD52886233B44B1A58373D5AFD0F309ABB939C39DF95D923F76B7300E83D182C2922EBB9FD018867A0E6D179EFF8C87"), exponent: PINManagementUtilities.fromHexString("010001"), checksum: PINManagementUtilities.fromHexString("46EAD2ED0B8645D4DCB2AE4B1D285A0632B452D4"), expiryDate: nil),
            
            PINManagementCAPK(rid: "A000000277", pkiIndex: "05", modulus: PINManagementUtilities.fromHexString("B0DF2D638760AB9279E07CFACC17F5892DA3253D87170044B0D91348BACE68149EDD3EFFE3AA5921DE78E922190F7F5117CBDC321B3216C5B71CC3EE7AB7DF94B999C3CB5D439DBA6AFFCEE40396ADAF60F4821D45F8212C654152C798BFE21C3D7313C6B00122ECED292EC9235BFDB4F754341D28D868AEA516D8D8A78060FEE33CD108BDCF86ABD249163A906D8A85"), exponent: PINManagementUtilities.fromHexString("010001"), checksum: PINManagementUtilities.fromHexString("93FAB321004ADD9A6741C648E473931B8CDF18AF"), expiryDate: nil),
            
            PINManagementCAPK(rid: "A000000277", pkiIndex: "06", modulus: PINManagementUtilities.fromHexString("DB43A71FF30392069A9600143DCB628A4DABDFE69E31CB6151D9A2EB18A53ABA1EF75518CD3EDA29B96D55B002870A649AAFC65CE472BD01352C2D2E77D4EE352B3A64BC2CC170E29D426D7B3317BD3C4FC32EA2151CA0F1071A2ACFECD70468D3EBC7A44440DD63EC9499F302348BB6235F964BF3CAA30B29939B9901C42B5540BF4F837DD898F5392076F9B95F0EBCB6846374FFE71895A422775D95CABA9C25510627D4F7B57A3DBD755608EDD843"), exponent: PINManagementUtilities.fromHexString("010001"), checksum: PINManagementUtilities.fromHexString("D18AB9F1518FBC0F6EB0EEFB00C5D07CAE8A2197"), expiryDate: nil),
        ]
    }
    
    /**
     *  EMV configuration used to configure how E255/E355 VeriFone pin pads work.
     */
    struct EMVConfiguration {
        
        /// EMV configuration for Visa
        static let emvVisaConfig = PINManagementEMVConfig(
            schemeReference: -1,
            issuerReference: -1,
            trmDataPresent: 1,
            targetRSPercent: 20,
            floorLimit: 624.65,
            rsThreshold: 0,
            maxTargetRSPercent: 60,
            merchantForceOnlineFlag: 0,
            blackListCardSupportFlag: 0,
            fallbackAllowedFlag: 1,
            tacDefault: "D840001800",
            tacDenial: "0010000000",
            tacOnline: "D84004F800",
            defaultTDOL: "9F02065F2A029A039C0195059F3704", //a list of tags that we want to be provided
            defaultDDOL: "9F3704",
            autoSelectApplication: 0,
            countryCode: "0124",
            currencyCode: "0124",
            terminalCapacity: "E0B8C8",//"E040C8"//"E0B8C8"
            additionalCapacity: "F000F0F001",
            terminalType: "11",
            merchantCategoryCode: "", //doesn't matter
            terminalCategoryCode: "", //doesn't matter
            terminalID: "12345678",//doesn't matter
            merchantID: "9999999999",//doesn't matter
            acquireID: "112233",//doesn't matter
            capkIndex: "FF",
            pinBypassFlag: "0",
            pinTimeout: "060",
            pinFormat: "0",
            pinScriptNumber: "8",
            pinMacroNumber: "0",
            pinDevriKey: "0",
            pinDevriMacro: "0",
            cardStatusDisplay: "1",
            termCurExp: 0,
            issAcqFlag: 0,
            noDisplaySupportFlag: 0,
            modifyCandidateListFlag: 1,
            rfu1String: "",
            rfu2String: "",
            rfu3String: "",
            vfi_rfu1: 0,
            vfi_rfu2: 0,
            vfi_rfu3: 0
        )
        
        /// EMV configuration for Interac
        static let emvInteracConfig = PINManagementEMVConfig(
            schemeReference: -1,
            issuerReference: -1,
            trmDataPresent: 1,
            targetRSPercent: 20,
            floorLimit: 0,
            rsThreshold: 0,
            maxTargetRSPercent: 60,
            merchantForceOnlineFlag: 0,
            blackListCardSupportFlag: 0,
            fallbackAllowedFlag: 1,
            tacDefault: "FFFFFFFFFF",
            tacDenial: "0000000000",
            tacOnline: "FFFFFFFFFF",
            defaultTDOL: "9F02065F2A029A039C0195059F3704", //a list of tags that we want to be provided
            defaultDDOL: "9F3704",
            autoSelectApplication: 0,
            countryCode: "0124",
            currencyCode: "0124",
            terminalCapacity: "E0B8C8",//"E040C8"//"E0B8C8"
            additionalCapacity: "F000F0F001",
            terminalType: "11",
            merchantCategoryCode: "", //doesn't matter
            terminalCategoryCode: "", //doesn't matter
            terminalID: "12345678",//doesn't matter
            merchantID: "9999999999",//doesn't matter
            acquireID: "112233",//doesn't matter
            capkIndex: "FF",
            pinBypassFlag: "0",
            pinTimeout: "060",
            pinFormat: "0",
            pinScriptNumber: "8",
            pinMacroNumber: "0",
            pinDevriKey: "0",
            pinDevriMacro: "0",
            cardStatusDisplay: "1",
            termCurExp: 0,
            issAcqFlag: 0,
            noDisplaySupportFlag: 0,
            modifyCandidateListFlag: 1,
            rfu1String: "",
            rfu2String: "",
            rfu3String: "",
            vfi_rfu1: 0,
            vfi_rfu2: 0,
            vfi_rfu3: 0
        )

    }
    
    /**
     *  Sleeping time in between each VeriFone command issued during pin pad initalization.  This is used to make sure there is enough time for the pin pad to finish processing the command before the next command is sent by the iPad.
     */
    struct Time {
        static let sleepingTime: UInt32 = 2
    }
    
    /**
     *  These are error code coming from the mid-tier during card validation but is considered only as warning during customer authenication.
     */
    struct CustomerSession {        
        static let warningCode = ["ACRISS", "ACL200", "ACL222", "ACVDCP", "ACVDSP"]
    }
    
}