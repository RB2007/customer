//
//  ManualSearchTypeTableViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-06.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

protocol ManualSearchTypeViewController {
    var selectedManualSearchType: ManualSearchType? {get set}
}

class ManualSearchTypeTableViewController: UITableViewController {
    
    var delegate: ManualSearchTypeViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        self.tableView.tableFooterView = UIView(frame: CGRectZero)
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        // Select the current selected manual search type
        if let uSelectedManualSearchType = delegate?.selectedManualSearchType {
            let indexPath = NSIndexPath(forRow: uSelectedManualSearchType.rawValue, inSection: 0)
            self.tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.Top)
        }
    }
    
    deinit {
        print("Deinit is called on ManualSearchTypeTableViewController")
    }
    
    // MARK: - Table View Delegate Methods
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return ManualSearchType.numberOfKeys
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("ManualSearchTypeCell", forIndexPath: indexPath) as? ManualSearchTypeTableViewCell else {
            return UITableViewCell()
        }
        let manualSearchTypeName = ManualSearchType(rawValue: indexPath.row)?.getKeyName() ?? ""
        cell.setCellContent(manualSearchTypeName)
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        delegate?.selectedManualSearchType = ManualSearchType(rawValue: indexPath.row)
    }
}
