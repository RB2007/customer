//
//  PersonalInfoKey.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-02.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

enum PersonalInfoKey: Int, CustomerKeys {
    case Name, AddressLine1, AddressLine2, City, Province, PostalCode, Country
    
    static var numberOfKeys: Int {
        get {
            return 7
        }
    }
    
    static var parentSectionNumber: Int {
        get {
            return CustomerPersonalInfoKeys.PersonalInfo.rawValue
        }
    }
    
    func getKeyName(object: AnyObject) -> String {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return ""
        }
        
        let address = uPersonProfile.getCustomerAddressWithType(AddressUsageType.CustomerResidence)
        
        switch self {
        case .Name:
            return NSLocalizedString("name", comment: "Customer Personal Info")
        case .AddressLine1:
            return NSLocalizedString("address_Line1", comment: "Customer Personal Info")
        case .AddressLine2:
            return NSLocalizedString("address_Line2", comment: "Customer Personal Info")
        case .City:
            return NSLocalizedString("city", comment: "Customer Personal Information")
        case .Province:
            var keyName = NSLocalizedString("province", comment: "Customer Personal Information")
            if let uCountryCode = address?.country where uCountryCode == CountryCode.UnitedStates.rawValue {
                keyName = NSLocalizedString("state", comment: "Customer Personal Information")
            }
            return keyName
        case .PostalCode:
            var keyName = NSLocalizedString("postal_code", comment: "Customer Personal Information")
            if let uCountryCode = address?.country where uCountryCode == CountryCode.UnitedStates.rawValue {
                keyName = NSLocalizedString("zip_code", comment: "Customer Personal Information")
            }
            return keyName
        case .Country:
            return NSLocalizedString("country", comment: "Customer Personal Information")
        }
    }
    
    func getValueFrom(object: AnyObject) -> String {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return ""
        }
        
        switch self {
            
        case .Name:
            return uPersonProfile.nameLine1?.capitalizedString ?? ""
            
        case .AddressLine1:
            return uPersonProfile.getCustomerAddressWithType(AddressUsageType.CustomerResidence)?.line1?.capitalizedString ?? ""
            
        case .AddressLine2:
            return uPersonProfile.getCustomerAddressWithType(AddressUsageType.CustomerResidence)?.line2?.capitalizedString ?? ""
            
        case .City:
            return uPersonProfile.getCustomerAddressWithType(AddressUsageType.CustomerResidence)?.cityOrLine3?.capitalizedString ?? ""
            
        case .Province:
            if let provinceCode = uPersonProfile.getCustomerAddressWithType(AddressUsageType.CustomerResidence)?.region {
                return CustomerMasterData.getValueForKey(provinceCode, customerMasterDataType: .ProvinceForAddress).capitalizedString
            }
            return ""
            
        case .PostalCode:
            return uPersonProfile.getCustomerAddressWithType(AddressUsageType.CustomerResidence)?.postalCode ?? ""
            
        case .Country:
            if let countryCode = uPersonProfile.getCustomerAddressWithType(AddressUsageType.CustomerResidence)?.country {
                return CustomerMasterData.getValueForKey(countryCode, customerMasterDataType: .CountryForAddress).capitalizedString
            }
            return ""
        }
    }
    
    func shouldDisplay(object: AnyObject) -> Bool {
        switch self {
        case .Name:
            return true
        case .AddressLine1:
            return true
        case .AddressLine2:
            return true
        case .City:
            return true
        case .Province:
            return true
        case .PostalCode:
            return true
        case .Country:
            return true
        }
    }
    
    func isEditable(object: AnyObject) -> Bool {
        switch self {
        case .Name:
            return false
        case .AddressLine1:
            return false
        case .AddressLine2:
            return false
        case .City:
            return false
        case .Province:
            return false
        case .PostalCode:
            return false
        case .Country:
            return false
        }
    }
    
    func editingType() -> CustomerInfoEditType {
        
        switch self {
        case .Name:
            return .Keyboard
        case .AddressLine1:
            return .Keyboard
        case .AddressLine2:
            return .Keyboard
        case .City:
            return .Keyboard
        case .Province:
            return .MasterDataListSelector
        case .PostalCode:
            return .Keyboard
        case .Country:
            return .MasterDataListSelector
        }
    }
    
    func getMasterDataForListSelector(object: AnyObject) -> [String: String]? {
        switch self {
        case .Name:
            return nil
        case .AddressLine1:
            return nil
        case .AddressLine2:
            return nil
        case .City:
            return nil
        case .Province:
            return CustomerMasterData.getDataForType(CustomerMasterDataType.ProvinceForAddress)
        case .PostalCode:
            return nil
        case .Country:
            return CustomerMasterData.getDataForType(CustomerMasterDataType.CountryForAddress)
        }
    }
    
    func valueChanged(object: AnyObject, newValue: String, newValue2: String?) -> String {
        // TODO: When field become editable
        return newValue
    }
    
    func setValueTo(object: AnyObject, newValue: String, newValue2: String?) {
        // TODO: When field becomes editable
    }
    
    func isValidValueIn(object: AnyObject) -> Bool {
        // TODO: When field becomes editable
        return true
    }
    
    func shouldChangeStringInRange(object: AnyObject, newString: String, range: NSRange) -> Bool {
        // TODO: When field becomes editable
        return true
    }
    
    func customizeTextField(object: AnyObject, textField: UITextField, editing: Bool) {
        // TODO: When field becomes editable
        textField.placeholder = ""
        textField.keyboardType = UIKeyboardType.Default
        textField.returnKeyType = UIReturnKeyType.Done
        textField.enabled = isEditable(object) ? editing : false
        textField.borderStyle = isEditable(object) && editing ? UITextBorderStyle.RoundedRect : UITextBorderStyle.None
        textField.backgroundColor = isEditable(object) && editing ? UIColor.whiteColor() : UIColor.clearColor()
        textField.autocapitalizationType = UITextAutocapitalizationType.Words
    }
    
    func reloadRowsAtIndexPathsWithAnimation(pobject: AnyObject) -> [[String: AnyObject]]? {
        return nil
    }
}
