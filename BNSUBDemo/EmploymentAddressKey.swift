//
//  EmploymentAddressKey.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-02.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

// MARK: - Employment Address Keys

enum EmploymentAddressKey: Int, CustomerKeys {
    case AddressLine1, AddressLine2, City, Province, PostalCode, Country
    
    static var numberOfKeys: Int {
        get {
            return 6
        }
    }
    
    static var parentSectionNumber: Int {
        get {
            return CustomerEmploymentInfoKeys.EmploymentAddress.rawValue
        }
    }
    
    func getKeyName(object: AnyObject) -> String {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return ""
        }
        
        let address = uPersonProfile.employment?.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)
        
        switch self {
        case .AddressLine1:
            return NSLocalizedString("address_Line1", comment: "Customer Employer Address Information")
        case .AddressLine2:
            return NSLocalizedString("address_Line2", comment: "Customer Employer Address Information")
        case .City:
            return NSLocalizedString("city", comment: "Customer Employer Address Information")
        case .Province:
            var keyName = NSLocalizedString("province", comment: "Customer Employer Address Information")
            if let uCountryCode = address?.country where uCountryCode == CountryCode.UnitedStates.rawValue {
                keyName = NSLocalizedString("state", comment: "Customer Employer Address Information")
            }
            return keyName
        case .PostalCode:
            var keyName = NSLocalizedString("postal_code", comment: "Customer Employer Address Information")
            if let uCountryCode = address?.country where uCountryCode == CountryCode.UnitedStates.rawValue {
                keyName = NSLocalizedString("zip_code", comment: "Customer Employer Address Information")
            }
            return keyName
        case .Country:
            return NSLocalizedString("country", comment: "Customer Employer Address Information")
        }
    }
    
    func getValueFrom(object: AnyObject) -> String {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return ""
        }
        
        guard let uAddress = uPersonProfile.employment?.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress) else {
            return ""
        }
        
        switch self {
            
        case .AddressLine1:
            return uAddress.line1?.capitalizedString ?? ""
            
        case .AddressLine2:
            return uAddress.line2?.capitalizedString ?? ""
            
        case .City:
            return uAddress.cityOrLine3?.capitalizedString ?? ""
            
        case .Province:
            if let uProvinceCode = uAddress.region {
                return CustomerMasterData.getValueForKey(uProvinceCode, customerMasterDataType: .ProvinceForAddress).capitalizedString
            }
            return ""
            
        case .PostalCode:
            
            if let uPostalCode = uAddress.postalCode, uCountryCode = uAddress.country {
                if uCountryCode == CountryCode.Canada.rawValue {
                    return Address.formatCanadianPostalCode(uPostalCode)
                } else if uCountryCode == CountryCode.UnitedStates.rawValue {
                    return Address.formatUSZipCode(uPostalCode)
                }
            }
            return uAddress.postalCode ?? ""
            
        case .Country:
            if let uCountryCode = uAddress.country {
                return CustomerMasterData.getValueForKey(uCountryCode, customerMasterDataType: .CountryForAddress).capitalizedString
            }
            return ""
        }
    }
    
    func shouldDisplay(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
        case .AddressLine1:
            return true
        case .AddressLine2:
            return true
        case .City:
            return true
        case .Province:
            if let uCountry = uPersonProfile.employment?.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.country where uCountry == CountryCode.Canada.rawValue || uCountry == CountryCode.UnitedStates.rawValue {
                return true
            }
            return false
        case .PostalCode:
            return true
        case .Country:
            return true
        }
    }
    
    func isEditable(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
        case .AddressLine1:
            return true
        case .AddressLine2:
            return true
        case .City:
            return true
        case .Province:
            if let uCountry = uPersonProfile.employment?.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.country where uCountry == CountryCode.Canada.rawValue || uCountry == CountryCode.UnitedStates.rawValue {
                return true
            }
            return false
        case .PostalCode:
            return true
        case .Country:
            return true
        }
    }
    
    func editingType() -> CustomerInfoEditType {
        switch self {
        case .AddressLine1:
            return .Keyboard
        case .AddressLine2:
            return .Keyboard
        case .City:
            return .Keyboard
        case .Province:
            return .MasterDataListSelector
        case .PostalCode:
            return .Keyboard
        case .Country:
            return .MasterDataListSelector
        }
    }
    
    func getMasterDataForListSelector(object: AnyObject) -> [String: String]? {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return nil
        }
        
        switch self {
        case .AddressLine1:
            return nil
        case .AddressLine2:
            return nil
        case .City:
            return nil
        case .Province:
            if let uCountry = uPersonProfile.employment?.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.country {
                if let uCountryCode = CountryCode(rawValue: uCountry) {
                    return CustomerMasterData.getProvinceStateDataForAddressFor(uCountryCode)
                }
            }
            return nil
        case .PostalCode:
            return nil
        case .Country:
            return CustomerMasterData.getDataForType(CustomerMasterDataType.CountryForAddress)
        }
    }
    
    func valueChanged(object: AnyObject, newValue: String, newValue2: String?) -> String {
        setValueTo(object, newValue: newValue, newValue2: newValue2)
        return getValueFrom(object)
    }
    
    func setValueTo(object: AnyObject, newValue: String, newValue2: String?) {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return
        }
        
        guard let uAddress = uPersonProfile.employment?.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress) else {
            let newAddress = Address(usageTp: AddressUsageType.BusinessEmployerAddress)
            uPersonProfile.employment?.addAddress(newAddress)
            self.setValueTo(uPersonProfile, newValue: newValue, newValue2: newValue2)
            return
        }
        
        switch self {
        case .AddressLine1:
            uAddress.line1 = newValue
        case .AddressLine2:
            uAddress.line2 = newValue
        case .City:
            uAddress.cityOrLine3 = newValue
        case .Province:
            uAddress.region = newValue
        case .PostalCode:
            var postalCodeNewValue = newValue
            if let uCountryCode = uAddress.country {
                if uCountryCode == CountryCode.Canada.rawValue {
                    postalCodeNewValue = Address.removeCanadianPostalCodeFormatting(newValue)
                } else if uCountryCode == CountryCode.UnitedStates.rawValue {
                    postalCodeNewValue = Address.removeUSZipCodeFormatteing(newValue)
                }
            }
            uAddress.postalCode = postalCodeNewValue.uppercaseString
        case .Country:
            uAddress.country = newValue
        }
    }
    
    func isValidValueIn(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        let address = uPersonProfile.employment?.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)
        
        switch self {
        case .AddressLine1:
            return address?.isValidLine1 ?? true
        case .AddressLine2:
            return address?.isValidLine2 ?? true
        case .City:
            return address?.isValidCityOrLine3 ?? true
        case .Province:
            return address?.isValidRegion ?? true
        case .PostalCode:
            return address?.isValidPostalCode ?? true
        case .Country:
            return address?.isValidCountry ?? true
        }
    }
    
    func shouldChangeStringInRange(object: AnyObject, newString: String, range: NSRange) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
        case .AddressLine1:
            return Address.isValidLine1InRange(newString, range: range)
        case .AddressLine2:
            return Address.isValidLine2InRange(newString, range: range)
        case .City:
            return Address.isValidCityOrLine3InRange(newString, range: range)
        case .Province:
            return true
        case .PostalCode:
            let countryCode = uPersonProfile.employment?.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.country
            return Address.isValidPostalCodeInRange(newString, range: range, countryCode: countryCode)
        case .Country:
            return true
        }
    }
    
    func customizeTextField(object: AnyObject, textField: UITextField, editing: Bool) {

        guard let uPersonProfile = object as? PersonProfile else {
            return
        }
        
        textField.keyboardType = UIKeyboardType.Default
        textField.returnKeyType = UIReturnKeyType.Done
        textField.enabled = isEditable(uPersonProfile) ? editing : false
        textField.borderStyle = isEditable(uPersonProfile) && editing ? UITextBorderStyle.RoundedRect : UITextBorderStyle.None
        textField.backgroundColor = isEditable(uPersonProfile) && editing ? UIColor.whiteColor() : UIColor.clearColor()
        textField.autocapitalizationType = UITextAutocapitalizationType.Words
        
        let address = uPersonProfile.employment?.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)
        
        switch self {
        case .AddressLine1:
            textField.placeholder = editing ? NSLocalizedString("enter_address_line1", comment: "Customer Employer Address Information") : ""
        case .AddressLine2:
            textField.placeholder = editing ? NSLocalizedString("enter_address_line2", comment: "Customer Employer Address Information") : ""
        case .City:
            textField.placeholder = editing ? NSLocalizedString("enter_city", comment: "Customer Employer Address Information") : ""
        case .Province:
            var placeholderText = NSLocalizedString("enter_province", comment: "Customer Employer Address Information")
            if let uCountryCode = address?.country where uCountryCode == CountryCode.UnitedStates.rawValue {
                placeholderText = NSLocalizedString("enter_state", comment: "Customer Employer Address Information")
            }
            textField.placeholder = isEditable(uPersonProfile) && editing ? placeholderText : ""
        case .PostalCode:
            var placeholderText = NSLocalizedString("enter_postal_code", comment: "Customer Employer Address Information")
            if let uCountryCode = address?.country where uCountryCode == CountryCode.UnitedStates.rawValue {
                placeholderText = NSLocalizedString("enter_zip_code", comment: "Customer Employer Address Information")
                textField.keyboardType = UIKeyboardType.NumberPad
            }
            textField.placeholder = editing ? placeholderText : ""
            textField.autocapitalizationType = UITextAutocapitalizationType.AllCharacters
        case .Country:
            textField.placeholder = editing ? NSLocalizedString("enter_country", comment: "Customer Employer Address Information") : ""
        }
    }
    
    func reloadRowsAtIndexPathsWithAnimation(object: AnyObject) -> [[String: AnyObject]]? {
        switch self {
        case .AddressLine1:
            return nil
        case .AddressLine2:
            return nil
        case .City:
            return nil
        case .Province:
            return nil
        case .PostalCode:
            return nil
        case .Country:
            var indexPathObjects = [[String: AnyObject]]()
            
            let indexPath1 = NSIndexPath(forRow: EmploymentAddressKey.Province.rawValue, inSection: EmploymentAddressKey.parentSectionNumber)
            let shouldDisplay1 = EmploymentAddressKey.Province.shouldDisplay(object)
            let indexPathObject1 = ["indexPath": indexPath1, "shouldDisplay": shouldDisplay1]
            indexPathObjects.append(indexPathObject1)
            
            let indexPath2 = NSIndexPath(forRow: EmploymentAddressKey.PostalCode.rawValue, inSection: EmploymentAddressKey.parentSectionNumber)
            let shouldDisplay2 = EmploymentAddressKey.PostalCode.shouldDisplay(object)
            let indexPathObject2 = ["indexPath": indexPath2, "shouldDisplay": shouldDisplay2]
            indexPathObjects.append(indexPathObject2)
            
            return indexPathObjects
        }
    }
}
