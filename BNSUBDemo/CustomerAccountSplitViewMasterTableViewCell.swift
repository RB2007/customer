//
//  CustomerAccountSplitViewMasterTableViewCell.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-14.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class CustomerAccountSplitViewMasterTableViewCell: UITableViewCell {

    // MARK: - Properties
    @IBOutlet weak var imageViewIcon: UIImageView!
    @IBOutlet weak var labelMasterKeyName: UILabel!
    @IBOutlet weak var viewLeftSelectionIndicator: UIView!
    
    // MARK: - View Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        self.backgroundColor = UIColor.clearColor()
    }

    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)

        // Configure the view for the selected state
        self.viewLeftSelectionIndicator.backgroundColor = selected ? Constants.UBColours.scotiaRed : UIColor.clearColor()
    }
    
    // MARK: - Instance Methods
    func setCellContent(masterKey: CustomerAccountSplitViewMasterKeys) {
        labelMasterKeyName.text = masterKey.getKeyName()
        imageViewIcon.image = masterKey.getIcon()
    }
}