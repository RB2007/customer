//
//  CustomerAccountSplitViewMasterViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-14.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

enum CustomerAccountSplitViewMasterKeys: Int {
    case Personal, Employer, Account, Identification
    
    static var numberOfKeys: Int {
        get {
            return 4
        }
    }
    
    func getKeyName() -> String {
        switch self {
        case .Personal:
            return NSLocalizedString("personal", comment: "Customer Account Side Menu")
        case .Employer:
            return NSLocalizedString("employer", comment: "Customer Account Side Menu")
        case .Account:
            return NSLocalizedString("accounts", comment: "Customer Account Side Menu")
        case .Identification:
            return NSLocalizedString("identification", comment: "Customer Account Side Menu")
        }
    }
    
    func getIcon() -> UIImage {
        var imageName: String
        switch self {
        case .Personal:
            imageName = "PersonIconLight"
        case .Employer:
            imageName = "BusinessIconLight"
        case .Account:
            imageName = "CardIconLight"
        case .Identification:
            imageName = "IdentificationIconLight"
        }
        return UIImage(named: imageName) ?? UIImage()
    }
    
    func getSegueIdentifier() -> String {
        switch self {
        case .Personal:
            return "showCustomerPersonalInfo"
        case .Employer:
            return "showCustomerEmployerInfo"
        case .Account:
            return "showCustomerAccounts"
        case .Identification:
            return "showCustomerIdentification"
        }
    }
    
    func configureToolBarButtons(endSessionButton: UIButton, authenticatedButton: UIButton, editButton: UIButton, saveButton: UIButton, cancelButton: UIButton, isUserAuthenticated: Bool, isEditing: Bool, hideToolBarButtons: Bool) {
        // Configure the tool bar buttons based on the key selected
        authenticatedButton.hidden = isUserAuthenticated
        
        switch self {
        case .Personal:
            endSessionButton.hidden = hideToolBarButtons
            editButton.hidden = !(isUserAuthenticated && !isEditing && !hideToolBarButtons)
            saveButton.hidden = !(isUserAuthenticated && isEditing && !hideToolBarButtons)
            cancelButton.hidden = !(isUserAuthenticated && isEditing && !hideToolBarButtons)
        case .Employer:
            endSessionButton.hidden = hideToolBarButtons
            editButton.hidden = !(isUserAuthenticated && !isEditing && !hideToolBarButtons)
            saveButton.hidden = !(isUserAuthenticated && isEditing && !hideToolBarButtons)
            cancelButton.hidden = !(isUserAuthenticated && isEditing && !hideToolBarButtons)
        case .Account:
            endSessionButton.hidden = hideToolBarButtons
            editButton.hidden = !(isUserAuthenticated && !isEditing && !hideToolBarButtons)
            saveButton.hidden = !(isUserAuthenticated && isEditing && !hideToolBarButtons)
            cancelButton.hidden = !(isUserAuthenticated && isEditing && !hideToolBarButtons)
        case .Identification:
            endSessionButton.hidden = hideToolBarButtons
            editButton.hidden = !(isUserAuthenticated && !isEditing && !hideToolBarButtons)
            saveButton.hidden = !(isUserAuthenticated && isEditing && !hideToolBarButtons)
            cancelButton.hidden = !(isUserAuthenticated && isEditing && !hideToolBarButtons)
        }
    }
}

class CustomerAccountSplitViewMasterViewController: UIViewController, UITableViewDelegate, UITableViewDataSource {
    
    // MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    
    weak var customerAccountDelegate: CustomerAccountDelegate?
    var selectedIndexPath: NSIndexPath = NSIndexPath(forRow: 0, inSection: 0)
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize UI
        // Navigation
        self.navigationController?.setNavigationBarHidden(true, animated: false)
        
        // Splitview
        self.splitViewController?.preferredPrimaryColumnWidthFraction = 0.10
        self.splitViewController?.minimumPrimaryColumnWidth = 200
        
        // Select the first row in the table view
        tableView.selectRowAtIndexPath(selectedIndexPath, animated: true, scrollPosition: UITableViewScrollPosition.Top)
        if let uCustomerAccountSplitViewMasterKey = CustomerAccountSplitViewMasterKeys(rawValue: selectedIndexPath.row) {
            self.performSegueWithIdentifier(uCustomerAccountSplitViewMasterKey.getSegueIdentifier(), sender: nil)
            customerAccountDelegate?.selectedSplitMasterViewKey = uCustomerAccountSplitViewMasterKey
        }
    }
    
    deinit {
        print("Deinit is called on CustomerAccountSplitViewMasterViewController")
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        
        guard let uIdentifier = segue.identifier else {
            return
        }
        
        switch uIdentifier {
        case CustomerAccountSplitViewMasterKeys.Account.getSegueIdentifier(), CustomerAccountSplitViewMasterKeys.Identification.getSegueIdentifier():
            if let nav = segue.destinationViewController as? UINavigationController {
                if let vc = nav.viewControllers.first as? AbstractCustomerAccountTableViewController {
                    vc.customerAccountDelegate = customerAccountDelegate
                }
            }
        default:
            if let vc = segue.destinationViewController as? AbstractCustomerAccountTableViewController {
                vc.customerAccountDelegate = customerAccountDelegate
            }
        }
        
        
    }
    
    @IBAction func endSessionButtonPressed() {
        self.removeCustomerProfile()
    }
    
    func removeCustomerProfile() {
        // Manually removing the view controllers from the split view
        // This had to be done manually for the views to be deinitialized
        self.dismissViewControllerAnimated(true) { () -> Void in
            self.splitViewController?.viewControllers.last?.removeFromParentViewController()
            self.removeFromParentViewController()
            self.splitViewController?.viewControllers.removeAll()
        }
    }
    
    // MARK: - Table Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return CustomerAccountSplitViewMasterKeys.numberOfKeys
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("SplitViewMasterCell", forIndexPath: indexPath) as? CustomerAccountSplitViewMasterTableViewCell, masterKey = CustomerAccountSplitViewMasterKeys(rawValue: indexPath.row) else {
            return UITableViewCell()
        }
        cell.setCellContent(masterKey)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if indexPath != selectedIndexPath {
            if let uCustomerAccountSplitViewMasterKey = CustomerAccountSplitViewMasterKeys(rawValue: indexPath.row) {
                self.performSegueWithIdentifier(uCustomerAccountSplitViewMasterKey.getSegueIdentifier(), sender: nil)
                customerAccountDelegate?.selectedSplitMasterViewKey = uCustomerAccountSplitViewMasterKey
                selectedIndexPath = indexPath
            }
        }
    }
}
