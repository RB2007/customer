//
//  SaveCancelActionButtonsTableViewCell.swift
//  Customer
//
//  Created by Emad Toukan on 2016-03-02.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import UIKit

protocol SaveCancelActionButtonsProtocol: class {
    func saveButtonPressed()
    func cancelButtonPressed()
}


class SaveCancelActionButtonsTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var buttonSave: UIButton!
    @IBOutlet weak var buttonCancel: UIButton!
    weak var delegate: SaveCancelActionButtonsProtocol?
    
    // MARK: - Cell Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        buttonSave.layer.cornerRadius = 5
        buttonSave.setTitle(NSLocalizedString("save", comment: "Save Action Button"), forState: UIControlState.Normal)
        buttonCancel.setTitle(NSLocalizedString("cancel", comment: "Cancel Action Button"), forState: UIControlState.Normal)
    }
    
    // MARK: - Instance Methods
    @IBAction func saveButtonPressed() {
        delegate?.saveButtonPressed()
    }
    
    @IBAction func cancelButtonPressed() {
        delegate?.cancelButtonPressed()
    }
}
