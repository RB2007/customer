//
//  CustomerLookupViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-06.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit
import SVProgressHUD

protocol CustomersLookupDelegate {
    func didReceiveCustomersSearchData(data: AnyObject?, error: NSError?, lookUpSubType: LookUpSubType)
    func loadSelectedCustomer(customerSearch: CustomerSearch)
    func cancelSearch()
}

protocol ScannerDriverLicenseDelegate {
    func didReceiveDriverLicense(driverLicense: DriverLicense)
}

class CustomerLookupViewController: UIViewController, UITextFieldDelegate, ManualSearchTypeViewController, CustomersLookupDelegate, ScannerDriverLicenseDelegate, UIPopoverPresentationControllerDelegate {
    
    // MARK: - Properties
    @IBOutlet weak var buttonScanDriverLicense: UIButton!
    @IBOutlet weak var buttonUsePINPad: UIButton!
    @IBOutlet weak var buttonManualSearchType: UIButton!
    @IBOutlet weak var customerSearchArrow: UIButton!
    @IBOutlet weak var buttonBack: UIButton!
    
    @IBOutlet weak var labelSearchPrefix: UILabel!
    @IBOutlet weak var labelBranchName: UILabel!
    @IBOutlet weak var labelTransitNumber: UILabel!
    @IBOutlet weak var labelTransitNumberKey: UILabel!
    
    @IBOutlet weak var textFieldCustomerSearch: UITextField!
    @IBOutlet weak var imageViewCustomerSearchIcon: UIImageView!
    @IBOutlet weak var constraintScrollViewBottom: NSLayoutConstraint!
    
    @IBOutlet weak var imageViewScotiabankLogo: UIImageView!
    @IBOutlet weak var labelOfficerName: UILabel!
    
    var selectedManualSearchType: ManualSearchType? {
        didSet {
            if let uSelectedManualSearchType = self.selectedManualSearchType {
                uSelectedManualSearchType.customizeTextField(textFieldCustomerSearch)
                uSelectedManualSearchType.customizeIcon(imageViewCustomerSearchIcon)
                uSelectedManualSearchType.customizePrefixText(labelSearchPrefix)
                buttonManualSearchType.setTitle(uSelectedManualSearchType.getKeyName(), forState: UIControlState.Normal)
            }
        }
    }
    
    // MARK: - View Delegate Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        customizeUI()
        registerForKeyboardNotifications()
    }
    
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
        // Launch view with Customer Name search as default
        selectedManualSearchType = ManualSearchType.CustomerName
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.LightContent
    }
    
    // MARK: - Setup
    private func customizeUI() {
        // Customize Buttons
        buttonUsePINPad.layer.cornerRadius = 5
        buttonScanDriverLicense.layer.cornerRadius = 5

        let usePinPadText = NSLocalizedString("use_pin_pad", comment: "Customer Lookup View Controller")
        buttonUsePINPad.setTitle(usePinPadText, forState: UIControlState.Normal)
        let scanDriverLicenseText = NSLocalizedString("scan_driver_license", comment: "Customer Lookup View Controller")
        buttonScanDriverLicense.setTitle(scanDriverLicenseText, forState: UIControlState.Normal)
        
        // Set the current branch name and transit
        labelOfficerName.text = Officer.name ?? ""
        labelTransitNumberKey.text = NSLocalizedString("transit_number_key", comment: "Customer Lookup View Controller")
        labelBranchName.text = Officer.currentSelectedRole?.transit?.name?.capitalizedString ?? ""
        labelTransitNumber.text = Officer.currentSelectedRole?.transit?.id ?? ""
        // Hide the back button if there is only 1 branch for the user
        buttonBack.hidden = Officer.officerRoles?.count == 1
        
        // Localizing the scotiabank logo
        let scotiaBank = NSLocalizedString("scotia_bank", comment: "")
        imageViewScotiabankLogo.image = UIImage(named: scotiaBank + " Logo")
        
        // Disable swipe back gestsure
        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
    }
    
    private func registerForKeyboardNotifications () {
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardDidShow:", name: UIKeyboardWillShowNotification, object: nil)
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHide:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if segue.identifier == "showPINPad" {
            if let vc = segue.destinationViewController as? PINManagemenetViewController {
                vc.customersLookupDelegate = self
            }
        } else if segue.identifier == "showDriverLicenseScanner" {
            if let vc = segue.destinationViewController as? ScannerDriverLicenseViewController {
                vc.delegate = self
            }
        } else if segue.identifier == "showManateeScanner" {
            if let vc = segue.destinationViewController as? ScannerController {
                vc.delegate = self
            }
        } else if segue.identifier == "showCustomersSearchResults" {
            if let vc = segue.destinationViewController as? CustomersSearchResultsViewController, customerSearchResults = sender as? [CustomerSearch] {
                vc.delegate = self
                vc.customerSearchResults = customerSearchResults
            }
        } else if segue.identifier == "showCustomerLookupTypePopover" {
            // Change the popover position when it is displayed
            if let vc = segue.destinationViewController as? ManualSearchTypeTableViewController {
                vc.delegate = self
                vc.popoverPresentationController?.sourceRect.offsetInPlace(dx: 75, dy: 60)
                vc.popoverPresentationController?.delegate = self
            }
        }
    }
    
    @IBAction func backButtonPressed() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    
    // MARK: - Manage Keyboard
    func keyboardDidShow(notification: NSNotification) {
        // First we check if the userInfo is available and we look for the key UIKeyboardFrameEndUserInfoKey
        if let keyboardFrame = notification.userInfo?[UIKeyboardFrameEndUserInfoKey] as? NSValue {
            // We get the height of the keyboard from the frame and change the bottom scroll view constraint constant
            let keyboardHeight = keyboardFrame.CGRectValue().size.height
            self.constraintScrollViewBottom.constant = -keyboardHeight + 100
        }
    }
    
    func keyboardWillHide(notification: NSNotification) {
        constraintScrollViewBottom.constant = 0
    }
    
    @IBAction func userTappedBackground(sender: UITapGestureRecognizer) {
        // Hide keyboard when user taps background
        self.view.endEditing(true)
    }
    
    // MARK: - TextField Delegate Methods
    func textField(textField: UITextField, shouldChangeCharactersInRange range: NSRange, replacementString string: String) -> Bool {
        guard let uSelectedManualSearchType = selectedManualSearchType else {
            return true
        }
        return uSelectedManualSearchType.shouldChangeStringInRangeFor(textField.text!, newString: string, range: range)
    }
    
    @IBAction func textFieldEditingChanged(sender: UITextField) {
        if let uSelectedManualSearchType = selectedManualSearchType, uTextFieldText = textFieldCustomerSearch.text {
            textFieldCustomerSearch.text = uSelectedManualSearchType.valueChanged(uTextFieldText)
        }
    }
    
    func textFieldShouldReturn(textField: UITextField) -> Bool {
        textField.resignFirstResponder()
        if textField.text?.characters.count > 0 {
            performManualCustomerSearch()
        }
        return true
    }
    
    // MARK: - Instance Methods
    @IBAction func usePINPadButtonPressed() {
        self.view.endEditing(true)
        performSegueWithIdentifier("showPINPad", sender: nil)
    }
    
    @IBAction func scanDriverLicenseButtonPressed() {
        self.view.endEditing(true)
//        performSegueWithIdentifier("showDriverLicenseScanner", sender: nil)
        performSegueWithIdentifier("showManateeScanner", sender: nil)
    }
    
    @IBAction func searchTypeButtonPressed() {
        // Remove keyboard and then show the popover
        self.view.endEditing(true)
        self.performSegueWithIdentifier("showCustomerLookupTypePopover", sender: nil)
        UIView.animateWithDuration(0.25) { () -> Void in
            self.customerSearchArrow.transform = CGAffineTransformMakeRotation(CGFloat(M_PI_2))
        }
    }
    
    // MARK: - Popover Presentatiol Controller Delegate
    func popoverPresentationControllerShouldDismissPopover(popoverPresentationController: UIPopoverPresentationController) -> Bool {
        UIView.animateWithDuration(0.25) { () -> Void in
            self.customerSearchArrow.transform = CGAffineTransformMakeRotation(CGFloat(0))
        }
        return true
    }
    
    // MARK: - Scanner Driver's License Delegate
    func didReceiveDriverLicense(driverLicense: DriverLicense) {
        if let customerFirstName = driverLicense.firstName, customerFirstNameInitial = driverLicense.firstName?.characters.first ,customerLastName = driverLicense.lastName, customerDateOfBirth = driverLicense.dateOfBirth {
            self.selectedManualSearchType = ManualSearchType.CustomerName
            self.textFieldCustomerSearch.text = customerFirstName + " " + customerLastName
            
            SVProgressHUD.showWithStatus(NSLocalizedString("searching_for_customer", comment: "Progress indicator."))
            APIManagement.sharedInstance.locateCustomerByFirstNameLastName(String(customerFirstNameInitial), customerLastName: customerLastName, dateOfBirth: Util.formatDate(customerDateOfBirth), postalCode: nil, lookUpSubType: LookUpSubType.DriverLicense, completion: { (data: AnyObject?, error: NSError?) -> () in
                SVProgressHUD.dismiss()
                self.didReceiveCustomersSearchData(data, error: error, lookUpSubType: LookUpSubType.DriverLicense)
            })
        } else {
            let alertController = AlertControllerUtil.createOKAlertController(NSLocalizedString("unable_to_read_driver_license", comment: "Error title."), message: NSLocalizedString("unable_to_read_driver_license_description", comment: "Error message body."))
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    // MARK: - Manual Search For Customers API Calls
    private func performManualCustomerSearch() {
        
        guard let uSelectedManualSearchType = selectedManualSearchType, uTextFieldText = textFieldCustomerSearch.text else {
            return
        }
        
        // 1. Validate fields
        if !uSelectedManualSearchType.isValidValueIn(uTextFieldText) {
            let alertMessage = uSelectedManualSearchType.getErrorMessage()
            let alertController = AlertControllerUtil.createOKAlertController(alertMessage.title, message: alertMessage.message)
            self.presentViewController(alertController, animated: true, completion: nil)
            return
        }
        
        // 2. Call API
        SVProgressHUD.showWithStatus(NSLocalizedString("searching_for_customer", comment: "Progress indicator."))
        uSelectedManualSearchType.locateCustomerFrom(uTextFieldText) { (data: AnyObject?, error: NSError?) -> () in
            SVProgressHUD.dismiss()
            self.didReceiveCustomersSearchData(data, error: error, lookUpSubType: LookUpSubType.Manual)
        }
    }
    
    // MARK: - Customers Search Results Delegate
    func didReceiveCustomersSearchData(data: AnyObject?, error: NSError?, lookUpSubType: LookUpSubType) {
        
        // There could only be a data object or an error, not both
        if let uData = data as? [String: AnyObject] {
            if let uPartyFound = uData["PartyFound"] as? [[String: AnyObject]]{
                var customerSearchResults = [CustomerSearch]()
                for customerData in uPartyFound {
                    if let customerSearch = CustomerSearch(json: customerData, pinned: lookUpSubType == LookUpSubType.Pinned) {
                        customerSearchResults.append(customerSearch)
                    }
                }
                
                if lookUpSubType == LookUpSubType.Pinned && customerSearchResults.count == 1 {
                    if let firstCustomerSearch = customerSearchResults.first {
                        loadSelectedCustomer(firstCustomerSearch)
                        return
                    }
                }
                self.performSegueWithIdentifier("showCustomersSearchResults", sender: customerSearchResults)
                
            } else {
                // No search results where available
                let alert = AlertControllerUtil.createOKAlertController(NSLocalizedString("no_search_results", comment: "Error title."), message: NSLocalizedString("no_search_results_found", comment: "Error message body."))
                self.presentViewController(alert, animated: true, completion: nil)
            }
            return
        }
        
        if let uError = error {
            let alertController = AlertControllerUtil.createOKAlertController(uError)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
        
    }
    
    func loadSelectedCustomer(customerSearch: CustomerSearch) {
        
        guard let uCid = customerSearch.cid else {
            return
        }
        
        var statusMessage = NSLocalizedString("loading_customer_profile", comment: "Progress indicator.")
        if let uNameLine1 = customerSearch.nameLine1 {
            statusMessage = String.localizedStringWithFormat(NSLocalizedString("loading_customer_name_profile", comment: "Progress indcator showing customer name."), uNameLine1.capitalizedString)
        }
        
        SVProgressHUD.showWithStatus(statusMessage)
        APIManagement.sharedInstance.loadCustomerProfile(uCid) { (data: AnyObject?, error: NSError?) -> () in
            SVProgressHUD.dismiss()
            
            // Check for data and display the Customer Account
            if let uData = data as? [String: AnyObject] {
                
                // Go through the data and check if it's a personal profile
                let tempPartyType = uData[CustomerProfile.kPartyTypeKey] as? [String: AnyObject]
                let tempPartyBase = tempPartyType?[CustomerProfile.kPartyBaseKey] as? [String: AnyObject]
                if tempPartyBase?[PersonProfile.kPersonKey] != nil {
                    
                    // Launch the CustomerAccount
                    let storyBoard = UIStoryboard(name: "CustomerAccount", bundle: nil)
                    if let viewController = storyBoard.instantiateInitialViewController() as? CustomerAccountContainerViewController {
                        Analytics.sharedInstance.addEventWithName(SplunkConstants.kCustomerPersonalProfileRetreivedKey, theList: SplunkConstants.params())
                        viewController.customerProfileJSON = uData
                        self.presentViewController(viewController, animated: true, completion: nil)
                    }
                } else {
                    Analytics.sharedInstance.addEventWithName(SplunkConstants.kCustomerBusinessProfileRetreivedKey, theList: SplunkConstants.params())
                    let alert = AlertControllerUtil.createOKAlertController(NSLocalizedString("cannot_display_business_profile", comment: "Error title."), message: NSLocalizedString("cannot_display_business_profile_description", comment: "Error body."))
                    self.presentViewController(alert, animated: true, completion: nil)
                }
                return
            }
            
            // Check for error and display an alert controller
            if let uError = error {
                let alertController = AlertControllerUtil.createOKAlertController(uError)
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func cancelSearch() {
        // Remove text from textfield
        self.textFieldCustomerSearch.text = ""
    }
}
