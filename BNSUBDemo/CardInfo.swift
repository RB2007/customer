//
//  Card.swift
//  BNSUBDemo
//
//  Created by Annie Lo on 2015-09-14.
//  Copyright (c) 2015 ScotiaBank. All rights reserved.
//

import Foundation


struct CardInfo {
    var accountNumber: String?
    var expiryMonth: String?
    var expiryYear: String?
    var serviceCode: Int32?
    var appPanSeq: NSData?
    var defaultCurrency :Bool?
    var currencyImage: UIImage?
    var atc: NSData?
    var terminalVerificationResults: NSData?
    var appCryptogram: NSData?
    var aip: NSData?
    var amountAuth: NSData?
    var amountAuthOther: NSData?
    var terminalCountryCode: NSData?
    var transactionCurrencyCode: NSData?
    var transactionType: NSData?
    var unpredictableNumber: NSData?
    var iad: NSData?
    var dki: NSData?
    var cvn: NSData?
    var appAid: String?
    var cdol1: NSData?
    var transactionDate: String?
}