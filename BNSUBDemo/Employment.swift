//
//  Employment.swift
//  Customer
//
//  Created by Emad Toukan on 2015-12-11.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

class Employment {
    
    // MARK: - JSON Keys
    static let kEmployerNameKey = "EmployerName"
    static let kGrossMonthlyIncomeKey = "GrossMonthlyIncome"
    static let kLengthOfEmploymentKey = "LengthOfEmployment"
    static let kLengthOfOccupationKey = "LengthOfOccupation"
    static let kOccupationCategoryKey = "OccupationCategory"
    static let kOccupationCodeKey = "OccupationCode"
    static let kOccupationDescriptionKey = "OccupationDescription"
    static let kOccupationStatusKey = "OccupationStatus"
    static let kOccupationTpKey = "OccupationTp"
    static let kContactMethodsKey = "ContactMethods"
    static let kPhoneKey = "Phone"
    static let kAddressesKey = "Addresses"
    static let kAddressKey = "Address"
    
    // MARK: - Constants
    static let kEmployerNameMaxCharacterLimit = 40
    static let kGrossMonthlyIncomeMaxAmount:Double = 99999
    static let kGrossMonthlyIncomeMaxCharacterLimit = 5
    static let kOccupationDescriptionCharacterCount = 20
    
    // MARK: - Properties
    let json: [String: AnyObject]
    weak var personProfile: PersonProfile?
    var employerName: String?
    var grossMonthlyIncome: Double?
    var lengthOfEmployment: TimeLength?
    var lengthOfOccupation: TimeLength?
    var occupationCategory: String?
    var occupationCode: String? {
        willSet {
            if newValue != "256" {
                // Remove occupation description if the occupation code is '256' OTHER
                occupationDescription = nil
            }
        }
    }
    var occupationDescription: String?
    var occupationStatus: String? {
        didSet {
            // Set Occupation Description as Occupation Status is for Retired, Student-Prim/Second, Student-Post Second, Not Working, Staff - Retired
            if let uOccupationStatus = occupationStatus where uOccupationStatus == "004" || uOccupationStatus == "005" || uOccupationStatus == "006" || uOccupationStatus == "007" || uOccupationStatus == "015" {
                occupationCode = "256"
                occupationCategory = "099"
                occupationTp = "180"
                occupationDescription = CustomerMasterData.getValueForKey(uOccupationStatus, customerMasterDataType: CustomerMasterDataType.OccupationStatus)
            }
        }
    }
    var occupationTp: String?
    var phones: [Phone]?
    var addresses: [Address]?
    
    // MARK: - Init
    init(json: [String: AnyObject]) {
        self.json = json
        self.employerName = json[Employment.kEmployerNameKey] as? String
        self.grossMonthlyIncome = json[Employment.kGrossMonthlyIncomeKey] as? Double
        
        if let uLengthOfEmploymentJSON = json[Employment.kLengthOfEmploymentKey] as? [String: AnyObject] {
            self.lengthOfEmployment = TimeLength(json: uLengthOfEmploymentJSON)
        }
        
        if let uLengthOfOccupationJSON = json[Employment.kLengthOfOccupationKey] as? [String: AnyObject] {
            self.lengthOfOccupation = TimeLength(json: uLengthOfOccupationJSON)
        }
        
        self.occupationCategory = json[Employment.kOccupationCategoryKey] as? String
        self.occupationCode = json[Employment.kOccupationCodeKey] as? String
        self.occupationDescription = json[Employment.kOccupationDescriptionKey] as? String
        self.occupationStatus = json[Employment.kOccupationStatusKey] as? String
        self.occupationTp = json[Employment.kOccupationTpKey] as? String
        
        // Phones
        if let uContactMethods = json[Employment.kContactMethodsKey] as? [String: AnyObject] {
            if let uPhone = uContactMethods[Employment.kPhoneKey] as? [[String: AnyObject]] {
                for phone in uPhone {
                    if let uTempPhone = Phone(json: phone) {
                        self.addPhone(uTempPhone)
                    }
                }
            }
        }
        
        // Addresses
        if let uAddresses = json[Employment.kAddressesKey] as? [String: AnyObject] {
            if let uAddress = uAddresses[Employment.kAddressKey] as? [[String: AnyObject]] {
                for address in uAddress {
                    if let uTempAddress = Address(json: address) {
                        self.addAddress(uTempAddress)
                    }
                }
            }
        }
    }
    
    // MARK: - Helper Methods
    
    // Add
    func addPhone(newPhone: Phone) {
        if self.phones == nil {
            self.phones = [Phone]()
        }
        self.phones?.append(newPhone)
    }
    
    func addAddress(newAddress: Address) {
        if self.addresses == nil {
            self.addresses = [Address]()
        }
        self.addresses?.append(newAddress)
    }
    
    // Get
    func getEmploymentPhoneWithType(phoneUsageType: PhoneUsageType) -> Phone? {
        guard let uPhones = phones else {
            return nil
        }
        
        for phone in uPhones where phone.usageTp == phoneUsageType {
            return phone
        }
        return nil
    }
    
    func getEmploymentAddressWithType(addressUsageType: AddressUsageType) -> Address? {
        guard let uAddresses = addresses else {
            return nil
        }
        
        for address in uAddresses where address.usageTp == addressUsageType {
            return address
        }
        return nil
    }
    
    // MARK: - Validation Properties
    
    // Employer Name
    var isValidEmployerName: Bool? {
        
        // Employer Name is mandatory if Customer Status is NOT ‘PR’ (Prospective) or ‘IP’ (Involved Party)
        if let uPartyStatus = personProfile?.partyStatus where uPartyStatus != "PR" && uPartyStatus != "IP" {
            
            // Employee Name is mandatory is Occupation Status is NOT Retired, Student-Prim/Second, Student-Post Second, Not Working, Staff - Retired
            if let uOccupationStatus = occupationStatus where uOccupationStatus != "004" && uOccupationStatus != "005" && uOccupationStatus != "006" && uOccupationStatus != "007" && uOccupationStatus != "015" {
                guard let uEmployerName = employerName where uEmployerName.additionalSpacesRemoved.isEmpty == false else {
                    return false
                }
            }
        }
        
        // Employer Name is mandatory if OccupationTp is NOT '100' (Homemaker) or '180' (Not Working)
        // Removing this. Not sure where these requirements came from
        //        if let uOccupationTp = occupationTp where uOccupationTp != "100" || uOccupationTp != "180" {
        //            // Check that the string is not only spaces
        //            guard let uEmployerName = employerName where uEmployerName.additionalSpacesRemoved.isEmpty == false else {
        //                return false
        //            }
        //        }
        
        guard let uEmployerName = employerName where uEmployerName.additionalSpacesRemoved.isEmpty == false else {
            return nil
        }
        
        for char in uEmployerName.lowercaseString.characters where !Util.kAllowedLettersForTextInput.contains(String(char)) && !Util.kAllowedCharacterForTextInput.contains(String(char)) && !Util.kAllowedNumbersForTextInput.contains(String(char)) {
            return false
        }
        
        if uEmployerName.characters.count > Employment.kEmployerNameMaxCharacterLimit {
            return false
        }
        
        
        return true
    }
    
    // Gross Monthly Income
    var isValidGrossMonthlyIncome: Bool? {
        
        // Gross Monthly Income is required if Customer Status is NOT ‘PR’ (Prospective) or ‘IP’ (Involved Party)
        if let uPartyStatus = personProfile?.partyStatus where uPartyStatus != "PR" || uPartyStatus != "IP" {
            guard let uGrossMonthlyIncome = grossMonthlyIncome where uGrossMonthlyIncome != 0 else {
                return false
            }
        }
        
        guard let uGrossMonthlyIncome = grossMonthlyIncome else {
            return nil
        }
        
        // Check that the income is >=0 or <=99999
        if uGrossMonthlyIncome < 0 && uGrossMonthlyIncome > Employment.kGrossMonthlyIncomeMaxAmount {
            return false
        }
        
        // Check that the income does not have any decimals
        if uGrossMonthlyIncome != floor(uGrossMonthlyIncome) {
            return false
        }
        
        return true
    }
    
    // Occuaption Code
    var isValidOccupationCode: Bool? {
        guard let uOccupationCode = self.occupationCode else {
            return nil
        }
        
        return CustomerMasterData.getKeysFor(CustomerMasterDataType.OccupationCode).contains(uOccupationCode)
    }
    
    // Occupation Description
    var isValidOccupationDescription: Bool? {
        
        //  If OccupationCode set to "256" OTHER, OccupationDescription is required.
        if let uOccupationCode = occupationCode where uOccupationCode == "256" {
            guard let uOccupationDescription = occupationDescription where uOccupationDescription.additionalSpacesRemoved.isEmpty == false else {
                return false
            }
        }
        
        guard let uOccupationDescription = occupationDescription where uOccupationDescription.additionalSpacesRemoved.isEmpty == false else {
            return nil
        }
        
        for char in uOccupationDescription.lowercaseString.characters where !Util.kAllowedLettersForTextInput.contains(String(char)) && !Util.kAllowedCharacterForTextInput.contains(String(char)) && !Util.kAllowedNumbersForTextInput.contains(String(char)) {
            return false
        }
        
        if occupationDescription?.characters.count > Employment.kOccupationDescriptionCharacterCount {
            return false
        }
        
        return true
    }
    
    // Occupation Status
    var isValidOccupationStatus: Bool? {
        guard let uOccupationStatus = occupationStatus else {
            return nil
        }
        
        return CustomerMasterData.getKeysFor(CustomerMasterDataType.OccupationStatus).contains(uOccupationStatus)
    }
    
    // Occupation Type
    var isValidOccupationType: Bool? {
        guard let uOccupationType = occupationTp else {
            return nil
        }
        
        return CustomerMasterData.getKeysFor(CustomerMasterDataType.OccupationType).contains(uOccupationType)
    }
    
    // Occupation Category
    var isValidOccupationCategory: Bool? {
        guard let uOccupationCategory = occupationCategory else {
            return nil
        }
        
        return CustomerMasterData.getKeysFor(CustomerMasterDataType.OccupationCategories).contains(uOccupationCategory)
    }
    
    // MARK: - Validation in Range
    static func isValidEmployerNameInRange(aString: String, range: NSRange) -> Bool {
        
        // Check that the first entry is not a space
        if range.location == 0 && aString == " " {
            return false
        }
        
        // Check that the string is an acceptable letter, character or a number
        if !Util.kAllowedLettersForTextInput.contains(aString.lowercaseString) && !Util.kAllowedCharacterForTextInput.contains(aString.lowercaseString) && !Util.kAllowedNumbersForTextInput.contains(aString) && aString != "" {
            return false
        }
        
        // Check if the string exceeds the allowed string limit
        // Adding 2 due to formatting
        if range.location >= Employment.kEmployerNameMaxCharacterLimit && aString != "" {
            return false
        }
        
        return true
    }
    
    static func isValidGrossMonthlyIncomeInRange(aString: String, range: NSRange) -> Bool {
        
        // Check if the entered string is an int or a backspace
        if  Int(aString) == nil && aString != "" {
            return false
        }
        
        // Check if the string exceeds the allowed string limit
        // Adding 2 due to formatting
        if range.location >= Employment.kGrossMonthlyIncomeMaxCharacterLimit + 2 && aString != "" {
            return false
        }
        
        return true
    }
    
    static func isValidOccupationDescriptionInRange(aString: String, range: NSRange) -> Bool {
        
        // Check that the first entry is not a space
        if range.location == 0 && aString == " " {
            return false
        }
        
        // Check if the entered string is a valid or a backspace
        if !Util.kAllowedLettersForTextInput.contains(aString.lowercaseString) && !Util.kAllowedNumbersForTextInput.contains(aString) && !Util.kAllowedCharacterForTextInput.contains(aString) && aString != "" {
            return false
        }
        
        // Check if the string exceeds the allowed string limit
        if range.location >= Employment.kOccupationDescriptionCharacterCount && aString != "" {
            return false
        }
        
        return true
    }
    
    // MARK: - Save
    var isValidForSave: Bool {
        return (self.isValidEmployerName ?? true) &&
            (self.isValidGrossMonthlyIncome ?? true) &&
            (self.lengthOfEmployment?.isValidYears ?? true) &&
            (self.lengthOfEmployment?.isValidMonths ?? true) &&
            (self.lengthOfOccupation?.isValidYears ?? true) &&
            (self.lengthOfOccupation?.isValidMonths ?? true) &&
            (self.isValidOccupationCode ?? true) &&
            (self.isValidOccupationDescription ?? true) &&
            (self.isValidOccupationStatus ?? true) &&
            (self.isValidOccupationType ?? true) &&
            (self.isValidOccupationCategory ?? true) &&
            (self.getEmploymentPhoneWithType(PhoneUsageType.BusinessPhone)?.isValidNumber ?? true) &&
            (self.getEmploymentPhoneWithType(PhoneUsageType.BusinessPhone)?.isValidPhoneExtension ?? true) &&
            (self.getEmploymentPhoneWithType(PhoneUsageType.EmployerPhone)?.isValidNumber ?? true) &&
            (self.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.isValidLine1 ?? true) &&
            (self.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.isValidLine2 ?? true) &&
            (self.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.isValidCityOrLine3 ?? true) &&
            (self.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.isValidRegion ?? true) &&
            (self.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.isValidPostalCode ?? true) &&
            (self.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.isValidCountry ?? true)
    }
    
    var paramsForSave: [String: AnyObject]? {
        
        let originalEmployment = Employment(json: json)
        
        var employmentParams = [String: AnyObject]()
        
        // Employer Name
        employmentParams[Employment.kEmployerNameKey.decapitalizedString] = self.employerName == originalEmployment.employerName ? nil : self.employerName ?? ""
        
        // Gross Monthly Income
        if self.grossMonthlyIncome != originalEmployment.grossMonthlyIncome {
            // Due to a bug in A6, we need to send two fields to the backend in order to update the Gross Monthly Income
            employmentParams[Employment.kEmployerNameKey.decapitalizedString] = self.employerName ?? ""
            employmentParams[Employment.kGrossMonthlyIncomeKey.decapitalizedString] = self.grossMonthlyIncome ?? 0
        }
        
        // Length Of Employment
        employmentParams[Employment.kLengthOfEmploymentKey.decapitalizedString] = lengthOfEmployment?.paramsForSave
        
        // Length Of Occupation
        employmentParams[Employment.kLengthOfOccupationKey.decapitalizedString] = lengthOfOccupation?.paramsForSave
        
        // Occupation Code
        employmentParams[Employment.kOccupationCodeKey.decapitalizedString] = self.occupationCode == originalEmployment.occupationCode ? nil : self.occupationCode // There is no default / unknown value
        
        // Occupation Description
        employmentParams[Employment.kOccupationDescriptionKey.decapitalizedString] = self.occupationDescription == originalEmployment.occupationDescription ? nil : self.occupationDescription ?? ""
        
        // Occupation Status
        employmentParams[Employment.kOccupationStatusKey.decapitalizedString] = self.occupationStatus == originalEmployment.occupationStatus ? nil : self.occupationStatus ?? "099" // Unknown occupation status
        
        // Occupation Type
        employmentParams[Employment.kOccupationTpKey.decapitalizedString] = self.occupationTp == originalEmployment.occupationTp ? nil : self.occupationTp ?? "160" // Unknown occupation type
        
        // Occupation Category
        employmentParams[Employment.kOccupationCategoryKey.decapitalizedString] = self.occupationCategory == originalEmployment.occupationCategory ? nil : self.occupationCategory  // There is no default / unknown value
        
        // Contact Methods
        var phonesArray = [[String: AnyObject]]()
        if let businessPhoneParams = self.getEmploymentPhoneWithType(PhoneUsageType.BusinessPhone)?.paramsForSave {
            phonesArray.append(businessPhoneParams)
        }
        if let employerPhoneParams = self.getEmploymentPhoneWithType(PhoneUsageType.EmployerPhone)?.paramsForSave {
            phonesArray.append(employerPhoneParams)
        }
        
        if !phonesArray.isEmpty {
            let phonesParams = [Employment.kPhoneKey.decapitalizedString: phonesArray]
            employmentParams[Employment.kContactMethodsKey.decapitalizedString] = phonesParams
        }
        
        // Addresses
        var addressesArray = [[String: AnyObject]]()
        if let businessEmployerAddresssParams = self.getEmploymentAddressWithType(AddressUsageType.BusinessEmployerAddress)?.paramsForSave {
            addressesArray.append(businessEmployerAddresssParams)
        }
        
        if !addressesArray.isEmpty {
            let addressParams = [Employment.kAddressKey.decapitalizedString: addressesArray]
            employmentParams[Employment.kAddressesKey.decapitalizedString] = addressParams
        }
        
        return employmentParams.isEmpty ? nil : employmentParams
    }
    
}