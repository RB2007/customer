//
//  IdentificationTableViewCell.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-15.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit


class IdentificationTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var viewIdentificationDetails: UIView!
    @IBOutlet weak var labelIdentificationName: UILabel!
    @IBOutlet weak var labelAccepted: UILabel!
    @IBOutlet weak var labelStatusTitle: UILabel!
    @IBOutlet weak var labelIdentificationNumberTitle: UILabel!
    @IBOutlet weak var labelIdentificationNumberValue: UILabel!
    @IBOutlet weak var labelDateCapturedTitle: UILabel!
    @IBOutlet weak var labelDateCapturedValue: UILabel!
    @IBOutlet weak var labelCountryOfIssueTitle: UILabel!
    @IBOutlet weak var labelCountryOfIssueValue: UILabel!
    @IBOutlet weak var labelProvinceTitle: UILabel!
    @IBOutlet weak var labelProvinceValue: UILabel!
    @IBOutlet weak var imageViewChevron: UIImageView!
    @IBOutlet weak var buttonRemove: UIButton!
    
    var identification: Identification?
    weak var customerIdentificationTableViewController: CustomerIdentificationTableViewController?
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = Constants.UBColours.scotiaGrey4
    }
    
    // MARK: - Instance Methods
    func setCellContent(customerIdentification: Identification, isUserAuthenticated: Bool, isEditing: Bool, customerIdentificationTableViewController: CustomerIdentificationTableViewController) {
        
        // Set variables
        self.identification = customerIdentification
        self.customerIdentificationTableViewController = customerIdentificationTableViewController
        
        // Identification Type
        var identificationNameString = customerIdentification.idTp.getName()
        if customerIdentification.idTp.isDescriptionApplicable() {
            if let uIDDescription = customerIdentification.idDescription {
                identificationNameString = identificationNameString + " / " + uIDDescription.capitalizedString
            }
        }
        labelIdentificationName.text = identificationNameString
        
        // ID Accepted
        labelStatusTitle.text = NSLocalizedString("status", comment: "Customer Identification") + ":"
        if let uRefused = customerIdentification.refused {
            labelAccepted.text = uRefused == true ? NSLocalizedString("refused", comment: "Customer Identification") : NSLocalizedString("accepted", comment: "Customer Identification")
            labelAccepted.textColor = uRefused == true ? Constants.UBColours.scotiaRed : UIColor.blackColor()
        } else if customerIdentification.refused == nil && customerIdentification.json == nil {
            labelAccepted.text = NSLocalizedString("new", comment: "Customer Identification")
            labelAccepted.textColor = Constants.UBColours.scotiaRed
        } else {
            labelAccepted.text = NSLocalizedString("unknown_status", comment: "Customer Identification")
            labelAccepted.textColor = Constants.UBColours.scotiaRed
        }
        
        // Card Number
        labelIdentificationNumberTitle.text = NSLocalizedString("identification_number", comment: "Customer Identification")
        if let uIdNumber = customerIdentification.idNumber {
            labelIdentificationNumberValue.text = isUserAuthenticated ? uIdNumber : uIdNumber.masked
        } else {
            labelIdentificationNumberValue.text = NSLocalizedString("unknown_id_number", comment: "Customer Identification")
        }
        
        // Date Captured
        labelDateCapturedTitle.text = NSLocalizedString("date_captured", comment: "Customer Identification")
        if let uDateCaptured = customerIdentification.dateCaptured {
            labelDateCapturedValue.text = Util.formatDate(uDateCaptured) ?? NSLocalizedString("unknown_date", comment: "Customer Identification")
        } else {
            labelDateCapturedValue.text = NSLocalizedString("unknown_date", comment: "Customer Identification")
        }
        
        // Country of Issue
        labelCountryOfIssueTitle.text = NSLocalizedString("country_of_issue", comment: "Customer Identification") + ":"
        if let uCountryOfIssue = customerIdentification.countryOfIssue {
            labelCountryOfIssueValue.text = CustomerMasterData.getValueForKey(uCountryOfIssue, customerMasterDataType: .CountryOfIssueForIdentification)
        } else {
            labelCountryOfIssueValue.text = NSLocalizedString("unknown_country", comment: "Customer Identification")
        }
        
        // Province/ Territory/ State
        labelProvinceTitle.text = NSLocalizedString("province_terr_state", comment: "Customer Identification") + ":"
        if let uProvinceOfIssue = customerIdentification.provinceOfIssue {
            labelProvinceValue.text = CustomerMasterData.getValueForKey(uProvinceOfIssue, customerMasterDataType: .ProvinceOfIssueForIdentification)
        } else {
            labelProvinceValue.text = NSLocalizedString("unknown_province", comment: "Customer Identification")
        }
        
        // Delete Button
        buttonRemove.hidden = !(isEditing && customerIdentification.isDeleteAllowed)
        buttonRemove.setTitle(NSLocalizedString("remove", comment: "Customer Identification"), forState: UIControlState.Normal)
    }
    
    func displayIdentificationDetails(shouldDisplay: Bool) {
        viewIdentificationDetails.hidden = !shouldDisplay
        let rotationAngle = shouldDisplay ? CGFloat(M_PI_2) : CGFloat(2*M_PI)
        UIView.animateWithDuration(0.25) { () -> Void in
            self.imageViewChevron.transform = CGAffineTransformMakeRotation(rotationAngle)
        }
    }
    
    @IBAction func removeButtonPressed() {
        customerIdentificationTableViewController?.deleteIdentificationInCell(self)
    }
}
