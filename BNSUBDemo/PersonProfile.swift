//
//  PersonProfile.swift
//  Customer
//
//  Created by Emad Toukan on 2015-12-11.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

class PersonProfile: CustomerProfile {
    
    // MARK: - JSON Keys
    static let kPersonKey = "Person"
    static let kEmployemntKey = "Employment"
    static let kBirthDateKey = "BirthDate"
    static let kGenderKey = "Gender"
    static let kMaritalStatusKey = "MaritalStatus"
    static let kNumberOfDependantsKey = "NumberOfDependants"
    static let kOtherIncomeKey = "OtherIncome"
    static let kOtherIncomeSourceKey = "OtherIncomeSource"
    
    // MARK: - Contants
    static let kOtherIncomeMaxAmount:Double = 99999
    static let kOtherIncomeMaxCharacterLimit = 5
    static let kOtherIncomeSourceMaxCharacterCount = 25
    
    // MARK: - Properties
    let employment: Employment?
    let birthDate: NSDate?
    var age: Int? {
        if let uBirthDate = birthDate {
            let todaysDate = NSDate()
            let components =  NSCalendar.currentCalendar().components(NSCalendarUnit.Year, fromDate: uBirthDate, toDate: todaysDate, options: NSCalendarOptions.WrapComponents)
            return components.year
        }
        return nil
    }
    let gender: String?
    let maritalStatus: String?
    let numberOfDependents: Int?
    var otherIncome: Double?
    var otherIncomeSource: String?
    
    // MARK: - Init
    
    override init(json: [String: AnyObject]) {
        let partyType = json[CustomerProfile.kPartyTypeKey] as? [String: AnyObject]
        let partyBase = partyType?[CustomerProfile.kPartyBaseKey] as? [String: AnyObject]
        let person = partyBase?[PersonProfile.kPersonKey] as? [String: AnyObject]
        
        if let uEmployment = person?[PersonProfile.kEmployemntKey] as? [String: AnyObject] {
            self.employment = Employment(json: uEmployment)
        } else {
            self.employment = nil
        }
        
        if let birthDateString = person?[PersonProfile.kBirthDateKey] as? String {
            self.birthDate = Util.convertAPIStringDate(birthDateString)
        } else {
            self.birthDate = nil
        }
        
        self.gender = person?[PersonProfile.kGenderKey] as? String
        self.maritalStatus = person?[PersonProfile.kMaritalStatusKey] as? String
        self.numberOfDependents = person?[PersonProfile.kNumberOfDependantsKey] as? Int
        self.otherIncome = person?[PersonProfile.kOtherIncomeKey] as? Double
        self.otherIncomeSource = person?[PersonProfile.kOtherIncomeSourceKey] as? String
        
        super.init(json: json)
        
        // Provide access to the person profile
        self.employment?.personProfile = self
    }
    
    // MARK: - Validation Properties
    override var validEditableFields: Bool {
        return isValidPartyIdStatus &&
            isValidEstablishedBy &&
            isValidGuardianshipPaperFiled &&
            isValidMinorSigningAuthority &&
            (self.getCustomerPhoneWithType(PhoneUsageType.HomePhone)?.isValidNumber ?? false) &&
            (self.getCustomerEmailWithType(EmailUsageType.GeneralEmail)?.isValidEmailAddress ?? true) &&
            (self.isValidOtherIncome ?? true) &&
            (self.isValidOtherIncomeSource ?? true) &&
            (employment?.isValidForSave ?? true)
        // TODO: Need to validate identifications
    }
    
    // Other Income
    var isValidOtherIncome: Bool? {
        
        guard let uOtherIncome = otherIncome else {
            return nil
        }
        
        // Check that the income is >=0 or <=99999
        if uOtherIncome < 0 && uOtherIncome > PersonProfile.kOtherIncomeMaxAmount {
            return false
        }
        
        // Check that the income does not have any decimals
        if uOtherIncome != floor(uOtherIncome) {
            return false
        }
        
        return true
    }
    
    // Other Income Source
    var isValidOtherIncomeSource: Bool? {
        
        guard let uOtherIncomeSource = otherIncomeSource where uOtherIncomeSource.additionalSpacesRemoved.isEmpty == false else {
            return nil
        }
        
        // Other Income Source must be alpha-numeric or a space
        for char in uOtherIncomeSource.lowercaseString.characters where !Util.kAllowedLettersForTextInput.contains(String(char)) && !Util.kAllowedNumbersForTextInput.contains(String(char)) && !Util.kAllowedCharacterForTextInput.contains(String(char)) {
            return false
        }
        
        if uOtherIncomeSource.characters.count > PersonProfile.kOtherIncomeSourceMaxCharacterCount {
            return false
        }
        
        return true
    }
    
    var isValidPartyIdStatus: Bool {
        // PartyIdStatus is mandatory
        guard let uPartyIdStatus = partyIdStatus else {
            return false
        }
        
        // Checking the PartyIdStatus does not exceed its max character limit
        if uPartyIdStatus.rawValue.characters.count > CustomerProfile.kPartyIdStatusMaxCharacterLimit {
            return false
        }
        
        // PartyIdStatus should be alphanumeric
        for char in uPartyIdStatus.rawValue.lowercaseString.characters where !Util.kAllowedLettersForTextInput.contains(String(char)) && !Util.kAllowedCharacterForTextInput.contains(String(char)) && !Util.kAllowedNumbersForTextInput.contains(String(char)) {
            return false
        }
        
        // Check for Age Requirements
        if let uAge = age {
            
            // Check if the minor is less than 12
            if uAge < 12 && uPartyIdStatus != PartyIdStatus.MinorLess12 {
                return false
            }
            
            // Check minor for Canadian Provinces
            if let uProvinceCode = self.getCustomerAddressWithType(AddressUsageType.CustomerResidence)?.region where CanadianProvinceCodes.allCodes().contains(uProvinceCode) {
                // Check minor for the province
                // Minor greater than 12
                if let uProvinceAgeOfMajority = CanadianProvinceCodes(rawValue: uProvinceCode)?.ageOfMajority() {
                    if uAge >= 12 && uAge < uProvinceAgeOfMajority && uPartyIdStatus != PartyIdStatus.MinorGreaterOrEqual12 {
                        return false
                    }
                    
                    // Check adult for the province
                    if uAge > uProvinceAgeOfMajority && uPartyIdStatus != PartyIdStatus.AdultStandard && uPartyIdStatus != PartyIdStatus.AdultNonStandard {
                        return false
                    }
                }
            } else {
                // Check for all other provinces and countries
                // Minnor greater than 12
                if uAge >= 12 && uAge < 18 && uPartyIdStatus != PartyIdStatus.MinorGreaterOrEqual12 {
                    return false
                }
                
                // Adult
                if uAge > 18 && uPartyIdStatus != PartyIdStatus.AdultStandard && uPartyIdStatus != PartyIdStatus.AdultNonStandard {
                    return false
                }
            }
            
        }
        return true
    }
    
    var isValidEstablishedBy: Bool {
        // If the PartyIdStatus is MinorLess12 or MinorGreaterOrEqual12, then EstablishedBy mandatory
        // If the PartyIdStatus is MinorLess12, then the EstablishedBy cannot be Minor
        if let uPartyIdStatus = partyIdStatus where uPartyIdStatus == PartyIdStatus.MinorLess12 || uPartyIdStatus == PartyIdStatus.MinorGreaterOrEqual12 {
            guard let uEstablishedBy = establishedBy where !(uEstablishedBy == IdentificationEstablishedBy.Minor && uPartyIdStatus == PartyIdStatus.MinorLess12) else {
                return false
            }
        }
        return true
    }
    
    var isValidGuardianshipPaperFiled: Bool {
        // If the EstablishedBy is Guardian, then GuardianshipPaperFiled is mandatory
        if let uEstablishedBy = establishedBy where uEstablishedBy == IdentificationEstablishedBy.Guardian {
            guard let uGuardianshipPaperFiled = guardianshipPaperFiled where uGuardianshipPaperFiled == true else {
                return false
            }
        }
        return true
    }
    
    var isValidMinorSigningAuthority: Bool {
        // EstablishedBy is not nil, then MinorSigningAuthroity is mandatory
        if let _ = partyIdStatus, uEstablishedBy = establishedBy where uEstablishedBy == IdentificationEstablishedBy.Parent || uEstablishedBy == IdentificationEstablishedBy.Guardian {
            guard let _ = minorSigningAuthority else {
                return false
            }
        }
        return true
    }
    
    // MARK: - Validation in Range
    static func isValidOtherIncomeInRange(aString: String, range: NSRange) -> Bool {
        
        // Check if the entered string is an int or a backspace
        if  Int(aString) == nil && aString != "" {
            return false
        }
        
        // Check if the string exceeds the allowed string limit
        // Adding 2 due to formatting
        if range.location >= PersonProfile.kOtherIncomeMaxCharacterLimit + 2 && aString != "" {
            return false
        }
        
        return true
    }
    
    static func isValidOtherIncomeSourceInRange(aString: String, range: NSRange) -> Bool {
        // Check that the first entry is not a space
        if range.location == 0 && aString == " " {
            return false
        }
        
        if !Util.kAllowedLettersForTextInput.contains(aString.lowercaseString) && !Util.kAllowedCharacterForTextInput.contains(aString.lowercaseString) && !Util.kAllowedNumbersForTextInput.contains(aString) && aString != "" {
            return false
        }
        
        // Check if the string exceeds the allowed string limit
        if range.location >= PersonProfile.kOtherIncomeSourceMaxCharacterCount && aString != "" {
            return false
        }
        
        return true
    }
    
    // MARK: - Save
    override var paramsForSave: [String: AnyObject]? {
        
        let originalPersonProfile = PersonProfile(json: self.json)
        
        // Phones Params
        var phonesArray = [[String: AnyObject]]()
        if let homePhoneParams = self.getCustomerPhoneWithType(PhoneUsageType.HomePhone)?.paramsForSave {
            phonesArray.append(homePhoneParams)
        }
        
        // Email Params
        var emailArray = [[String: AnyObject]]()
        if let generalEmailParams = self.getCustomerEmailWithType(EmailUsageType.GeneralEmail)?.paramsForSave {
            emailArray.append(generalEmailParams)
        }
        
        // Contact Methods Params
        var contactMethodsParams = [String: AnyObject]()
        contactMethodsParams[PersonProfile.kPhoneKey.decapitalizedString] = phonesArray.isEmpty ? nil : phonesArray
        contactMethodsParams[PersonProfile.kEmailKey.decapitalizedString] = emailArray.isEmpty ? nil : emailArray
        
        // Person Params
        var personParams = [String: AnyObject]()
        personParams[PersonProfile.kOtherIncomeKey.decapitalizedString] = self.otherIncome == originalPersonProfile.otherIncome ? nil : self.otherIncome ?? 0
        if self.otherIncomeSource != originalPersonProfile.otherIncomeSource {
            // Due to a bug in A6, we need to send two fields to the backend in order to update the Gross Monthly Income
            personParams[PersonProfile.kOtherIncomeKey.decapitalizedString] = self.otherIncome ?? 0
            personParams[PersonProfile.kOtherIncomeSourceKey.decapitalizedString] = self.otherIncomeSource ?? ""
        }
        
        // Employment Params
        personParams[PersonProfile.kEmployemntKey.decapitalizedString] = self.employment?.paramsForSave
        
        // Identifications Params
        var identificationsParams = [String: AnyObject]()
        identificationsParams[PersonProfile.kPartyIDStatusKey.decapitalizedString] = self.partyIdStatus == originalPersonProfile.partyIdStatus ? nil : self.partyIdStatus?.rawValue
        identificationsParams[PersonProfile.kEstablishedByKey.decapitalizedString] = self.establishedBy == originalPersonProfile.establishedBy ? nil : self.establishedBy?.rawValue
        identificationsParams[PersonProfile.kGuardianshipPaperFiledKey.decapitalizedString] = self.guardianshipPaperFiled == originalPersonProfile.guardianshipPaperFiled ? nil : self.guardianshipPaperFiled
        identificationsParams[PersonProfile.kMinorSigningAuthorityKey.decapitalizedString] = self.minorSigningAuthority == originalPersonProfile.minorSigningAuthority ? nil : minorSigningAuthority
        
        // Identification Params
        var identificationParams = [[String: AnyObject]]()
        if let uIdentifications = identifications {
            for identification in uIdentifications {
                if let uParamsForSave = identification.paramsForSave {
                    identificationParams.append(uParamsForSave)
                }
            }
        }
        
        if !identificationParams.isEmpty {
            identificationsParams[PersonProfile.kIdentificationKey.decapitalizedString] = identificationParams
            identificationsParams[PersonProfile.kPartyIDStatusKey.decapitalizedString] = self.partyIdStatus?.rawValue
        }
        
        // Party Base Params
        var partyBaseParams = [String: AnyObject]()
        partyBaseParams[PersonProfile.kPersonKey.decapitalizedString] = personParams.isEmpty ? nil : personParams
        partyBaseParams[PersonProfile.kContactMethodsKey.decapitalizedString] = contactMethodsParams.isEmpty ? nil : contactMethodsParams
        partyBaseParams[PersonProfile.kIdentificationsKey.decapitalizedString] = identificationsParams.isEmpty ? nil : identificationsParams
        
        // Return nil if there are no updates
        if partyBaseParams.isEmpty {
            return nil
        }
        
        // Save Params
        var params = [String: AnyObject]()
        params[PersonProfile.kCidKey.decapitalizedString] = self.cid
        params[PersonProfile.kPartyBaseKey.decapitalizedString] = partyBaseParams
        
        return params
    }
}