//
//  PINManagementSecurity.swift
//  Customer
//
//  Created by Annie Lo on 2015-10-19.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation
import Security

/// PIN management scurity class
class PINManagementSecurity {
    
    /**
     Descrypt data with the private key
     
     - parameter encryptedData: <#encryptedData description#>
     
     - returns: <#return value description#>
     */
    static func decryptWithPrivateKey(encryptedData: NSData) -> String? {
        
        if let securityKey = PINManagementSecurity.obtainKey() {
            let cipherLen = encryptedData.length
            let encryptedKeyData = UnsafePointer<UInt8>(encryptedData.bytes)
            
            var plainLen = SecKeyGetBlockSize(securityKey)
            let decryptedKey = NSMutableData(length: plainLen)
            let plain = UnsafeMutablePointer<UInt8>(decryptedKey!.mutableBytes)
            let keyStatus = SecKeyDecrypt(securityKey, SecPadding.OAEP, encryptedKeyData, cipherLen, plain, &plainLen)
            if keyStatus == noErr {
                let decryptedNSData  = NSData(bytes: plain, length: plainLen)
                let decryptedHexString = PINManagementUtilities.base64ToHexString(PINManagementUtilities.dataToBase64(decryptedNSData)!)
                return decryptedHexString
            }
        }
        return nil
    }
    
    /**
     Get p12 from VerifoneDevice model, import and retrieve its identities and certificates in a PKCS #12-formatted blob
     
     - returns: returns key used to decrypt the message.  Return nil otherwise.
     */
    static func obtainKey() -> SecKeyRef? {
        var privateKey: SecKeyRef? = nil
        
        if (VerifoneDevice.pkcs12 != nil && VerifoneDevice.p12Code != nil) {
            let optionDict: NSMutableDictionary = NSMutableDictionary()
            optionDict.setObject(VerifoneDevice.p12Code!, forKey: kSecImportExportPassphrase as String)
            var citems: CFArray? = nil
            
            
            withUnsafeMutablePointer(&citems, { citemsPtr in
                
                let certFile = PINManagementUtilities.fromHexString(VerifoneDevice.pkcs12!)
                let result = SecPKCS12Import(certFile, optionDict, citemsPtr)
                
                if (result == errSecSuccess) {
                    let unmanagedObject = CFArrayGetValueAtIndex(citems, 0)
                    let dict = unsafeBitCast(unmanagedObject, NSDictionary.self)
                    let identity = dict[kSecImportItemIdentity as String] as! SecIdentity
                    
                    withUnsafeMutablePointer(&privateKey, { privateKeyPtr in
                        SecIdentityCopyPrivateKey(identity, privateKeyPtr)
                    })
                    
                }
            } )
        }
    
        return privateKey
    }
    
    /**
     Load PKI key
     
     - parameter pkP7s:   signature files for public key
     - parameter pkIdP7s: signature files for cert id
     - parameter cert:    public key
     - parameter certId:  cert id
     
     - returns: return true if loading is succesfully.  Otherwise, return false
     */
    static func loadPKIKeys(pkP7s: NSData, pkIdP7s: NSData, cert: String, certId: String) -> Bool {
        //set encryption mode
        
        VerifoneDevice.pinpad()?.selectEncryptionMode(EncryptionMode_PKI)
        
        /// Load signature of PKI Public Key File
        var e12 = VerifoneDevice.pinpad()?.E12(1, p75Data: pkP7s)
        if (e12 != 0) {
            return false
        }
        
        /**
        *  Load signature for Key ID File
        *
        *  @return return true if E12 is excited successfully.  Otherwise, return false
        */
        e12 = VerifoneDevice.pinpad()?.E12(2, p75Data: pkIdP7s)
        if (e12 != 0) {
            return false
        }
        
        /// E08 load both public key and its cert ID
        let e08 = VerifoneDevice.pinpad()?.E08_RSA(cert, publicKeyID: certId)
        if (e08 != 0) {
            return false
        }
        
        /// Load PKI encryption mode again.
        VerifoneDevice.pinpad()?.selectEncryptionMode(EncryptionMode_PKI)
        return true
    }
    
}