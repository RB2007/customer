//
//  PINManagementAction.swift
//  BNSUBDemo
//
//  Created by Annie Lo on 2015-09-01.
//  Copyright (c) 2015 ScotiaBank. All rights reserved.
//

import Foundation

/// A series of VeriFone related commands that is mostly used for pin pad initialization, and transaction to complete EMV flow.
class PINManagementAction {
    
    /**
     CVN card type
     
     - CVNAmex:             Amex
     - CVNWiredInteracVisa: Interac Visa
     - CVNContactless:      Contactless
     - CVNUnknown:          unknown type
     */
    enum CVNCardType {
        /// Amex
        case CVNAmex
        
        /// Interac Visa
        case CVNWiredInteracVisa
        
        /// Contactless
        case CVNContactless
        
        /// Unknown type
        case CVNUnknown
    }
    
    /// Current environemnt the iPad is set to
    static let environmentDev = "DEV"
    
    /**
     returns true if pin counter is exceeded
     Look for the 5th byte of the TVR.  If the byte is 2, 4, 6, 8, A, and E, then pin counter exeeds.
     
     - returns: returns true terminal verification results shows that the pin counter exceeds.  Otherwise, returns false.
     */
    static func pinExceeded() -> Bool {
        var tvr:NSData? = nil
        
        if let emvC32Tags = VerifoneDevice.pinpad()?.vfiEMVAuthorization.emvTags {
            
            tvr = emvC32Tags["95"] as? NSData
        }
        
        let tvrString = PINManagementUtilities.toHexString(tvr!)
        var tvrPINTryLimitExceedBit = tvrString.substringWithRange(Range<String.Index>(start: tvrString.startIndex.advancedBy(4), end: tvrString.startIndex.advancedBy(5)))
        tvrPINTryLimitExceedBit = tvrPINTryLimitExceedBit.uppercaseString
        
        if (tvrPINTryLimitExceedBit == "2" || tvrPINTryLimitExceedBit == "4" || tvrPINTryLimitExceedBit == "6"
         || tvrPINTryLimitExceedBit == "8" || tvrPINTryLimitExceedBit == "A" || tvrPINTryLimitExceedBit == "C"
         || tvrPINTryLimitExceedBit == "E") {
                return true
        } else {
            return false
        }
    }
    
    /**
     Clean pinpad
     
     - parameter instructionLabel: The description used to be displayed on the customer app
     
     - returns: the response code from the pinpad cleaning command
     */
    static func cleanPinpad(instructionLabel : UILabel) -> Bool {
        updateStatus(instructionLabel, text: NSLocalizedString("cleaning_table", comment: "PIN Management Action"), percent: 1)
        let response = VerifoneDevice.pinpad()?.D12()
        return response == 0
    }

    /**
     Create EST table
     
     - parameter instructionLabel: The status of what the message on the iPad should be.
     
     - returns: return true if run successfully.  Otherwise returns false.
     */
    static func createEST(instructionLabel : UILabel) -> Bool {
        var callResponse : Int32? = nil
        
        //D16 - Create EST table
        updateStatus(instructionLabel, text: NSLocalizedString("create_est_table", comment: "PIN Management Action"), percent: 10)
        callResponse = VerifoneDevice.pinpad()?.D16(1, numRecords: 2)
        
        //D15 - Add VISA to EST table
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("add_entry_to_visa", comment: "PIN Management Action"), percent: 15)
            sleep(PINManagementConstants.Time.sleepingTime)
            callResponse = VerifoneDevice.pinpad()?.D15(0, numberTrans: 0, scheme: "VISA", regId: "A000000003", CSNList: "A000000003.CSN", emvTableRecord: 0, CSNFile: "")
        } else {
            reportError("D16", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.ESTTableError.rawValue)" + "\n" + PMErrorCode.ESTTableError.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        //D15 - Add INTERAC to EST table
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("add_entry_to_interac", comment: "PIN Management Actoin"), percent: 20)
            sleep(PINManagementConstants.Time.sleepingTime)
            callResponse = VerifoneDevice.pinpad()?.D15(1, numberTrans: 0, scheme: "INTERAC", regId: "A000000277", CSNList: "A000000277.CSN", emvTableRecord: 0, CSNFile: "")
        } else {
            reportError("D15", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.VISAEntryFailed.rawValue)" + "\n" + PMErrorCode.VISAEntryFailed.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        //D13 - Update AID records - VISA
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("update_visa_aid", comment: "PIN Management Action"), percent: 25)
            
            sleep(PINManagementConstants.Time.sleepingTime)
            let visaAID = "A0000000031010" //scotia visa
            let visaTermAVN = "008C"
            callResponse = VerifoneDevice.pinpad()?.D13(0, AID: 1, partialNameMatch: false, supportedAID: visaAID.dataUsingEncoding(NSUTF8StringEncoding), termAVN: visaTermAVN.dataUsingEncoding(NSUTF8StringEncoding), term2ndAVN: visaTermAVN.dataUsingEncoding(NSUTF8StringEncoding), recommendedAIDName: "VISA")
        } else {
            reportError("D13", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.VISAEntryFailed.rawValue)" + "\n" + PMErrorCode.VISAEntryFailed.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        //D13 - Update AID records - INTERAC
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("update_interac_aid", comment: "PIN Management Action"), percent: 30)
            sleep(PINManagementConstants.Time.sleepingTime)
            let interacAID = "A0000002771010" //scotiacard (A0000002771010)
            let interacTermAVN = "0001"
            callResponse = VerifoneDevice.pinpad()?.D13(1, AID: 1, partialNameMatch: false, supportedAID: interacAID.dataUsingEncoding(NSUTF8StringEncoding), termAVN: interacTermAVN.dataUsingEncoding(NSUTF8StringEncoding), term2ndAVN: interacTermAVN.dataUsingEncoding(NSUTF8StringEncoding), recommendedAIDName: "")
        } else {
            reportError("D13", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.UpdateInteracFailed.rawValue)" + "\n" + PMErrorCode.UpdateInteracFailed.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        //D11 - Create CAPK key files
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("capk_visa_key", comment: "PIN Management Action"), percent: 35)
            sleep(PINManagementConstants.Time.sleepingTime)
            
            //Visa
            var visaCapk = APIManagement.sharedInstance.currentEnv == PINManagementAction.environmentDev ?
                PINManagementConstants.CAPK.visaTestCAPK : PINManagementConstants.CAPK.visaProductionCAPK
            for i in 0..<visaCapk.count {
                let capk = visaCapk[i]
                callResponse = VerifoneDevice.pinpad()?.D11a(capk.rid!, pkIndex: capk.pkiIndex!, modulus: capk.modulus!, exponent: capk.exponent!, checksum: capk.checksum!, expiryDate: capk.expiryDate)
                if (callResponse == nil || Int(callResponse!) != 0) {
                    reportError("D11", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.VisaLoadFailed.rawValue)" + "\n" + PMErrorCode.VisaLoadFailed.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
                    return false
                }
                
                if (i != visaCapk.count-1) {
                    sleep(PINManagementConstants.Time.sleepingTime)
                }
            }
            
            updateStatus(instructionLabel, text: NSLocalizedString("capk_interac_key", comment: "PIN Management Action"), percent: 45)
            //Interac
            var interacCapk = APIManagement.sharedInstance.currentEnv == PINManagementAction.environmentDev ?
                PINManagementConstants.CAPK.interacTestCAPK : PINManagementConstants.CAPK.interacProductionCAPK
            for i in 0..<interacCapk.count {
                let capk = interacCapk[i]
                callResponse = VerifoneDevice.pinpad()?.D11a(capk.rid!, pkIndex: capk.pkiIndex!, modulus: capk.modulus!, exponent: capk.exponent!, checksum: capk.checksum!, expiryDate: capk.expiryDate)
                if (callResponse == nil || Int(callResponse!) != 0) {
                    reportError("D11", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.VISACAPKFailed.rawValue)" + "\n" + PMErrorCode.VISACAPKFailed.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
                    return false
                }
                
                if (i != interacCapk.count-1) {
                    sleep(PINManagementConstants.Time.sleepingTime)
                }
            }
            
        } else {
            reportError("D13", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.UpdateInteracFailed.rawValue)" + "\n" + PMErrorCode.UpdateInteracFailed.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        return true
    }
    
    
    /**
     Create MVT table
     
     - parameter instructionLabel: The status of what the message on the iPad should be.
     
     - returns: return true if run successfully.  Otherwise returns false.
     */
    static func createMVT(instructionLabel : UILabel) -> Bool {
        var callResponse : Int32? = nil
        
        //D16 - Create MVT table
        updateStatus(instructionLabel, text: NSLocalizedString("create_mvt_table", comment: "PIN Management Action"), percent: 55)
        callResponse = VerifoneDevice.pinpad()?.D16(2, numRecords: 2)
        
        //D14 - Update EMV config record in MVT table - Visa
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("config_visa_emv", comment: "PIN Management Action"), percent: 60)
            sleep(PINManagementConstants.Time.sleepingTime)
            callResponse = VerifoneDevice.pinpad()?.D14(0, tableData: PINManagementConstants.EMVConfiguration.emvVisaConfig.emv)
        } else {
            reportError("D16", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.MVTTableError.rawValue)" + "\n" + PMErrorCode.MVTTableError.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        //D14 - Update EMV config record in MVT table - Interac
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("config_interac_emv", comment: "PIN Management Action"), percent: 65)
            sleep(PINManagementConstants.Time.sleepingTime)
            callResponse = VerifoneDevice.pinpad()?.D14(0, tableData: PINManagementConstants.EMVConfiguration.emvVisaConfig.emv)
        } else {
            reportError("D14", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.VisaConfigError.rawValue)" + "\n" + PMErrorCode.VisaConfigError.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        if (callResponse != 0) {
            reportError("D14", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.InteracConfigError.rawValue)" + "\n" + PMErrorCode.InteracConfigError.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        return true
    }
    
    /**
     Create EEST table
     
     - parameter instructionLabel: The status of what the message on the iPad should be.
     
     - returns: return true if run successfully.  Otherwise returns false.
     */
    static func createEEST(instructionLabel : UILabel) -> Bool {
        var callResponse : Int32? = nil
        
        //D16 - Create MVT table
        updateStatus(instructionLabel, text: NSLocalizedString("create_eest_table", comment: "PIN Management Action"), percent: 70)
        callResponse = VerifoneDevice.pinpad()?.D16(3, numRecords: 2)
        
        //D17 - Update EMV config record in EEST table - Visa
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("update_visa_eest", comment: "PIN Management Action"), percent: 75)
            sleep(PINManagementConstants.Time.sleepingTime)
            callResponse = VerifoneDevice.pinpad()?.D17(0, emvRecord: 0, AID: "A0000000031010", maxAIDLen: 16, appFlow: 6, partialName: true, acctSelect: false, enableCtls: true, transScheme: 192)
        } else {
            reportError("D16", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.EESTTableError.rawValue)" + "\n" + PMErrorCode.EESTTableError.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        //D17 - Update EMV config record in EEST table - Visa
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("update_interac_eest", comment: "PIN Management Action"), percent: 80)
            sleep(PINManagementConstants.Time.sleepingTime)
            callResponse = VerifoneDevice.pinpad()?.D17(1, emvRecord: 1, AID: "A0000002771010", maxAIDLen: 16, appFlow: 21, partialName: true, acctSelect: false, enableCtls: true, transScheme: 128)
        } else {
            reportError("D17", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.UpdateVISAEESTError.rawValue)" + "\n" + PMErrorCode.UpdateVISAEESTError.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        
        if (callResponse != 0) {
            reportError("D17", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.UpdateInteracEESTError.rawValue)" + "\n" + PMErrorCode.UpdateInteracEESTError.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        return true
    }
    
    /**
     Create EMVT table
     
     - parameter instructionLabel: The status of what the message on the iPad should be.
     
     - returns: return true if run successfully.  Otherwise returns false.
     */
    static func createEMVT(instructionLabel : UILabel) -> Bool {
        var callResponse : Int32? = nil
        
        //D16 - Create EMVT table
        updateStatus(instructionLabel, text: NSLocalizedString("create_emvt_table", comment: "PIN Management Action"), percent: 85)
        callResponse = VerifoneDevice.pinpad()?.D16(4, numRecords: 2)
        
        //D19 - Update EMV config record in EMVT table - Visa
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("update_visa_emvt", comment: "PIN Management Action"), percent: 90)
            sleep(PINManagementConstants.Time.sleepingTime)
            let visaCtlsTACDefault = "CC50848800"
            let visaCtlsTACDenial = "0000000000"
            let visaCtlsTACOnline = "CC50848800"
            let visaVisaTTQ = "20800000" //B2"
            callResponse = VerifoneDevice.pinpad()?.D19(0, mvtIndex: 0, groupName: "VISA", ctlsFloorLimit: 0000, ctlsCVMLimit: 25.00, ctlsTranLimit: 50.00, ctlsTacDefault: visaCtlsTACDefault.dataUsingEncoding(NSUTF8StringEncoding), ctlsTacDenial: visaCtlsTACDenial.dataUsingEncoding(NSUTF8StringEncoding), ctlsTacOnline: visaCtlsTACOnline.dataUsingEncoding(NSUTF8StringEncoding), visaTTQ: visaVisaTTQ.dataUsingEncoding(NSUTF8StringEncoding), termCapabilities: "E008C8", addTermCapabilities: "6000003000", ctlsCountry: "124", ctlsCurrency: "124")
        } else {
            reportError("D16", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.EMVTTableCreateError.rawValue)" + "\n" + PMErrorCode.EMVTTableCreateError.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        //D19 - Update EMV config record in EMVT table - Visa
        if (callResponse == 0) {
            updateStatus(instructionLabel, text: NSLocalizedString("update_interac_emvt", comment: "PIN Management Action"), percent: 95)
            sleep(PINManagementConstants.Time.sleepingTime)
            let interacCtlsTACDefault = "FC78BCA000" //"FC78BCA000"
            let interacCtlsTACDenial = "0000000000"
            let interacCtlsTACOnline = "FC58BCF800" //"FC58BCF800"
            let interacVisaTTQ = "20800000" //actually for visa, but it doesn't hurt if we put it here
            callResponse = VerifoneDevice.pinpad()?.D19(1, mvtIndex: 1, groupName: "INTERAC", ctlsFloorLimit: 0000, ctlsCVMLimit: 25.00, ctlsTranLimit: 50.00, ctlsTacDefault: interacCtlsTACDefault.dataUsingEncoding(NSUTF8StringEncoding), ctlsTacDenial: interacCtlsTACDenial.dataUsingEncoding(NSUTF8StringEncoding), ctlsTacOnline: interacCtlsTACOnline.dataUsingEncoding(NSUTF8StringEncoding), visaTTQ: interacVisaTTQ.dataUsingEncoding(NSUTF8StringEncoding), termCapabilities: "E008C8", addTermCapabilities: "6000003000", ctlsCountry: "124", ctlsCurrency: "124")
        } else {
            reportError("D19", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.VISAEMVTRecordError.rawValue)" + "\n" + PMErrorCode.VISAEMVTRecordError.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        
        
        if (callResponse != 0) {
            reportError("D19", commandDescription: "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.InteracEMVTRecordError.rawValue)" + "\n" + PMErrorCode.InteracEMVTRecordError.localizedDescription(), response: callResponse!, instructionLabel: instructionLabel)
            return false
        }
        updateStatus(instructionLabel, text: "", percent: 100)
        return true
    }
    
    /**
     Get CVN card type
     
     - returns: returns the card type enum (eg. Amex, Wired Interac Visa, Contactless, Unknown)
     */
    static func getCVNCardType() -> CVNCardType?  {
        var cardType: CVNCardType? = nil
        var iad:NSData? = nil
        if let emvC36Tags = VerifoneDevice.pinpad()?.vfiEMVTags.emvTags {
            iad = emvC36Tags["9F10"] as? NSData
            let iadString = PINManagementUtilities.toHexString(iad!)
            
            let cardTypeCVNBit = iadString.substringWithRange(Range<String.Index>(start: iadString.startIndex.advancedBy(4), end: iadString.startIndex.advancedBy(6))).lowercaseString
            
            switch cardTypeCVNBit {
            case "01":
                cardType = CVNCardType.CVNAmex
            case "0a":
                cardType = CVNCardType.CVNWiredInteracVisa
            case "85":
                cardType = CVNCardType.CVNContactless
            default:
                cardType = CVNCardType.CVNUnknown
            }
        }
        
        return cardType
        
        
    }
    
    /**
     Load Issuer script into the card using the response from the mid-tier request
     
     - parameter verifyPinOfflineResponse: the response of the verify pin offline request
     */
    static func loadIssuerScript(verifyPinOfflineResponse: NSDictionary) {
        if let scrRec = verifyPinOfflineResponse["scrRec"] as? NSDictionary {
            if let hdr = scrRec["hdr"] as? String{
                let hdrFirstByte = hdr.substringWithRange(Range<String.Index>(start: hdr.startIndex.advancedBy(0), end: hdr.startIndex.advancedBy(2)))
                
                let scriptId = Int(hdrFirstByte)
                
                if (scriptId == 71 || scriptId == 72) {
                    if let commData = scrRec["commData"] as? String{
                        if (!commData.hasPrefix("00")) {
                            
                            let commDataLengthByte = hdr.substringWithRange(Range<String.Index>(start: hdr.startIndex.advancedBy(2), end: hdr.startIndex.advancedBy(4)))
                            
                            let commDataLength = Int32(PINManagementUtilities.hexToDecimal(commDataLengthByte)) + 2 // the first two bytes that include the header name and the length size
                            
                            let cleanedUpData = PINManagementUtilities.truncate(commData, length: Int(commDataLength)*2, trailing: nil)
                            
                            VerifoneDevice.pinpad()?.C25(Int32(scriptId!), clear: true, numScripts: 1, length: commDataLength, data: PINManagementUtilities.fromHexString(cleanedUpData))
                        }
                    }
                }
            }
        }
    }
    
    
    /**
     Complete EMV transaction
     
     - parameter hasError:                 Boolean of rather or not there is an error
     - parameter verifyPinOfflineResponse: the NSDictionary response from verify pin offline
     
     - returns: <#return value description#>
     */
    static func completeEMVTransaction(hasError: Bool, verifyPinOfflineResponse: NSDictionary) -> Bool {
        if let hostauthDetails = verifyPinOfflineResponse["hostauthDetails"] as? NSDictionary {
            let haArc = hostauthDetails["haArc"] as? String
            let haArpcRc = hostauthDetails["haArpcRc"] as? String
            var issuerAuthData = hostauthDetails["haArpc"] as? String
            let cVNCardType = PINManagementAction.getCVNCardType()
            var arcArpcRc: String? = nil
            
            switch cVNCardType! {
            case PINManagementAction.CVNCardType.CVNContactless:
                arcArpcRc = haArpcRc
            case PINManagementAction.CVNCardType.CVNWiredInteracVisa:
                arcArpcRc = haArc
            case PINManagementAction.CVNCardType.CVNAmex:
                arcArpcRc = haArc
            default: break
            }
            
            if (arcArpcRc != nil) {
                issuerAuthData = issuerAuthData! + arcArpcRc!
            }
            
            let emvTags  = NSMutableDictionary.init(capacity: 0)
            emvTags.setObject(arcArpcRc!, forKey: "8A")
            emvTags.setObject("123456", forKey: "89")
            emvTags.setObject(issuerAuthData!, forKey: "91")
            
            var hostDecision = 0
            if (hasError) {
                hostDecision = 1
            }
            
            return VerifoneDevice.pinpad()?.C34(Int32(hostDecision), displayResult: false, hostTags: emvTags) == 0
        }
        return false
    }
    
    /**
     Set error to pinpad instruction screen with any errors that need to be displayed.
     
     - parameter command:            The name of the command used
     - parameter commandDescription: The description of the command used
     - parameter response:           The error of the command
     - parameter instructionLabel:   The UILabel of the instruction UI component
     */
    static func reportError(command: String, commandDescription: String, response: Int32, instructionLabel :UILabel){
        instructionLabel.text = NSLocalizedString("command", comment: "PIN Management Action") + ": \(command) " + NSLocalizedString("error", comment: "") + ": \(commandDescription) " + NSLocalizedString("code", comment: "PIN Management Action") + ": \(response)\n"
    }
    
    /**
     Update the status of the UILabel instruction UI component
     
     - parameter instructionLabel: The UILabel of the instruction label
     - parameter text:             The text description of the text
     - parameter percent:          the progress percentage that should be shown on the UILabel
     */
    static func updateStatus(instructionLabel: UILabel, text: String, percent: Int) {
        let lineBreak = text.characters.count > 0 ? "\n" : ""
        instructionLabel.text = "\(text)\(lineBreak)\(percent)%"
    }
}