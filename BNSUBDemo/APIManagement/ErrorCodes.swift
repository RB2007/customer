//
//  ErrorCodes.swift
//  Customer
//
//  Created by Harish Mullapudi on 2015-11-04.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

protocol CustomerAppErrorCode {
    var getRawValue: Int {get}
    func localizedDescription() -> String
}

enum CXErrorCode: Int, CustomerAppErrorCode {
    case
    //SSO ERROR CODES
    APIUrlError = 100,
    EnvironmentError = 101,
    AuthenticationError = 102,
    OfficerInfoError = 103,
    MasterDataError = 104,
    
    //CUSTOMER UPDATE ERROR CODES
    CustomerUpdateError = 105,
    
    //CUSTOMER LOCATE ERROR CODES
    CustomerLocateError = 106,
    CustomerBusinessLocateError = 107,
    CustomerProductLocateError = 108,
    CustomerLocateByCustomerIdError = 109,
    
    //GET TRANSACTION HISTORY ERROR CODES
    TransactionHistoryRetrievalError = 110,
    
    //OTHER ERROR CODES
    APIVersionMisMatch = 150,
    
    NOInternetError = 1000,
    UnknownError = 9999,
    
    //PINMANAGEMENT ERROR CODES
    PinPadCardStatusError = 300,
    InvalidCustomeressionDefaultError = 301,
    InvalidCardError = 302,
    AccountNumberError = 303,
    ValidCustomerSessionWithWarnings = 304,
    PinpadConfigError = 305,
    CannotFindRecordError = 306,
    FindFieldEmptyError = 307,
    RequestKeyError = 308
    
    var getRawValue: Int {
        get {
            return self.rawValue
        }
    }
    
    func localizedDescription() -> String {
        switch (self) {
            
        case .AuthenticationError:
            return NSLocalizedString("unable_to_authenticate", comment: "Error")
        case .NOInternetError:
            return NSLocalizedString("network_unreachable", comment: "Error")
        case .UnknownError:
            return NSLocalizedString("unexpected_error", comment: "Error")
        case .CustomerUpdateError:
            return NSLocalizedString("unable_to_update_customer_information", comment: "Error")
        case .APIVersionMisMatch:
            return NSLocalizedString("ensure_you_have_latest_version_of_app", comment: "Error")
        case .CustomerLocateError:
            return NSLocalizedString("unable_to_locate_customer", comment: "Error")
        case .CustomerBusinessLocateError:
            return NSLocalizedString("unable_to_locate_business_customer", comment: "Error")
        case .CustomerProductLocateError:
            return NSLocalizedString("unable_to_locate_customer_by_product", comment: "Error")
        case .CustomerLocateByCustomerIdError:
            return NSLocalizedString("unable_to_locate_customer_by_id", comment: "Error")
            
            //PINMANAMENT ERROR DESCRIPTION:
        case .PinPadCardStatusError:
            return NSLocalizedString("card_statused", comment: "Error")
        case .InvalidCustomeressionDefaultError:
            return NSLocalizedString("there_is_an_unexpected_error", comment: "Error")
        case .InvalidCardError:
            return NSLocalizedString("please_use_scotiacard", comment: "Error")
        case .AccountNumberError:
            return NSLocalizedString("error_message", comment: "Error")
        case .CannotFindRecordError:
            return NSLocalizedString("cannot_find_record_message", comment: "Error")
        case .FindFieldEmptyError:
            return NSLocalizedString("find_field_empty_error", comment: "Error")
        case .RequestKeyError:
            return NSLocalizedString("unable_to_request_key", comment: "Error")
            
        default:
            return NSLocalizedString("default_initialization_error", comment: "Error")
        }
    }
}

enum UIErrorCode : Int, CustomerAppErrorCode {
    case
    InvalidScotiaCard = 200
    
    var getRawValue: Int {
        get {
            return self.rawValue
        }
    }
    
    func localizedDescription() -> String {
        return ""
    }
}

enum PMErrorCode : Int, CustomerAppErrorCode {
    case
    PKILoadError = 100,
    PinpadConnectionError = 101,
    PinpadDecryptionError = 102,
    PinpadQueryCheckError = 103,
    
    RetrievingSerialError = 104,
    ValidateCardUnexpectedError  = 105,
    ValidateCardFetchError  = 106,
    TimeOutError  = 107,
    CardReadError  = 108,
    InvalidDataFormat  = 109,
    PinTryExceededError  = 110,
    PinEmptyError  = 111,
    CardRemovedError  = 112,
    CardTimedOutError  = 113,
    EMVCancelledError  = 114,
    UnhandledC30Error  = 115,
    
    ESTTableError = 116,
    VISAEntryFailed = 117,
    UpdateInteracFailed = 118,
    VisaLoadFailed = 119,
    VISACAPKFailed = 120,
    InteracCAPKFailed = 121,
    MVTTableError = 122,
    VisaConfigError = 123,
    InteracConfigError = 124,
    EESTTableError = 125,
    UpdateVISAEESTError = 126,
    UpdateInteracEESTError = 127,
    EMVTTableCreateError = 128,
    VISAEMVTRecordError = 129,
    InteracEMVTRecordError = 130,
    PinTryExceeded2Error  = 131,
    c30GenericError = 132,
    c32GenericError = 133,
    UpdateVisaFailed = 134,
    XpiVersionCheckFailed = 135,
    VerifyPINOfflineFailed = 136,
    
    C30_C32_error_1 = 200,
    C30_C32_error_2 = 201,
    C30_C32_error_3 = 202,
    C30_C32_error_4 = 203,
    C30_C32_error_5 = 204,
    C30_C32_error_7 = 205,
    C30_C32_error_8 = 206,
    C30_C32_error_9 = 207,
    C30_C32_error_10 = 208,
    C30_C32_error_22 = 209,
    C30_C32_error_23 = 210,
    C30_C32_error_24 = 211,
    C30_C32_error_25 = 212,
    C30_C32_error_42 = 213,
    C30_C32_error_88 = 214
    
    var getRawValue: Int {
        get {
            return self.rawValue
        }
    }
    
    func localizedDescription() -> String {
        
        switch(self) {
        case .PKILoadError:
            return NSLocalizedString("instruction_label_pinpad_offline", comment: "Error")
        case .PinpadConnectionError:
            return NSLocalizedString("instruction_label_pinpad_offline", comment: "Error")
        case .PinpadDecryptionError:
            return NSLocalizedString("instruction_label_pinpad_offline", comment: "Error")
        case .PinpadQueryCheckError:
            return NSLocalizedString("instruction_label_pinpad_offline", comment: "Error")
        case .RetrievingSerialError:
            return NSLocalizedString("cannot_retrieve_pinpad_serial_number", comment: "Error")
        case .ValidateCardUnexpectedError:
            return NSLocalizedString("there_is_an_unexpected_error", comment: "Error")
        case .ValidateCardFetchError:
            return NSLocalizedString("there_is_an_unexpected_error", comment: "Error")
        case .TimeOutError:
            return NSLocalizedString("C30_card_read_timeout_error_message", comment: "Error")
        case .c30GenericError:
            return NSLocalizedString("C30_default_card_read_error_message", comment: "Error")
        case .c32GenericError:
            return NSLocalizedString("C32_default_card_read_error_message", comment: "Error")
        case .InvalidDataFormat:
            return NSLocalizedString("C30_C32_error_2", comment: "Error")
        case .PinTryExceededError:
            return NSLocalizedString("instruction_label_pin_try_exceeded_message", comment: "Error")
        case .PinEmptyError:
            return NSLocalizedString("pin_failed_empty_message", comment: "Error")
        case .PinTryExceeded2Error:
            return NSLocalizedString("instruction_label_pin_try_exceeded_message", comment: "Error")
        case .CardRemovedError:
            return NSLocalizedString("instruction_label_card_removed", comment: "Error")
        case .CardTimedOutError:
            return NSLocalizedString("pin_management_emv_timed_out", comment: "Error")
        case .EMVCancelledError:
            return NSLocalizedString("emv_cancelled_message", comment: "Error")
        case .UnhandledC30Error:
            return NSLocalizedString("C30_default_card_read_error_title", comment: "Error")
        case .ESTTableError:
            return NSLocalizedString("create_est_table_error", comment: "Error")
        case .VISAEntryFailed:
            return NSLocalizedString("add_entry_to_visa_failed", comment: "Error")
        case .UpdateInteracFailed:
            return NSLocalizedString("update_aid_record_for_interac_failed", comment: "Error")
        case .UpdateVisaFailed:
            return NSLocalizedString("update_aid_record_for_visa_failed", comment: "Error")
        case .VisaLoadFailed:
            return NSLocalizedString("load_visa_capk_key_file_failed", comment: "Error")
        case .VISACAPKFailed:
            return NSLocalizedString("load_interac_capk_key_file_failed", comment: "Error")
        case .InteracCAPKFailed:
            return NSLocalizedString("update_aid_record_for_interac_failed", comment: "Error")
        case .MVTTableError:
            return NSLocalizedString("create_mvt_table_error", comment: "Error")
        case .VisaConfigError:
            return NSLocalizedString("add_visa_emv_configuration_error", comment: "Error")
        case .InteracConfigError:
            return NSLocalizedString("add_interac_emv_configuration_error", comment: "Error")
        case .EESTTableError:
            return NSLocalizedString("create_eest_table_error", comment: "Error")
        case .UpdateVISAEESTError:
            return NSLocalizedString("update_eest_visa_error", comment: "Error")
        case .UpdateInteracEESTError:
            return NSLocalizedString("update_eest_interac_error", comment: "Error")
        case .EMVTTableCreateError:
            return NSLocalizedString("create_emvt_table_error", comment: "Error")
        case .VISAEMVTRecordError:
            return NSLocalizedString("update_emvt_table_visa_error", comment: "Error")
        case .InteracEMVTRecordError:
            return NSLocalizedString("update_emvt_table_interac_error", comment: "Error")
        case .XpiVersionCheckFailed:
            return NSLocalizedString("xpi_version_check_failed", comment: "Error")
        case .VerifyPINOfflineFailed:
            return "We are unable to verify the PIN offline."
            
        case .C30_C32_error_1:
            return NSLocalizedString("C30_C32_error_1", comment: "Error")
        case .C30_C32_error_2:
            return NSLocalizedString("C30_C32_error_2", comment: "Error")
        case .C30_C32_error_3:
            return NSLocalizedString("C30_C32_error_3", comment: "Error")
        case .C30_C32_error_4:
            return NSLocalizedString("C30_C32_error_4", comment: "Error")
        case .C30_C32_error_5:
            return NSLocalizedString("C30_C32_error_5", comment: "Error")
        case .C30_C32_error_7:
            return NSLocalizedString("C30_C32_error_7", comment: "Error")
        case .C30_C32_error_8:
            return NSLocalizedString("C30_C32_error_8", comment: "Error")
        case .C30_C32_error_9:
            return NSLocalizedString("C30_C32_error_9", comment: "Error")
        case .C30_C32_error_10:
            return NSLocalizedString("C30_C32_error_10", comment: "Error")
        case .C30_C32_error_22:
            return NSLocalizedString("C30_C32_error_22", comment: "Error")
        case .C30_C32_error_23:
            return NSLocalizedString("C30_C32_error_23", comment: "Error")
        case .C30_C32_error_24:
            return NSLocalizedString("C30_C32_error_24", comment: "Error")
        case .C30_C32_error_25:
            return NSLocalizedString("C30_C32_error_25", comment: "Error")
        case .C30_C32_error_42:
            return NSLocalizedString("C30_C32_error_42", comment: "Error")
        case .C30_C32_error_88:
            return NSLocalizedString("C30_C32_error_88", comment: "Error")
        default:
            return "Unexpected PinPad Error"
        }
    }
    
}

enum Domain: String {
    case
    PinManagement  = "PM",
    CustomerApp = "CX"
}

class ErrorCodes : NSObject {
  
    class func mapPMErorrCodes(inputCode : Int) -> PMErrorCode {
        switch (inputCode) {
        case 1:
            return PMErrorCode.C30_C32_error_1
        case 2:
            return PMErrorCode.C30_C32_error_2
        case 3:
            return PMErrorCode.C30_C32_error_3
        case 4:
            return PMErrorCode.C30_C32_error_4
        case 5:
            return PMErrorCode.C30_C32_error_5
        case 7:
            return PMErrorCode.C30_C32_error_7
        case 8:
            return PMErrorCode.C30_C32_error_8
        case 9:
            return PMErrorCode.C30_C32_error_9
        case 10:
            return PMErrorCode.C30_C32_error_10
        case 22:
            return PMErrorCode.C30_C32_error_22
        case 23:
            return PMErrorCode.C30_C32_error_23
        case 24:
            return PMErrorCode.C30_C32_error_24
        case 25:
            return PMErrorCode.C30_C32_error_25
        case 42:
            return PMErrorCode.C30_C32_error_42
        case 88:
            return PMErrorCode.C30_C32_error_88
        default:
            return PMErrorCode.PKILoadError
        }
    }
    
    class func mapErrorFromDomain (domain : Domain, code: Int) -> String? {
        if domain == Domain.PinManagement {
            let tempString = Domain.PinManagement.rawValue + "\(code)"
            return tempString
        } else {
            let tempString = Domain.CustomerApp.rawValue + "\(code)"
            return tempString
        }
    }
    
}