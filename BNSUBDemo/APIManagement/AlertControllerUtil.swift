//
//  ErrorController.swift
//  Customer
//
//  Created by Harish Mullapudi on 2015-11-09.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

struct AlertControllerUtil {
    
    /**
     Create a UIAlertController from an NSError and add an OK alert action button
     
     - parameter error: Error used to create alert controller from
     
     - returns: UIAlertController with an OK alert action button
     */
    static func createOKAlertController(error: NSError) -> UIAlertController {
        return AlertControllerUtil.createOKAlertController(error, handler: nil)
    }
    
    /**
     Create a UIAlertController from an NSError and add an OK alert action button with a handler
     
     - parameter error:   Error used to create alert controller from
     - parameter handler: Handler to be added on the OK alert action button
     
     - returns: UIAlertController with an OK alert action button that has a handler
     */
    static func createOKAlertController(error: NSError, handler:((UIAlertAction) -> ())?) -> UIAlertController {
        let alertMessage = Util.createErrorMessage(error)
        return AlertControllerUtil.createOKAlertController(alertMessage.title, message: alertMessage.message, handler: handler)
    }
    
    /**
     Create a UIAlertController from a title string and a message string
     
     - parameter title:   Alert title
     - parameter message: Alert message
     
     - returns: UIAlertController with an OK alert action button
     */
    static func createOKAlertController(title: String, message: String) -> UIAlertController {
        return AlertControllerUtil.createOKAlertController(title, message: message, handler: nil)
    }
    
    /**
     Create a UIAlertController from a title string, a message string and a handler
     
     - parameter title:   Alert title
     - parameter message: Alert message
     - parameter handler: Alert action handler
     
     - returns: UIAlertController with an OK alert action button with a handler
     */
    static func createOKAlertController(title: String, message: String, handler: ((UIAlertAction) -> ())?) -> UIAlertController {
        let alertController = UIAlertController(title: title, message: message, preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction = UIAlertAction(title: "OK", style: UIAlertActionStyle.Cancel, handler: handler)
        alertController.addAction(alertAction)
        return alertController
    }
}
