//
//  APINetowrk.swift
//  Customer
//
//  Created by Harish Mullapudi on 2015-11-03.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation
import UIKit
import Alamofire


/**
 Base URL is coming from the AIR WATCH. Initially we are using this url and once the airwatch returns
 the url we replace it with the server url
 */
enum APIURL {
    case currentAuthorized,
    officerProfiles,
    masterData,
    locateByPersonName,
    locateByNonPersonalName,
    locateByProduct,
    customerInquiry,
    customerUpdate,
    accountTransactionHistorySav,
    accountTransactionHistoryCredit,
    pinPadConfig,
    pinPadRequestKeys,
    pinPadValidateCard,
    pinPadvalidatePinOffline
    
    func getURLString(baseURL: String) -> String {
        // Employees
        let employees = "employees/"
        
        // Customers
        let customers = "customers/"
        let locate = "locate/"
        
        // PinPad
        let pinPads = "pinpads/"
        let managePIN = "managepin/"
        
        //transaction history
        let txnhistory = "txnhistory/"
        
        switch self {
        case .currentAuthorized:
            return baseURL + employees + "current-authorized"
        case .officerProfiles:
            return baseURL  + employees + "officer-profiles"
        case .masterData:
            return baseURL + customers + "masterdata"
        case .locateByPersonName:
            return baseURL + customers + locate + "bypersonname"
        case .locateByNonPersonalName:
            return baseURL + customers + locate + "bynonpersonalname"
        case .locateByProduct:
            return baseURL + customers + locate + "byproduct"
        case .customerInquiry:
            return baseURL + customers + "inquiry/"
        case .customerUpdate:
            return baseURL + customers + "update/customer"
        case .accountTransactionHistorySav:
            return baseURL + customers + txnhistory + "depositacct"
        case .accountTransactionHistoryCredit:
            return baseURL + customers + txnhistory + "creditacct"
        case .pinPadValidateCard:
            return baseURL + customers + managePIN + "validate-card"
        case .pinPadvalidatePinOffline:
            return baseURL + customers + managePIN + "validate-pin-offline"
        case .pinPadConfig:
            return baseURL + pinPads + "config"
        case .pinPadRequestKeys:
            return baseURL + pinPads + managePIN + "request-keys"
        }
    }
}


/**
 Splunk parameters
 
 - DirverLicense: Passed when customer is located with the means of their Driver's License
 - Pinned: Passed when the customer is located with the help of pin pad
 - Manual: Passed when a manual search for a customer is performed
 
 */

enum LookUpSubType : String {
    case
    DriverLicense = "DriverLicense",
    Pinned = "Pinned",
    Manual = "ManualSearch"
}


/**
 
 Products codes through which the customer information can be fetched.
 Currently we only support Debit Card. As more cards are supported this enum needs to be updated
 
 - DebitCard: Scotia Card
 */

enum ProductCode : String {
    case
    DebitCard = "CSSSC"
}

/// A Centralized wrapper where all the business logic and data handling is being taken care of
class APIManagement {
    /// Singleton Instance for the object
    static let sharedInstance = APIManagement()
    
    /// Contains the base url and gets replaced with the url from AirWatch
    var apiBaseURL = "https://api.universalbanker-dev.bns/ub/"
    //    var apiBaseURL = "https://api-universalbanker-uat.bns/ub/"
    
    /// API VERSION - This is the version of the mid-tier. Must be equal to the app version
    var API_VERSION = "3.0"
    
    /// Stores current environment off the app.
    var currentEnv:String?
    
    /// Default header for the Almofire
    var defaultHeader: [String: String] {
        get {
            return ["Accept-Language": Util.currentLanguage.rawValue]
        }
    }
    
    // MARK: - Initiate Networking
    
    /**
    Initiates a sequence of networking calls.
    
    Starts with setting the API URL and Verifone p12 code
    
    Upon success it initiates the call for obtaining the SSO Token
    In case of failure it generates an CX100 error
    
    - parameter completion: Block with Data parameter and Error Parameter and a return type of Void
    
    - returns: Void
    */
    func initiateNetworking(completion: (AnyObject?, NSError?) -> ()) {
        
        if let uApiBaseURL = Util.getConfigurationVariableFor(EnvironmentConfigurationKeys.APIBaseURL), uP12Code = Util.getConfigurationVariableFor(EnvironmentConfigurationKeys.P12Code) {
            self.apiBaseURL = uApiBaseURL
            VerifoneDevice.p12Code = uP12Code
            
            // Obtain SSO Token
            self.getSSOToken(completion)
            return
        }
        
        // Generate an CX100 Error
        let error = self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.APIUrlError.rawValue, errorDescription: CXErrorCode.APIUrlError.localizedDescription())
        completion(nil, error)
        
        Analytics.sharedInstance.addEventWithName(SplunkConstants.kApiInitiateNetworkingErrorKey)
    }
    
    /**
     Creates a connection to Airwatch and obtains the SSO Token
     
     - Upon Success: it initiates the download proccess for obtaining the officer profiles
     - Upon Failure: it generates an Authentication Error
     
     - parameter completion: Block with Data parameter and Error Parameter and a return type of Void
     - returns: Void
     
     */
    
    func getSSOToken(completion: (AnyObject?, NSError?) -> ()) {
        
        // URL
        let urlString = APIURL.currentAuthorized.getURLString(self.apiBaseURL)
        print("getSSOToken URL: \(urlString)")
        
        // Request
        Alamofire.request(.GET, urlString, parameters: nil, encoding: ParameterEncoding.URL, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("getSSOToken responseJSON: \(response.result.value) error: \(response.result.error)")
            
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.AuthenticationError)
            var error: NSError?
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                if let responseJSON = responseObject as? [String: String] {
                    
                    // Check for API version
                    if responseJSON["version"] == self.API_VERSION {
                        self.currentEnv = responseJSON["environment"]
                        VerifoneDevice.xpiVersion = responseJSON["xpiVersion"]
                        
                        // Request officer profiles
                        self.getOfficerProfile(completion)
                        return
                    } else {
                        // Version error
                        error = self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.APIVersionMisMatch.rawValue, errorDescription: CXErrorCode.APIVersionMisMatch.localizedDescription())
                    }
                }
            }
            
            if error == nil {
                error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.AuthenticationError.rawValue, errorDescription: CXErrorCode.AuthenticationError.localizedDescription())
            }
            completion(nil, error)
        }
    }
    
    /**
     Function that requests the officer profiles and upon success, it stores the employee object and
     initiates the call for downloading the master data as part of the sequence.
     
     In case of failure it generates an CX103 error
     
     - parameter completion: Block with Data parameter and Error Parameter and a return type of Void
     
     - returns: Void
     */
    
    func getOfficerProfile(completion: (AnyObject?, NSError?) -> ())
    {
        // URL
        let urlString = APIURL.officerProfiles.getURLString(self.apiBaseURL)
        print("getOfficerProfiles URL: \(urlString)")
        
        // Request
        Alamofire.request(.GET, urlString, parameters: nil, encoding: ParameterEncoding.URL, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("getOfficerProfiles response: \(response.result.value) error: \(response.result.error)")
            
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.OfficerInfoError)
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                if let uData = responseObject as? [String: AnyObject] {
                    
                    // Store officer data
                    Officer.saveOfficer(uData)
                    // Get master data
                    self.getMasterData(completion)
                    return
                }
            }
            
            // Generate an error if we couldn't set the Employee object
            let error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.OfficerInfoError.rawValue, errorDescription: CXErrorCode.OfficerInfoError.localizedDescription())
            
            completion(nil, error)
        }
    }
    
    /**
     Downloads Master data
     - Upon success sends information in the completion block
     - Upon failure sends CX104 in the completion block
     
     - parameter completion: Block with Data parameter and Error Parameter and a return type of Void
     */
    func getMasterData(completion:(AnyObject?, NSError?) -> ()) {
        
        // URL
        let urlString = APIURL.masterData.getURLString(self.apiBaseURL)
        print("getMasterData URL: \(urlString)")
        
        // Parameters
        let params = Officer.officerTransitParams
        print("getMasterData params: \(params)")
        
        // Request
        Alamofire.request(.GET, urlString, parameters: nil, encoding: ParameterEncoding.URL, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("getMasterData response: \(response.result.value) error: \(response.result.error)")
            
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.MasterDataError)
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                if let uData = responseObject as? [String: AnyObject] {
                    CustomerMasterData.setCustomerMasterData(uData)
                    completion(nil, nil)
                    
                    return
                }
            }
            
            // Generate an error if we couldn't set the master data
            let error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.MasterDataError.rawValue, errorDescription: CXErrorCode.MasterDataError.localizedDescription())
            
            completion(nil, error)
        }
    }
    
    //MARK: - Fetch Customer Information
    
    /**
    Fetches the customer information by their First and Last Name
    
    - parameter customerName:     Required Field Customer First Name
    - parameter customerLastName: Required Field Customer Last Name
    - parameter dateOfBirth:      Optional Date of Birth Field
    - parameter postalCode:       Optional Postal Code field
    - parameter completion:       Completion Block when network returns a data
    - parameter error:            NSError value when network service fails to return
    */
    func locateCustomerByFirstNameLastName(customerFirstName:String, customerLastName: String, dateOfBirth: String?, postalCode: String?, lookUpSubType: LookUpSubType, completion:(AnyObject?, NSError?) -> ()) {
        
        // URL
        let urlString = APIURL.locateByPersonName.getURLString(self.apiBaseURL)
        print("locateCustomerByFirstNameLastName URL: \(urlString)")
        
        // Parameters
        var params = [String: AnyObject]()
        params["firstName"] = customerFirstName
        params["lastName"] = customerLastName
        params["birthDate"] = dateOfBirth
        params["postalCode"] = postalCode
        
        var traceInfo = Officer.officerTransitParams
        traceInfo["subType"] = lookUpSubType.rawValue
        
        params["traceInfo"] = traceInfo
        print("locateCustomerByFirstNameLastName params: \(params)")
        
        // Request
        Alamofire.request(.POST, urlString, parameters: params, encoding: ParameterEncoding.JSON, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("locateCustomerByFirstNameLastName response: \(response.result.value) error: \(response.result.error)")
            
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.CustomerLocateError)
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                completion(responseObject, nil)
                return
            }
            
            let error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.CustomerLocateError.rawValue, errorDescription: CXErrorCode.CustomerLocateError.localizedDescription())
            
            completion(nil, error)
        }
    }
    
    /**
     Fetches customer information using business name
     
     - parameter businessName: Required - Business Name
     - parameter completion:   Completion block for network call
     - parameter error:        NSError upon network call failure
     */
    func locateCustomerByBusiness(businessName: String, completion:(AnyObject?, NSError?) -> ()) {
        
        // URL
        let urlString = APIURL.locateByNonPersonalName.getURLString(self.apiBaseURL)
        print("locateCustomerByBusiness URL: \(urlString)")
        
        // Parameters
        var params = [String: AnyObject]()
        params["businessName"] = businessName
        params["traceInfo"] = Officer.officerTransitParams
        print("locateCustomerByBusiness params: \(params)")
        
        // Request
        Alamofire.request(.POST, urlString, parameters: params, encoding: ParameterEncoding.JSON, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("locateCustomerByBusiness response: \(response.result.value) error: \(response.result.error)")
            
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.CustomerBusinessLocateError)
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                completion(responseObject, nil)
                return
            }
            
            let error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.CustomerBusinessLocateError.rawValue, errorDescription: CXErrorCode.CustomerBusinessLocateError.localizedDescription())
            
            completion(nil, error)
        }
    }
    
    /**
     Fetches the customer information by Product name
     
     - parameter productName: Product Name String Value
     - parameter completion:  Completion block for network
     - parameter error:       NSError value for network failure
     */
    func locateCustomerByProduct(productNumber:String, lookupSubType: LookUpSubType, completion:(AnyObject?, NSError?) -> ()) {
        
        // URL
        let urlString = APIURL.locateByProduct.getURLString(self.apiBaseURL)
        print("locateCustomerByProduct URL: \(urlString)")
        
        // Parameters
        var params = [String: AnyObject]()
        params["accountNumber"] = productNumber
        
        var traceInfo = Officer.officerTransitParams
        traceInfo["productCode"] = ProductCode.DebitCard.rawValue
        traceInfo["subType"] = lookupSubType.rawValue
        
        params["traceInfo"] = traceInfo
        print("locateCustomerByProduct params: \(params)")
        
        // Request
        Alamofire.request(.POST, urlString, parameters: params, encoding: ParameterEncoding.JSON, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("locateCustomerByProduct response: \(response.result.value) error: \(response.result.error)")
            
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.CustomerProductLocateError)
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                completion(responseObject, nil)
                return
            }
            
            let error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.CustomerProductLocateError.rawValue, errorDescription: CXErrorCode.CustomerProductLocateError.localizedDescription())
            
            completion(nil, error)
        }
    }
    
    /**
     Locates Customer Information by CustomerID
     
     - parameter customerId: ID of the customer -> Required Parameter
     - parameter completion: Complete
     */
    func loadCustomerProfile(customerId:String, completion:(AnyObject?, NSError?) -> ()) {
        
        // URL
        let urlString = APIURL.customerInquiry.getURLString(self.apiBaseURL) + customerId
        print("loadCustomerProfile URL: \(urlString)")
        
        // Parameters
        let params = Officer.officerTransitParams
        print("loadCustomerProfile params: \(params)")
        
        // Request
        Alamofire.request(.GET, urlString, parameters: params, encoding: ParameterEncoding.URL, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("loadCustomerProfile response: \(response.result.value) error: \(response.result.error)")
            
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.CustomerLocateByCustomerIdError)
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                completion(responseObject, nil)
                return
            }
            
            let error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.CustomerLocateByCustomerIdError.rawValue, errorDescription: CXErrorCode.CustomerLocateByCustomerIdError.localizedDescription())
            
            completion(nil, error)
        }
    }
    
    //MARK: - Update Customer Information
    /**
    Updates customer information and sends it webservice
    
    - parameter params:     Paramets to be updated
    - parameter completion: Completion blocks sends nil data and an error code
    */
    func updateCustomerData(var params: [String: AnyObject], completion:(AnyObject?, NSError?) -> ()) {
        
        // URL
        let urlString = APIURL.customerUpdate.getURLString(self.apiBaseURL)
        print("updateCustomerData URL: \(urlString)")
        
        // Parameters
        params["traceInfo"] = Officer.officerTransitParams
        print("updateCustomerData params: \(params)")
        
        // Request
        Alamofire.request(.POST, urlString, parameters: params, encoding: ParameterEncoding.JSON, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("updateCustomerData response: \(response.result.value) error: \(response.result.error)")
            
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.CustomerUpdateError)
            completion(nil, apiResponseHandlerResult.error)
            
        }
    }
    
    func getAccountTransactionHistory(accountNumber: String, accountType: String, dateFrom: String, dateTo: String, completion: (AnyObject?, NSError?) -> ()) {
        
        var urlString = ""
        
        // URL
        
        guard let bankinglist = ScotiaProductTypes.getProductsCodes(ScotiaProductTypes.Banking) else {
            completion(nil, generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.TransactionHistoryRetrievalError.rawValue, errorDescription: CXErrorCode.TransactionHistoryRetrievalError.localizedDescription()))
            return
        }
        
        
        if bankinglist.contains(accountType) {
            urlString = APIURL.accountTransactionHistorySav.getURLString(self.apiBaseURL)
        } else {
            urlString = APIURL.accountTransactionHistoryCredit.getURLString(self.apiBaseURL)
        }
        
        print("getAccountTransactionHistory URL: \(urlString)")
        
        // Parameters
        var params = [String: AnyObject]()
        params["accountNumber"] = accountNumber
        params["accountType"] = accountType
        params["dateFrom"] = dateFrom
        params["dateTo"] = dateTo
        params["traceInfo"] = Officer.officerTransitParams
        print("getAccountTransactionHistory params: \(params)")
        
        // Request
        Alamofire.request(.POST, urlString, parameters: params, encoding: ParameterEncoding.JSON, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("getAccountTransactionHistory response: \(response.result.value) error: \(response.result.error)")
            
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.TransactionHistoryRetrievalError)
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                completion(responseObject, nil)
                return
            }
            completion(nil, apiResponseHandlerResult.error)
            
        }
        
    }
    
    // MARK: - PIN Management API
    
    func pinPadConfig(completion:(AnyObject?, NSError?) -> ()) {
        
        // URL
        let urlString = APIURL.pinPadConfig.getURLString(self.apiBaseURL)
        print("config URL: \(urlString)")
        
        // Parameters
        let params = Officer.officerTransitParams
        print("config params: \(params)")
        
        // Request
        Alamofire.request(.GET, urlString, parameters: nil, encoding: ParameterEncoding.URL, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("config response: \(response.result.value) error: \(response.result.error)")
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.PinpadConfigError)
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                completion(responseObject, nil)
                return
            }
            
            let error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.PinpadConfigError.rawValue, errorDescription: CXErrorCode.PinpadConfigError.localizedDescription())
            
            completion(nil, error)
        }
    }
    
    func requestPINPadKey(serialNumber:String, completion:(AnyObject?, NSError?) -> ()) {
        
        // URL
        let urlString = APIURL.pinPadRequestKeys.getURLString(self.apiBaseURL)
        print("requestKey URL: \(urlString)")
        
        // Parameters
        // TODO: Let's verify this when we get to online PIN management
        var params = [String:AnyObject]()
        params["vcryptSerialNo"] = "0000000\(serialNumber)"
        params["traceInfo"] = Officer.officerTransitParams
        print("requestKey params: \(params)")
        
        // Request
        Alamofire.request(.POST, urlString, parameters: params, encoding: ParameterEncoding.JSON, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("requestKey response: \(response.result.value) error: \(response.result.error)")
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.CustomerApp, errorCode: CXErrorCode.RequestKeyError)
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                completion(responseObject, nil)
                return
            }
            
            let error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.RequestKeyError.rawValue, errorDescription: CXErrorCode.RequestKeyError.localizedDescription())
            
            completion(nil, error)
        }
    }
    
    func validateScotiaCard(cardNumber:String, expiryMonth: String, expiryYear: String, serialNumber: String, completion:(AnyObject?, NSError?) -> ()) {
        
        // URL
        let urlString = APIURL.pinPadValidateCard.getURLString(self.apiBaseURL)
        print("validateCard URL: \(urlString)")
        
        //Parameters
        var params = [String: AnyObject]()
        var cardHolderInfoReq = [String: String]()
        cardHolderInfoReq["cardholderProduct"] = "CSS"
        cardHolderInfoReq["cardholderNumber"] = cardNumber
        params["chipCardholderInfo"] = cardHolderInfoReq
        
        var cryptoBlockReq = [String: String]()
        cryptoBlockReq["cryptoPinblock"] = ""
        cryptoBlockReq["cryptoCardnumber"] = ""
        cryptoBlockReq["cryptoPvn"] = ""
        cryptoBlockReq["cryptoMac"] = ""
        params["chipCardCryptoBlock"] = cryptoBlockReq
        
        var expDate = [String: String]()
        expDate["expDteMm"] = expiryMonth
        expDate["expDteYy"] = expiryYear
        params["chipCardExpiryDate"] = expDate
        
        params["vcryptSerialNo"] = "0000000\(serialNumber)"
        params["traceInfo"] = Officer.officerTransitParams
        print("validateCard params: \(params)")
        
        // Request
        Alamofire.request(.POST, urlString, parameters: params, encoding: ParameterEncoding.JSON, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
            
            print("validateCard response: \(response.result.value) error: \(response.result.error)")
            let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.PinManagement, errorCode: PMErrorCode.ValidateCardUnexpectedError)
            
            if let responseObject = apiResponseHandlerResult.responseObject {
                completion(responseObject, nil)
                return
            }
            
            let error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode.ValidateCardUnexpectedError.rawValue, errorDescription: PMErrorCode.ValidateCardUnexpectedError.localizedDescription())
            
            completion(nil, error)
        }
    }
    
    func validatePinOffline(cardNumber:String, expiryMonth: Int, expiryYear: Int,
        serviceCode: String, appPanSeq: String, appCrypto: String, appAid: String, atc: String, aip: String,
        cdol1Data: String, iadLen: Int, iadData: String, vcryptSerialNo: String,
        completion:(AnyObject?, NSError?) -> ()) {
            
            // URL
            let urlString = APIURL.pinPadvalidatePinOffline.getURLString(self.apiBaseURL)
            print("validatePinOffline URL: \(urlString)")
            
            // Parameters
            var params = [String:AnyObject]()
            
            var cardHolderInfoReq = [String:String]()
            cardHolderInfoReq["cardholderProduct"] = "CSS"
            cardHolderInfoReq["cardholderNumber"] = cardNumber
            params["chipCardholderInfo"] = cardHolderInfoReq
            
            var expDateReq = [String:Int]()
            expDateReq["expDteMm"] = expiryMonth
            expDateReq["expDteYy"] = expiryYear
            params["chipCardExpiryDate"] = expDateReq
            
            params["servCd"] = serviceCode
            params["appPanSeq"] = appPanSeq
            
            var emvDetailsReq = [String:AnyObject]()
            emvDetailsReq["appCrypto"] = appCrypto
            emvDetailsReq["appAid"] = appAid
            emvDetailsReq["atc"] = atc
            emvDetailsReq["aip"] = aip
            
            var cdol1Req = [String:AnyObject]()
            cdol1Req["cdol1Len"] = 0
            cdol1Req["cdol1Data"] = cdol1Data
            emvDetailsReq["cdol1"] = cdol1Req
            
            var iadIssApplnDataReq = [String:AnyObject]()
            iadIssApplnDataReq["iadLen"] = 22
            iadIssApplnDataReq["iadData"] = iadData
            emvDetailsReq["iadIssApplnData"] = iadIssApplnDataReq
            params["emvDetails"] = emvDetailsReq
            
            params["pinVerifiedSw"] = "Y"
            
            params["pinCpyForReplSw"] = ""
            
            params["vcryptSerialNo"] = "0000000\(vcryptSerialNo)"
            
            params["traceInfo"] = Officer.officerTransitParams
            print("validatePinOffline params: \(params)")
            
            // Request
            Alamofire.request(.POST, urlString, parameters: params, encoding: ParameterEncoding.JSON, headers: defaultHeader).responseJSON { (response: Response<AnyObject, NSError>) -> Void in
                
                print("validatePinOffline response: \(response.result.value) error: \(response.result.error)")
                let apiResponseHandlerResult = self.apiResponseHandler(response, domain: Domain.PinManagement, errorCode: PMErrorCode.VerifyPINOfflineFailed)
                
                if let responseObject = apiResponseHandlerResult.responseObject {
                    completion(responseObject, nil)
                    return
                }
                
                let error = apiResponseHandlerResult.error ?? self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode.VerifyPINOfflineFailed.rawValue, errorDescription: PMErrorCode.VerifyPINOfflineFailed.localizedDescription())
                
                completion(nil, error)
            }
    }
    
    
    // MARK: - Check For Network Connection
    
    /**
    Presents a UIAlertController when there is no network connection
    */
    func notReachableMessage() {
        
        self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.NOInternetError.rawValue, errorDescription: CXErrorCode.NOInternetError.localizedDescription())
    }
    
    // MARK: - Utility Methods
    func generateErrorFromInfo(domain: String, code: Int, errorDescription: String) -> NSError{
        var userInfo = [NSObject : AnyObject]()
        userInfo[NSLocalizedDescriptionKey] = errorDescription
        return NSError(domain: domain, code: code, userInfo: userInfo)
    }
    
    func createNSErrorFromAPI(responseObject: AnyObject?, domain: Domain, errorCode: CustomerAppErrorCode) -> NSError {
        
        var errorDescription = String()
        
        // Create the error description from the API Error
        // Append all errors delimited by new line
        if let errorData = responseObject as? [[String: String]] {
            for error in errorData {
                if let uCode = error["code"], uUserMessage = error["userMessage"] {
                    var tempErrorDescription = "\(uCode)" + " - " + "\(uUserMessage)"
                    if let lastError = errorData.last where lastError != error {
                        tempErrorDescription.appendContentsOf("\n")
                    }
                    errorDescription.appendContentsOf(tempErrorDescription)
                }
            }
        }
        
        // Use the generic message if the error description is empty
        if errorDescription.isEmpty {
            errorDescription = errorCode.localizedDescription()
        }
        
        // Return the NSError
        return self.generateErrorFromInfo(domain.rawValue, code: errorCode.getRawValue, errorDescription:errorDescription)
    }
    
    func apiResponseHandler(response: Response<AnyObject, NSError>, domain: Domain, errorCode: CustomerAppErrorCode) -> (responseObject: AnyObject?, error: NSError?) {
        
        var error: NSError?
        var responseObject: AnyObject?
        
        // 1. Check for the status code
        if let statusCode = response.response?.statusCode {
            
            switch statusCode {
            case 200, 204:
                // 2. On success use the result
                responseObject = response.result.value
            default:
                // 3. On status codes 400, 402, 422, 500 use the array in the error payload to create a single NSError Object, and send back through the completion block
                error = self.createNSErrorFromAPI(response.result.value, domain: domain, errorCode: errorCode)
            }
        }
        
        if let networkingError = response.result.error where networkingError.domain != NSCocoaErrorDomain && networkingError.code != 3840 {
            // 4. Check if NSError is available, if available send it back in the completion block
            // Disregarding the case where the domain is NSCocoaErrorDomain and the error code is 3840. This is a result from wrong authentication and the response type is String not JSON
            error = networkingError
            Analytics.sharedInstance.addEventWithName(SplunkConstants.kApiNetworkingErrorKey, theList: SplunkConstants.params(error))
        }
        
        return (responseObject, error)
    }
    
}
