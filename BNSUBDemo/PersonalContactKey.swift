//
//  PersonalContactKey.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-02.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

// MARK: - Personal Contact Keys

enum PersonalContactKey: Int, CustomerKeys {
    
    case HomePhone, BusinessPhone, BusinessPhoneExtension, EmailAddress
    
    static var numberOfKeys: Int {
        get {
            return 4
        }
    }
    
    static var parentSectionNumber: Int {
        get {
            return CustomerPersonalInfoKeys.PersonalContactInfo.rawValue
        }
    }
    
    func getKeyName(object: AnyObject) -> String {
        switch self {
        case .HomePhone:
            return NSLocalizedString("home_phone", comment: "Customer Personal Contact")
        case .BusinessPhone:
            return NSLocalizedString("business_phone", comment: "Customer Personal Contact")
        case .BusinessPhoneExtension:
            return NSLocalizedString("bus_phone_extension", comment: "Customer Personal Contact")
        case .EmailAddress:
            return NSLocalizedString("email_address", comment: "Customer Personal Contact")
        }
    }

    func getValueFrom(object: AnyObject) -> String {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return ""
        }
        
        switch self {
            
        case .HomePhone:
            guard let phoneNumber = uPersonProfile.getCustomerPhoneWithType(PhoneUsageType.HomePhone)?.formattedPhoneNumber else {
                return ""
            }
            return phoneNumber
            
        case .BusinessPhone:
            guard let phoneNumber = uPersonProfile.employment?.getEmploymentPhoneWithType(PhoneUsageType.BusinessPhone)?.formattedPhoneNumber else {
                return ""
            }
            return phoneNumber
            
        case .BusinessPhoneExtension:
            return uPersonProfile.employment?.getEmploymentPhoneWithType(PhoneUsageType.BusinessPhone)?.phoneExtension ?? ""
            
        case .EmailAddress:
            return uPersonProfile.getCustomerEmailWithType(EmailUsageType.GeneralEmail)?.address ?? ""
        }
    }
    
    func shouldDisplay(object: AnyObject) -> Bool {
        switch self {
        case .HomePhone:
            return true
        case .BusinessPhone:
            return true
        case .BusinessPhoneExtension:
            return true
        case .EmailAddress:
            return true
        }
    }
    
    func isEditable(object: AnyObject) -> Bool {
        switch self {
        case .HomePhone:
            return true
        case .BusinessPhone:
            return true
        case .BusinessPhoneExtension:
            return true
        case .EmailAddress:
            return true
        }
    }
    
    func editingType() -> CustomerInfoEditType {
        switch self {
        case .HomePhone:
            return .Keyboard
        case .BusinessPhone:
            return .Keyboard
        case .BusinessPhoneExtension:
            return .Keyboard
        case .EmailAddress:
            return .Keyboard
        }
    }
    
    func getMasterDataForListSelector(object: AnyObject) -> [String: String]? {
        switch self {
        case .HomePhone:
            return nil
        case .BusinessPhone:
            return nil
        case .BusinessPhoneExtension:
            return nil
        case .EmailAddress:
            return nil
        }
    }

    func valueChanged(object: AnyObject, newValue: String, newValue2: String?) -> String {
        setValueTo(object, newValue: newValue, newValue2: newValue2)
        return getValueFrom(object)
    }

    func setValueTo(object: AnyObject, newValue: String, newValue2: String?) {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return
        }
        
        switch self {
            
        case .HomePhone:
            guard let phone = uPersonProfile.getCustomerPhoneWithType(PhoneUsageType.HomePhone) else {
                let unformattedValue = Phone.removePhoneNumberFormatting(newValue)
                let newPhone = Phone(number: unformattedValue, phoneExtension: nil, usageTp: PhoneUsageType.HomePhone)
                uPersonProfile.addPhone(newPhone)
                return
            }
            phone.number = Phone.removePhoneNumberFormatting(newValue)
            
        case .BusinessPhone:
            guard let phone = uPersonProfile.employment?.getEmploymentPhoneWithType(PhoneUsageType.BusinessPhone) else {
                let unformattedValue = Phone.removePhoneNumberFormatting(newValue)
                let newPhone = Phone(number: unformattedValue, phoneExtension: nil, usageTp: PhoneUsageType.BusinessPhone)
                uPersonProfile.employment?.addPhone(newPhone)
                return
            }
            phone.number = Phone.removePhoneNumberFormatting(newValue)
            
        case .BusinessPhoneExtension:
            guard let phone = uPersonProfile.employment?.getEmploymentPhoneWithType(PhoneUsageType.BusinessPhone) else {
                let newPhone = Phone(number: nil, phoneExtension: newValue, usageTp: PhoneUsageType.BusinessPhone)
                uPersonProfile.employment?.addPhone(newPhone)
                return
            }
            phone.phoneExtension = newValue
            
        case .EmailAddress:
            guard let email = uPersonProfile.getCustomerEmailWithType(EmailUsageType.GeneralEmail) else {
                let newEmail = Email(address: newValue, usageTp: EmailUsageType.GeneralEmail)
                uPersonProfile.addEmail(newEmail)
                return
            }
            email.address = newValue
        }
    }
    
    func isValidValueIn(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
            
        case .HomePhone:
            return uPersonProfile.getCustomerPhoneWithType(PhoneUsageType.HomePhone)?.isValidNumber ?? false
            
        case .BusinessPhone:
            return uPersonProfile.employment?.getEmploymentPhoneWithType(PhoneUsageType.BusinessPhone)?.isValidNumber ?? true
            
        case .BusinessPhoneExtension:
            return uPersonProfile.employment?.getEmploymentPhoneWithType(PhoneUsageType.BusinessPhone)?.isValidPhoneExtension ?? true
            
        case .EmailAddress:
            return uPersonProfile.getCustomerEmailWithType(EmailUsageType.GeneralEmail)?.isValidEmailAddress ?? true
        }
    }

    func shouldChangeStringInRange(object: AnyObject, newString: String, range: NSRange) -> Bool {
        
        switch self {
            
        case .HomePhone:
            return Phone.isValidPhoneNumberStringInRange(newString, range: range)
            
        case .BusinessPhone:
            return Phone.isValidPhoneNumberStringInRange(newString, range: range)
            
        case .BusinessPhoneExtension:
            return Phone.isValidPhoneExtensionNumberStringInRange(newString, range: range)
            
        case .EmailAddress:
            return Email.isValidEmailStringInRange(newString, range: range)
        }
    }
    
    func customizeTextField(object: AnyObject, textField: UITextField, editing: Bool) {

        textField.keyboardType = UIKeyboardType.Default
        textField.returnKeyType = UIReturnKeyType.Done
        textField.enabled = isEditable(object) ? editing : false
        textField.borderStyle = isEditable(object) && editing ? UITextBorderStyle.RoundedRect : UITextBorderStyle.None
        textField.backgroundColor = isEditable(object) && editing ? UIColor.whiteColor() : UIColor.clearColor()
        textField.autocapitalizationType = UITextAutocapitalizationType.Words

        switch self {
            
        case .HomePhone:
            textField.keyboardType = UIKeyboardType.NumberPad
            textField.placeholder = editing ? NSLocalizedString("enter_home_phone_number", comment: "Customer Personal Contact") : ""
            
        case .BusinessPhone:
            textField.keyboardType = UIKeyboardType.NumberPad
            textField.placeholder = editing ? NSLocalizedString("enter_business_phone_number", comment: "Customer Personal Contact") : ""
            
        case .BusinessPhoneExtension:
            textField.keyboardType = UIKeyboardType.NumberPad
            textField.placeholder = editing ? NSLocalizedString("enter_business_extension_number", comment: "Customer Personal Contact") : ""
            
        case .EmailAddress:
            textField.keyboardType = UIKeyboardType.EmailAddress
            textField.autocapitalizationType = UITextAutocapitalizationType.None
            textField.placeholder = editing ? NSLocalizedString("enter_email_address", comment: "Customer Personal Contact") : ""
        }
    }
    
    func reloadRowsAtIndexPathsWithAnimation(object: AnyObject) -> [[String: AnyObject]]? {
        return nil
    }
}
