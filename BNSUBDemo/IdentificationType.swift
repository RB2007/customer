//
//  IdentificationType.swift
//  Customer
//
//  Created by Emad Toukan on 2016-02-25.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import Foundation

enum IdentificationType: String {
    case Unknown = "Unknown",
    DriverLicense = "001",
    CanadianPassport = "002",
    CanadianCitizenshipCert = "003",
    NaturalizationCertificate = "004",
    PermanentResidentCard = "005",
    RecordOfLandingImm1000 = "006",
    PermanentResidenceImm5292 = "007",
    Imm1442Document = "008",
    HealthCard = "009",
    CertificateOfIndianStatus = "010",
    BcInsCorpCard = "011",
    AlbertaRegistriesCardOrDoc = "012",
    SaskatchewanGovtInsCardOrDoc = "013",
    NsDeptOfServiceCardOrDoc = "014",
    PeiDeptOfTransportCardOrDoc = "015",
    NbServiceNbCardOrDoc = "016",
    NlDeptOfGovtServCardOrDoc = "017",
    NwtDeptOfTransportCardOrDoc = "018",
    NunavutCgtDeptCardOrDoc = "019",
    SocialInsuranceCard = "020",
    CanadianBirthCertificate = "022",
    OfficialGovtCardOrDoc = "023",
    StudentVisa = "024",
    PgDriverLicence = "025",
    PgCanadianPassport = "026",
    PgHealthCard = "027",
    PgCanadianBirthCertificate = "028",
    PgOfficialCanGovtCardOrDoc = "029",
    OtherId = "030",
    CreditCardCANNonBns = "031",
    CnibClientCard = "032",
    CanCollegeOrUniversityIdCard = "033",
    EmployeeIdCard = "034",
    OtherBankClientCardNonBns = "035",
    ForeignPassport = "036",
    ForeignDriverLicence = "037",
    CreditCardForeign = "038",
    ReferredBnsCustomer = "039",
    ReferredIndivInCommunity = "040",
    PgVisaCardBns = "041",
    PgCreditCardCanNonBns = "042",
    PgScotiacard = "043",
    PgOthBkClientCardNonBns = "044",
    PgOtherId = "045",
    PgForeignPassport = "046",
    PgUspermanentResidentcard = "047",
    PgNationalIdentitycard = "048",
    PgUsdriversLicense = "049",
    PgUsbirthCertificate = "050",
    UsPermanentResidentcard = "051",
    NationalIdentityCard = "052",
    UsDriversLicense = "053",
    UsBirthCertificate = "054",
    OldAgeSecurityCardWith = "055",
    OldAgeSecurityCardWithout = "056"
    //    BusinessNumber = "101",
    //    IncorporationNumber = "102",
    //    PartnershipRegistrationNumber = "103",
    //    TIN = "104",
    //    BusinessRegistrationNumber = "105",
    //    TradeNameRegistrationNumber = "106",
    
    // MARK: - Instance Methods
    func getName() -> String {
        switch self {
        case .Unknown:
            return ""
        default:
            return CustomerMasterData.getValueForKey(self.rawValue, customerMasterDataType: CustomerMasterDataType.IdentityTp)
        }
    }
    
    // We have to display id description for the following set of ids
    //    020 = "Social Insurance Card"
    //    029 = "Pg Official Can Govt Card Or Doc"
    //    030 = "Other Id"
    //    031 = "Credit Card Can Non Bns"
    //    033 = "Can College Or University Id Card"
    //    034 = "Employee Id Card"
    //    035 = "Other Bank Client Card Non Bns"
    //    038 = "Credit Card Foreign"
    
    // MARK: - Property Application
    func isDescriptionApplicable() -> Bool {
        switch self {
        case .SocialInsuranceCard, .OfficialGovtCardOrDoc, .PgOfficialCanGovtCardOrDoc, .OtherId, .CreditCardCANNonBns, .CanCollegeOrUniversityIdCard, .EmployeeIdCard, .OtherBankClientCardNonBns, .CreditCardForeign, .ReferredBnsCustomer, .ReferredIndivInCommunity, .PgCreditCardCanNonBns, .PgScotiacard, .PgOthBkClientCardNonBns, .PgOtherId:
            return true
        default:
            return false
        }
    }
    
    func isIdNumberApplicable() -> Bool {
        switch self {
        case .ReferredBnsCustomer, .ReferredIndivInCommunity, .Unknown:
            return false
        default:
            return true
        }
    }
    
    func isCountryOfIssueApplicable() -> Bool {
        switch self {
        case .Unknown:
            return false
        default:
            return true
        }
    }
    
    func isPlaceOfBirthApplicable() -> Bool {
        switch self {
        case .CanadianPassport, .RecordOfLandingImm1000, .PermanentResidenceImm5292, .Imm1442Document, .PgCanadianPassport, .ForeignPassport, .PgForeignPassport, .PgUspermanentResidentcard, .UsPermanentResidentcard:
            return true
        default:
            return false
        }
    }
    
    func isIdNotExpiredApplicable() -> Bool {
        switch self {
        case .SocialInsuranceCard, .CanadianBirthCertificate, .OfficialGovtCardOrDoc, .PgCanadianBirthCertificate, .PgOfficialCanGovtCardOrDoc, .OtherId, .CnibClientCard, .EmployeeIdCard, .OtherBankClientCardNonBns, .ReferredBnsCustomer, .ReferredIndivInCommunity, .PgOthBkClientCardNonBns, .PgOtherId, .PgUsbirthCertificate, .UsBirthCertificate, .OldAgeSecurityCardWith, .OldAgeSecurityCardWithout, .Unknown:
            return false
        default:
            return true
        }
    }
    
    func isIdRefusedApplicable() -> Bool {
        switch self {
        case .SocialInsuranceCard:
            return true
        default:
            return false
        }
    }
    
    func isProvideLaterApplicable() -> Bool {
        switch self {
        case .SocialInsuranceCard:
            return true
        default:
            return false
        }
    }
    
    func isIdConfirmedApplicable() -> Bool {
        switch self {
        default:
            return true
        }
    }
    
    // MARK: - Default Values
    func countryOfIssueDefaultValue() -> String? {
        switch self {
        case .DriverLicense, .CanadianPassport, .CanadianCitizenshipCert, .NaturalizationCertificate, .PermanentResidentCard, .RecordOfLandingImm1000, .PermanentResidenceImm5292, .Imm1442Document, .HealthCard, .CertificateOfIndianStatus, .BcInsCorpCard, .AlbertaRegistriesCardOrDoc, .SaskatchewanGovtInsCardOrDoc, .NsDeptOfServiceCardOrDoc, .PeiDeptOfTransportCardOrDoc, .NbServiceNbCardOrDoc, .NlDeptOfGovtServCardOrDoc, .NwtDeptOfTransportCardOrDoc, .NunavutCgtDeptCardOrDoc, .SocialInsuranceCard, .CanadianBirthCertificate, .OfficialGovtCardOrDoc, .StudentVisa, .PgDriverLicence, .PgCanadianPassport, .PgHealthCard, .PgCanadianBirthCertificate, .PgOfficialCanGovtCardOrDoc, .CreditCardCANNonBns, .CnibClientCard, .CanCollegeOrUniversityIdCard, .OtherBankClientCardNonBns, .PgCreditCardCanNonBns, .PgOthBkClientCardNonBns, .OldAgeSecurityCardWith, .OldAgeSecurityCardWithout:
            return CountryCode.Canada.rawValue
        case .PgUspermanentResidentcard, .PgUsdriversLicense, .PgUsbirthCertificate, .UsPermanentResidentcard, .UsDriversLicense, .UsBirthCertificate:
            return CountryCode.UnitedStates.rawValue
        case .Unknown, .OtherId, .EmployeeIdCard, .ForeignPassport, .ForeignDriverLicence, .CreditCardForeign, .PgVisaCardBns, .PgScotiacard, .PgOtherId, .PgForeignPassport, .PgNationalIdentitycard, .NationalIdentityCard:
            return nil
        case .ReferredBnsCustomer, .ReferredIndivInCommunity:
            return CountryCode.NotApplicable.rawValue
        }
    }
    
    func provinceOfIssueDefaultValue() -> String? {
        switch self {
        case .CanadianPassport, .CanadianCitizenshipCert, .NaturalizationCertificate, .PermanentResidentCard, .RecordOfLandingImm1000, .PermanentResidenceImm5292, .Imm1442Document, .CertificateOfIndianStatus, .SocialInsuranceCard, .StudentVisa, .PgCanadianPassport, .OldAgeSecurityCardWith, .OldAgeSecurityCardWithout:
            return "098" // Federally Issued
        case .ReferredBnsCustomer, .ReferredIndivInCommunity:
            return "000" // Not Applicable
        case .BcInsCorpCard:
            return "BC"
        case .AlbertaRegistriesCardOrDoc:
            return "AB"
        case .SaskatchewanGovtInsCardOrDoc:
            return "SK"
        case .NsDeptOfServiceCardOrDoc:
            return "NS"
        case .PeiDeptOfTransportCardOrDoc:
            return "PE"
        case .NbServiceNbCardOrDoc:
            return "NB"
        case .NlDeptOfGovtServCardOrDoc:
            return "NL"
        case .NwtDeptOfTransportCardOrDoc:
            return "NT"
        case .NunavutCgtDeptCardOrDoc:
            return "NU"
        default:
            return nil
        }
    }
    
    // MARK: - Master Data
    func getCountryOfIssueMasterData() -> [String: String]? {
        var masterData: [String: String]?
        switch self {
        case .OtherId, .EmployeeIdCard, .ForeignDriverLicence, .PgVisaCardBns, .PgScotiacard, .PgOtherId, .PgNationalIdentitycard, .NationalIdentityCard:
            masterData = CustomerMasterData.getDataForType(CustomerMasterDataType.CountryOfIssueForIdentification)
        case .ForeignPassport, .CreditCardForeign, .PgForeignPassport:
            // Remove Canada from the options
            masterData = CustomerMasterData.getDataForType(CustomerMasterDataType.CountryOfIssueForIdentification)
            masterData?[CountryCode.Canada.rawValue] = nil
        default:
            break
        }
        masterData?["000"] = nil // Removing Not Applicable
        masterData?["099"] = nil // Removing Unkown
        return masterData
    }
    
    func getProvinceOfIssueMasterData(countryCode: CountryCode) -> [String: String] {
        var masterData = CustomerMasterData.getProvinceStateDataForIdentification(countryCode)
        switch self {
        case .HealthCard, .PgHealthCard:
            masterData["MB"] = nil
            masterData["ON"] = nil
            masterData["PE"] = nil
            masterData["QC"] = nil
            masterData["000"] = nil // Removing "000" NOT Applicable from the list
            masterData["098"] = nil // Removing "098" Fedrally Issued from the list
            masterData["099"] = nil // Removing "099" Unknown from the list
        default:
            break
        }
        return masterData
    }
}