//
//  CustomerIdentificationFormTableViewController
//  Customer
//
//  Created by Emad Toukan on 2016-02-19.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import UIKit

class CustomerIdentificationFormTableViewController: AbstractCustomerAccountTableViewController, SaveCancelActionButtonsProtocol {
    
    // MARK: - Properties
    var identification: Identification?
    weak var customerIdentificationTableViewController: CustomerIdentificationTableViewController?
    
    // MARK: - View Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Hide toolbar buttons
        self.customerAccountDelegate?.isToolbarHidden = true
    }
    
    override func viewWillDisappear(animated: Bool) {
        super.viewWillAppear(animated)
        
        // Show toolbar Buttons
        self.customerAccountDelegate?.isToolbarHidden = false
    }
    
    // MARK: - Save Cancel Action Buttons Protocol
    func saveButtonPressed() {
        if let uIdentification = identification {
            if uIdentification.isValidIdentification {
                if uIdentification.json == nil {
                    customerAccountDelegate?.customerProfile?.addIdentification(uIdentification)
                }
                customerIdentificationTableViewController?.tableView.reloadData()
                self.navigationController?.popViewControllerAnimated(true)
            } else {
                let alertController = AlertControllerUtil.createOKAlertController(NSLocalizedString("invalid_fields", comment: "Error title."), message: NSLocalizedString("check_fields", comment: "Error message body."))
                self.presentViewController(alertController, animated: true, completion: nil)
            }
        }
    }
    
    func cancelButtonPressed() {
        self.navigationController?.popViewControllerAnimated(true)
    }
    // MARK: - Table View Delegate
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return IdentificationFormKey.numberOfKeys + 1 // Adding 1 for the Save / Cancel cell
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        if 0..<IdentificationFormKey.numberOfKeys ~= indexPath.row {
            // Form cells
            guard let cell = tableView.dequeueReusableCellWithIdentifier("CustomerInfoCell", forIndexPath: indexPath) as? CustomerInfoTableViewCell, uIdentification = identification, let customerKey = IdentificationFormKey(rawValue: indexPath.row), isEditingCustomer = customerAccountDelegate?.isEditingCustomer else {
                return UITableViewCell()
            }
            cell.setCellContent(uIdentification, customerKey: customerKey, isEditing: isEditingCustomer, indexPath: indexPath, delegate: self)
            return cell
        } else {
            // Save / Cancel cells
            guard let cell = tableView.dequeueReusableCellWithIdentifier("SaveCancelActionButtonsCell", forIndexPath: indexPath) as? SaveCancelActionButtonsTableViewCell else {
                return UITableViewCell()
            }
            cell.delegate = self
            return cell
        }
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Remove keyboard from the view
        self.view.endEditing(true)
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("CustomerInfoSectionHeader") as? CustomerInfoSectionHeader else {
            return UIView()
        }
        headerView.frame = CGRectMake(0, 0, tableView.bounds.size.width, 50)
        headerView.setHeaderContent(NSLocalizedString("new_identification", comment: "Add Identification Title"), section: section, alternateBackgroundColor: true, addbuttonHidden: true, delegate: nil)
        return headerView
    }
}
