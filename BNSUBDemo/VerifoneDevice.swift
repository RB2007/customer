//
//  VerifoneDevice.swift
//  BNSUBDemo
//
//  Created by Annie Lo on 2015-06-03.
//  Copyright (c) 2015 ScotiaBank. All rights reserved.
//

struct VerifoneDevice {
    
    private static let pinpadInstance = VFIPinpad()
    private static let controlInstance = VFIControl()
    private static let barcodeInstance = VFIBarcode()
    static var isReady = false
    static var publicKey: String?
    static var publicKeyP7s: String?
    static var publicID: String?
    static var publicIDP7s: String?
    static var pkcs12: String?
    static var keyLoaded = false
    static var p12Code: String?
    static var xpiVersion: String?
    
    
    static func pinpad() -> VFIPinpad? {
        return pinpadInstance
    }
    
    static func barcode() -> VFIBarcode? {
        return barcodeInstance
    }
    
    static func control() -> VFIControl? {
        return controlInstance
    }
    
    static func deviceSerialNumber() -> String? {
        return self.pinpadInstance!.vfiDiagnostics.pinpadSerialNumber
    }
    
    static func deviceModelNumber() -> String? {
        return self.pinpadInstance!.vfiDiagnostics.terminalName
    }
    
    static func deviceVersionInfo() -> String? {
        return self.pinpadInstance?.vfiDiagnostics.xpiVersion
    }
}