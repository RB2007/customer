//
//  MasterDataListSelectorTableViewCell.swift
//  Customer
//
//  Created by Emad Toukan on 2016-02-08.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import UIKit

class MasterDataListSelectorTableViewCell: UITableViewCell {

    // MARK: - Properties
    @IBOutlet weak var labelTitle: UILabel!
    @IBOutlet weak var imageViewCheckmark: UIImageView!
    
    // MARK: - Override Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clearColor()
        self.selectionStyle = UITableViewCellSelectionStyle.None
        
    }
    
    override func setSelected(selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
        
        // Configure the view for the selected state
        imageViewCheckmark.image = selected ? UIImage(named: "CheckmarkWhite") : UIImage()
    }
    
    // MARK: - Instance Methods
    func setCellContent(masterDataValue: String?) {
        labelTitle.text = masterDataValue
    }
}
