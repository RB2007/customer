//
//  CustomerPersonalInfoTableViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-16.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class CustomerPersonalInfoTableViewController: AbstractCustomerAccountTableViewController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }

    // MARK: - Table View Data Source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return customerAccountDelegate?.customerProfile == nil ? 0 : CustomerPersonalInfoKeys.numberOfSections
    }

    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let customerPersonalInfoKeys = CustomerPersonalInfoKeys(rawValue: section), uPersonProfile = customerAccountDelegate?.customerProfile as? PersonProfile else {
            return 0
        }
        return customerPersonalInfoKeys.getNumberOfKeys(uPersonProfile)
    }

    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCellWithIdentifier("CustomerInfoCell", forIndexPath: indexPath) as? CustomerInfoTableViewCell, uPersonProfile = customerAccountDelegate?.customerProfile as? PersonProfile, let customerKey = CustomerPersonalInfoKeys(rawValue: indexPath.section)?.getDelegateFor(indexPath.row), isEditingCustomer = customerAccountDelegate?.isEditingCustomer else {
            return UITableViewCell()
        }
        cell.setCellContent(uPersonProfile, customerKey: customerKey, isEditing: isEditingCustomer, indexPath: indexPath, delegate: self)
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        // Remove keyboard from the view
        self.view.endEditing(true)
    }

    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("CustomerInfoSectionHeader") as? CustomerInfoSectionHeader, customerPersonalInfoKeys = CustomerPersonalInfoKeys(rawValue: section) else {
            return UIView()
        }
        headerView.frame = CGRectMake(0, 0, tableView.bounds.size.width, 50)
        headerView.setHeaderContent(customerPersonalInfoKeys.getName(), section: section, alternateBackgroundColor: true, addbuttonHidden: true, delegate: nil)
        return headerView
    }
}
