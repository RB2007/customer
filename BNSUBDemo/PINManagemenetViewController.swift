//
//  PINManagemenetViewController.swift
//  BNSUBDemo
//
//  Created by Laura Reategui on 2015-04-28.
//  Copyright (c) 2015 ScotiaBank. All rights reserved.
//

import UIKit

/// PINManagemenetViewController class for pin management view
class PINManagemenetViewController: UIViewController, VFIPinpadDelegate, VFIBarcodeDelegate, VFIControlDelegate,UIAlertViewDelegate {
    
    /// initialize pin pad button
    @IBOutlet weak var initializePinPadButton: UIButton!
    
    /// pin pad content view
    @IBOutlet weak var pinPadContentView: UIView!
    
    /// pin pad scroll view
    @IBOutlet weak var pinPadScrollView: UIScrollView!
    
    /// pin pad top iamge
    @IBOutlet weak var pinPadTop: UIImageView!
    
    /// pin pad instruction label
    @IBOutlet weak var instructionsLabel: UILabel!
    
    /// scotia card image view
    @IBOutlet weak var scotiaCardImageView: UIImageView!
    
    /// credit card vertical constraint
    @IBOutlet weak var creditCardVerticalConstraint: NSLayoutConstraint!
    
    /// credit card horizontal constraint
    @IBOutlet weak var creditCardHorizontalConstraint: NSLayoutConstraint!
    
    /// boolean for storing checking rather or not pin pad is initialized in this view controller is finished
    static var initFinished :Bool = false
    
    ///  boolean for storing rather or not the card is read
    private var startCardRead: Bool = false
    
    /// reference to UIKit bundle
    private let uiKitBundle = NSBundle(identifier: "com.apple.UIKit")
    
    /// back button on the pin pad content view
    private var backButton: UIBarButtonItem?
    
    /// a flag of rather or not pin pad debugging log should be enabled
    private var logFlag = false
    
    /// a flag of rather or not initialization load should be forced (for testing only)
    private var forceLoadInitialization = false
    
    /// reference to card data
    private var cardData: CardInfo?
    
    /// try counter of how many times initialization has been attempted
    private var initCount = 0
    
    /// card first time activated flag from mid-tier
    private var cardFirstTimeActivated = false
    
    /// timer for pin pad initialization and checking rather or not pin pad is successfully connected
    private var initializeTimer: NSTimer?
    
    /// timer for the pin pad initialization job
    private var initializeJob : NSTimer?
    
    /// boolean of rather or not transaction has already started
    static var transactionStarted = false
    
    /// customer lookup delegate
    var customersLookupDelegate: CustomersLookupDelegate?
    
    /// button to close the pin pad content view
    @IBOutlet weak var buttonClose: UIButton?
    
    /**
     Setting up the UI and setting the pin pad log option
     */
    override func viewDidLoad() {
        super.viewDidLoad()
        PINManagemenetViewController.initFinished = false
        setUpPinPadButton()
        VerifoneDevice.control()?.logEnabled(true)
        
        // NEW
        buttonClose?.layer.cornerRadius = 5
    }
    
    /**
     setting up the look and feel of the buttons shown on the pin pad view
     */
    func setUpPinPadButton () {
        initializePinPadButton.backgroundColor = Constants.UBColours.scotiaRed
        initializePinPadButton.layer.cornerRadius = 5
        initializePinPadButton?.setTitleColor(UIColor.whiteColor(), forState: UIControlState.Normal)
        initializePinPadButton?.setTitleColor(Constants.UBColours.scotiaRedHighlighted, forState: UIControlState.Highlighted)
        initializePinPadButton.setTitle(NSLocalizedString("instruction_initialize_pinpad", comment: "PIN Management View Controller"), forState: UIControlState.Normal)
        buttonClose?.setTitle(NSLocalizedString("close", comment: ""), forState: UIControlState.Normal)
    }
    
    /**
     viewWillAppear
     
     - parameter animated: boolean animated
     */
    override func viewWillAppear(animated: Bool) {
        super.viewWillAppear(animated)
    }
    
    
    /**
     trigger to start animation when view appears
     
     - parameter animated: boolean animated
     */
    override func viewDidAppear(animated: Bool) {
        configureUI()
    }
    
    /**
     Reset parameter and cancel any existing command if transaction already started
     
     - parameter animated: boolean animated
     */
    override func viewWillDisappear(animated: Bool) {
        if PINManagemenetViewController.transactionStarted {
            PINManagemenetViewController.transactionStarted = false
            NSTimer.scheduledTimerWithTimeInterval(1, target: self, selector: Selector("cancelVerifoneCommand"), userInfo: nil, repeats: false)
        }
    }
    
    /**
     When deinit is triggered, set all the delegate back to nil
     */
    deinit {
        VerifoneDevice.pinpad()?.delegate = nil
        VerifoneDevice.control()?.delegate = nil
    }
    
    /**
     Start scotiacard animation with the pin pad (instruct customer how to insert the card)
     */
    func configureUI () {
        if let scotiaCardImageView = scotiaCardImageView {
            scotiaCardImageView.alpha = 0.0
            rotateImage()
        }
        
    }
    
    /**
     Animation of scotiacard getting inserted into the pin pad
     */
    func rotateImage() {
        
        UIView.transitionWithView(scotiaCardImageView, duration: 1.0, options: UIViewAnimationOptions.TransitionFlipFromTop,
            animations: {
                self.scotiaCardImageView.alpha = 1.0
                self.scotiaCardImageView.image = UIImage(named: "ScotiaCard_Back")
            }, completion: {(finished:Bool)->Void in
                UIView.animateWithDuration(1.0, delay: 0, options:UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                    self.scotiaCardImageView.transform = CGAffineTransformMakeRotation(CGFloat(M_PI)/2)
                    self.pinPadContentView.sendSubviewToBack(self.pinPadTop)
                    self.creditCardVerticalConstraint.constant = -60
                    self.creditCardHorizontalConstraint.constant = 160
                    
                    }, completion: {(finished:Bool)->Void in
                        UIView.animateWithDuration(1.0, animations: {
                            self.scotiaCardImageView.alpha = 1.0
                            self.creditCardVerticalConstraint.constant = 50
                            self.view.layoutIfNeeded()
                            }, completion: {(finished:Bool)->Void in
                                UIView.animateWithDuration(1.0, delay: 0, options:UIViewAnimationOptions.TransitionCrossDissolve, animations: {
                                    self.scotiaCardImageView.alpha = 0.0
                                    }, completion: {(finished:Bool)->Void in
                                })
                                
                        })
                })
        })
        
    }
    
    
    
    // MARK verifone
    
    /**
    Initializing the pin pad with the view controller (establishing a referenced connection)
    
    - returns: nothing
    */
    func initVFDevice() {
        VerifoneDevice.pinpad()?.delegate = self
        VerifoneDevice.control()?.delegate = self
        
        //if one of them is both initialized (control or pinpad), start initialization
        if (VerifoneDevice.control()!.initialized && VerifoneDevice.pinpad()!.initialized) {
            startInitialization()
        }
        else {
            VerifoneDevice.pinpad()?.initDevice()
            VerifoneDevice.control()?.initDevice()
        }
    }
    

    /**
     Triggered by VMF SDK when pin pad is connected
     
     - parameter isConnected: passed by VMF on rather or not pin pad is connected successfully
     */
    func pinpadConnected(isConnected: Bool) {
        if (isConnected) {
            instructionsLabel.text = NSLocalizedString("instruction_label_Connected_initializing", comment: "PIN Management View Controller")
        } else {
            instructionsLabel.text = NSLocalizedString("instruction_label_disconnected", comment: "PIN Management View Controller")
        }
    }
    
    /**
     Triggered by VMF SDK when pin pad is initialized or when an error has occurred during initialization
     
     - parameter isInitialized: passed by VMF on rather or not the pin pad is initialized successfully
     */
    func pinPadInitialized(isInitialized: Bool) {
        startInitialization()
    }
    
    /**
     Triggered by VMF SDK when pin pad control is initialized or when an error has occurred during initialization
     
     - parameter isInitialized: passed by VMF on rather or not the pin pad control is initialized successfully
     */
    func controlInitialized(isInitialized: Bool) {
        startInitialization()
    }

    /**
     Start initialization of the pinpad to load PKI keys, pin pad EMV configuration check and load
     */
    func startInitialization() {
        if (VerifoneDevice.pinpad()!.initialized && VerifoneDevice.control()!.initialized) {
            if (!PINManagemenetViewController.initFinished) {
                PINManagemenetViewController.initFinished = true
                VerifoneDevice.control()?.keypadBeepEnabled(true)
                setPinPadConfigInitialization()
            } else {
                instructionsLabel.text = NSLocalizedString("instruction_label_ready", comment: "PIN Management View Controller")
                initializePinPadButton.setTitle(NSLocalizedString("instruction_initialize_pinpad", comment: "PIN Management View Controller"), forState: UIControlState.Normal)
            }
        }
    }
    
    //MARK: - Initiate pinpad configuartion
    
    /**
    Step 1:
    If we have all the required PublicKeys and PublicIDs, then load them from memory. Otherwise make a network call to airwatch in order to retrieve them and store them into memory
    */
    func setPinPadConfigInitialization() {
        
        if (VerifoneDevice.publicKeyP7s == nil ||
            VerifoneDevice.publicIDP7s == nil ||
            VerifoneDevice.publicKey == nil ||
            VerifoneDevice.publicID == nil) {
                
                // Call mid-tier to get the public key, cert id, public key signature, cert id signatures and load them into the pin pad
                APIManagement.sharedInstance.pinPadConfig({ (data: AnyObject?, error: NSError?) -> () in
                    
                    if let uData = data as? [String: String] {
                        if let pkcsP12 = uData["pkcsP12"],
                            xpiPubKeyIdDat = uData["xpiPubKeyIdDat"],
                            xpiPubKeyIdDatP7s = uData["xpiPubKeyIdDatP7s"],
                            xpiPubKeyPem = uData["xpiPubKeyPem"],
                            xpiPubKeyPemP7s = uData["xpiPubKeyPemP7s"] {
                                VerifoneDevice.pkcs12 = pkcsP12
                                VerifoneDevice.publicID = xpiPubKeyIdDat
                                VerifoneDevice.publicIDP7s = xpiPubKeyIdDatP7s
                                VerifoneDevice.publicKey = xpiPubKeyPem
                                VerifoneDevice.publicKeyP7s = xpiPubKeyPemP7s
                                self.loadPKIKeysfromMemory()
                        } else {
                            let message = "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.PKILoadError.rawValue)" + "\n" + "\(NSLocalizedString("instruction_label_pinpad_offline", comment: "PIN Management View Controller"))"
                            self.endTransactionWithError(nil, instructionText: message, showInitializeButton: false)
                        }
                        return
                    }
                    if let uError = error {
                        let alertController = AlertControllerUtil.createOKAlertController(uError)
                        self.presentViewController(alertController, animated: true, completion: nil)
                        self.endTransactionWithError()
                    }
                })
        } else {
            //load PKI key
            loadPKIKeysfromMemory()
        }
        
        
    }
    
    
    /**
     STEP 2:
     
     - If we are sending the c30 command then start reading the Card
     - Otherwise load the encryption key. If that is successfull, then start pinpad initialization.
     - If encryption key loading is a failure, then throw "Pinpad offline" error
     
     */
    func loadPKIKeysfromMemory() {
        if (!VerifoneDevice.keyLoaded && VerifoneDevice.publicKeyP7s != nil &&
            VerifoneDevice.publicIDP7s != nil &&
            VerifoneDevice.publicKey != nil &&
            VerifoneDevice.publicID != nil) {
                
                let pkP7s = PINManagementUtilities.fromHexString(VerifoneDevice.publicKeyP7s!)
                let pkIdP7s = PINManagementUtilities.fromHexString(VerifoneDevice.publicIDP7s!)
                
                //if encryption key is loaded properly, then check initialization to setup pinpad
                if (PINManagementSecurity.loadPKIKeys(pkP7s, pkIdP7s: pkIdP7s, cert: VerifoneDevice.publicKey!, certId: VerifoneDevice.publicID!)) {
                    self.checkInitialization()
                    
                } else {
                    VerifoneDevice.publicKeyP7s = nil
                    VerifoneDevice.publicIDP7s = nil
                    VerifoneDevice.publicKey = nil
                    VerifoneDevice.publicID = nil
                    VerifoneDevice.pkcs12 = nil
                    
                    let message = "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.PKILoadError.rawValue)" + "\n" + "\(NSLocalizedString("instruction_label_pinpad_offline", comment: "PIN Management View Controller"))"
                    
                    endTransactionWithError(nil, instructionText: message, showInitializeButton: false)
                }
        } else if (VerifoneDevice.keyLoaded) {
            self.cardRead()
        }
    }
    
    /**
     STEP 3: 
     
     - Check pin pad configuration by calling D10 to retrieve a PKI index in the EST table
     - Compare the PKI index with the last PKI index that is supposed to be loaded onto the pin pad
     - If the PKI index is not matching with the one expected, load configuration 
     - If the PKI index is matching with the one expected, start reading card (skip loading configuration)
     */
    func checkInitialization() {
        
        //Query EST table for supported AIDs:
        //D10010030080100600511A
        // ^^---^^^--^^^---^-^^^^^^^^
        // | | | | | | |||
        // | | | | | | ||+-- Value to search for
        // | | | | | | |+--- 0=text,1=value
        // | | | | | | +---- 1=not equal
        // | | | | | +------- field to search
        // | | | | +--------- size of following data
        // | | | +------------ # of search criteria
        // | | +-------------- field to retrieve
        // | +----------------- start rec#
        // +-------------------- table
        //
        // Structure is based on the EST Table (EMV Scheme Table) - see Appendix A if VeriFone XPI API specs
        // Field
        // 1 number of transactions (constant: 0)
        // 2 Scheme Label (e.g. "VISA")
        // 3 RID (e.g. "A000000003")
        // 4 CSN File Name (e.g. "A000000003.CSN")
        // 5-49 Public key info - 15 groups of 3 fields each
        // 5,8,11... PKI index, e.g. 0x99
        // 6,9,12... file name, e.g. "../../flash/xpi/A000000003.99"
        // 7,10,13... constant, 311219 (0x4BFB3)
        // 50-99 Key information - 10 groups of 5 fields each (see structure aidList)
        // 50,55,60... AID (e.g. "A0000000031010")
        // 51,56,61... Partial Match flag (0/1)
        // 52,57,62... PVN (e.g. 008C)
        // 53,58,63... PVN (e.g. 008C)
        // 54,59,64... App Name (e.g. "VISA")
        
        
        //Main goal of this is to query on the pinpad table structure to check rather or not CAPK key files
        //are already loaded onto the pinpad. If it is, it can be assumed that
        if (forceLoadInitialization) {
            loadInitialization()
        } else {
            var interacCAPKList = APIManagement.sharedInstance.currentEnv == PINManagementAction.environmentDev ?
                PINManagementConstants.CAPK.interacTestCAPK : PINManagementConstants.CAPK.interacProductionCAPK
            let fieldToRetrieveInterac = 5 + (interacCAPKList.count-1) * 3
            var response = VerifoneDevice.pinpad()?.D10(01, startRecord: 002, fieldToRetrieve: Int32(fieldToRetrieveInterac), condition: 01, fieldToSearch: 003, condition1: 10, condition2: 00, fieldValue: "01")
            if (response == 0) {
                
                if let recordResponse = VerifoneDevice.pinpad()?.vfiEMVResponseData.dictionaryWithValuesForKeys(["recordNumber","retrievedData"]) {
                    if let retrievedData = recordResponse["retrievedData"] as? NSData {
                        let retrievedDataString = PINManagementUtilities.toHexString(retrievedData)
                        
                        if (retrievedDataString == "00\(interacCAPKList[interacCAPKList.count-1].pkiIndex!)") { //check the last capk key pki index, and this is ok
                            
                            let visaCAPKList = APIManagement.sharedInstance.currentEnv == PINManagementAction.environmentDev ?
                                PINManagementConstants.CAPK.visaTestCAPK : PINManagementConstants.CAPK.visaProductionCAPK
                            
                            let fieldToRetrieveVisa = 5 + (visaCAPKList.count-1) * 3
                            
                            response = VerifoneDevice.pinpad()?.D10(01, startRecord: 001, fieldToRetrieve: Int32(fieldToRetrieveVisa), condition: 01, fieldToSearch: 003, condition1: 10, condition2: 00, fieldValue: "01")
                            
                            
                            if (response == 0) {
                                initCount = 0
                                if let recordVisaResponse = VerifoneDevice.pinpad()?.vfiEMVResponseData.dictionaryWithValuesForKeys(["recordNumber","retrievedData"]) {
                                    if let retrievedDataVisa = recordVisaResponse["retrievedData"] as? NSData {
                                        let retrievedDataVisaString = PINManagementUtilities.toHexString(retrievedDataVisa)
                                        
                                        
                                        if (retrievedDataVisaString == "00\(visaCAPKList[visaCAPKList.count-1].pkiIndex!)") {
                                            cardRead()
                                        } else {
                                            loadInitialization()
                                        }
                                    }
                                    else {
                                        checkInitialization()
                                    }
                                }
                                
                            }
                            
                        } else {
                            loadInitialization()
                        }
                    }
                    
                }
                
            } else {
                initCount++
                if (initCount < 3) {
                    checkInitialization()
                } else {
                    let message = "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.PinpadQueryCheckError.rawValue)" + "\n" + "\(NSLocalizedString("instruction_label_pinpad_offline", comment: "PIN Management View Controller"))"
                    endTransactionWithError(nil, instructionText: message, showInitializeButton: false) //check this out later
                }
            }
        }
        
    }
    
    /**
     Step 4:
     
     - Remove all configuration of the pin pad
     - Create EST table
     - Create MVT table
     - Create EEST table
     - Create EMVT table
     - Set idle home screen string
     */
    func loadInitialization() {
        var isOk: Bool? = nil
        
        // Remove all configurations on the VeriFone pin pad
        isOk = PINManagementAction.cleanPinpad(instructionsLabel)
        
        // Create EST table
        if (isOk!) {
            sleep(PINManagementConstants.Time.sleepingTime)
            isOk = PINManagementAction.createEST(instructionsLabel)
        }
        
        // Create MVT table
        if (isOk!) {
            sleep(PINManagementConstants.Time.sleepingTime)
            isOk = PINManagementAction.createMVT(instructionsLabel)
        }
        
        // Create EEST table
        if (isOk!) {
            sleep(PINManagementConstants.Time.sleepingTime)
            isOk = PINManagementAction.createEEST(instructionsLabel)
        }
        
        // Create EMVT table
        if (isOk!) {
            sleep(PINManagementConstants.Time.sleepingTime)
            isOk = PINManagementAction.createEMVT(instructionsLabel)
        }
        
        // Set idle home screen string
        if (isOk!) {
            sleep(PINManagementConstants.Time.sleepingTime)
            VerifoneDevice.pinpad()?.Z8("Welcome/Bonjour!")
        }
        
        // read card
        if (isOk!) {
            cardRead()
        }
        
    }
    
    
    
    /**
     Cancel executing command on the Verifone pin pad
     */
    func cancelVerifoneCommand() {
        VerifoneDevice.control()?.keypadEnabled(false)
        
        VerifoneDevice.pinpad()?.cancelCommandWithDisplayOptions(1)
        VerifoneDevice.pinpad()?.S01()
        VerifoneDevice.pinpad()?.S01() //need to repeat this as it E255 doesn't cancel the first time
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++ C30 ++++++++++++++++++++++++++++++++++++++++++++++++++
    
    /**
    C30
    1. Try to get XPI version, and have it compare with the expected XPI version returned from mid-tier
    2. Send D30 to set language on the pin pad
    3. send C30 to ask customer to insert card
    */
    func cardRead(){
        PINManagemenetViewController.transactionStarted = true
        VerifoneDevice.pinpad()?.diagnosticInfo()
        if let xpiVersion = VerifoneDevice.pinpad()?.vfiDiagnostics.xpiVersion, xpiMidTierVersion = VerifoneDevice.xpiVersion {
            if (xpiVersion != xpiMidTierVersion) {
                let error = self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode.XpiVersionCheckFailed.rawValue, errorDescription: PMErrorCode.XpiVersionCheckFailed.localizedDescription())
                
                self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in }, skip: true)
                return
            }
        }
        VerifoneDevice.keyLoaded = true
        
        VerifoneDevice.control()?.keypadEnabled(true)
        instructionsLabel.text = NSLocalizedString("instruction_label_insert_card", comment: "PIN Management View Controller")
        initializePinPadButton.setTitle(NSLocalizedString("cancel", comment: ""), forState: UIControlState.Normal)
        initializePinPadButton.hidden = false
        startCardRead = true
        
        //card read (by inserting card)
        
        //set C30 message
        //D30
        VerifoneDevice.pinpad()?.D30(NSLocalizedString("pin_management_pinpad_insert_line1", comment: "PIN Management View Controller"),
            line2: NSLocalizedString("pin_management_pinpad_insert_line2", comment: "PIN Management View Controller"),
            line3: NSLocalizedString("pin_management_pinpad_insert_line3", comment: "PIN Management View Controller"),
            line4: NSLocalizedString("pin_management_pinpad_insert_line4", comment: "PIN Management View Controller"),
            amt1: false, amt2: false, amt3: false, amt4: false)
        
        cardData = nil
        //card read (by inserting card)
        let cardReadResponse = VerifoneDevice.pinpad()?.C30(30, language: 1, amount: 0.00, otherAmount: 0.00)
        
        if (cardReadResponse >= 0) {
            let responseCode = UInt(cardReadResponse!)
            
            if (startCardRead) {
                
                switch responseCode {
                case 0, 21:
                    handleSuccessfulCardRead()
                case 6:
                    handleCardReadTimedOut()
                default:
                    handledefaultCardRead(cardReadResponse!)
                }
                
            }
        } else {
            if (startCardRead) {
                handledefaultCardRead(cardReadResponse!)
            }
        }
        
        
    }
    
    /**
    Retrieving the decrypted PIN and service code from blob1 data returned from pin pad
    
    - parameter blob1Data: NSData blob1 data
    */
    func retrieveDecryptedPAN(blob1Data: NSData) {
        if let decryptedText = PINManagementSecurity.decryptWithPrivateKey(blob1Data) {
            
            //try searching for the 5a EMV tag (Application Primary Account Number [PAN])
            if (decryptedText.hasPrefix("5a")) {
                let panLengthHex = decryptedText.substringWithRange(Range<String.Index>(start: decryptedText.startIndex.advancedBy(2), end: decryptedText.startIndex.advancedBy(4)))
                if let panLengthByte = Int(panLengthHex) {
                    let panLength = panLengthByte * 2
                    let pan = decryptedText.substringWithRange(Range<String.Index>(start: decryptedText.startIndex.advancedBy(4), end: decryptedText.startIndex.advancedBy(4+panLength)))
                    cardData?.accountNumber = pan
                }
                
            }
            
            // try searching for 5f30 EMV tag (service code)
            if let serviceCodeRange = decryptedText.rangeOfString("5f30") {
                let serviceCodeLengthHex = decryptedText.substringWithRange(Range<String.Index>(start: serviceCodeRange.endIndex, end: serviceCodeRange.endIndex.advancedBy(2)))
                if let serviceCodeLengthByte = Int(serviceCodeLengthHex) {
                    let serviceCodeLength = serviceCodeLengthByte * 2
                    let serviceCode = decryptedText.substringWithRange(Range<String.Index>(start: serviceCodeRange.endIndex.advancedBy(2), end: serviceCodeRange.endIndex.advancedBy(2+serviceCodeLength)))
                    cardData?.serviceCode = Int32(serviceCode)
                }
                
                
            }
        }
    }
    
    /**
     C30 handler - good read
     This is to handle the good response when a card is read successfully
     1. get expiry date from card
     2. get pin pad serial number
     3. get track1 data (encrypted)
     4. get blob1 data from track1 data
     5. get PAN from blob1
     6. get interac bin number and compare with the card being read
     7. send a request to validate card
    */
    func handleSuccessfulCardRead() {
        if (!PINManagemenetViewController.transactionStarted) {
            return
        }
        instructionsLabel.text = NSLocalizedString("instruction_label_please_wait", comment: "PIN Management View Controller")
        cardData = CardInfo()
        let expiryDate = VerifoneDevice.pinpad()?.vfiCardData.expiryDate
        let pinpadSerialNumber = VerifoneDevice.pinpad()?.vfiDiagnostics.pinpadSerialNumber
        VerifoneDevice.pinpad()?.getPKICipheredData()
        if let blob1 = VerifoneDevice.pinpad()?.vfiCipheredData.encryptedBlob_1 {
            if let blob1Data = NSData(base64EncodedString: blob1, options: NSDataBase64DecodingOptions(rawValue: 0)) {
                retrieveDecryptedPAN(blob1Data)
            }
        } else {
            let error = generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode.PinpadDecryptionError.rawValue, errorDescription: PMErrorCode.PinpadDecryptionError.localizedDescription())
            
            self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
                self.endTransaction()
            })
            return
        }
        
        
        let pinManagementPList = NSBundle.mainBundle().pathForResource("PinManagementProperties", ofType: "plist")!
        
        let pinManagementDict = NSDictionary(contentsOfFile: pinManagementPList)
        let interacBin = pinManagementDict?.objectForKey("interacBin") as? String
        let cardNumber = cardData?.accountNumber
        
        if (cardNumber != nil && interacBin != nil && cardNumber!.hasPrefix(interacBin!) && expiryDate != nil) {
            //240630
            let expiryYear = expiryDate!.substringWithRange(Range<String.Index>(start: expiryDate!.startIndex.advancedBy(0), end: expiryDate!.startIndex.advancedBy(2)))
            
            let expiryMonth = expiryDate!.substringWithRange(Range<String.Index>(start: expiryDate!.startIndex.advancedBy(2), end: expiryDate!.startIndex.advancedBy(4)))
            
            if (cardFirstTimeActivated) {
                cardAuthorization()
            } else {
                if (pinpadSerialNumber != nil) {
                    
                    APIManagement.sharedInstance.validateScotiaCard(cardNumber!, expiryMonth: expiryMonth, expiryYear: expiryYear, serialNumber: pinpadSerialNumber!, completion: { (data: AnyObject?, error: NSError?) -> () in
                        if let uData = data as? [String: AnyObject] {
                            if let uResponse = uData["response"] as? [String: AnyObject]{
                                if let uErrorCode = uResponse["errorCode"] as? String where uErrorCode == "ACSUCC02" {
                                    self.cardAuthorization()
                                    return
                                }
                            }
                            var errorObject: AnyObject?
                            if let uError = uData["error"] as? [String: String] {
                                errorObject = [uError]
                            }
                            let tempError = APIManagement.sharedInstance.createNSErrorFromAPI(errorObject, domain: Domain.CustomerApp, errorCode: CXErrorCode.PinPadCardStatusError)
                            let alertController = AlertControllerUtil.createOKAlertController(tempError)
                            self.presentViewController(alertController, animated: true, completion: nil)
                        }
                        
                        if let uError = error {
                            let alertController = AlertControllerUtil.createOKAlertController(uError)
                            self.presentViewController(alertController, animated: true, completion: nil)
                        }
                        
                        let delayTime = dispatch_time(DISPATCH_TIME_NOW,
                            Int64(1 * Double(NSEC_PER_SEC)))
                        dispatch_after(delayTime, dispatch_get_main_queue()) {
                            self.endTransaction()
                        }
                    })
                } else {
                    
                    let error = self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode.RetrievingSerialError.rawValue, errorDescription: PMErrorCode.RetrievingSerialError.localizedDescription())
                    
                    self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
                        self.endTransaction()
                    })
                    
                }
                
            }
            
        } else if (cardNumber != nil && !cardNumber!.hasPrefix(interacBin!)){
            
            let error = self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.InvalidCardError.rawValue, errorDescription: CXErrorCode.InvalidCardError.localizedDescription())
            
            self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
                self.endTransaction()
            })
        } else if (cardNumber != nil){
            let error = self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.InvalidCustomeressionDefaultError.rawValue, errorDescription: CXErrorCode.InvalidCustomeressionDefaultError.localizedDescription())
            
            self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
                self.endTransaction()
            })
        }
        
        
        
    }

    /**
    C30 handler - when card times out
    */
    func handleCardReadTimedOut() {
        
        let error = self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode.TimeOutError.rawValue, errorDescription: PMErrorCode.TimeOutError.localizedDescription())
        
        self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
            self.endTransactionWithError()
        })
        
    }
    
    /**
    C30 Handler - Handle uncommon errors that weren't handled before
    
    - parameter responseCode: Int32 responseCode
    */
    func handledefaultCardRead(responseCode: Int32) {
        let errorMessage = verifoneErrorMessage(responseCode)
        let c30DefaultErrorMessage = PMErrorCode.c30GenericError.localizedDescription()
        let wholeDefaultCardReadError = NSString(format: c30DefaultErrorMessage, errorMessage)
        
        let error = self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode.c30GenericError.rawValue, errorDescription: wholeDefaultCardReadError as String)
        
        self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
            self.endTransactionWithError()
            self.cancelVerifoneCommand()
        })
        
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++ C32 ++++++++++++++++++++++++++++++++++++++++++++++++++
    
    /**
    Calling c32 to request PIN from the customer
    */
    func cardAuthorization() {
        let array = NSMutableArray()
        array.addObject("4F")
        //emv request (card authorization)
        
        initializePinPadButton.hidden = true
        instructionsLabel.text = NSLocalizedString("instruction_label_enter_pin", comment: "PIN Management View Controller")
        
        let cardAuthResponse = VerifoneDevice.pinpad()?.C32(0.00, otherAmount: 0.00, merchantDecision: 0, minRequestObjects: array)
        
        if (cardAuthResponse >= 0) {
            switch UInt(cardAuthResponse!) {
            case 0:
                handleEMVSuccess()
            case 6:
                handleEMVTimedOut()
            case 8:
                handleEMVCancelled()
            case 23:
                handleEMVRemoved()
            default:
                handleEMVError(cardAuthResponse!)
                
            }
        } else {
            cancelVerifoneCommand()
        }
    }
    
    /**
    C32 handler - handle success case
    If PIN is good, call validate PIN offline, otherwise show an error
    */
    func handleEMVSuccess() {
        getCardEMVTags()
        let pinSuccessResponse = VerifoneDevice.pinpad()?.vfiEMVAuthorization.pinSuccessful
        
        //pin success return call
        switch UInt(pinSuccessResponse!) {
        case 0:
            validatePinOffline()
            
        case 2:
            let error = self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode.PinTryExceededError.rawValue, errorDescription: PMErrorCode.PinTryExceededError.localizedDescription())
            
            self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
                self.endTransaction()
            })
            
        default: //1
            
            let error = self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode.PinTryExceeded2Error.rawValue, errorDescription: PMErrorCode.PinTryExceeded2Error.localizedDescription())
            
            self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
                self.endTransaction()
            })
            
        }
        
    }
    

     /**
     This is to be used after C32 is called.  The main purpose of this is to retrieve the EMV tag information for the mid-tier calls
     */
    func getCardEMVTags() {
        
        VerifoneDevice.pinpad()?.S95()
        
        //get more emv tags
        let tagsArray = NSMutableArray()
        tagsArray.addObject("9F34")
        tagsArray.addObject("82")
        tagsArray.addObject("9F02")
        tagsArray.addObject("9F03")
        tagsArray.addObject("9F1A")
        tagsArray.addObject("5F2A")
        tagsArray.addObject("9C")
        tagsArray.addObject("9F37")
        tagsArray.addObject("9F10")
        
        VerifoneDevice.pinpad()?.C36(tagsArray)
        
        let expiryDate = VerifoneDevice.pinpad()?.vfiCardData.expiryDate
        
        //expiryDate in format of yymm30
        let expiryDateYear = expiryDate!.substringWithRange(Range<String.Index>(start: expiryDate!.startIndex.advancedBy(0), end: expiryDate!.startIndex.advancedBy(2)))
        let expiryDateMonth = expiryDate!.substringWithRange(Range<String.Index>(start: expiryDate!.startIndex.advancedBy(2), end: expiryDate!.startIndex.advancedBy(4)))
        cardData?.expiryMonth = expiryDateMonth
        cardData?.expiryYear = expiryDateYear
        
        var appPanSeq:NSData? = nil
        
        if let emvTags = VerifoneDevice.pinpad()?.vfiCardData.emvTags {
            
            appPanSeq = emvTags["5F34"] as? NSData
            cardData?.appPanSeq = appPanSeq
        }
        
        var atc:NSData? = nil
        var terminalVerificationResults:NSData? = nil
        var appCryptogram:NSData? = nil
        
        if let emvC32Tags = VerifoneDevice.pinpad()?.vfiEMVAuthorization.emvTags {
            
            atc = emvC32Tags["9F36"] as? NSData
            cardData?.atc = atc
            
            terminalVerificationResults = emvC32Tags["95"] as? NSData
            cardData?.terminalVerificationResults = terminalVerificationResults
            
            appCryptogram = emvC32Tags["9F26"] as? NSData
            cardData?.appCryptogram = appCryptogram
        }
        
        var aip: NSData? = nil
        var amountAuth: NSData? = nil
        var amountAuthOther: NSData? = nil
        var terminalCountryCode: NSData? = nil
        var transactionCurrencyCode: NSData? = nil
        var transactionType: NSData? = nil
        var unpredictableNumber: NSData? = nil
        var iad: NSData? = nil
        
        if let emvC36Tags = VerifoneDevice.pinpad()?.vfiEMVTags.emvTags {
            aip = emvC36Tags["82"] as? NSData
            cardData?.aip = aip
            
            amountAuth = emvC36Tags["9F02"] as? NSData
            cardData?.amountAuth = amountAuth
            
            amountAuthOther = emvC36Tags["9F03"] as? NSData
            cardData?.amountAuthOther = amountAuthOther
            
            terminalCountryCode = emvC36Tags["9F1A"] as? NSData
            cardData?.terminalCountryCode = terminalCountryCode
            
            transactionCurrencyCode = emvC36Tags["5F2A"] as? NSData
            cardData?.transactionCurrencyCode = transactionCurrencyCode
            
            transactionType = emvC36Tags["9C"] as? NSData
            cardData?.transactionType = transactionType
            
            unpredictableNumber = emvC36Tags["9F37"] as? NSData
            cardData?.unpredictableNumber = unpredictableNumber
            
            iad = emvC36Tags["9F10"] as? NSData
            cardData?.iad = iad
            
            let dki = iad?.subdataWithRange(NSMakeRange(2, 2))
            cardData?.dki = dki
            
            let cvn = iad?.subdataWithRange(NSMakeRange(4, 2))
            cardData?.cvn = cvn
            
        }
        
        let appAid = VerifoneDevice.pinpad()?.vfiCardData.AID
        cardData?.appAid = appAid
        
        
        let date = NSDate()
        let dateFormatter = NSDateFormatter()
        dateFormatter.dateFormat = "yyMMdd"
        
        
        let transactionDate = dateFormatter.stringFromDate(date)
        cardData?.transactionDate = transactionDate
        
        if (amountAuth != nil &&
            terminalCountryCode != nil &&
            terminalVerificationResults != nil &&
            transactionCurrencyCode != nil &&
            transactionType != nil &&
            unpredictableNumber != nil) {
                let cdol1 = NSMutableData(data: amountAuth!)
                cdol1.appendData(amountAuthOther!)
                cdol1.appendData(terminalCountryCode!)
                cdol1.appendData(terminalVerificationResults!)
                cdol1.appendData(transactionCurrencyCode!)
                cdol1.appendData(PINManagementUtilities.fromHexString(transactionDate))
                cdol1.appendData(transactionType!)
                cdol1.appendData(unpredictableNumber!)
                cardData?.cdol1 = cdol1
        }
        
    }
    
    /**
    validatePinOffline called when C32 is called successfully when pin matched
     1. call validate pin offline
     2. if there is no error, display customer profile
    */
    func validatePinOffline() {
        if (cardData != nil && cardData!.expiryMonth != nil &&
            cardData!.expiryYear != nil && cardData!.serviceCode != nil &&
            cardData!.appPanSeq != nil && cardData!.appCryptogram != nil &&
            VerifoneDevice.pinpad()?.vfiDiagnostics.pinpadSerialNumber != nil &&
            cardData!.cdol1 != nil && cardData!.iad != nil &&
            cardData!.atc != nil && cardData!.aip != nil) {
                let expiryMonth  = cardData!.expiryMonth!
                let expiryYear = cardData!.expiryYear!
                let serviceCode = cardData!.serviceCode!
                let appPanSeq = PINManagementUtilities.toHexString(cardData!.appPanSeq!)
                let appCrypto = PINManagementUtilities.toHexString(cardData!.appCryptogram!)
                let serialNumber = VerifoneDevice.pinpad()!.vfiDiagnostics.pinpadSerialNumber!
                let cdol1 = PINManagementUtilities.toHexString(cardData!.cdol1!)
                let iadLength = cardData!.iad!.length
                let iad = PINManagementUtilities.toHexString(cardData!.iad!)
                let atc = PINManagementUtilities.toHexString(cardData!.atc!)
                let aip = PINManagementUtilities.toHexString(cardData!.aip!)
                
                var validCard = false
                var validCustomerSession = false
                
                if (cardData!.accountNumber != nil) {
                    
                    APIManagement.sharedInstance.validatePinOffline(cardData!.accountNumber!, expiryMonth: Int(expiryMonth)!, expiryYear: Int(expiryYear)!, serviceCode: String(serviceCode), appPanSeq: appPanSeq, appCrypto: appCrypto, appAid: cardData!.appAid!, atc: atc, aip: aip, cdol1Data: cdol1, iadLen: iadLength, iadData: iad, vcryptSerialNo: serialNumber, completion: { (data: AnyObject?, error: NSError?) -> () in
                        if let uData = data as? [String: AnyObject] {
                            if let uResponse = uData["response"] as? [String: AnyObject] {
                                if let uErrorCode = uResponse["errorCode"] as? String {
                                    validCard = uErrorCode == "ACSUCC01"
                                    validCustomerSession = validCard || PINManagementConstants.CustomerSession.warningCode.contains(uErrorCode)
                                }
                                
                                //this is for the brand new card issued by call center
                                if let cssFirstUseActSw = uResponse["cssFirstUseActSw"] as? String {
                                    PINManagementAction.completeEMVTransaction(validCard, verifyPinOfflineResponse: uResponse)
                                    if (cssFirstUseActSw.characters.count > 0) {
                                        self.cardFirstTimeActivated = true
                                        self.cardRead()
                                        return
                                    }
                                }
                            }
                            
                            if let uError = uData["error"] as? [String: String] {
                                let tempError = APIManagement.sharedInstance.createNSErrorFromAPI([uError], domain: Domain.CustomerApp, errorCode: CXErrorCode.PinPadCardStatusError)
                                let alertController = AlertControllerUtil.createOKAlertController(tempError, handler: { (alertAction: UIAlertAction) -> () in
                                    if validCustomerSession {
                                        self.loadCustomerProfile(nil)
                                    }
                                })
                                self.presentViewController(alertController, animated: true, completion: nil)
                            } else {
                                if validCustomerSession {
                                    self.loadCustomerProfile(nil)
                                }
                            }
                        }
                        
                        if let uError = error {
                            let alertController = AlertControllerUtil.createOKAlertController(uError)
                            self.presentViewController(alertController, animated: true, completion: nil)
                        }
                        
                        self.endTransaction(validCustomerSession)
                    })
                    return
                }
        }
        self.endTransaction()
    }
    
    /**
     C32 - handle case when card is removed
     */
    func handleEMVRemoved() {
        let accountNumber =  cardData?.accountNumber
        if accountNumber != nil {
            
            let error = self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode
                .CardRemovedError.rawValue, errorDescription: PMErrorCode.CardRemovedError.localizedDescription())
            
            self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
                self.endTransaction()
            })
        }
        
        
    }
    
    /**
     C32 - handle case when transaction times out
     */
    func handleEMVTimedOut() {
        
        let error = self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode
            .CardTimedOutError.rawValue, errorDescription: PMErrorCode.CardTimedOutError.localizedDescription())
        
        self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
            self.endTransactionWithError()
        })
    }
    
    /**
     C32 - handle case when transaction gets cancelled
     */
    func handleEMVCancelled() {
        
        let error = self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode
            .EMVCancelledError.rawValue, errorDescription: PMErrorCode.EMVCancelledError.localizedDescription())
        
        self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
            self.endTransactionWithError()
        })
    }
    
    /**
     C32 - handle case when transaction gets an generic error.
     
     - parameter error: <#error description#>
     */
    func handleEMVError(error:Int32) {
        let errorMessage = verifoneErrorMessage(error)
        let c32DefaultErrorMessage = PMErrorCode.c32GenericError.localizedDescription()
        let wholeDefaultCardReadError = NSString(format: c32DefaultErrorMessage, errorMessage)
        
        let error = self.generateErrorFromInfo(Domain.PinManagement.rawValue, code: PMErrorCode
            .c32GenericError.rawValue, errorDescription: wholeDefaultCardReadError as String)
        
        self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in
            self.endTransactionWithError()
        })
    }
    
    //++++++++++++++++++++++++++++++++++++++++++++ end transaction ++++++++++++++++++++++++++++++++++++++++++++++++++
    
    /**
    Set transaction to false and have buttons shown
    */
    func endTransaction() {
        endTransaction(false)
        
    }
    
    
    /**
     Set transaction to false
     
     - parameter display: should initialize button and close button be hidden
     */
    func endTransaction(display: Bool) {
        VerifoneDevice.control()?.keypadEnabled(false)
        instructionsLabel.text = NSLocalizedString("instruction_label_remove_card", comment: "PIN Management View Controller")
        initializePinPadButton.setTitle(NSLocalizedString("instruction_initialize_pinpad", comment: "PIN Management View Controller"), forState: UIControlState.Normal)
        initializePinPadButton.hidden = display
        buttonClose?.hidden = display
        cardFirstTimeActivated = false
        PINManagemenetViewController.transactionStarted = false
        startCardRead = false
        let delayTime = dispatch_time(DISPATCH_TIME_NOW,
            Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            [weak self] in
            let removeCard = VerifoneDevice.pinpad()?.removeCard()
            if (removeCard == 0) {
                self?.instructionsLabel?.text = NSLocalizedString("instruction_label_card_removed", comment: "PIN Management View Controller")
            }
        }
    }
    
    /**
     end transaction and show the error
     
     - parameter errorMessage:         the error message
     - parameter instructionText:      the instruction message
     - parameter showInitializeButton: should show initialize button?
     */
    func endTransactionWithError(var errorMessage: String?, instructionText: String?, showInitializeButton: Bool) {
        if (errorMessage == nil) {
            errorMessage = NSLocalizedString("instruction_initialize_pinpad", comment: "PIN Management View Controller")
        }
        
        PINManagemenetViewController.transactionStarted = false
        startCardRead = false
        instructionsLabel.text = instructionText != nil ? instructionText : NSLocalizedString("error_title", comment: "")
        initializePinPadButton.setTitle(errorMessage, forState: UIControlState.Normal)
        initializePinPadButton.hidden = !showInitializeButton
        buttonClose?.hidden = false
        cardFirstTimeActivated = false
        VerifoneDevice.pinpad()?.S01()
    }
    
    func endTransactionWithError() {
        endTransactionWithError(nil, instructionText: nil, showInitializeButton: true)
    }
    
    
    //++++++++++++++++++++++++++++++++++++++++++++ show alert message ++++++++++++++++++++++++++++++++++++++++++++++++++
    
    /**
    generate error object from a given domain, code, error description
    
    - parameter domain:           error domain
    - parameter code:             error code
    - parameter errorDescription: error description
    
    - returns: NSError object
    */
    func generateErrorFromInfo(domain: String?, code: Int?, errorDescription: String?) -> NSError {
        var error : NSError
        if let uDomain = domain, uCode = code, uED = errorDescription {
            var userInfo = [NSObject : AnyObject]()
            userInfo[NSLocalizedDescriptionKey] = uED
            error = NSError(domain: uDomain, code: uCode, userInfo: userInfo)
            print (error.localizedDescription)
        } else {
            var userInfo = [NSObject : AnyObject]()
            userInfo[NSLocalizedDescriptionKey] = CXErrorCode.UnknownError.localizedDescription()
            error = NSError (domain: Domain.CustomerApp.rawValue, code: CXErrorCode.UnknownError.rawValue, userInfo: userInfo)
        }
        return error
    }
    
    /**
     show alert message based error message, cancel button title, with completion block
     
     - parameter error:             error message
     - parameter cancelButtonTitle: cancel button title
     - parameter completion:        completion block
     - parameter skip:              rather or not the alert message should be displayed
     */
    func showAlertMessage(error: NSError, cancelButtonTitle: String?, completion:() -> (), skip: Bool) {
        
        let errorTitle = NSLocalizedString("error", comment: "") + " - " + error.domain + String(error.code)
        let errorMessage = error.localizedDescription
        
        let alertController = UIAlertController(title: errorTitle, message: errorMessage, preferredStyle: UIAlertControllerStyle.Alert)
        let alertAction = UIAlertAction(title: cancelButtonTitle, style: UIAlertActionStyle.Cancel, handler: nil)
        alertController.addAction(alertAction)
        
        dispatch_async(dispatch_get_main_queue(), {
            if (PINManagemenetViewController.transactionStarted || skip) {
                self.presentViewController(alertController, animated: true, completion: nil)
            }
            let delayTime = dispatch_time(DISPATCH_TIME_NOW,
                Int64(1 * Double(NSEC_PER_SEC)))
            dispatch_after(delayTime, dispatch_get_main_queue()) {
                completion()
            }
        })
    }
    
    /**
     show alert message with error block, cancel button title, and completion block
     
     - parameter error:             NSError error
     - parameter cancelButtonTitle: cancel button title description
     - parameter completion:        completion block
     */
    func showAlertMessage(error: NSError, cancelButtonTitle: String?, completion:() -> ()) {
        
        showAlertMessage(error, cancelButtonTitle: cancelButtonTitle, completion: completion, skip: false)
    }
    
    /**
     show alert message based on error block and cancel button title
     
     - parameter error:             NSError error
     - parameter cancelButtonTitle: cancel button title
     */
    func showAlertMessage(error: NSError, cancelButtonTitle: String?) {
        showAlertMessage(error, cancelButtonTitle: cancelButtonTitle) { () -> () in
        }
    }
    
    
    //++++++++++++++++++++++++++++++++++++++++++++ localization ++++++++++++++++++++++++++++++++++++++++++++++++++
    
    /**
    generate a generic Verifone error message
    
    - parameter responseCode: response code from VeriFone
    
    - returns: Map the error code to a localized message
    */
    func verifoneErrorMessage(responseCode:Int32) -> String {
        return NSLocalizedString("C30_C32_error_\(responseCode)", comment: "PIN Management View Controller")
    }
    
    
    //++++++++++++++++++++++++++++++++++++++++++++ load customer profile ++++++++++++++++++++++++++++++++++++++++++++++++++
    
    /**
    load customer profile
    
    - parameter accountNumber: String account number
    */
    func loadCustomerProfile(var accountNumber: String?) {
        
        if (accountNumber == nil) {
            accountNumber =  cardData?.accountNumber
        }
        
        if let accountNumber = accountNumber?.stringByTrimmingCharactersInSet(NSCharacterSet.whitespaceCharacterSet()) {
            APIManagement.sharedInstance.locateCustomerByProduct(accountNumber, lookupSubType: LookUpSubType.Pinned, completion: { (data, error) -> (
                ) in
                if error == nil {
                    
                    self.dismissViewControllerAnimated(true, completion: { () -> Void in
                        self.customersLookupDelegate?.didReceiveCustomersSearchData(data, error: error, lookUpSubType: LookUpSubType.Pinned)
                    })
                }
                else {
                    let error = self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.CannotFindRecordError.rawValue, errorDescription: CXErrorCode.CannotFindRecordError.localizedDescription())
                    
                    self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in }, skip: true)
                    
                    self.initializePinPadButton.hidden = false
                    self.buttonClose?.hidden = false
                    
                }
            })
        }
        else {
            
            let error = self.generateErrorFromInfo(Domain.CustomerApp.rawValue, code: CXErrorCode.CannotFindRecordError.rawValue, errorDescription: CXErrorCode.CannotFindRecordError.localizedDescription())
            
            self.showAlertMessage(error, cancelButtonTitle: self.uiKitBundle?.localizedStringForKey("OK", value: nil, table: nil), completion: { () -> () in }, skip: true)
            
            initializePinPadButton.hidden = false
            buttonClose?.hidden = false
            
        }
        
    }
    
    
    //++++++++++++++++++++++++++++++++++++++++++++ verifone pinpad logging ++++++++++++++++++++++++++++++++++++++++++++++++++
    
    /**
    control serial data method used for VeriFone for troubleshooting data flow on pin pad
    
    - parameter data:
    - parameter isIncoming: Bool isIncoming
    */
    func controlSerialData(data: NSData!, incoming isIncoming: Bool) {
        printDataToConsole(data)
    }
    
    /**
     pinpad serial data method used for VeriFone for troubleshooting data flow on pin pad
     
     - parameter data:       NSData data
     - parameter isIncoming: Bool isIncoming
     */
    func pinpadSerialData(data: NSData!, incoming isIncoming: Bool) {
        printDataToConsole(data)
    }
    
    /**
     barcode serial data method used for VeriFone for troubleshooting data flow on pin pad
     
     - parameter data:       NSData data
     - parameter isIncoming: Bool isIncoming
     */
    func barcodeSerialData(data: NSData!, incoming isIncoming: Bool) {
        printDataToConsole(data)
    }
    
    /**
     bluetooth serial data method used for VeriFone for troubleshooting data flow on pin pad
     
     - parameter data:       NSData data
     - parameter isIncoming: Bool isIncoming
     */
    func btSerialData(data: NSData!, incoming isIncoming: Bool) {
        printDataToConsole(data)
    }
    
    /**
     barcode log entry method used by VeriFone
     
     - parameter logEntry: String log entry from VeriFone
     - parameter severity: Int32 severity
     */
    func barcodeLogEntry(logEntry: String!, withSeverity severity: Int32) {
        printToConsole(logEntry)
    }
    
    /**
     control log entry method used by VeriFone
     
     - parameter logEntry: String log entry from VeriFone
     - parameter severity: Int32 severity
     */
    func controlLogEntry(logEntry: String!, withSeverity severity: Int32) {
        printToConsole(logEntry)
    }
    
    /**
     pinpad log entry method used by VeriFone
     
     - parameter logEntry: String log entry from VeriFone
     - parameter severity: Int32 severity
     */
    func pinpadLogEntry(logEntry: String!, withSeverity severity: Int32) {
        printToConsole(logEntry)
    }
    
    /**
     pinpad data sent out method used by VeriFone
     
     - parameter data: binary data from VeriFone
     */
    func pinpadDataSent(data: NSData!) {
        printDataToConsole(data)
    }
    
    /**
     pinpad data received method used by VeriFone
     
     - parameter data: binary data from VeriFone
     */
    func pinpadDataReceived(data: NSData!) {
        printDataToConsole(data)
    }
    
    /**
     pinpad server data method used by VeriFone
     
     - parameter data: binary data from VeriFone
     */
    func pinpadServerData(data: NSData!, incoming isIncoming: Bool) {
        printDataToConsole(data)
    }
    
    /**
     print out onto the console for debugging purposes
     
     - parameter logEntry: the string that needs to be printed out
     */
    func printToConsole(logEntry: String!) {
        if logFlag {
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
            
            let date = NSDate()
            
            let timestamp = dateFormatter.stringFromDate(date)
            print("\(timestamp) ======== \(logEntry)")
        }
    }
    
    /**
     print out onto the console for debugging purposes
     
     - parameter logEntry: NSData log entry
     */
    func printDataToConsole(logEntry: NSData!) {
        if logFlag {
            let dateFormatter = NSDateFormatter()
            dateFormatter.locale = NSLocale(localeIdentifier: "en_US_POSIX")
            dateFormatter.dateFormat = "yyyy-MM-dd HH:mm:ss.SSS"
            
            let date = NSDate()
            
            let timestamp = dateFormatter.stringFromDate(date)
            print("\(timestamp) ======== \(logEntry)")
        }
    }
    
    /**
     Action when initialize pin pad button is pressed.
     1. if pin pad isn't initialized, call checkPinPad to make sure the pin pad connection is established
     2. if pin pad is initialized, and that transaction isn't started yet, if pinpad config hasn't returned any pin pad configuration yet, call to check configuation, and load pin pad EMV configuration is possible
     3. if pin pad configuration is already returned by mid-tier, assuming that pin pad is already configured (if there is an error on the mid-tier, VeriFoneDevice values will get reset back to nil), continue the customer session to read card
     
     - parameter sender: UIButton reference to initialize pin pad button
     
     - returns: none
     */
    @IBAction func initializePinPadButtonPressed(sender: UIButton) {
        
        sender.hidden = true
        sender.highlighted = false
        buttonClose?.hidden = true
        
        if (!PINManagemenetViewController.initFinished) {
            instructionsLabel.text = NSLocalizedString("instruction_label_initializing", comment: "PIN Management View Controller")
            initializeJob = NSTimer.scheduledTimerWithTimeInterval(0, target: self, selector: Selector("checkPinPad"), userInfo: nil, repeats: false)
        } else if (!PINManagemenetViewController.transactionStarted){
            if (VerifoneDevice.publicKey == nil || VerifoneDevice.publicKeyP7s == nil || VerifoneDevice.publicID == nil || VerifoneDevice.publicIDP7s == nil || VerifoneDevice.pkcs12 == nil) {
                setPinPadConfigInitialization()
            } else {
                cardRead()
            }
            
        } else {
            instructionsLabel.text = NSLocalizedString("cancelling", comment: "")
            cancelVerifoneCommand()
            initializePinPadButton.hidden = false
            buttonClose?.hidden = false
            initializePinPadButton.setTitle(NSLocalizedString("instruction_initialize_pinpad", comment: "PIN Management View Controller"), forState: UIControlState.Normal)
            instructionsLabel.text = NSLocalizedString("instruction_label_ready", comment: "PIN Management View Controller")
            PINManagemenetViewController.transactionStarted = false
        }
        
    }
    
    /**
    check to see if pinpad is connected and paired properly.  After 3 seconds, if pin pad is still not initialized successfully (meaning there is a problem on the pin pad or the iPad itself), show an error to the user
    */
    func checkPinPadOffline() {
        if (!PINManagemenetViewController.initFinished) {
            initializeTimer?.invalidate()
            initializeJob?.invalidate()
            let message = "\(Domain.PinManagement.rawValue)" + "\(PMErrorCode.PinpadConnectionError.rawValue)" + "\n" + "\(NSLocalizedString("instruction_label_pinpad_offline", comment: "PIN Management View Controller"))"
            endTransactionWithError(nil, instructionText: message, showInitializeButton: false)
        }
    }
    
    /**
     Give pin pad and iPad somet time to respond to in order to get it initialized.  After 3 seconds, call checkPinPadOffline to see if the pin pad is established successfully.  This is to make sure the pin pad is connected successfully, just in case pin pad is not responding to the iPad.
     */
    func checkPinPad() {
        let delayTime = dispatch_time(DISPATCH_TIME_NOW,
            Int64(1 * Double(NSEC_PER_SEC)))
        dispatch_after(delayTime, dispatch_get_main_queue()) {
            [weak self] in
            self?.initVFDevice()
            self?.initializeTimer = NSTimer.scheduledTimerWithTimeInterval(3, target: self!, selector: Selector("checkPinPadOffline"), userInfo: nil, repeats: false)
        }
    }
    
    // MARK: - NEW
    /**
    Action to dismiss the view controller
    */
    @IBAction func closeButtonPressed() {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
}

