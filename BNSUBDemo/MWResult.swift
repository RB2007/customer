//
//  MWResult.swift
//  MWBarcodeCameraDemo
//
//  Created by plaisio on 1/14/15.
//  Copyright (c) 2015 Manateeworks. All rights reserved.
//

import Foundation
import CoreGraphics

func bufferToInt32(buf:UnsafeMutablePointer<UInt8>!, start:Int) -> Int32{
    
    var value:Int32 = 0;
    
    for var i = 0; i < 4; ++i{
        if i > 0 {
            value *= 256;
        }
        value = value + Int32(buf[start + 3 - i]);
    }
    
    return value;
}

func bufferToString(buf:UnsafeMutablePointer<UInt8>!, start:Int, length:Int) -> NSString{
    
    var value:String = "";
    
    for var i = 0; i < length; ++i{
       
        value.append(Character(UnicodeScalar(Int(buf[start + i]))));
    }
    
    return value;
}


class MWResult: NSObject{
    
    var locationPoints:[CGPoint]! = nil;
    var isGS1:Bool = false;
    var imageHeight:Int32 = 0;
    var subtype:Int32 = 0;
    var imageWidth:Int32 = 0;
    var typeName:String! = nil;
    var type:Int32 = 0;
    var bytesLength:Int32 = 0;
    var bytes:[UInt8]! = nil;
    var encryptedResult:[UInt8]! = nil;
    var text:String! = nil;
    
  
}


class MWResults: NSObject {

    var results:NSMutableArray!
    var count:Int = 0;
    var version:UInt8 = 0;

   
    
    
    init(buffer:UnsafeMutablePointer<UInt8>!, length:Int){
       // println("inited")
        super.init()
        if buffer[0] != 77 || buffer[1] != 87 || buffer[2] != 82 {
            return
        }
        
//        let bufferdata = NSData(bytes: buffer, length: length)
        
        results = NSMutableArray()
        
        self.count = 0
        version = buffer[3]
        let count = buffer[4]
        var currentPos:Int = 5
        for var i=0; i<Int(count); ++i {
            let result:MWResult = MWResult()
            
            let fieldsCount: UInt8 = buffer[currentPos]
            ++currentPos
            
            for var f = 0; f<Int(fieldsCount);++f{
                
                let fieldType = Int32(buffer[currentPos])
                let fieldNameLength = Int(buffer[currentPos + 1])
                let fieldContentLength = Int((256 * Int32(buffer[currentPos + 3 + fieldNameLength])) + Int32(buffer[currentPos + 2 + fieldNameLength]))
//                let fieldName:String
//                
//                if fieldNameLength > 0 {
//                   // let data = NSData(bytes: buffer, length:fieldContentLength Int(fieldNameLength))
//                    //Not sure if it will work
//                    fieldName = (NSString(bytes: UnsafePointer<UInt8>(bufferdata.bytes + currentPos + 2), length: fieldNameLength, encoding: NSUTF8StringEncoding) as? String)!
//                }
                
//                let floatSize = sizeof(Float)
                let contentPos:Int = currentPos + fieldNameLength + 4
                var locations:[Float] = Array(count: 8 , repeatedValue: 0.0)
                
                
                
                switch fieldType {
                    //notsure if it works
                case Int32(MWB_RESULT_FT_TYPE):
                    result.type = bufferToInt32(buffer, start: contentPos)
                    //not sure
                    result.typeName = getTypeName(bufferToInt32(buffer, start: contentPos))
                    break
                case Int32(MWB_RESULT_FT_SUBTYPE):
                    result.subtype = bufferToInt32(buffer, start: contentPos)
                    break
                case Int32(MWB_RESULT_FT_ISGS1):
                    result.isGS1 = buffer[contentPos]==1
                    break
                case Int32(MWB_RESULT_FT_IMAGE_WIDTH):
                    result.imageWidth = bufferToInt32(buffer, start: contentPos)
                    break
                case Int32(MWB_RESULT_FT_IMAGE_HEIGHT):
                    result.imageHeight = bufferToInt32(buffer, start: contentPos)
                    break
                case Int32(MWB_RESULT_FT_LOCATION):
                    for var i:Int = 0;i<8;++i {
                        var f:Float = 0;
                        let bytes:Array<UInt8> = [buffer[contentPos + i * 4 + 0], buffer[contentPos + i * 4 + 1], buffer[contentPos + i * 4 + 2], buffer[contentPos + i * 4 + 3]]
                        memcpy(&f, bytes, 4)
                        
                        locations[i] = f;
                    }
                    
                    result.locationPoints = [CGPoint](count: 4, repeatedValue: CGPoint(x:0,y:0));
                    
//                    var points:[CGPoint]!
                    result.locationPoints[0].x = CGFloat(locations[0])
                    result.locationPoints[0].y = CGFloat(locations[1])
                    result.locationPoints[1].x = CGFloat(locations[2])
                    result.locationPoints[1].y = CGFloat(locations[3])
                    result.locationPoints[2].x = CGFloat(locations[4])
                    result.locationPoints[2].y = CGFloat(locations[5])
                    result.locationPoints[3].x = CGFloat(locations[6])
                    result.locationPoints[3].y = CGFloat(locations[7])
                    
                    break
                case Int32(MWB_RESULT_FT_TEXT):
                    
                    result.text = bufferToString(buffer, start: contentPos, length: fieldContentLength) as String;
                    break
                case Int32(MWB_RESULT_FT_BYTES):
                    result.bytes = [UInt8](count: fieldContentLength, repeatedValue: 0)
                    for var i = 0; i < fieldContentLength; ++i{
                        result.bytes[i] = buffer[contentPos + i];
                    }
                    result.bytesLength = Int32(fieldContentLength)

                    break
                case Int32(MWB_RESULT_FT_PARSER_BYTES):
                    result.encryptedResult = [UInt8](count: fieldContentLength, repeatedValue: 0)
                    for var i = 0; i < fieldContentLength; ++i{
                        result.encryptedResult[i] = buffer[contentPos + i];
                    }
                    
                    break

                    
                default: break
                    
                }
                
                

                
                
                currentPos =  currentPos + (fieldNameLength + fieldContentLength + 4);
            }
            
            
            self.results.addObject(result)
        }

        
//                    case MWB_RESULT_FT_BYTES:
//                        result.bytes = malloc(fieldContentLength);
//                        result.bytesLength = fieldContentLength;
//                        memcpy(result.bytes, &buffer[contentPos], fieldContentLength);
//                        
//                        break;
//                        
//                        
//                    default:
//                        break;
//                    }
//                  
        
        self.count = Int(count)

        
    }
   
    func getTypeName( typeID: Int32)->String{
        var typeName = "Unknown"
        
        switch (typeID) {
        case FOUND_25_IATA: typeName = "Code 25 IATA";break;
        case FOUND_25_INTERLEAVED: typeName = "Code 25 Interleaved";break;
        case FOUND_25_STANDARD: typeName = "Code 25 Standard";break;
        case FOUND_128: typeName = "Code 128";break;
        case FOUND_128_GS1: typeName = "Code 128 GS1";break;
        case FOUND_39: typeName = "Code 39";break;
        case FOUND_93: typeName = "Code 93";break;
        case FOUND_AZTEC: typeName = "AZTEC";break;
        case FOUND_DM: typeName = "Datamatrix";break;
        case FOUND_QR: typeName = "QR";break;
        case FOUND_EAN_13: typeName = "EAN 13";break;
        case FOUND_EAN_8: typeName = "EAN 8";break;
        case FOUND_NONE: typeName = "None";break;
        case FOUND_RSS_14: typeName = "Databar 14";break;
        case FOUND_RSS_14_STACK: typeName = "Databar 14 Stacked";break;
        case FOUND_RSS_EXP: typeName = "Databar Expanded";break;
        case FOUND_RSS_LIM: typeName = "Databar Limited";break;
        case FOUND_UPC_A: typeName = "UPC A";break;
        case FOUND_UPC_E: typeName = "UPC E";break;
        case FOUND_PDF: typeName = "PDF417";break;
        case FOUND_CODABAR: typeName = "Codabar";break;
        case FOUND_DOTCODE: typeName = "Dotcode";break;
        case FOUND_11: typeName = "Code 11";break;
        case FOUND_MSI: typeName = "MSI Plessey";break;
        default: typeName = "Unknown type"
        }
        
        
        return typeName
    }
    
}

class MWLocation: NSObject {
    var threeDoubles = [Double](count: 3, repeatedValue: 0.0)
    var points = [CGPoint](count: 4, repeatedValue: CGPointMake(0, 0))
    var p1:CGPoint!
    var p2:CGPoint!
    var p3:CGPoint!
    var p4:CGPoint!
    
    init(x1:CGFloat,y1:CGFloat,x2:CGFloat,y2:CGFloat,x3:CGFloat,y3:CGFloat,x4:CGFloat,y4:CGFloat) {
        self.p1 = CGPointMake(x1, y1)
        self.p2 = CGPointMake(x2, y2)
        self.p3 = CGPointMake(x3, y3)
        self.p4 = CGPointMake(x4, y4)
        
        self.points[0] = CGPointMake(x1, y1);
        self.points[1] = CGPointMake(x2, y2);
        self.points[2] = CGPointMake(x3, y3);
        self.points[3] = CGPointMake(x4, y4);
        
    }

}
