//
//  CustomerAccountsTableViewCell.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-16.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class CustomerAccountsTableViewCell: UITableViewCell {
    
    // MARK: - Properties
    @IBOutlet weak var labelProductNameValue: UILabel!
    @IBOutlet weak var labelStatusTitle: UILabel!
    @IBOutlet weak var labelStatusValue: UILabel!
    @IBOutlet weak var viewProductDetails: UIView!
    @IBOutlet weak var labelDateOpenedTitle: UILabel!
    @IBOutlet weak var labelDateOpenedValue: UILabel!
    @IBOutlet weak var labelDateClosedTitle: UILabel!
    @IBOutlet weak var labelDateClosedValue: UILabel!
    @IBOutlet weak var labelAccountBalanceValue: UILabel!
    @IBOutlet weak var imageViewChevron: UIImageView!
    @IBOutlet weak var buttonTransactionHistory: UIButton!
    weak var customerAccountsTableViewController: CustomerAccountsTableViewController?
    var product: Product?
    
    // MARK: - Cell Methods
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
        self.backgroundColor = UIColor.clearColor()
    }

    // MARK: - Instance Methods
    func setCellContent(product: Product, isUserAuthenticated: Bool, customerAccountsTableViewController: CustomerAccountsTableViewController?) {
        
        // Set property
        self.customerAccountsTableViewController = customerAccountsTableViewController
        self.product = product
        
        // Check if it's a ScotiaCard
        let isScotiaCard = product.productCode == "CSS"
        
        // Product Name
        var productName = ""
        if let uProductName = Util.currentLanguage == AppSupportedLanguages.English ? product.productNameEnglish : product.productNameFrench {
            productName = uProductName
        }
        if var uAccountNumber = product.accountNumber {
            uAccountNumber = isUserAuthenticated ? uAccountNumber : uAccountNumber.masked
            productName += productName.isEmpty ? uAccountNumber : " － " + uAccountNumber
        }
        labelProductNameValue.text = productName.isEmpty ? NSLocalizedString("unknown_account", comment: "Customer Accounts") : productName
        
        // Status
        labelStatusTitle.text = NSLocalizedString("status", comment: "Customer Accounts") + ":"
        if let uAccountStatus = product.accountStatus {
            labelStatusValue.text = ScotiaProductsStatusCodes.getStringForCode(uAccountStatus)
            labelStatusValue.textColor = uAccountStatus == "Active" ? UIColor.blackColor() : Constants.UBColours.scotiaRed
        } else {
            labelStatusValue.text = NSLocalizedString("unknown_status", comment: "Customer Accounts")
            labelStatusValue.textColor = Constants.UBColours.scotiaRed
        }

        // Date Opened
        labelDateOpenedTitle.text = NSLocalizedString("date_opened", comment: "Customer Accounts")
        if let uDateOpened = product.dateOpened {
            labelDateOpenedValue.text = Util.formatDate(uDateOpened) ?? NSLocalizedString("unknown_date_opened", comment: "Customer Accounts")
        } else {
            labelDateOpenedValue.text = NSLocalizedString("unknown_date_opened", comment: "Customer Accounts")
        }
        
        // Date Closed
        labelDateClosedTitle.text = NSLocalizedString("date_closed", comment: "Customer Accounts")
        if let uDateClosed = product.dateClosed {
            labelDateClosedTitle.hidden = false
            labelDateClosedValue.hidden = false
            labelDateClosedValue.text = Util.formatDate(uDateClosed) ?? NSLocalizedString("unknown_date_closed", comment: "Customer Accounts")
        } else {
            labelDateClosedTitle.hidden = true
            labelDateClosedValue.hidden = true
        }
        
        // Account Balance
        labelAccountBalanceValue.hidden = true
        if !isScotiaCard {
            if let uQuantity = product.balanceQuantity {
                let quantity = Util.formatCurrency(uQuantity)
                var currency = product.balanceCurrency ?? ""
                currency = currency.isEmpty ? "" : currency + " "
                let balance = currency + quantity
                labelAccountBalanceValue.hidden = false
                labelAccountBalanceValue.text = balance
                labelAccountBalanceValue.textColor = uQuantity < 0 ? Constants.UBColours.scotiaRed : Constants.UBColours.scotiaGrey1
            }
        }
        
        // Transaction History Button
        if let bankinglist = ScotiaProductTypes.getProductsCodes(ScotiaProductTypes.Banking), creditcardList = ScotiaProductTypes.getProductsCodes(ScotiaProductTypes.Creditcard), code = product.productCode {
            buttonTransactionHistory.hidden = !(isUserAuthenticated && (bankinglist.contains(code) || creditcardList.contains(code)))
        } else {
            buttonTransactionHistory.hidden = true
        }
    }
    
    func displayProductDetails(shouldDisplay: Bool) {
        viewProductDetails.hidden = !shouldDisplay
        let rotationAngle = shouldDisplay ? CGFloat(M_PI_2) : CGFloat(2*M_PI)
        UIView.animateWithDuration(0.25) { () -> Void in
            self.imageViewChevron.transform = CGAffineTransformMakeRotation(rotationAngle)
        }
    }
    
    @IBAction func transactionHistoryButtonPressed() {
        if let uProduct = product {
            customerAccountsTableViewController?.didSelectTransactionHistoryFor(uProduct)
        }
    }
}
