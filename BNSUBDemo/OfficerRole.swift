//
//  OfficerRole.swift
//  Customer
//
//  Created by Emad Toukan on 2016-01-15.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import Foundation

class OfficerRole {
    
    // MARK: - JSON Keys
    static let kLevelKey = "level"
    static let kRoleKey = "role"
    static let kTransitIdKey = "transit"
    static let kTransitDetailsDto = "transitDetailsDto"
    
    // MARK: - Properties
    let level: String?
    let role: String?
    let transit: Transit?
    
    // MARK: - Init
    init(json: [String: AnyObject]) {
        self.level = json[OfficerRole.kLevelKey] as? String
        self.role = json[OfficerRole.kRoleKey] as? String
        
        if let uTransitId = json[OfficerRole.kTransitIdKey] as? String, uTransitJSON = json[OfficerRole.kTransitDetailsDto] as? [String: String] {
            self.transit = Transit(transitId: uTransitId, json: uTransitJSON)
        } else {
            self.transit = nil
        }
    }
}