//
//  Identification.swift
//  Customer
//
//  Created by Emad Toukan on 2015-12-11.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

enum PartyIdStatus: String {
    case AdultStandard = "AdltStd",
    MinorGreaterOrEqual12 = "MinorGE12",
    MinorLess12 = "MinorLT12",
    AdultNonStandard = "AdltNStd"
    
    func getName() -> String {
        switch self {
        case .AdultStandard:
            return NSLocalizedString("adult_standard", comment: "Identification Party Id Status")
        case .MinorGreaterOrEqual12:
            return NSLocalizedString("minor_greater_or_equal_12", comment: "Identification Party Id Status")
        case .MinorLess12:
            return NSLocalizedString("minor_less_12", comment: "Identification Party Id Status")
        case .AdultNonStandard:
            return NSLocalizedString("adult_non_standard", comment: "Identification Party Id Status")
        }
    }
    
    func getApplicableIdentificationTypes() -> [IdentificationType] {
        switch self {
        case .AdultStandard:
            return [IdentificationType.DriverLicense, .CanadianPassport, .CanadianCitizenshipCert, .NaturalizationCertificate, .PermanentResidentCard, .RecordOfLandingImm1000, .PermanentResidenceImm5292, .Imm1442Document, .HealthCard, .CertificateOfIndianStatus, .BcInsCorpCard, .AlbertaRegistriesCardOrDoc, .SaskatchewanGovtInsCardOrDoc, .NsDeptOfServiceCardOrDoc, .PeiDeptOfTransportCardOrDoc, .NbServiceNbCardOrDoc, .NlDeptOfGovtServCardOrDoc, .NwtDeptOfTransportCardOrDoc, .NunavutCgtDeptCardOrDoc, .SocialInsuranceCard, .CanadianBirthCertificate, .OfficialGovtCardOrDoc, .StudentVisa, .OtherId, .CreditCardCANNonBns, .CnibClientCard, .CanCollegeOrUniversityIdCard, .EmployeeIdCard, .OtherBankClientCardNonBns , .ForeignPassport, .ForeignDriverLicence, .CreditCardForeign, .UsPermanentResidentcard, .NationalIdentityCard, .UsDriversLicense, .UsBirthCertificate, .OldAgeSecurityCardWith, .OldAgeSecurityCardWithout]
        case .AdultNonStandard:
            return [IdentificationType.ReferredBnsCustomer, .ReferredIndivInCommunity]
        case .MinorGreaterOrEqual12:
            return [IdentificationType.CanadianPassport, .CanadianCitizenshipCert, .NaturalizationCertificate, .PermanentResidentCard, .RecordOfLandingImm1000, .PermanentResidenceImm5292, .Imm1442Document, .HealthCard, .CertificateOfIndianStatus, .BcInsCorpCard, .AlbertaRegistriesCardOrDoc, .SaskatchewanGovtInsCardOrDoc, .NsDeptOfServiceCardOrDoc, .PeiDeptOfTransportCardOrDoc, .NbServiceNbCardOrDoc, .NlDeptOfGovtServCardOrDoc, .NwtDeptOfTransportCardOrDoc, .NunavutCgtDeptCardOrDoc, .SocialInsuranceCard, .CanadianBirthCertificate, .OfficialGovtCardOrDoc, .StudentVisa, .PgDriverLicence, .PgCanadianPassport, .PgHealthCard, .PgCanadianBirthCertificate, .PgOfficialCanGovtCardOrDoc, .OtherId, .CreditCardCANNonBns, .CnibClientCard, .CanCollegeOrUniversityIdCard, .EmployeeIdCard, .OtherBankClientCardNonBns , .ForeignPassport, .ForeignDriverLicence, .CreditCardForeign, .ReferredBnsCustomer, .ReferredIndivInCommunity , .PgVisaCardBns, .PgCreditCardCanNonBns, .PgScotiacard, .PgOthBkClientCardNonBns, .PgOtherId, .PgForeignPassport , .PgUspermanentResidentcard, .PgNationalIdentitycard, .PgUsdriversLicense, .PgUsbirthCertificate, .UsPermanentResidentcard, .NationalIdentityCard, .UsDriversLicense, .UsBirthCertificate]
        case .MinorLess12:
            return [IdentificationType.CanadianPassport, .CanadianCitizenshipCert, .NaturalizationCertificate, .PermanentResidentCard, .RecordOfLandingImm1000, .PermanentResidenceImm5292, .Imm1442Document, .HealthCard, .CertificateOfIndianStatus, .SocialInsuranceCard, .CanadianBirthCertificate, .OfficialGovtCardOrDoc, .StudentVisa, .PgDriverLicence, .PgCanadianPassport, .PgHealthCard, .PgCanadianBirthCertificate, .PgOfficialCanGovtCardOrDoc, .OtherId, .CnibClientCard, .ForeignPassport, .PgVisaCardBns, .PgCreditCardCanNonBns, .PgScotiacard, .PgOthBkClientCardNonBns, .PgOtherId, .PgForeignPassport , .PgUspermanentResidentcard, .PgNationalIdentitycard, .PgUsdriversLicense, .PgUsbirthCertificate, .UsPermanentResidentcard, .NationalIdentityCard, .UsBirthCertificate]
        }
    }
}

enum IdentificationEstablishedBy: String {
    case Parent = "P",
    Guardian = "G",
    Minor = "M"
    
    func getName() -> String {
        switch self {
        case .Parent:
            return NSLocalizedString("parent", comment: "Identification Established By")
        case .Guardian:
            return NSLocalizedString("guardian", comment: "Identification Established By")
        case .Minor:
            return NSLocalizedString("minor", comment: "Identification Established By")
        }
    }
}

class Identification {
    
    // MARK: - JSON Keys
    static let kIdTypeKey = "IdTp"
    static let kDescriptionKey = "Description"
    static let kIdNumberKey = "IdNumber"
    static let kCountryOfIssueKey = "CountryOfIssue"
    static let kProvinceOfIssueKey = "ProvinceOfIssue"
    static let kPlaceOfBirthKey = "PlaceOfBirth"
    static let kDateCapturedKey = "DateCaptured"
    static let kRefusedKey = "Refused"
    static let kProvidedLaterKey = "ProvideLater"
    static let kNotExpiredKey = "NotExpired"
    static let kOfficerIdKey = "OfficerId"
    static let kTransitKey = "Transit"
    static let kConfirmedKey = "Confirmed"
    
    // MARK: - Constants
    static let kIdentificationDescriptionMaxCharacterCount = 30
    static let kIdentificationNumberMaxCharacterCount = 20
    static let kSocialIncuranceCardNumberMaxCharacterCount = 9
    
    // MARK: - Properties
    let json: [String: AnyObject]?
    var idTp: IdentificationType {
        willSet {
            if idTp != newValue {
                idDescription = nil
                idNumber = nil
                countryOfIssue = newValue.countryOfIssueDefaultValue()
                provinceOfIssue = newValue.provinceOfIssueDefaultValue()
                placeOfBirth = nil
                notExpired = nil
            }
        }
    }
    var idDescription: String?
    var idNumber: String?
    var countryOfIssue: String? {
        didSet {
            provinceOfIssue = nil
        }
    }
    var provinceOfIssue: String?
    var placeOfBirth: String?
    let dateCaptured: NSDate?
    let refused: Bool?
    let provideLater: Bool?
    let officerId: String?
    let transit: String?
    var notExpired: Bool? = true
    let confirmed: Bool = true
    var deleted: Bool = false
    weak var customerProfile: CustomerProfile?
    
    // MARK: - Init
    init?(json: [String: AnyObject]) {
        self.json = json
        
        // If there is IdTp then return nil
        var tempIdTp = IdentificationType.Unknown
        if let uIdTp = json[Identification.kIdTypeKey] as? String {
            if let uIdentificationType = IdentificationType(rawValue: uIdTp) {
                tempIdTp = uIdentificationType
            }
        }
        self.idTp = tempIdTp
        self.idDescription = json[Identification.kDescriptionKey] as? String
        self.idNumber = json[Identification.kIdNumberKey] as? String
        self.countryOfIssue = json[Identification.kCountryOfIssueKey] as? String
        self.provinceOfIssue = json[Identification.kProvinceOfIssueKey] as? String
        self.placeOfBirth = json[Identification.kPlaceOfBirthKey] as? String
        if let dateCapturedString = json[Identification.kDateCapturedKey] as? String {
            self.dateCaptured = Util.convertAPIStringDate(dateCapturedString)
        } else {
            self.dateCaptured = nil
        }
        self.refused = json[Identification.kRefusedKey] as? Bool
        self.provideLater = json[Identification.kProvidedLaterKey] as? Bool
        self.officerId = json[Identification.kOfficerIdKey] as? String
        self.transit = json[Identification.kTransitKey] as? String
        
        // If there the IdTp is .Unkown, then return nil
        if idTp == .Unknown {
            return nil
        }
    }
    
    init(customerProfile: CustomerProfile) {
        self.customerProfile = customerProfile
        idTp = .Unknown
        dateCaptured = NSDate()
        officerId = Officer.id
        transit = Officer.currentSelectedRole?.transit?.id
        json = nil
        refused = nil
        provideLater = nil
        notExpired = nil
    }
    
    // MARK: - Validation
    var isDeleteAllowed: Bool {
        if self.idTp == IdentificationType.SocialInsuranceCard {
            return false
        }
        
        if let uIdentificationsNotDeleted = customerProfile?.identificationsNotDeleted {
            // Cannot delete identification if there is less than 2 identifications on profile
            if uIdentificationsNotDeleted.count <= 2 {
                return false
            }
            
            // Cannot delete identification if there is less than 3 identifications on profile and one of them is a Social Insurance Card with no number
            for identification in uIdentificationsNotDeleted where identification.idTp == IdentificationType.SocialInsuranceCard && identification.idNumber?.isEmpty == true && uIdentificationsNotDeleted.count <= 3 {
                return false
            }
        }
        return true
    }
    
    var isValidIdentification: Bool {
        return isValidIdentificationType && isValidIdentificationDescription && isValidIdentificationNumber && isValidProvinceOfIssue && isValidCountryOfIssue && isValidPlaceOfBirth && isValidNotExpired
    }
    
    var isValidIdentificationType: Bool {
        
        if idTp == IdentificationType.Unknown {
            return false
        }
        
        // Check if the identification type currently exists in the list. No more than 1 identification of the same type should be in the list
        var counter: Int = 0
        if let uIdentifications = customerProfile?.identificationsNotDeleted {
            for identification in uIdentifications where identification.idTp == idTp {
                counter++
            }
        }
        if (counter != 1 && json != nil) || (counter != 0 && json == nil) {
            return false
        }
        
        return true
    }
    
    var isValidIdentificationDescription: Bool {
        
        // If description is applicable, then a value must be present
        descriptionApplicable: if idTp.isDescriptionApplicable() {
            
            // Description for SIN where it is provided, a description is not needed
            if idTp == IdentificationType.SocialInsuranceCard && refused == nil && provideLater == nil {
                break descriptionApplicable
            }
            
            guard let uIdDescription = idDescription where uIdDescription.isEmpty == false else {
                return false
            }
            
            // Check text does not exceed max allowed value
            if uIdDescription.characters.count > Identification.kIdentificationDescriptionMaxCharacterCount {
                return false
            }
            
            // Check for alpha-numeric text
            for char in uIdDescription.lowercaseString.characters {
                if !Util.kAllowedLettersForTextInput.contains(String(char)) && !Util.kAllowedCharacterForTextInput.contains(String(char)) && !Util.kAllowedNumbersForTextInput.contains(String(char)) {
                    return false
                }
            }
        }
        
        return true
    }
    
    var isValidIdentificationNumber: Bool {
        // If number is applicable then it must be present
        if idTp.isIdNumberApplicable() {
            guard let uIdNumber = idNumber where uIdNumber.isEmpty == false else {
                return false
            }
            
            // Check that the text does not exceed max character limit
            // Check that the characters are acceptable
            if idTp == IdentificationType.SocialInsuranceCard {
                if uIdNumber.characters.count > Identification.kSocialIncuranceCardNumberMaxCharacterCount {
                    return false
                }
                
                for char in uIdNumber.characters where !Util.kAllowedNumbersForTextInput.contains(String(char))  {
                    return false
                }
            } else {
                if uIdNumber.characters.count > Identification.kIdentificationNumberMaxCharacterCount {
                    return false
                }
                
                for char in uIdNumber.lowercaseString.characters {
                    if !Util.kAllowedLettersForTextInput.contains(String(char)) && !Util.kAllowedCharacterForTextInput.contains(String(char)) && !Util.kAllowedNumbersForTextInput.contains(String(char)) {
                        return false
                    }
                }
            }
        }
        
        return true
    }
    
    var isValidCountryOfIssue: Bool {
        
        guard let uCountryOfIssue = countryOfIssue where uCountryOfIssue.isEmpty == false else {
            return false
        }
        
        // Check if the country of issue is equal to the default value
        if let uDefaultCountryOfIssue = idTp.countryOfIssueDefaultValue() where uDefaultCountryOfIssue != uCountryOfIssue {
            return false
        }
        
        // Check if the country of issue is in the master data
        if !CustomerMasterData.getKeysFor(CustomerMasterDataType.CountryOfIssueForIdentification).contains(uCountryOfIssue) {
            return false
        }
        
        return true
    }
    
    var isValidProvinceOfIssue: Bool {
        
        // Province is mandatory
        if let uCountryOfIssues = countryOfIssue {
            if let uCountryCode = CountryCode(rawValue: uCountryOfIssues) where uCountryCode == CountryCode.Canada || uCountryCode == CountryCode.UnitedStates {
                guard let uProvinceOfIssue = provinceOfIssue where uProvinceOfIssue.isEmpty == false else {
                    return false
                }
                
                // Check if the province of issue is equal to the default value
                if let uDefaultProvinceOfIssue = idTp.provinceOfIssueDefaultValue() where uDefaultProvinceOfIssue != uProvinceOfIssue {
                    return false
                }
                
                // Check if the province / state is in its master data
                if !CustomerMasterData.getKeysFor(CustomerMasterDataType.ProvinceOfIssueForIdentification).contains(uProvinceOfIssue) {
                    return false
                }
                
                // Check that the province is in the allowed province list
                if !idTp.getProvinceOfIssueMasterData(uCountryCode).keys.contains(uProvinceOfIssue) {
                    return false
                }
            }
        }
        
        return true
    }
    
    var isValidPlaceOfBirth: Bool {
        
        if idTp.isPlaceOfBirthApplicable() {
            guard let uPlaceOfBirth = placeOfBirth where uPlaceOfBirth.isEmpty == false else {
                return false
            }
            
            // Check if the place of birth is in the master data
            if !CustomerMasterData.getKeysFor(CustomerMasterDataType.CountryPlaceOfBirthForIdentification).contains(uPlaceOfBirth) {
                return false
            }
        }
        return true
    }
    
    var isValidNotExpired: Bool {
        
        if idTp.isIdNotExpiredApplicable() {
            guard let uNotExpired = notExpired where uNotExpired == true else {
                return false
            }
        }
        return true
    }
    
    static func isValidIdDescriptionInRange(aString: String, range: NSRange) -> Bool {
        // Check that the first entry is not a space
        if range.location == 0 && aString == " " {
            return false
        }
        
        if !Util.kAllowedLettersForTextInput.contains(aString.lowercaseString) && !Util.kAllowedCharacterForTextInput.contains(aString.lowercaseString) && !Util.kAllowedNumbersForTextInput.contains(aString) && aString != "" {
            return false
        }
        
        // Check if the string exceeds the allowed string limit
        if range.location >= Identification.kIdentificationDescriptionMaxCharacterCount && aString != "" {
            return false
        }
        return true
    }
    
    static func isValidIdNumberInRange(idType: IdentificationType, aString: String, range: NSRange) -> Bool {
        // Check that the first entry is not a space
        if range.location == 0 && aString == " " {
            return false
        }
        
        if idType == .SocialInsuranceCard {
            if !Util.kAllowedNumbersForTextInput.contains(aString) && aString != "" {
                return false
            }
            
            // Check if the string exceeds the allowed string limit
            if range.location >= Identification.kSocialIncuranceCardNumberMaxCharacterCount && aString != "" {
                return false
            }
        } else {
            if !Util.kAllowedLettersForTextInput.contains(aString.lowercaseString) && !Util.kAllowedCharacterForTextInput.contains(aString.lowercaseString) && !Util.kAllowedNumbersForTextInput.contains(aString) && aString != "" {
                return false
            }
            
            // Check if the string exceeds the allowed string limit
            if range.location >= Identification.kIdentificationNumberMaxCharacterCount && aString != "" {
                return false
            }
        }
        return true
    }
    // MARK: - Save
    var paramsForSave: [String: AnyObject]? {
        // If the json is nil, then the object was added
        guard let uJSON = json else {
            // If it was added and then deleted, then don't send anything to the backend
            if self.deleted == true {
                return nil
                
            }
            return createIdentificationParams(SaveActions.Added)
        }
        
        // Check if the identification is marked as delete
        if self.deleted == true {
            return createIdentificationParams(SaveActions.Deleted)
        }
        
        let originalIdentification = Identification(json: uJSON)
        if self.notExpired != originalIdentification?.notExpired || self.confirmed != originalIdentification?.confirmed {
            return createIdentificationParams(SaveActions.Changed)
        }
        
        // Return nil if nothing has changed
        return nil
    }
    
    private func createIdentificationParams(action: SaveActions) -> [String: AnyObject] {
        var params = [String: AnyObject]()
        params[kAction] = action.rawValue
        params[Identification.kConfirmedKey.decapitalizedString] = self.confirmed
        params[Identification.kCountryOfIssueKey.decapitalizedString] = self.countryOfIssue
        
        // For ProvinceOfIssue, We can't send Unknown(099) to the backend, otherwise it will through an error
        if let uProvinceOfIssue = provinceOfIssue where uProvinceOfIssue != "099" {
            params[Identification.kProvinceOfIssueKey.decapitalizedString] = self.provinceOfIssue
        }
        
        if let uDateCaptured = dateCaptured {
            params[Identification.kDateCapturedKey.decapitalizedString] = Util.convertAPIStringDate(uDateCaptured)
        }
        params[Identification.kDescriptionKey.decapitalizedString] = self.idTp.isDescriptionApplicable() ? self.idDescription : nil
        params[Identification.kIdNumberKey.decapitalizedString] = self.idTp.isIdNumberApplicable() ? self.idNumber : nil
        params[Identification.kIdTypeKey.decapitalizedString] = self.idTp.rawValue
        params[Identification.kRefusedKey.decapitalizedString] = self.idTp.isIdRefusedApplicable() ? self.refused : nil
        params[Identification.kNotExpiredKey.decapitalizedString] = self.idTp.isIdNotExpiredApplicable() ? self.notExpired : nil
        params[Identification.kPlaceOfBirthKey.decapitalizedString] = self.idTp.isPlaceOfBirthApplicable() ? self.placeOfBirth : nil
        params[Identification.kProvidedLaterKey.decapitalizedString] = self.idTp.isProvideLaterApplicable() ? self.provideLater : nil
        params[Officer.kOfficerIdKey.decapitalizedString] = Officer.id
        params[OfficerRole.kTransitIdKey.decapitalizedString] = Officer.currentSelectedRole?.transit?.id
        return params
    }
    
    // MARK: - Helpers
    func getApplicableIdentificationTypesMasterData() -> [String: String] {
        var masterData = [String: String]()
        if let uIdentificationTypes = customerProfile?.partyIdStatus?.getApplicableIdentificationTypes() {
            for type in uIdentificationTypes {
                masterData[type.rawValue] = type.getName()
            }
        }
        
        // Filter out existing ids
        if let uIdentificationsUndeleted = customerProfile?.identificationsNotDeleted {
            for identification in uIdentificationsUndeleted {
                masterData[identification.idTp.rawValue] = nil
            }
        }
        return masterData
    }
}