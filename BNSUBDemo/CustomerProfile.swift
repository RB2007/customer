//
//  CustomerProfile.swift
//  Customer
//
//  Created by Emad Toukan on 2015-12-09.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

class CustomerProfile {
    
    // MARK: - JSON Keys

    // Domocile
    static let kDomicileBranchKey = "domicileBranch"
    
    // Party Type
    static let kPartyTypeKey = "partyType"
    static let kCidKey = "Cid"
    static let kPartyStatus = "PartyStatus"
    static let kNameLine1Key = "NameLine1"
    static let kPartyBaseKey = "PartyBase"
    static let kTransitNumberKey = "DomicileBranch"
    static let kAddressesKey = "Addresses"
    static let kAddressKey = "Address"
    static let kContactMethodsKey = "ContactMethods"
    static let kEmailKey = "Email"
    static let kPhoneKey = "Phone"
    static let kProductsKey = "Products"
    static let kProductKey = "Product"
    static let kIdentificationsKey = "Identifications"
    static let kIdentificationKey = "Identification"
    static let kPartyIDStatusKey = "PartyIDStatus"
    static let kEstablishedByKey = "EstablishedBy"
    static let kGuardianshipPaperFiledKey = "GuardianshipPaperFiled"
    static let kMinorSigningAuthorityKey = "MinorSigningAuthority"

    // MARK: - Constants
    static let kPartyIdStatusMaxCharacterLimit = 20
    
    // MARK: - Properties
    let json: [String: AnyObject]
    let domicileBranch: Transit?
    let cid: String?
    let partyStatus: String?
    let nameLine1: String?
    let addresses: [Address]?
    var emails: [Email]?
    var phones: [Phone]?
    let products: [Product]?
    var identifications: [Identification]?
    var identificationsNotDeleted: [Identification]? { // Returns track of identifications that are not deleted
        var identificationsNotDeleted: [Identification]?
        if let uIdentifications = identifications {
            for identification in uIdentifications where identification.deleted == false {
                if identificationsNotDeleted == nil {
                   identificationsNotDeleted = [Identification]()
                }
                identificationsNotDeleted?.append(identification)
            }
        }
        return identificationsNotDeleted
    }
    var partyIdStatus: PartyIdStatus? {
        didSet {
            establishedBy = nil
            guardianshipPaperFiled = nil
            minorSigningAuthority = nil
        }
    }
    var establishedBy: IdentificationEstablishedBy? {
        didSet {
            guardianshipPaperFiled = nil
            minorSigningAuthority = nil
        }
    }
    var guardianshipPaperFiled: Bool?
    var minorSigningAuthority: Bool?
    
    // MARK: - Init
    init(json: [String: AnyObject]) {
        self.json = json
        
        let partyType = json[CustomerProfile.kPartyTypeKey] as? [String: AnyObject]
        let partyBase = partyType?[CustomerProfile.kPartyBaseKey] as? [String: AnyObject]
        
        self.cid = partyType?[CustomerProfile.kCidKey] as? String
        self.partyStatus = partyBase?[CustomerProfile.kPartyStatus] as? String
        self.nameLine1 = partyType?[CustomerProfile.kNameLine1Key] as? String
        
        // Transit
        let domicileBranch = json[CustomerProfile.kDomicileBranchKey] as? [String: String]
        let transitNumber = partyBase?[CustomerProfile.kTransitNumberKey] as? String
        if let uTransitId = transitNumber, uDomicileBranch = domicileBranch {
            self.domicileBranch = Transit(transitId: uTransitId, json: uDomicileBranch)
        } else {
            self.domicileBranch = nil
        }
        
        // Products
        var tempProducts: [Product]?
        if let uProducts = partyType?[CustomerProfile.kProductsKey] as? [String: AnyObject] {
            if let uProduct = uProducts[CustomerProfile.kProductKey] as? [[String: AnyObject]] {
                for product in uProduct {
                    let tempProduct = Product(json: product)
                    if tempProducts == nil {
                        tempProducts = [Product]()
                    }
                    tempProducts?.append(tempProduct)
                }
            }
        }
        self.products = tempProducts
        
        // Addresses
        var tempAddressArray: [Address]?
        if let uAddresses = partyBase?[CustomerProfile.kAddressesKey] as? [String: AnyObject] {
            if let uAddress = uAddresses[CustomerProfile.kAddressKey] as? [[String: AnyObject]] {
                for address in uAddress {
                    if let uTempAddress = Address(json: address) {
                        if tempAddressArray == nil {
                            tempAddressArray = [Address]()
                        }
                        tempAddressArray?.append(uTempAddress)
                    }
                }
            }
        }
        self.addresses = tempAddressArray
        
        // Email & Phone
        if let uContactMethods = partyBase?[CustomerProfile.kContactMethodsKey] as? [String: AnyObject] {
            
            // Email
            if let uEmail = uContactMethods[CustomerProfile.kEmailKey] as? [[String: AnyObject]] {
                for email in uEmail {
                    if let uTempEmail = Email(json: email) {
                        self.addEmail(uTempEmail)
                    }
                }
            }
            
            // Phone
            if let uPhone = uContactMethods[CustomerProfile.kPhoneKey] as? [[String: AnyObject]] {
                for phone in uPhone {
                    if let uTempPhone = Phone(json: phone) {
                        self.addPhone(uTempPhone)
                    }
                }
            }
        }

        
        // Identifications
        var tempPartyIdStatus: PartyIdStatus?
        var tempEstablishedBy: IdentificationEstablishedBy?
        var tempGuardianshipPaperFiled: Bool?
        var tempMinorSigningAuthority: Bool?
        if let uIdentifications = partyBase?[CustomerProfile.kIdentificationsKey] as? [String: AnyObject] {
            
            // Identifications
            if let uIdentification = uIdentifications[CustomerProfile.kIdentificationKey] as? [[String: AnyObject]] {
                for identification in uIdentification {
                    if let tempIdentification = Identification(json: identification) {
                        self.addIdentification(tempIdentification)
                    }
                }
            }
            
            // Party Status
            if let uPartyIdStatus = uIdentifications[CustomerProfile.kPartyIDStatusKey] as? String {
                tempPartyIdStatus = PartyIdStatus(rawValue: uPartyIdStatus)
            }
            
            // Established By
            if let uEstablishedBy = uIdentifications[CustomerProfile.kEstablishedByKey] as? String {
                tempEstablishedBy = IdentificationEstablishedBy(rawValue: uEstablishedBy)
            }
    
            // Guardianship and minor
            tempGuardianshipPaperFiled = uIdentifications[CustomerProfile.kGuardianshipPaperFiledKey] as? Bool
            tempMinorSigningAuthority = uIdentifications[CustomerProfile.kMinorSigningAuthorityKey] as? Bool
        }

        // Make sure the order of setting is partyIdStatus, establishedBy, guardianshipPaperFiled, minorSigningAuthority, due to didSet method
        self.partyIdStatus = tempPartyIdStatus
        self.establishedBy = tempEstablishedBy
        self.guardianshipPaperFiled = tempGuardianshipPaperFiled
        self.minorSigningAuthority = tempMinorSigningAuthority
        
        // Provide access to the customer profile
        if let uIdentification = identifications {
            for identification in uIdentification {
                identification.customerProfile = self
            }
        }
    }
    
    // MARK: - Helper Methods
    
    // Add
    func addPhone(newPhone: Phone) {
        if self.phones == nil {
            self.phones = [Phone]()
        }
        self.phones?.append(newPhone)
    }
    
    func addEmail(newEmail: Email) {
        if self.emails == nil {
            self.emails = [Email]()
        }
        self.emails?.append(newEmail)
    }
    
    func addIdentification(newIdentification: Identification) {
        if self.identifications == nil {
            self.identifications = [Identification]()
        }
        self.identifications?.append(newIdentification)
    }
    
    // Get
    func getCustomerAddressWithType(addressUsageType: AddressUsageType) -> Address? {
        guard let uAddresses = addresses else {
            return nil
        }
        
        for address in uAddresses where address.usageTp == addressUsageType {
            return address
        }
        return nil
    }
    
    func getCustomerEmailWithType(emailUsageType: EmailUsageType) -> Email? {
        guard let uEmails = emails else {
            return nil
        }
        
        for email in uEmails where email.usageTp == emailUsageType {
            return email
        }
        return nil
    }
    
    func getCustomerPhoneWithType(phoneUsageType: PhoneUsageType) -> Phone? {
        guard let uPhones = phones else {
            return nil
        }

        for phone in uPhones where phone.usageTp == phoneUsageType {
            return phone
        }
        return nil
    }
    
    // MARK: - Validation
    var validEditableFields: Bool {
        fatalError("Subclass must override")
    }
    
    // MARK: - Save
    var paramsForSave: [String: AnyObject]? {
        fatalError("Subclass must override")
    }
    
    deinit {
        print("Deinit is called on Customer Profile")
    }
}