//
//  Phone.swift
//  Customer
//
//  Created by Emad Toukan on 2015-12-10.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

enum PhoneUsageType: String {
    case HomePhone = "HomePhone", // Personal Party
    BusinessPhone = "BusPhone", // Personal Party
    EmployerPhone = "EmplPhone", // Personal Party
    BusinessTelephone = "NPBusPhone", // Non-Personal Party
    ContactForBusiness = "BusCtPhone", // Non-Personal Party
    BusinessFaxNumber = "BusFax", // Non-Personal Party
    Unknown = "Unknown" // Used for failure to identify type
}

class Phone {
    
    // MARK: - JSON Keys
    static let kUsageTpKey = "UsageTp"
    static let kPhoneNumberKey = "PhoneNumber"
    static let kPhoneExtensionKey = "PhoneExtension"

    // MARK: - Constants
    static let kPhoneNumberCharacterCount = 10
    static let kPhoneExtensionNumberMaxCharacterCount = 5
    
    // MARK: - Properties
    let json: [String: AnyObject]?
    let usageTp: PhoneUsageType
    var number: String?
    var phoneExtension: String?
    
    // MARK: - Init
    init?(json: [String: AnyObject]) {
        self.json = json
        
        self.number = json[Phone.kPhoneNumberKey] as? String
        self.phoneExtension = json[Phone.kPhoneExtensionKey] as? String
        
        // Usage type
        // If the usage type is not found, return a nil object
        guard let tempUsageTp = json[Phone.kUsageTpKey] as? String else {
            self.usageTp = PhoneUsageType.Unknown
            return nil
        }
        
        guard let uUsageTp = PhoneUsageType(rawValue: tempUsageTp) else {
            self.usageTp = PhoneUsageType.Unknown
            return nil
        }

        self.usageTp = uUsageTp
    }
    
    init(number: String?, phoneExtension: String?, usageTp: PhoneUsageType) {
        self.json = nil
        self.usageTp = usageTp
        self.number = number
        self.phoneExtension = phoneExtension
    }
    
    // MARK: - Validation
    var isValidNumber: Bool? {
        
        guard let uNumber = number where uNumber.isEmpty == false else {
            return nil
        }
        
        // Check that all characters are numbers
        for char in uNumber.characters where (Int(String(char)) == nil) {
            return false
        }
        
        // Check that its a 10 digit number
        if uNumber.characters.count != Phone.kPhoneNumberCharacterCount {
            return false
        }
        
        // Check that the first digit it not a 0
        if uNumber[uNumber.startIndex] == "0" {
            return false
        }
        
        // Check that the fourth digit is not a 0
        if uNumber[uNumber.startIndex.advancedBy(3)] == "0" {
            return false
        }
        return true
    }
    
    var isValidPhoneExtension: Bool? {
        
        guard let uPhoneExtension = phoneExtension where uPhoneExtension.isEmpty == false else {
            return nil
        }
        
        // Check that all characters are numbers
        for char in uPhoneExtension.characters where Int(String(char)) == nil {
            return false
        }
        
        // Check that its less than 5 or less characters
        if uPhoneExtension.characters.count > Phone.kPhoneExtensionNumberMaxCharacterCount {
            return false
        }
        
        return true
    }
    
    // MARK: - Validation in Range
    static func isValidPhoneNumberStringInRange(aString: String, range: NSRange) -> Bool {
        
        // Check if the entered string is a number or a backspace
        if Int(aString) == nil && aString != "" {
            return false
        }
        
        // Check if the first and fourth digit is not 0
        if aString == "0" && (range.location == 0 || range.location == 3) {
            return false
        }
        
        // Check if the string exceeds the allowed string limit for a phone number
        // Adding 4 more characters due to formatting phone number
        if range.location >= Phone.kPhoneNumberCharacterCount + 4 && aString != "" {
            return false
        }
        
        return true
    }
    
    static func isValidPhoneExtensionNumberStringInRange(aString: String, range: NSRange) -> Bool {
        
        // Check that the number is a string or a backspace
        if Int(aString) == nil && aString != "" {
            return false
        }
        
        // Check if the string exceeds the allowed string limit for a phone extension number
        if range.location >= Phone.kPhoneExtensionNumberMaxCharacterCount && aString != "" {
            return false
        }
        
        return true
    }
    
    // MARK: - Save
    var paramsForSave: [String: AnyObject]? {
        // If the json is nil, then the object was added
        guard let uJSON = json else {
            return createPhoneParams(SaveActions.Added)
        }
        
        // Check if anything changed from the original values
        if let uOriginalPhone = Phone(json: uJSON) {
            if self.number != uOriginalPhone.number || self.phoneExtension != uOriginalPhone.phoneExtension {
                return createPhoneParams(SaveActions.Changed)
            }
        }
        
        // Return nil if nothing has changed 
        return nil
    }
    
    private func createPhoneParams(action: SaveActions) -> [String: AnyObject] {
        var params = [String: AnyObject]()
        params[kAction] = action.rawValue
        params[Phone.kUsageTpKey.decapitalizedString] = self.usageTp.rawValue
        params[Phone.kPhoneNumberKey.decapitalizedString] = self.number ?? ""
        params[Phone.kPhoneExtensionKey.decapitalizedString] = self.phoneExtension ?? ""
        return params
    }
    
    // MARK: - Utitlity
    
    // Format a phone number to (416) 111-1111
    var formattedPhoneNumber: String? {
        guard var tempFormattedPhoneNumber = number else {
            return nil
        }

        switch tempFormattedPhoneNumber.characters.count {
        case 4,5,6:
            let startIndex = tempFormattedPhoneNumber.startIndex
            tempFormattedPhoneNumber.insert(" ", atIndex: startIndex.advancedBy(3))
            tempFormattedPhoneNumber.insert(")", atIndex: startIndex.advancedBy(3))
            tempFormattedPhoneNumber.insert("(", atIndex: startIndex)
        case 7,8,9,10:
            let startIndex = tempFormattedPhoneNumber.startIndex
            tempFormattedPhoneNumber.insert("-", atIndex: startIndex.advancedBy(6))
            tempFormattedPhoneNumber.insert(" ", atIndex: startIndex.advancedBy(3))
            tempFormattedPhoneNumber.insert(")", atIndex: startIndex.advancedBy(3))
            tempFormattedPhoneNumber.insert("(", atIndex: startIndex)
        default:
            break
        }
        
        return tempFormattedPhoneNumber
    }
    
    // Remove formatting from the phone number
    static func removePhoneNumberFormatting(phoneNumber: String) -> String {
        var unformattedPhoneNumber = String()
        for char in phoneNumber.characters where (char != " " && char != "(" && char != ")" && char != "-") {
            unformattedPhoneNumber.append(char)
        }
        return unformattedPhoneNumber
    }
}