//
//  Transit.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-27.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

class Transit {
    
    // MARK: - JSON Keys
    static let kNameKey = "branchName"
    static let kStatusKey = "bcuStatus"
    static let kStatusCodeKey = "branchStatusCode"
    static let kInstitutionCodeKey = "institutionCode"
    
    static let kAddressKey = "branchAddress"
    static let kCityKey = "branchCity"
    static let kPostalCodeKey = "branchPostalCode"
    static let kProvinceCodeKey = "provinceCode"
    static let kRegionCodeKey = "regionCode"
    
    static let kTelephoneNumberKey = "telephoneNo"
    
    // MARK: - Properties
    let id: String
    let name: String?
    let status: String?
    let statusCode: String?
    let institutionCode: String?

    let address: String?
    let city: String?
    let postalCode: String?
    let provinceCode: String?
    let regionCode: String?

    let telephoneNo: String?
    
    // MARK: - Init
    init(transitId: String, json: [String: String]) {
        
        self.id = transitId
        self.name = json[Transit.kNameKey]
        self.status = json[Transit.kStatusKey]
        self.statusCode = json[Transit.kStatusCodeKey]
        self.institutionCode = json[Transit.kInstitutionCodeKey]
        
        self.address = json[Transit.kAddressKey]
        self.city = json[Transit.kCityKey]
        self.postalCode = json[Transit.kPostalCodeKey]
        self.provinceCode = json[Transit.kProvinceCodeKey]
        self.regionCode = json[Transit.kRegionCodeKey]
        
        self.telephoneNo = json[Transit.kTelephoneNumberKey]
    }
}