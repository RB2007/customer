//
//  TransactionHistoryViewController.swift
//  Customer
//
//  Created by Annie Lo on 2016-02-22.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import UIKit

class TransactionHistoryViewController: UIViewController, UITableViewDelegate, UITableViewDataSource, UISearchBarDelegate {
    
    @IBOutlet weak var creditLabel: UILabel!
    @IBOutlet weak var debitLabel: UILabel!
    @IBOutlet weak var transactionLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    @IBOutlet weak var balanceHeader: UILabel!
    @IBOutlet weak var searchBar: UISearchBar!
    @IBOutlet weak var tableView: UITableView!
    
    var text: String = ""
    var pageIndex: Int = 0
    var transactions: [AccountTransactionHistory]? {
        didSet {
            filteredResult = transactions
        }
    }
    var startDate: String?
    var filteredResult: [AccountTransactionHistory]?
    var productCode: String?
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize UI
        dateLabel.text = NSLocalizedString("date", comment: "date header")
        transactionLabel.text = NSLocalizedString("transaction", comment: "transaction header")
        creditLabel.text = NSLocalizedString("credit", comment: "transaction header")
        debitLabel.text = NSLocalizedString("debit", comment: "transaction header")
        balanceHeader.text = NSLocalizedString("balance", comment: "transaction header")
        
        if let bankinglist = ScotiaProductTypes.getProductsCodes(ScotiaProductTypes.Banking), uProductCode = productCode {
            balanceHeader.hidden = !bankinglist.contains(uProductCode)
        }
    }
    
    func transactionHistoryChanged() {
        tableView?.reloadData()
    }
    
    // MARK: - Tableview Delegate
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        
        guard let uTransactions = filteredResult else {
            return 0
        }
        return uTransactions.count
        
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCellWithIdentifier("transactionDetail", forIndexPath: indexPath) as? TransactionHistoryTableViewCell, transactions = filteredResult, uProductCode = productCode else {
            return UITableViewCell()
        }
        
        cell.setField(uProductCode, transaction: transactions[indexPath.row], indexPath: indexPath)
        
        return cell
    }
    
    
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        if let uTransactionSearchResults = transactions where searchText.characters.count != 0 {
            
            let result = uTransactionSearchResults.filter({ (item: AccountTransactionHistory) -> Bool in
                let line1Match = item.line1Desc?.rangeOfString(searchText, options:
                    NSStringCompareOptions.CaseInsensitiveSearch)
                let line2Match = item.line2Desc?.rangeOfString(searchText, options:
                    NSStringCompareOptions.CaseInsensitiveSearch)
                let line3Match = item.line3Desc?.rangeOfString(searchText, options:
                    NSStringCompareOptions.CaseInsensitiveSearch)
                let txnAmountMatch = String(item.txnAmount).rangeOfString(searchText, options:
                    NSStringCompareOptions.CaseInsensitiveSearch)
                let txnDateMatch = String(item.txnDate).rangeOfString(searchText, options:
                    NSStringCompareOptions.CaseInsensitiveSearch)
                let txnRunningBalanceMatch = String(item.runningBalance).rangeOfString(searchText, options:
                    NSStringCompareOptions.CaseInsensitiveSearch)
                return line1Match != nil || line2Match != nil || line3Match != nil || txnAmountMatch != nil || txnDateMatch != nil || txnRunningBalanceMatch != nil
            })
            
            filteredResult = result
        }
        else {
            filteredResult = transactions
        }
        tableView.reloadData()
    }
}
