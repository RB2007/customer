//
//  ScotiaProductsStatusCodes.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-30.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

struct ScotiaProductsStatusCodes {
    
    static func getStringForCode(code: String) -> String? {
        switch code {
        case "Active":
            return "Active"
        case "Open":
            return "Open"
        case "Closed":
            return "Closed"
        case "Dormant":
            return "Dormant"
        case "CU":
            return "Current"
        case "PO":
            return "Paid Out"
        case "BD":
            return "Bad Debt"
        case "NA":
            return "Non Accrual"
        case "U":
            return "Unfunded"
        case "D":
            return "Deleted"
        case "Pending":
            return "Pending"
        case "PN":
            return "Pending MMS"
        case "F":
            return "Funded"
        case "CD":
            return "Deceased"
        case "FM":
            return "Frozen"
        case "FO":
            return "Overdrawn"
        case "CN":
            return "Closed, Not Returned"
        case "LR":
            return "Lost/Stolen"
        case "FF":
            return "Suspended Fraud"
        case "LM":
            return "LSWPrst"
        case "I":
            return "Incomplete"
        case "B5":
            return "Bankrupt"
        case "C":
            return "Delinquent"
        case "FX":
            return "Fixed Pymt"
        case "Y":
            return "In Force"
        case "A":
            return "In Arrears"
        case "T":
            return "Terminated"
        case "X":
            return "Cancelled"
        case "W":
            return "Waived"
        case "M":
            return "Claim In Effect"
        case "G":
            return "Claim Pending"
        case "R":
            return "Pending Underwriting"
        case "V":
            return "Ineligible"
        case "**":
            return "Purged"
        case "OX":
            return "Open/Prv"
        case "CX":
            return "Closed/Prv"
        case "PK":
            return "Annual Fee Unpaid"
        case "XH":
            return "Balance Exceeds Limit"
        case "PendClose" :
            return "Pending Close"
        case "WO":
            return "WOR"
        default:
            return nil
        }
    }
}
