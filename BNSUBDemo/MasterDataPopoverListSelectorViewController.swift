//
//  MasterDataPopoverListSelectorViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2016-02-08.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import UIKit

protocol MasterDataPoppverListSelectorProtocol {
    func didSelectMasterDataKey(key: String)
}

class MasterDataPopoverListSelectorViewController: UIViewController, UITableViewDataSource, UITableViewDelegate, UISearchBarDelegate {
    
    // MARK: - Properties
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var searchField: UISearchBar!
    var data: [String: String]? {
        didSet {
            if let uData = data {
                let keys = [String](uData.keys)
                orderedKeys = keys.sort(<)
            } else {
                orderedKeys = nil
            }
        }
    }
    var orderedKeys: [String]? {
        didSet {
            filteredOrderedKeys = orderedKeys
        }
    }
    var filteredOrderedKeys: [String]?
    var selectedValue: String?
    var delegate: MasterDataPoppverListSelectorProtocol?
    
    // MARK: - View Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        
        // Customize view
        self.popoverPresentationController?.backgroundColor = Constants.UBColours.scotiaGrey1
        
        // Initialization
        // Show the selected item when the table view appears
        if let uOrderedKeys = orderedKeys, uData = data  {
            for index in 0..<uOrderedKeys.count {
                let key = uOrderedKeys[index]
                let value = uData[key]
                if  value?.lowercaseString == selectedValue?.lowercaseString {
                    let indexPath = NSIndexPath(forItem: index, inSection: 0)
                    self.tableView.selectRowAtIndexPath(indexPath, animated: true, scrollPosition: UITableViewScrollPosition.Top)
                }
            }
        }
        
        // Notification
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "keyboardWillHideNotification:", name: UIKeyboardWillHideNotification, object: nil)
    }
    
    deinit {
        NSNotificationCenter.defaultCenter().removeObserver(self)
    }
    
    // MARK: - Instance Methods
    func keyboardWillHideNotification(notification: NSNotification) {
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Tableview Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return filteredOrderedKeys?.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("MasterDataListSelectorCell", forIndexPath: indexPath) as? MasterDataListSelectorTableViewCell, uKey = filteredOrderedKeys?[indexPath.row] else {
            return UITableViewCell()
        }
    
        let value = data?[uKey]
        cell.setCellContent(value)
        return cell
    }
    
    func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        if let uKey = filteredOrderedKeys?[indexPath.row] {
            self.delegate?.didSelectMasterDataKey(uKey)
        }
        self.dismissViewControllerAnimated(true, completion: nil)
    }
    
    // MARK: - Search Bar Delegate Methods
    func searchBar(searchBar: UISearchBar, textDidChange searchText: String) {
        
        // Filter the list based on the search
        if let uData = data where searchText.characters.count != 0 {
            filteredOrderedKeys?.removeAll()
            for (key, value) in uData {
                if key.lowercaseString.rangeOfString(searchText.lowercaseString) != nil || value.lowercaseString.rangeOfString(searchText.lowercaseString) != nil {
                    filteredOrderedKeys?.append(key)
                }
            }
        } else {
            filteredOrderedKeys = orderedKeys
        }
        tableView.reloadData()
    }
}
