//
//  CustomerMasterData.swift
//  BNSUBDemo
//
//  Created by Laura Reategui on 2015-09-29.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

enum CustomerMasterDataType: String {
    case IdentityTp,
    PhoneUsageType,
    ProvinceOfIssueForIdentification,
    CountryOfIssueForIdentification,
    CountryPlaceOfBirthForIdentification,
    ProvinceForAddress,
    CountryForAddress,
    OccupationCode,
    OccupationType,
    OccupationStatus,
    OccupationCategories,
    HouseHoldLanguage,
    ImmCategory,
    MaritalStatus,
    PaymentFrequency,
    Source,
    Title
}

struct CustomerMasterData {
    
    private static var customerMasterData: [String: AnyObject] = [String: AnyObject]()
    
    // Set customer master data
    static func setCustomerMasterData(data: [String: AnyObject]?) {
        if let uData = data  {
            self.customerMasterData = uData
        }
    }
    
    // Return master data value for data type and key
    // If data type or key is not found, return code
    static func getValueForKey(key: String, customerMasterDataType: CustomerMasterDataType) -> String {
        
        guard let categoryData = getDataForType(customerMasterDataType) else {
            return key
        }
        
        guard let value = categoryData[key] else {
            return key
        }
        
        return value
    }
    
    // Return keys for data type
    // If data type is not found, return empty string array
    static func getKeysFor(customerMasterDataType: CustomerMasterDataType) -> [String] {
        
        guard let categoryData = getDataForType(customerMasterDataType) else {
            return [String]()
        }
        
        return [String](categoryData.keys)
    }
    
    // Get dictionary for data type
    static func getDataForType(customerMasterDataType: CustomerMasterDataType) -> [String: String]? {
        return customerMasterData[customerMasterDataType.rawValue] as? [String: String]
    }
    
    // Get dictionary for Canadian Provinces
    static func getProvinceStateDataForAddressFor(countryCode: CountryCode) -> [String: String] {
        var data = [String: String]()
        if let provinceData = self.getDataForType(CustomerMasterDataType.ProvinceForAddress) {
            for (key, value) in provinceData {
                if countryCode == .Canada && CanadianProvinceCodes.allCodes().contains(key)  {
                    data[key] = value
                } else if countryCode == .UnitedStates && !CanadianProvinceCodes.allCodes().contains(key) {
                    data[key] = value
                }
            }
        }
        return data
    }
    
    static func getProvinceStateDataForIdentification(countryCode: CountryCode) -> [String: String] {
        var data = [String: String]()
        if let provinceData = self.getDataForType(CustomerMasterDataType.ProvinceOfIssueForIdentification) {
            for (key, value) in provinceData {
                // Add "000" NOT Applicable, "098" Fedrally Issued, "099" Unknown to the list
                if key == "000" || key == "098" || key == "099" {
                    data[key] = value
                } else if countryCode == .Canada && CanadianProvinceCodes.allCodes().contains(key)  {
                    data[key] = value
                } else if countryCode == .UnitedStates && !CanadianProvinceCodes.allCodes().contains(key) {
                    data[key] = value
                }
            }
        }
        return data
    }

}

