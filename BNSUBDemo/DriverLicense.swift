//
//  DriverLicenseCustomDecoder.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-19.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

enum DriverLicensePrefixCode: String {
    case FirstName = "DAC",
    GivenNames = "DCT",
    MiddleNames = "DAD",
    LastName = "DCS",
    DateOfBirth = "DBB",
    
    // Manatee Codes
    ManateeGivenNames = "Customer Given Names : ",
    ManateeLastName = "Customer Family Name : ",
    ManateeDateOfBirth = "Date Of Birth : "
}

class DriverLicense {
    
    var firstName: String?
    var lastName: String?
    var dateOfBirth: NSDate?
    
    init(driverLicenseCode: String) {
        // Extracting components from the driver license code
        for driverLicenseComponent in driverLicenseCode.componentsSeparatedByString("\n") {
            
            if driverLicenseComponent.hasPrefix(DriverLicensePrefixCode.FirstName.rawValue) || driverLicenseComponent.hasPrefix(DriverLicensePrefixCode.GivenNames.rawValue) {
                self.firstName = self.extractNameFromDriverLicense(driverLicenseComponent, code: .FirstName)
            } else if driverLicenseComponent.hasPrefix(DriverLicensePrefixCode.LastName.rawValue) {
                self.lastName = self.extractNameFromDriverLicense(driverLicenseComponent, code: .LastName)
            } else if driverLicenseComponent.hasPrefix(DriverLicensePrefixCode.DateOfBirth.rawValue) {
                self.dateOfBirth = self.extractBirthDateFromDriverLicense(driverLicenseComponent, code: .DateOfBirth)
            }
        }
    }
    
    init(driverLicenseString: String) {
        // Extracting components from the driver license string
        for driverLicenseComponent in driverLicenseString.componentsSeparatedByString("\n") {
            
            if driverLicenseComponent.hasPrefix(DriverLicensePrefixCode.ManateeGivenNames.rawValue) {
                self.firstName = self.extractNameFromDriverLicense(driverLicenseComponent, code: .ManateeGivenNames)
            } else if driverLicenseComponent.hasPrefix(DriverLicensePrefixCode.ManateeLastName.rawValue) {
                self.lastName = self.extractNameFromDriverLicense(driverLicenseComponent, code: .ManateeLastName)
            } else if driverLicenseComponent.hasPrefix(DriverLicensePrefixCode.ManateeDateOfBirth.rawValue) {
                self.dateOfBirth = self.extractBirthDateFromDriverLicense(driverLicenseComponent, code: .ManateeDateOfBirth)
            }
        }
    }
    
    func extractNameFromDriverLicense(string: String, code: DriverLicensePrefixCode) -> String? {
        let namesWithoutCode = self.removeCodeFromString(string, code: code)
        let cleanNamesWithoutCode = namesWithoutCode.additionalSpacesRemoved.capitalizedString
        let names = cleanNamesWithoutCode.componentsSeparatedByString(",")
        return names.first
    }
    
    func extractBirthDateFromDriverLicense(string: String, code: DriverLicensePrefixCode) -> NSDate? {
        let dateOfBirthString = self.removeCodeFromString(string, code: code)
    
        let dateFormatter1 = NSDateFormatter()
        dateFormatter1.dateFormat = "yyyyMMdd" // CCYYMMDD Format
        
        let dateFormatter2 = NSDateFormatter()
        dateFormatter2.dateFormat = "MMddyyyy" // MMDDCCYY Format
        
        return dateFormatter1.dateFromString(dateOfBirthString) ?? dateFormatter2.dateFromString(dateOfBirthString)
    }
    
    func removeCodeFromString(string: String, code: DriverLicensePrefixCode) -> String {
        let index = string.startIndex.advancedBy(code.rawValue.characters.count)
        return string.substringFromIndex(index)
    }

}