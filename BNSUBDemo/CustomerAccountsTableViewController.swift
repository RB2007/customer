
//
//  CustomerAccountsTableViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-16.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class CustomerAccountsTableViewController: AbstractCustomerAccountTableViewController {
        
    // MARK: - Properties
    var selectedIndexPath: NSIndexPath?
    var customerScotiaProductTypes: [ScotiaProductTypes] = [ScotiaProductTypes]()

    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
        // Check for available products for each type
        if let uProducts = customerAccountDelegate?.customerProfile?.products {
            customerScotiaProductTypes = ScotiaProductTypes.getProductTypes(uProducts)
        }
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let uIdentifier = segue.identifier, _ = sender where uIdentifier == "showTransactionHistory" {
            
            if let vc = segue.destinationViewController as? TransactionsViewController, product = sender as? Product {
                vc.customerAccountDelegate = customerAccountDelegate
                vc.product = product
            }
        }
    }
    
    // MARK: - Instance Methods
    func didSelectTransactionHistoryFor(product: Product) {
        self.performSegueWithIdentifier("showTransactionHistory", sender: product)
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return customerScotiaProductTypes.count
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let uProducts = customerAccountDelegate?.customerProfile?.products else {
            return 0
        }
        let scotiaProductType = customerScotiaProductTypes[section]
        return scotiaProductType.getProductsFrom(uProducts).count
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCellWithIdentifier("CustomerAccountsCell", forIndexPath: indexPath) as? CustomerAccountsTableViewCell, uProducts = customerAccountDelegate?.customerProfile?.products, uIsUserAuthenticated = customerAccountDelegate?.isUserAuthenticated else {
            return UITableViewCell()
        }
        
        // Get the product and set the cell content
        let scotiaProductType = customerScotiaProductTypes[indexPath.section]
        let customerProductsForType = scotiaProductType.getProductsFrom(uProducts)
        let customerProduct = customerProductsForType[indexPath.row]
        cell.setCellContent(customerProduct, isUserAuthenticated: uIsUserAuthenticated, customerAccountsTableViewController: self)
        
        // Determine wether to display the cell details
        cell.displayProductDetails(selectedIndexPath == indexPath)
        return cell
    }

    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var indexPathsToReload = [NSIndexPath]()
        indexPathsToReload.append(indexPath)
        
        if let uSelectedIndexPath = selectedIndexPath {
            indexPathsToReload.append(uSelectedIndexPath)
        }
        
        selectedIndexPath = selectedIndexPath == indexPath ? nil : indexPath
        tableView.reloadRowsAtIndexPaths(indexPathsToReload, withRowAnimation: UITableViewRowAnimation.Fade)
        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.None, animated: false)
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("CustomerInfoSectionHeader") as? CustomerInfoSectionHeader else {
            return UIView()
        }
        headerView.frame = CGRectMake(0, 0, tableView.bounds.size.width, 40)
        let scotiaProductType = customerScotiaProductTypes[section]
        headerView.setHeaderContent(scotiaProductType.getName(), section: section, alternateBackgroundColor: false, addbuttonHidden: true, delegate: nil)
        return headerView
    }
    
    override func tableView(tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 40
    }
}
