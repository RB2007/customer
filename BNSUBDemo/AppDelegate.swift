//
//  AppDelegate.swift
//  BNSUBDemo
//
//  Created by Laura Reategui on 2015-04-24.
//  Copyright (c) 2015 ScotiaBank. All rights reserved.
//

import UIKit
import ReachabilitySwift
import SVProgressHUD

@UIApplicationMain

class AppDelegate: UIResponder, UIApplicationDelegate {
    
    var window: UIWindow?
    var reachability : Reachability?
    var hasConnection : Bool = true
    
    func application(application: UIApplication, didFinishLaunchingWithOptions launchOptions: [NSObject: AnyObject]?) -> Bool {
        self.setupAnalytics()
        self.setupSVProgressHUD()
        
        do {
            self.reachability = try Reachability.reachabilityForInternetConnection()
        } catch {
            print("Unable to create Reachability")
        }
        
        
        NSNotificationCenter.defaultCenter().addObserver(self, selector: "reachabilityChanged:", name: ReachabilityChangedNotification, object: reachability)
        
        do {
            try self.reachability?.startNotifier()
        } catch {
            
        }
        
        UIDevice.currentDevice().batteryMonitoringEnabled = true
        
        return true
    }
    
    func reachabilityChanged(note: NSNotification) {
        
        if let reachability = note.object as? Reachability {
            self.reachability = reachability
            if reachability.isReachable() {
                self.hasConnection = true
                if reachability.isReachableViaWiFi() {
                    print("Reachable via WiFi")
                } else {
                    print("Reachable via Cellular")
                }
            } else {
                self.hasConnection = false
                print("Please ensure you have Internet connection before retrying")
            }
        }
    }
    
    func setupAnalytics() {
        if let uSplunkAPIKey = Util.getConfigurationVariableFor(EnvironmentConfigurationKeys.SplunkAPIKey) {
            Analytics.sharedInstance.isActive = true
            Analytics.sharedInstance.initAnalytics(uSplunkAPIKey)
        }
    }
    
    func setupSVProgressHUD() {
        SVProgressHUD.setForegroundColor(Constants.UBColours.grey50)
        SVProgressHUD.setRingThickness(2.5)
        SVProgressHUD.setFont(UIFont(name: "Lato-Regular", size: 16))
        SVProgressHUD.setSuccessImage(UIImage(named: "CheckmarkGrey"))
        SVProgressHUD.setErrorImage(UIImage(named: "CrossGrey"))
        SVProgressHUD.setDefaultMaskType(SVProgressHUDMaskType.Black)
    }
    
    func applicationWillResignActive(application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and throttle down OpenGL ES frame rates. Games should use this method to pause the game.
        print ("APP IS NOW ENTERING BACKGROUND STATE")
    }
    
    func applicationDidEnterBackground(application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
        print ("APP IS NOW ENTERING BACKGROUND STATE")
        
        VerifoneDevice.pinpad()?.cancelCommandWithDisplayOptions(1)
        VerifoneDevice.pinpad()?.S01()
        VerifoneDevice.pinpad()?.S01()
        PINManagemenetViewController.initFinished = false
        PINManagemenetViewController.transactionStarted = false
    }
    
    func batteryLevel() -> Float {
        
        return UIDevice.currentDevice().batteryLevel
    }
}