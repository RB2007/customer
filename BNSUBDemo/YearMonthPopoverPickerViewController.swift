//
//  YearMonthPopoverListSelectorViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2016-02-09.
//  Copyright © 2016 ScotiaBank. All rights reserved.
//

import UIKit

protocol YearMonthPoppverPickerProtocol {
    func didSelectYearMonth(year: Int, month: Int)
}

class YearMonthPopoverPickerViewController: UIViewController, UIPickerViewDataSource, UIPickerViewDelegate {

    // MARK: - Properties
    @IBOutlet weak var pickerView: UIPickerView!
    var selectedValue: String?
    var delegate: YearMonthPoppverPickerProtocol?
    
    // MARK: - View Override Methods
    override func viewDidLoad() {
        super.viewDidLoad()

        // Customize view
        self.popoverPresentationController?.backgroundColor = Constants.UBColours.scotiaGrey1

        // Assuming the selected value is in this format 0 Years 0 Months
        if let selectedValueComponents = selectedValue?.componentsSeparatedByString(" ") where selectedValueComponents.count == 4 {
            let years = Int(selectedValueComponents[0]) ?? 0
            let months = Int(selectedValueComponents[2]) ?? 0
            if years < 61 {
                pickerView.selectRow(years, inComponent: 0, animated: true)
            }
            if months < 12 {
                pickerView.selectRow(months, inComponent: 1, animated: true)
            }
        }
    }
    
    // MARK: - Picker View Delegate Methods
    func numberOfComponentsInPickerView(pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        return component == 0 ? 61 : 12
    }
    
    func pickerView(pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        return component == 0 ? String(row) + " Years" : String(row) + " Months"
    }
    
    func pickerView(pickerView: UIPickerView, viewForRow row: Int, forComponent component: Int, reusingView view: UIView?) -> UIView {
        var pickerView = view
        
        if pickerView == nil {
            pickerView = NSBundle.mainBundle().loadNibNamed("YearMonthPickerView", owner: self, options: nil).first as? YearMonthPickerView
            pickerView?.frame = CGRectMake(500, 0, self.view.bounds.width/2, 100)
        }
        
        guard let uPickerView = pickerView as? YearMonthPickerView else {
            return UIView()
        }
        
        uPickerView.labelValue.text = component == 0 ? String(row) + " " + NSLocalizedString("years", comment: "Year Month Popover"): String(row) + " " + NSLocalizedString("months", comment: "Year Month Popover")
        return uPickerView
    }
    
    func pickerView(pickerView: UIPickerView, rowHeightForComponent component: Int) -> CGFloat {
        return 30
    }
    
    func pickerView(pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        let years = pickerView.selectedRowInComponent(0)
        let months = pickerView.selectedRowInComponent(1)
        delegate?.didSelectYearMonth(years, month: months)
    }
}
