//
//  CustomerKeysViewModel.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-24.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

// MARK: - Customer Keys Protocol
protocol CustomerKeys {
    static var numberOfKeys: Int { get }
    static var parentSectionNumber: Int {get}
    func getKeyName(object: AnyObject) -> String
    func getValueFrom(object: AnyObject) -> String
    func shouldDisplay(object: AnyObject) -> Bool
    func isEditable(object: AnyObject) -> Bool
    func editingType() -> CustomerInfoEditType
    func getMasterDataForListSelector(object: AnyObject) -> [String: String]?
    func valueChanged(object: AnyObject, newValue: String, newValue2: String?) -> String
    func setValueTo(object: AnyObject, newValue: String, newValue2: String?)
    func isValidValueIn(object: AnyObject) -> Bool
    func shouldChangeStringInRange(object: AnyObject, newString: String, range: NSRange) -> Bool
    func customizeTextField(object: AnyObject, textField: UITextField, editing: Bool) // editing: If the user is currently in edit mode
    func reloadRowsAtIndexPathsWithAnimation(object: AnyObject) -> [[String: AnyObject]]?
}

// MARK: - Customer Info Edit Types
enum CustomerInfoEditType {
    case Keyboard, MasterDataListSelector, YearMonthPicker
}

// MARK: - Customer Personal Info Keys
enum CustomerPersonalInfoKeys: Int {
    case PersonalInfo, PersonalContactInfo
    
    static let numberOfSections: Int = 2
    
    func getName() -> String {
        switch self {
        case .PersonalInfo:
            return NSLocalizedString("personal_information", comment: "Customer Personal Info")
        case .PersonalContactInfo:
            return NSLocalizedString("contact_info", comment: "Customer Personal Info")
        }
    }
    
    func getNumberOfKeys(personProfile: PersonProfile) -> Int {
        switch self {
        case .PersonalInfo:
            return PersonalInfoKey.numberOfKeys
        case .PersonalContactInfo:
            return PersonalContactKey.numberOfKeys
        }
    }
    
    func getDelegateFor(position: Int) -> CustomerKeys? {
        switch self {
        case .PersonalInfo:
            return PersonalInfoKey(rawValue: position)
        case .PersonalContactInfo:
            return PersonalContactKey(rawValue: position)
        }
    }
}

// Customer Employment Info Keys
enum CustomerEmploymentInfoKeys: Int {
    case EmploymentInfo, EmploymentAddress, EmploymentContact
    
    static let numberOfSections: Int = 3
    
    func getName() -> String {
        switch self {
        case .EmploymentInfo:
            return NSLocalizedString("employment_information", comment: "Customer Employer Info")
        case .EmploymentAddress:
            return NSLocalizedString("employer_address_information", comment: "Customer Employer Info")
        case .EmploymentContact:
            return NSLocalizedString("employer_contact_information", comment: "Customer Employer Info")
        }
    }
    
    func getNumberOfKeys(personProfile: PersonProfile) -> Int {
        switch self {
        case .EmploymentInfo:
            return EmploymentInfoKey.numberOfKeys
        case .EmploymentAddress:
            return EmploymentAddressKey.numberOfKeys
        case .EmploymentContact:
            return EmploymentContactKey.numberOfKeys
        }
    }
    
    func getDelegateFor(position: Int) -> CustomerKeys? {
        switch self {
        case .EmploymentInfo:
            return EmploymentInfoKey(rawValue: position)
        case .EmploymentAddress:
            return EmploymentAddressKey(rawValue: position)
        case .EmploymentContact:
            return EmploymentContactKey(rawValue: position)
        }
    }
}

// Customer Identification Info Keys
enum CustomerIdentificationKeys: Int {
    case IdentificationStatus, Identifications
    
    static let numberOfSections: Int = 2
    
    func getName() -> String {
        switch self {
        case .IdentificationStatus:
            return NSLocalizedString("identification_status", comment: "Customer Identifications")
        case .Identifications:
            return NSLocalizedString("identifications", comment: "Customer Identifications")
        }
    }
    
    func getNumberOfKeys(personProfile: PersonProfile) -> Int {
        switch self {
        case .IdentificationStatus:
            return IdentificationStatusKey.numberOfKeys
        case .Identifications:
            return personProfile.identificationsNotDeleted?.count ?? 0
        }
    }
    
    func getDelegateFor(position: Int) -> CustomerKeys? {
        switch self {
        case .IdentificationStatus:
            return IdentificationStatusKey(rawValue: position)
        case .Identifications:
            return nil
        }
    }
}