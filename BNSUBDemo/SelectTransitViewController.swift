//
//  SelectTransitViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-03.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit
import SVProgressHUD

class SelectTransitViewController: UIViewController, UITableViewDataSource, UITableViewDelegate {

    // MARK: - Properties
    @IBOutlet weak var labelWelcome: UILabel!
    @IBOutlet weak var labelSelectBranch: UILabel!
    @IBOutlet weak var buttonGetStarted: UIButton!
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var imageViewScotiabankLogo: UIImageView!
    @IBOutlet weak var labelOfficerName: UILabel!
    
    // MARK: - View Delegate Methods
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Customize UI
        buttonGetStarted.layer.cornerRadius = 5
        tableView.rowHeight = UITableViewAutomaticDimension
        tableView.estimatedRowHeight = 100
        tableView.layer.borderColor = Constants.UBColours.scotiaGrey4.CGColor
        tableView.layer.borderWidth = 1
        customizeView()
        
        // Select the first row in the table view
        let firstIndexPath = NSIndexPath(forRow: 0, inSection: 0)
        tableView.selectRowAtIndexPath(firstIndexPath, animated: true, scrollPosition: UITableViewScrollPosition.Top)

        // Disable swipe back gestsure
        self.navigationController?.interactivePopGestureRecognizer?.enabled = false
    }
    
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return UIStatusBarStyle.Default
    }
    
    // MARK: - Class Methods
    
    func customizeView() {
        labelWelcome.text = NSLocalizedString("welcome", comment: "Select Transit View Controller")
        labelOfficerName.text = Officer.name ?? ""
        labelSelectBranch.text = NSLocalizedString("please_select_branch", comment: "Select Transit View Controller")
        let getStartedText = NSLocalizedString("get_started", comment: "Select Transit View Controller")
        buttonGetStarted.setTitle(getStartedText, forState: .Normal)
        
        // Localizing the scotiabank logo
        let scotiaBank = NSLocalizedString("scotia_bank", comment: "")
        imageViewScotiabankLogo.image = UIImage(named: scotiaBank + " Logo")
    }
    
    @IBAction func getStartedButtonPressed() {
        if let selectedIndexPath = tableView.indexPathForSelectedRow {
            
            // Set the selected branch on the user object
            let position = selectedIndexPath.row
            Officer.currentSelectedRole = Officer.officerRoles?[position]

            let storyBoard = UIStoryboard(name: "CustomerLookup", bundle: nil)
            if let viewController = storyBoard.instantiateInitialViewController() {
                self.navigationController?.pushViewController(viewController, animated: true)
            }
        }
    }
    
    // MARK: - Table View Delegate Methods
    func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return Officer.officerRoles?.count ?? 0
    }
    
    func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        guard let cell = tableView.dequeueReusableCellWithIdentifier("OfficerTransitCell", forIndexPath: indexPath) as? OfficerTransitTableViewCell, officerRoles = Officer.officerRoles else {
            return UITableViewCell()
        }
        
        let officerRole = officerRoles[indexPath.row]
        cell.setCellContent(indexPath.row, officerRole: officerRole)
        
        return cell
    }

}
