//
//  EmploymentInfoKey.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-02.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import Foundation

// MARK: - Employment Information Keys

enum EmploymentInfoKey: Int, CustomerKeys {
    case Employer, GrossMonthlyIncome, OtherIncome, OtherIncomeSource, LengthOfCurrentEmployment, LengthOfOccupation, Occupation, OccupationDescription, OccupationStatus, OccupationType, OccupationCategory
    
    static var numberOfKeys: Int {
        get {
            return 11
        }
    }
    
    static var parentSectionNumber: Int {
        get {
            return CustomerEmploymentInfoKeys.EmploymentInfo.rawValue
        }
    }
    
    func getKeyName(object: AnyObject) -> String {
        
        switch self {
            
        case .Employer:
            return NSLocalizedString("employer", comment: "Customer Employment Info")
            
        case .GrossMonthlyIncome:
            return NSLocalizedString("gross_monthly_income", comment: "Customer Employment Info")
            
        case .OtherIncome:
            return NSLocalizedString("other_income_amount_per_month", comment: "Customer Employment Info")
            
        case OtherIncomeSource:
            return NSLocalizedString("other_income_source", comment: "Customer Employment Info")
            
        case .LengthOfCurrentEmployment:
            return NSLocalizedString("length_of_employment", comment: "Customer Employment Info")
            
        case .LengthOfOccupation:
            return NSLocalizedString("length_of_occupation", comment: "Customer Employment Info")
            
        case .Occupation:
            return NSLocalizedString("occupation", comment: "Customer Employment Info")
            
        case .OccupationDescription:
            return NSLocalizedString("occupation_description", comment: "Customer Employment Info")
        case .OccupationStatus:
            return NSLocalizedString("status", comment: "Customer Employment Info")
            
        case .OccupationType:
            return NSLocalizedString("type", comment: "Customer Employment Info")
            
        case .OccupationCategory:
            return NSLocalizedString("category", comment: "Customer Employment Info")
        }
    }

    func getValueFrom(object: AnyObject) -> String {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return ""
        }
        
        switch self {
            
        case .Employer:
            return uPersonProfile.employment?.employerName?.capitalizedString ?? ""
            
        case .GrossMonthlyIncome:
            if let monthlyIncome = uPersonProfile.employment?.grossMonthlyIncome {
                return Util.formatCurrency(monthlyIncome)
            }
            return Util.formatCurrency(0)
            
        case .OtherIncome:
            if let otherMonthlyIncome = uPersonProfile.otherIncome {
                return Util.formatCurrency(otherMonthlyIncome)
            }
            return Util.formatCurrency(0)
            
        case .OtherIncomeSource:
            return uPersonProfile.otherIncomeSource?.capitalizedString ?? ""
            
        case .LengthOfCurrentEmployment:
            var years = "0"
            var months = "0"
            
            if let employmentYears = uPersonProfile.employment?.lengthOfEmployment?.years {
                years = String(employmentYears)
            }
            
            if let employmentMonths = uPersonProfile.employment?.lengthOfEmployment?.months {
                months = String(employmentMonths)
            }
            years += " " + NSLocalizedString("years", comment: "")
            months += " " + NSLocalizedString("months", comment: "")
            return years + " " + months
            
        case .LengthOfOccupation:
            var years = "0"
            var months = "0"
            
            if let occupationYears = uPersonProfile.employment?.lengthOfOccupation?.years {
                years = String(occupationYears)
            }
            
            if let occupatonMonths = uPersonProfile.employment?.lengthOfOccupation?.months {
                months = String(occupatonMonths)
            }
            years += " " + NSLocalizedString("years", comment: "")
            months += " " + NSLocalizedString("months", comment: "")
            return years + " " + months
            
        case .Occupation:
            if let occupationCode = uPersonProfile.employment?.occupationCode {
                return CustomerMasterData.getValueForKey(occupationCode, customerMasterDataType: .OccupationCode).capitalizedString
            }
            return ""
            
        case .OccupationDescription:
            return uPersonProfile.employment?.occupationDescription?.capitalizedString ?? ""
            
        case .OccupationStatus:
            if let statusCode = uPersonProfile.employment?.occupationStatus {
                return CustomerMasterData.getValueForKey(statusCode, customerMasterDataType: .OccupationStatus).capitalizedString
            }
            return ""
            
        case .OccupationType:
            if let typeCode = uPersonProfile.employment?.occupationTp {
                return CustomerMasterData.getValueForKey(typeCode, customerMasterDataType: .OccupationType).capitalizedString
            }
            return ""
            
        case .OccupationCategory:
            if let categoryCode = uPersonProfile.employment?.occupationCategory {
                return CustomerMasterData.getValueForKey(categoryCode, customerMasterDataType: .OccupationCategories).capitalizedString
            }
            return ""
        }
    }
    
    func shouldDisplay(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
        case .Employer:
            return true
        case .GrossMonthlyIncome:
            return true
        case .OtherIncome:
            return true
        case .OtherIncomeSource:
            return true
        case .LengthOfCurrentEmployment:
            return true
        case .LengthOfOccupation:
            return true
        case .Occupation:
            return true
        case .OccupationDescription:
            // Occupation description is displayed only if the occupation code is set to '256' OTHER
            return uPersonProfile.employment?.occupationCode == "256"
        case .OccupationStatus:
            return true
        case .OccupationType:
            return true
        case .OccupationCategory:
            return true
        }
    }
    
    func isEditable(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
        case .Employer:
            return true
        case .GrossMonthlyIncome:
            return true
        case .OtherIncome:
            return true
        case .OtherIncomeSource:
            return true
        case .LengthOfCurrentEmployment:
            return true
        case .LengthOfOccupation:
            return true
        case .Occupation:
            // Occupation is not editable when occupation status is set to Retired, Student-Prim/Second, Student-Post Second, Not Working, Staff - Retired
            return uPersonProfile.employment?.occupationStatus != "004" && uPersonProfile.employment?.occupationStatus != "005" && uPersonProfile.employment?.occupationStatus != "006" && uPersonProfile.employment?.occupationStatus != "007" && uPersonProfile.employment?.occupationStatus != "015"
        case .OccupationDescription:
            // Occupation description is only editable if the occupation code is set to '256' OTHER and occupation status is NOT set to Retired, Student-Prim/Second, Student-Post Second, Not Working, Staff - Retired
            return uPersonProfile.employment?.occupationCode == "256" && uPersonProfile.employment?.occupationStatus != "004" && uPersonProfile.employment?.occupationStatus != "005" && uPersonProfile.employment?.occupationStatus != "006" && uPersonProfile.employment?.occupationStatus != "007" && uPersonProfile.employment?.occupationStatus != "015"
        case .OccupationStatus:
            return true
        case .OccupationType:
            // Occupation Type is not editable when occupation status is set to Retired, Student-Prim/Second, Student-Post Second, Not Working, Staff - Retired
            return uPersonProfile.employment?.occupationStatus != "004" && uPersonProfile.employment?.occupationStatus != "005" && uPersonProfile.employment?.occupationStatus != "006" && uPersonProfile.employment?.occupationStatus != "007" && uPersonProfile.employment?.occupationStatus != "015"
        case .OccupationCategory:
            // Occupation Category is not editable when occupation status is set to Retired, Student-Prim/Second, Student-Post Second, Not Working, Staff - Retired
            return uPersonProfile.employment?.occupationStatus != "004" && uPersonProfile.employment?.occupationStatus != "005" && uPersonProfile.employment?.occupationStatus != "006" && uPersonProfile.employment?.occupationStatus != "007" && uPersonProfile.employment?.occupationStatus != "015"
        }
    }
    
    func editingType() -> CustomerInfoEditType {
        switch self {
        case .Employer:
            return .Keyboard
        case .GrossMonthlyIncome:
            return .Keyboard
        case .OtherIncome:
            return .Keyboard
        case .OtherIncomeSource:
            return .Keyboard
        case .LengthOfCurrentEmployment:
            return .YearMonthPicker
        case .LengthOfOccupation:
            return .YearMonthPicker
        case .Occupation:
            return .MasterDataListSelector
        case .OccupationDescription:
            return .Keyboard
        case .OccupationStatus:
            return .MasterDataListSelector
        case .OccupationType:
            return .MasterDataListSelector
        case .OccupationCategory:
            return .MasterDataListSelector
        }
    }

    func getMasterDataForListSelector(object: AnyObject) -> [String: String]? {
        switch self {
        case .Employer:
            return nil
        case .GrossMonthlyIncome:
            return nil
        case .OtherIncome:
            return nil
        case .OtherIncomeSource:
            return nil
        case .LengthOfCurrentEmployment:
            return nil
        case .LengthOfOccupation:
            return nil
        case .Occupation:
            return CustomerMasterData.getDataForType(CustomerMasterDataType.OccupationCode)
        case .OccupationDescription:
            return nil
        case .OccupationStatus:
            return CustomerMasterData.getDataForType(CustomerMasterDataType.OccupationStatus)
        case .OccupationType:
            return CustomerMasterData.getDataForType(CustomerMasterDataType.OccupationType)
        case .OccupationCategory:
            return CustomerMasterData.getDataForType(CustomerMasterDataType.OccupationCategories)
        }
    }
    
    func valueChanged(object: AnyObject, newValue: String, newValue2: String?) -> String {
        setValueTo(object, newValue: newValue, newValue2: newValue2)
        return getValueFrom(object)
    }

    func setValueTo(object: AnyObject, newValue: String, newValue2: String?) {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return
        }
        
        switch self {
        case .Employer:
            uPersonProfile.employment?.employerName = newValue
        case .GrossMonthlyIncome:
            let unformattedValue = Util.unformatCurrency(newValue)
            uPersonProfile.employment?.grossMonthlyIncome = Double(unformattedValue)
        case .OtherIncome:
            let unformattedValue = Util.unformatCurrency(newValue)
            uPersonProfile.otherIncome = Double(unformattedValue)
        case .OtherIncomeSource:
            uPersonProfile.otherIncomeSource = newValue
        case .LengthOfCurrentEmployment:
            if let uYears = Int(newValue), uMonths = Int(newValue2 ?? "") {
                uPersonProfile.employment?.lengthOfEmployment = TimeLength(years: uYears, months: uMonths, days: 0, hours: 0, minutes: 0)
            }
        case .LengthOfOccupation:
            if let uYears = Int(newValue), uMonths = Int(newValue2 ?? "") {
                uPersonProfile.employment?.lengthOfOccupation = TimeLength(years: uYears, months: uMonths, days: 0, hours: 0, minutes: 0)
            }
        case .Occupation:
            uPersonProfile.employment?.occupationCode = newValue
        case .OccupationDescription:
            uPersonProfile.employment?.occupationDescription = newValue
        case .OccupationStatus:
            uPersonProfile.employment?.occupationStatus = newValue
        case .OccupationType:
            uPersonProfile.employment?.occupationTp = newValue
        case .OccupationCategory:
            uPersonProfile.employment?.occupationCategory = newValue
        }
    }

    func isValidValueIn(object: AnyObject) -> Bool {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return false
        }
        
        switch self {
        case .Employer:
            return uPersonProfile.employment?.isValidEmployerName ?? true
        case .GrossMonthlyIncome:
            return uPersonProfile.employment?.isValidGrossMonthlyIncome ?? true
        case .OtherIncome:
            return uPersonProfile.isValidOtherIncome ?? true
        case .OtherIncomeSource:
            return uPersonProfile.isValidOtherIncomeSource ?? true
        case .LengthOfCurrentEmployment:
            return uPersonProfile.employment?.lengthOfEmployment?.isValidYears ?? true && uPersonProfile.employment?.lengthOfEmployment?.isValidMonths ?? true
        case .LengthOfOccupation:
            return uPersonProfile.employment?.lengthOfOccupation?.isValidYears ?? true && uPersonProfile.employment?.lengthOfOccupation?.isValidMonths ?? true
        case .Occupation:
            return uPersonProfile.employment?.isValidOccupationCode ?? true
        case .OccupationDescription:
            return uPersonProfile.employment?.isValidOccupationDescription ?? true
        case .OccupationStatus:
            return uPersonProfile.employment?.isValidOccupationStatus ?? true
        case .OccupationType:
            return uPersonProfile.employment?.isValidOccupationType ?? true
        case .OccupationCategory:
            return uPersonProfile.employment?.isValidOccupationType ?? true
        }
    }

    func shouldChangeStringInRange(object: AnyObject, newString: String, range: NSRange) -> Bool {
        switch self {
        case .Employer:
            return Employment.isValidEmployerNameInRange(newString, range: range)
        case .GrossMonthlyIncome:
            return Employment.isValidGrossMonthlyIncomeInRange(newString, range: range)
        case .OtherIncome:
            return PersonProfile.isValidOtherIncomeInRange(newString, range: range)
        case .OtherIncomeSource:
            return PersonProfile.isValidOtherIncomeSourceInRange(newString, range: range)
        case .LengthOfCurrentEmployment:
            return true
        case .LengthOfOccupation:
            return true
        case .Occupation:
            return true
        case .OccupationDescription:
            return Employment.isValidOccupationDescriptionInRange(newString, range: range)
        case .OccupationStatus:
            return true
        case .OccupationType:
            return true
        case .OccupationCategory:
            return true
        }
    }
    
    func customizeTextField(object: AnyObject, textField: UITextField, editing: Bool) {

        textField.keyboardType = UIKeyboardType.Default
        textField.returnKeyType = UIReturnKeyType.Done
        textField.enabled = isEditable(object) ? editing : false
        textField.borderStyle = isEditable(object) && editing ? UITextBorderStyle.RoundedRect : UITextBorderStyle.None
        textField.backgroundColor = isEditable(object) && editing ? UIColor.whiteColor() : UIColor.clearColor()
        textField.autocapitalizationType = UITextAutocapitalizationType.Words
        
        switch self {
        case .Employer:
            textField.placeholder = editing ? NSLocalizedString("enter_employer_name", comment: "Customer Employer Information") : ""
        case .GrossMonthlyIncome:
            textField.keyboardType = UIKeyboardType.NumberPad
        case .OtherIncome:
            textField.keyboardType = UIKeyboardType.NumberPad
        case .OtherIncomeSource:
            textField.placeholder = editing ? NSLocalizedString("enter_other_income_source", comment: "Customer Employer Infomration") : ""
        case .LengthOfCurrentEmployment:
            break
        case .LengthOfOccupation:
            break
        case .Occupation:
            textField.placeholder = editing ? NSLocalizedString("enter_occupation", comment: "Customer Employer Information") : ""
        case .OccupationDescription:
            textField.placeholder = isEditable(object) && editing ? NSLocalizedString("enter_occupation_description", comment: "Customer Employer Information") : ""
        case .OccupationStatus:
            textField.placeholder = editing ? NSLocalizedString("enter_occupation_status", comment: "Customer Employer Information") : ""
        case .OccupationType:
            textField.placeholder = editing ? NSLocalizedString("enter_occupation_type", comment: "Customer Employer Information") : ""
        case .OccupationCategory:
            textField.placeholder = editing ? NSLocalizedString("enter_occuaption_category", comment: "Customer Employer Information") : ""
        }
    }
    
    func reloadRowsAtIndexPathsWithAnimation(object: AnyObject) -> [[String: AnyObject]]? {
        
        guard let uPersonProfile = object as? PersonProfile else {
            return nil
        }
        
        switch self {
        case .Employer:
            return nil
        case .GrossMonthlyIncome:
            return nil
        case .OtherIncome:
            return nil
        case .OtherIncomeSource:
            return nil
        case .LengthOfCurrentEmployment:
            return nil
        case .LengthOfOccupation:
            return nil
        case .Occupation:
            var indexPathObjects = [[String: AnyObject]]()
            let indexPath1 = NSIndexPath(forRow: EmploymentInfoKey.OccupationDescription.rawValue, inSection: EmploymentInfoKey.parentSectionNumber)
            let shouldDisplay1 = EmploymentInfoKey.OccupationDescription.shouldDisplay(uPersonProfile)
            let indexPathObject1 = ["indexPath": indexPath1, "shouldDisplay": shouldDisplay1]
            indexPathObjects.append(indexPathObject1)
            return indexPathObjects
        case .OccupationDescription:
            return nil
        case .OccupationStatus:
            var indexPathObjects = [[String: AnyObject]]()
            let indexPath1 = NSIndexPath(forRow: EmploymentInfoKey.Employer.rawValue, inSection: EmploymentInfoKey.parentSectionNumber)
            let shouldDisplay1 = EmploymentInfoKey.Employer.shouldDisplay(uPersonProfile)
            let indexPathObject1 = ["indexPath": indexPath1, "shouldDisplay": shouldDisplay1]
            indexPathObjects.append(indexPathObject1)
            
            let indexPath2 = NSIndexPath(forRow: EmploymentInfoKey.Occupation.rawValue, inSection: EmploymentInfoKey.parentSectionNumber)
            let shouldDisplay2 = EmploymentInfoKey.Occupation.shouldDisplay(uPersonProfile)
            let indexPathObject2 = ["indexPath": indexPath2, "shouldDisplay": shouldDisplay2]
            indexPathObjects.append(indexPathObject2)
            
            let indexPath3 = NSIndexPath(forRow: EmploymentInfoKey.OccupationDescription.rawValue, inSection: EmploymentInfoKey.parentSectionNumber)
            let shouldDisplay3 = EmploymentInfoKey.OccupationDescription.shouldDisplay(uPersonProfile)
            let indexPathObject3 = ["indexPath": indexPath3, "shouldDisplay": shouldDisplay3]
            indexPathObjects.append(indexPathObject3)
            
            let indexPath4 = NSIndexPath(forRow: EmploymentInfoKey.OccupationType.rawValue, inSection: EmploymentInfoKey.parentSectionNumber)
            let shouldDisplay4 = EmploymentInfoKey.OccupationType.shouldDisplay(uPersonProfile)
            let indexPathObject4 = ["indexPath": indexPath4, "shouldDisplay": shouldDisplay4]
            indexPathObjects.append(indexPathObject4)
            
            let indexPath5 = NSIndexPath(forRow: EmploymentInfoKey.OccupationCategory.rawValue, inSection: EmploymentInfoKey.parentSectionNumber)
            let shouldDisplay5 = EmploymentInfoKey.OccupationCategory.shouldDisplay(uPersonProfile)
            let indexPathObject5 = ["indexPath": indexPath5, "shouldDisplay": shouldDisplay5]
            indexPathObjects.append(indexPathObject5)
            
            return indexPathObjects
        case .OccupationType:
            return nil
        case .OccupationCategory:
            return nil
        }
    }
}
