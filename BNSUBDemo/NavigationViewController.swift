//
//  NavigationViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-06.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class NavigationViewController: UINavigationController {

    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // Let the top view controller manage the status bar style
    override func preferredStatusBarStyle() -> UIStatusBarStyle {
        return self.topViewController!.preferredStatusBarStyle()
    }

}
