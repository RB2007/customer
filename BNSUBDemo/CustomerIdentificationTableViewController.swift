//
//  CustomerIdentificationTableViewController.swift
//  Customer
//
//  Created by Emad Toukan on 2015-11-15.
//  Copyright © 2015 ScotiaBank. All rights reserved.
//

import UIKit

class CustomerIdentificationTableViewController: AbstractCustomerAccountTableViewController, CustomerInfoSectionHeaderProtocol {
    
    // MARK: - Properties
    var selectedIndexPath: NSIndexPath?
    
    // MARK: - View Methods
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    
    // MARK: - Navigation
    override func prepareForSegue(segue: UIStoryboardSegue, sender: AnyObject?) {
        if let uIdentifier = segue.identifier where uIdentifier == "showIdentificationForm" {
            if let vc = segue.destinationViewController as? CustomerIdentificationFormTableViewController, uIdentification = sender as? Identification {
                vc.customerAccountDelegate = customerAccountDelegate
                vc.identification = uIdentification
                vc.customerIdentificationTableViewController = self
            }
        }
    }
    
    // MARK: - Delegate Methods
    func deleteIdentificationInCell(identificationCell: IdentificationTableViewCell) {
        
        if let uIdentification = identificationCell.identification {
            // Display an alert view and let the user confirm that they would like to delete the identification
            let alertTitle = NSLocalizedString("confirm_remove_identification", comment: "Identification Remove Alert Title")
            let alertBody = String.localizedStringWithFormat(NSLocalizedString("confirm_remove_identification_description", comment: "Identification Remove Alert Body"), uIdentification.idTp.getName())
            let alertController = UIAlertController(title: alertTitle, message: alertBody, preferredStyle: UIAlertControllerStyle.Alert)
            // Yes Action
            let alertActionYes = UIAlertAction(title: NSLocalizedString("yes", comment: ""), style: UIAlertActionStyle.Default) { (alertAction: UIAlertAction) -> Void in
                // Set the identification as deleted and delete the cell at the indexpath
                uIdentification.deleted = true
                self.tableView.reloadData()
            }
            alertController.addAction(alertActionYes)
            // No Action
            let alertActionNo = UIAlertAction(title: NSLocalizedString("no", comment: ""), style: UIAlertActionStyle.Cancel, handler: nil)
            
            alertController.addAction(alertActionNo)
            self.presentViewController(alertController, animated: true, completion: nil)
        }
    }
    
    func addButtonPressedInSection(section: Int) {
        if let uCustomerProfile = customerAccountDelegate?.customerProfile where section == CustomerIdentificationKeys.Identifications.rawValue {
            let identification = Identification(customerProfile: uCustomerProfile)
            self.performSegueWithIdentifier("showIdentificationForm", sender: identification)
        }
    }
    
    // MARK: - Table view data source
    override func numberOfSectionsInTableView(tableView: UITableView) -> Int {
        return customerAccountDelegate?.customerProfile == nil ? 0 : CustomerIdentificationKeys.numberOfSections
    }
    
    override func tableView(tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        guard let customerIdentificationKeys = CustomerIdentificationKeys(rawValue: section), uPersonProfile = customerAccountDelegate?.customerProfile as? PersonProfile else {
            return 0
        }
        return customerIdentificationKeys.getNumberOfKeys(uPersonProfile)
    }
    
    override func tableView(tableView: UITableView, cellForRowAtIndexPath indexPath: NSIndexPath) -> UITableViewCell {
        
        // Display the Customer ID Status cell in the first row
        if indexPath.section == 0 {
            guard let cell = tableView.dequeueReusableCellWithIdentifier("CustomerInfoCell", forIndexPath: indexPath) as? CustomerInfoTableViewCell, uPersonProfile = customerAccountDelegate?.customerProfile as? PersonProfile, let customerKey = IdentificationStatusKey(rawValue: indexPath.row), isEditingCustomer = customerAccountDelegate?.isEditingCustomer else {
                return UITableViewCell()
            }
            cell.setCellContent(uPersonProfile, customerKey: customerKey, isEditing: isEditingCustomer, indexPath: indexPath, delegate: self)
            return cell
        }
        
        // Display the customer identifications
        guard let cell = tableView.dequeueReusableCellWithIdentifier("CustomerIdentificationCell", forIndexPath: indexPath) as? IdentificationTableViewCell, uIdentifications = customerAccountDelegate?.customerProfile?.identificationsNotDeleted, uIsUserAuthenticated = customerAccountDelegate?.isUserAuthenticated, uIsEditingCustomer = customerAccountDelegate?.isEditingCustomer else {
            return UITableViewCell()
        }
        
        let customerIdentification = uIdentifications[indexPath.row]
        cell.setCellContent(customerIdentification, isUserAuthenticated: uIsUserAuthenticated, isEditing: uIsEditingCustomer, customerIdentificationTableViewController: self)
        cell.displayIdentificationDetails(selectedIndexPath == indexPath)
        return cell
    }
    
    override func tableView(tableView: UITableView, didSelectRowAtIndexPath indexPath: NSIndexPath) {
        var indexPathsToReload = [NSIndexPath]()
        indexPathsToReload.append(indexPath)
        
        if let uSelectedIndexPath = selectedIndexPath {
            indexPathsToReload.append(uSelectedIndexPath)
        }
        selectedIndexPath = selectedIndexPath == indexPath ? nil : indexPath
        tableView.reloadRowsAtIndexPaths(indexPathsToReload, withRowAnimation: UITableViewRowAnimation.Fade)
        tableView.scrollToRowAtIndexPath(indexPath, atScrollPosition: UITableViewScrollPosition.None, animated: false)
    }
    
    override func tableView(tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        guard let headerView = tableView.dequeueReusableHeaderFooterViewWithIdentifier("CustomerInfoSectionHeader") as? CustomerInfoSectionHeader, customerIdentificationKeys = CustomerIdentificationKeys(rawValue: section) else {
            return UIView()
        }
        headerView.frame = CGRectMake(0, 0, tableView.bounds.size.width, 50)
        var addButtonHidden = true
        if let uIsUserAuthenticated = customerAccountDelegate?.isUserAuthenticated, uIsEditingCustomer = customerAccountDelegate?.isEditingCustomer {
            addButtonHidden = !(uIsUserAuthenticated && uIsEditingCustomer && section == CustomerIdentificationKeys.Identifications.rawValue)
        }
        
        headerView.setHeaderContent(customerIdentificationKeys.getName(), section: section, alternateBackgroundColor: true, addbuttonHidden: addButtonHidden, delegate: self)
        return headerView
    }
}
